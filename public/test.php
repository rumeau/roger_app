<?php
ini_set('display_error', 1);
error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
session_start();
$db = mysqli_connect('localhost', 'root', 'rumeau', 'comb2');

if (!empty($_SESSION['usuario_nombre'])) { // comprobamos que las variables de sesi�n est�n vac�as

    ?>
    <?php
} else {
    ?><!DOCTYPE html><!-- HTML5 -->
    <html lang="es" dir="ltr">
    <head>
        <meta charset="utf-8"/>
        <!--[if IE]>
        <meta http-equiv="ImageToolbar" content="False"/><![endif]-->
        <meta name="author" content="Patricio Obregon"/>
        <meta name="generator" content="Incomedia WebSite X5 Evolution 9.0.6.1775 - www.websitex5.com"/>
        <link rel="stylesheet" type="text/css" href="style/reset.css" media="screen,print"/>
        <link rel="stylesheet" type="text/css" href="style/print.css" media="print"/>
        <link rel="stylesheet" type="text/css" href="style/style.css" media="screen,print"/>
        <link rel="stylesheet" type="text/css" href="style/template.css" media="screen"/>
        <link rel="stylesheet" type="text/css" href="style/menu.css" media="screen"/>
        <!--[if lte IE 7]>
        <link rel="stylesheet" type="text/css" href="style/ie.css" media="screen"/><![endif]-->
        <script type="text/javascript" src="res/swfobject.js"></script>
        <script type="text/javascript" src="res/jquery.js"></script>
        <script type="text/javascript" src="res/x5engine.js"></script>
        <script type="text/javascript" src="res/x5cartengine.js"></script>
        <script type="text/javascript" src="res/l10n.js"></script>
        <script type="text/javascript" src="res/x5settings.js"></script>
        <script type="text/javascript" src="select_dependientes.js"></script>


        <title>Maquinas - Sistema de Control de Petroleo</title>
        <link rel="stylesheet" type="text/css" href="pcss/maquinas-1.css" media="screen"/>

    </head>
    <body>
    <div id="imPage">
    <div id="imHeader">
        <h1 class="imHidden">Maquinas - Sistema de Control de Petroleo</h1>

    </div>
    <a class="imHidden" href="#imGoToCont" title="Salta el menu principal">Vaya al Contenido</a>

    <div id="imContentGraphics"></div>
    <div id="imContent">
    <a id="imGoToCont"></a>
    <h2 id="imPgTitle">Maquinas</h2>
    <div id="imBreadcrumb">Consultas</div>


    <?php
    $_GET['enviar'] = true;
    if (isset($_GET['enviar'])) { // comprobamos que se han enviado los datos desde el formulario
        // "limpiamos" los campos del formulario de posibles c�digos maliciosos


        if (empty($_GET['fecha_d'])) {
            echo " Debe ingresar Fecha de Inicio de Consulta";
        } elseif (empty($_GET['select1']) && empty($_GET['maquina'])) {
            $fechai = $_GET['fecha_d'];

            $fecha_d = explode("-", $fechai);
            if (empty($_GET['fecha_h'])) {
                $date = date("d-m-Y");
                $fechat = $date;
                $fecha_h = explode("-", $date);
            } else {
                $fechat = $_GET['fecha_h'];
                $fecha_h = explode("-", $fechat);
            }
            echo " Resumen de todas  las cargas entre fechas $fecha_d[0]-$fecha_d[1]-$fecha_d[2] y $fecha_h[0]-$fecha_h[1]-$fecha_h[2] por faena :";
            $COD_A = 1;
            $Total_AN = 0;

            $qTotal = "SELECT SUM(litros) FROM comb2.ingresos WHERE fecha BETWEEN '{$fecha_d[2]}-{$fecha_d[1]}-{$fecha_d[0]}' AND '{$fecha_h[2]}-{$fecha_h[1]}-{$fecha_h[0]}' ORDER BY fecha ASC";
            $total = mysqli_fetch_row(mysqli_query($db, $qTotal));
            $Total_Porcent = 0;
            $Total_litros = $total[0];
            ?>
            <table width="942" border="1" cellpadding=2 cellspacing=1 style="font-size: 10pt">
                <tr>
                    <td width="64" height="51" Colspan="3">
                        <div align="center" class="Estilo5"><font face="verdana">&nbsp</font></div>
                    </td>
                    <td width="90">
                        <div align="center" class="Estilo6"><font face="verdana">&nbsp</font></div>
                    </td>
                </tr>
                <tr>
                    <td width="64" height="51">
                        <div align="left" class="Estilo5"><font face="verdana">A.N.</font></div>
                    </td>
                    <td width="290">
                        <div align="left" class="Estilo6"><font face="verdana">Faena</font></div>
                    </td>
                    <td width="167">
                        <div align="center" class="Estilo5"><span class="Estilo2">Litros</span></div>
                    </td>
                    <td width="40">
                        <div align="center" class="Estilo6"><font face="verdana">% Total</font></div>
                    </td>
                    <td width="70">
                        <div align="center" class="Estilo5"><span class="Estilo6">Valorizado</span></div>
                    </td>
                </tr>

                <?php
                $colorfila = 0;
                $sql2 = "select a.*, (select COALESCE(SUM(i.litros), 0) from comb2.ingresos as i where i.faena=a.COD_A and i.fecha BETWEEN '{$fecha_d[2]}-{$fecha_d[1]}-{$fecha_d[0]}' AND '{$fecha_h[2]}-{$fecha_h[1]}-{$fecha_h[0]}') as totalAN from comb2.par_faena as a ORDER BY a.COD_A ASC";
                $an = mysqli_query($db, $sql2);//REALIZA LA CONSULTA
                while ($area_neg = mysqli_fetch_array($an)) {
                    if ($colorfila == 0) {
                        $color = "#DEDEBE";
                        $colorfila = 1;
                    } else {
                        $color = "#F0F0F0";
                        $colorfila = 0;
                    }
                    ?>
                    <tr>
                        <td width='106' bgcolor="<?php echo $color; ?>">
                            <div align='left'><?php echo $area_neg[0]; ?></td>
                        <?php $fae = $area_neg[0]; ?>
                        <td width='121'>
                            <div align='left' class='Estilo6'><a
                                    href='det_cons_maq.php?fechai=<?php echo $fechai; ?>&fechat=<?php echo $fechat; ?>&fae=<?php echo $fae; ?>'><?php echo $area_neg[1]; ?></a>
                        </td>
                        <?php $Total_AN = $area_neg['totalAN']; ?>
                        <td width='147'>
                            <div align='center' class='Estilo6'><?php echo $Total_AN; ?></td>
                        <?php
                        // calcula el procentaje de la faena en comparacion con el total (lina 77)
                        $porcentaje = ($Total_AN * 100) / $Total_litros;
                        $Total_Porcent = $Total_Porcent + $porcentaje;
                        $porcent = number_format($porcentaje, 10);
                        ?>
                        <td width='181'>
                            <div align='center'><?php echo $porcent; ?> %
                        </td>
                        <td width='120'>
                            <div align='center' class='Estilo6'>&nbsp
                        </td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <!-- MUESTRA LOS LITROS TOTALS Y EL PORCENTAJE TOTAL (deberia ser 100% obviamente?) -->
                    <td width="64" height="51" Colspan="2">
                        <div align="center" class="Estilo5"><font face="verdana">Litros Totales</font></div>
                    </td>
                    <td width="90">
                        <div align="center" class="Estilo6"><font face="verdana"><?php echo $Total_litros ?></font>
                        </div>
                    </td>
                    <td width="90">
                        <div align="center" class="Estilo6"><font
                                face="verdana"><?php echo "$Total_Porcent %" ?></font></div>
                    </td>
                </tr>
            </table>

            Detalle:
            <table width="942" border="1" cellpadding=2 cellspacing=1 style="font-size: 8pt">
                <tr>
                    <td width="64" height="51">
                        <div align="center" class="Estilo3 Estilo5"><font face="verdana">ID</font></div>
                    </td>
                    <td width="90">
                        <div align="center" class="Estilo6"><font face="verdana">Fecha</font></div>
                    </td>
                    <td width="167">
                        <div align="center" class="Estilo5"><span class="Estilo2">Faena</span></div>
                    </td>
                    <td width="20">
                        <div align="center" class="Estilo6"><font face="verdana">Maquina</font></div>
                    </td>
                    <td width="250">
                        <div align="center" class="Estilo5"><span class="Estilo6">Operador</span></div>
                    </td>
                    <td width="40">
                        <div align="center" class="Estilo6"><font face="verdana">Turno</font></div>
                    </td>
                    <td width="40">
                        <div align="center" class="Estilo5"><span class="Estilo2">Dispensador</span></div>
                    </td>
                    <td width="120">
                        <div align="center" class="Estilo5"><span class="Estilo2">Ubicacion</span></div>
                    </td>
                    <td width="120">
                        <div align="center" class="Estilo5"><span class="Estilo2">Litros</span></div>
                    </td>
                    <td width="120">
                        <div align="center" class="Estilo5"><span class="Estilo2">Horometro</span></div>
                    </td>
                </tr>

                <?php
                // obtiene los ingresos de un rango de fechas
                $sql = "select * from comb2.ingresos where fecha BETWEEN '{$fecha_d[2]}-{$fecha_d[1]}-{$fecha_d[0]}' AND '{$fecha_h[2]}-{$fecha_h[1]}-{$fecha_h[0]}' ORDER BY fecha ASC LIMIT 10";
                $resultado = mysqli_query($db, $sql);//REALIZA LA CONSULTA
                while ($reg = mysqli_fetch_array($resultado)) {
                    $fec = explode("-", $reg[1]);
                    ?>
                    <tr>
                        <td width='64'>
                            <div align='center' class='Estilo6'><?php echo $reg[0]; ?></div>
                        </td>
                        <td width='90'>
                            <div align='center' class='Estilo6'><?php echo $fec[2] . "-" . $fec[1] . "-" . $fec[0]; ?>
                        </td>
                        <td width='71'>
                            <div align='center' class='Estilo6'><?php echo $reg[2]; ?></td>
                        <td width='70'>
                            <div align='center' class='Estilo6'><?php echo $reg[3]; ?></td>
                        <td width='106'>
                            <div align='left' class='Estilo6'><?php echo 'OPERADOR'; ?></td>
                        <td width='121'>
                            <div align='center' class='Estilo6'><?php echo $reg[4]; ?></td>
                        <td width='147'>
                            <div align='center' class='Estilo6'><?php echo $reg[9]; ?></td>
                        <td width='181'>
                            <div align='center' class='Estilo6'><?php echo $reg[6]; ?></td>
                        <td width='120'>
                            <div align='center' class='Estilo6'><?php echo $reg[7]; ?></td>
                        <td width='120'>
                            <div align='center' class='Estilo6'><?php echo $reg[8]; ?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>

            <table width="942" border="1" cellpadding=2 cellspacing=1 style="font-size: 12pt">
                <tr>&nbsp</tr>
                <tr>
                    <td width='64'>
                        <div align='center' class='Estilo6'>&nbsp</div>
                    </td>
                    <td width='90'>
                        <div align='center' class='Estilo6'>&nbsp
                    </td>
                    <td width='71'>
                        <div align='center' class='Estilo6'>&nbsp
                    </td>
                    <td width='106'>
                        <div align='left' class='Estilo6'>&nbsp
                    </td>
                    <td width='121'>
                        <div align='center' class='Estilo6'>&nbsp
                    </td>
                    <td width='147'>
                        <div align='center' class='Estilo6'>&nbsp
                    </td>
                    <td width='181'>
                        <div align='center' class='Estilo6'>Total de Litros
                    </td>
                    <td width='120'>
                        <div align='center' class='Estilo6'><?php echo $Total_litros; ?></td>
                    <td width='120'>
                        <div align='center' class='Estilo6'>&nbsp
                    </td>
                </tr>
            </table>
            <?php
        }//FINALIZA LA TABLA
        elseif (empty($_GET['maquina'])) {
            $fecha_d = explode("-", $_GET['fecha_d']);
            if (empty($_GET['fecha_h'])) {
                $date = date("d-m-Y");
                $fecha_h = explode("-", $date);
            } else {
                $fecha_h = explode("-", $_GET['fecha_h']);
            }
            $faena = mysqli_real_escape_string($db, $_GET['select1']);

            // Selecciona el detalle de una faena especifica
            $sql = "select AN from comb2.par_faena where COD_A={$faena}";
            $resultado = mysqli_query($db, $sql);//REALIZA LA CONSULTA
            $reg = mysqli_fetch_array($resultado);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA

            ?>

            Resumen de todas  las cargas de la faena <?php echo $reg[0]; ?> entre las fechas <?php echo "$fecha_d[0]-$fecha_d[1]-$fecha_d[2]"; ?> y <?php echo "$fecha_h[0]-$fecha_h[1]-$fecha_h[2]"; ?>

            <?php
            // obtiene los ingresos de una faena para cierta fecha
            $sql = "select * from comb2.ingresos where faena=$faena and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "' ORDER BY fecha ASC LIMIT 100";
            $resultado = mysqli_query($db, $sql);//REALIZA LA CONSULTA
            $reg = mysqli_fetch_array($resultado);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA

            $qTotalF = "select SUM(litros) from comb2.ingresos where faena={$faena} and fecha BETWEEN '{$fecha_d[2]}-{$fecha_d[1]}-{$fecha_d[0]}' AND '{$fecha_h[2]}-{$fecha_h[1]}-{$fecha_h[0]}' GROUP BY faena";
            $totalF = mysqli_query($db, $qTotalF);
            $oTotalF = mysqli_fetch_array($totalF);
            $Total_litros = $oTotalF[0];
            // recorre los registros
            ?>
            <table width="942" border="1" cellpadding=2 cellspacing=1 style="font-size: 10pt">
                <tr>
                    <td width="64" height="51" Colspan="3">
                        <div align="center" class="Estilo5"><font face="verdana">&nbsp</font></div>
                    </td>
                    <td width="90">
                        <div align="center" class="Estilo6"><font face="verdana">&nbsp</font></div>
                    </td>
                </tr>
                <tr>
                    <td width="70" height="51">
                        <div align="center" class="Estilo3 Estilo5"><font face="verdana">ID</font></div>
                    </td>
                    <td width="80">
                        <div align="left" class="Estilo6"><font face="verdana">PPU</font></div>
                    </td>
                    <td width="130">
                        <div align="left" class="Estilo5"><span class="Estilo6"><font face="verdana">Tipo</font></span>
                        </div>
                    </td>
                    <td width="150">
                        <div align="left" class="Estilo6"><font face="verdana">Marca</font></div>
                    </td>
                    <td width="150">
                        <div align="left" class="Estilo5"><span class="Estilo2">Modelo</span></div>
                    </td>
                    <td width="150">
                        <div align="center" class="Estilo5"><span class="Estilo2">Litros</span></div>
                    </td>
                    <td width="150">
                        <div align="left" class="Estilo6"><font face="verdana">Porcentaje</font></div>
                    </td>
                    <td width="150">
                        <div align="left" class="Estilo5"><span class="Estilo2">Horas Trabajadas</span></div>
                    </td>
                    <td width="150">
                        <div align="left" class="Estilo5"><span class="Estilo2">Rendimiento</span></div>
                    </td>
                </tr>
                <?php
                $Total_Porcent2 = 0;
                // busca maquinas asociadas a una faena
                $sql = "SELECT DISTINCT num_maquina FROM comb2.ingresos where faena={$faena} ORDER By num_maquina ASC";
                $resultado = mysqli_query($db, $sql);//REALIZA LA CONSULTA
                $reg = mysqli_fetch_array($resultado);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                // para cada maquina
                while ($reg) {
                    echo '<tr>';
                    // obtiene el detalle de la maquina
                    //$sql1 = "select * from comb2.par_maquinas where ID_MAQUINA=$reg[0] ORDER By ID_MAQUINA DESC";
                    //$resultado1 = mysqli_query($db, $sql1);//REALIZA LA CONSULTA
                    //$reg1 = mysqli_fetch_array($resultado1);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                    echo "<td width='70'><div align='center' class='Estilo6'>" . $reg['num_maquina'] . "</div></td>";//EN CADA CELDA SE COLOCA EL CONTENIDO DE REG
                    echo "<td width='80'><div align='left' class='Estilo6'>PPU</td>";
                    echo "<td width='130'><div align='left' class='Estilo6'>TIPO</td>";
                    echo "<td width='150'><div align='left' class='Estilo6'>MARCA</td>";
                    echo "<td width='150'><div align='left' class='Estilo6'>MODELO</td>";
                    $Litros_maq = 0;
                    $Litros_maqf = 0;
                    // obtiene ingresos de una maquina
                    $sql2i = "select * from ingresos where num_maquina=$reg[0] and faena=$faena and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "'";
                    $resultado2i = mysqli_query($db, $sql2i);//REALIZA LA CONSULTA
                    $carga_i = mysqli_fetch_array($resultado2i);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                    // obtiene los litros de la maquina
                    $Litros_maqi = $carga_i[7];

                    // obtiene ingresos de una maquina
                    $sql2 = "select * from ingresos where num_maquina=$reg[0] and faena=$faena and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "'";
                    $resultado2 = mysqli_query($db, $sql2);//REALIZA LA CONSULTA
                    $carga_m = mysqli_fetch_array($resultado2);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                    // Obtiene cargas totales de la maquina
                    while ($carga_m) {
                        $Litros_maq = $Litros_maq + $carga_m[7];
                        $carga_m = mysqli_fetch_array($resultado2);
                    }
                    // balance de litros, descuenta del total el ultimo ingreso, ($carga_m[7] a que columna correspondera?)
                    $Litros_maqf = $Litros_maq - $Litros_maqi;
                    echo "<td width='150'><div align='center' class='Estilo6'>" . $Litros_maq . "</td>";
                    // porcentaje de litros de esta maquina en comparacion con el total de la faena
                    $porcentaje2 = ($Litros_maq * 100) / $Total_litros;
                    $Total_Porcent2 = $Total_Porcent2 + $porcentaje2;
                    $porcent2 = number_format($porcentaje2, 2);
                    echo "<td width='150'><div align='center' class='Estilo6'>" . $porcent2 . " %</td>";
                    $Horo_ini = 0;
                    // obtiene el horometro de la maquina al inicio de la fecha
                    $sql = "select horometro from ingresos where num_maquina=$reg[0] and faena=$faena and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "' ORDER BY horometro ASC LIMIT 1";
                    $resultado4 = mysqli_query($db, $sql);//REALIZA LA CONSULTA
                    $reg4 = mysqli_fetch_array($resultado4);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                    $Horo_ini = $reg4[0];
                    //echo $reg4[0];

                    $Horo_fin = 0;
                    // obtiene el horometro de la maquina al fin de la fecha
                    $sql = "select horometro from ingresos where num_maquina=$reg[0] and faena=$faena and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "' ORDER BY horometro DESC LIMIT 1";
                    $resultado5 = mysqli_query($db, $sql);//REALIZA LA CONSULTA
                    $reg5 = mysqli_fetch_array($resultado5);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                    $Horo_fin = $reg5[0];
                    // echo $reg[0];

                    // obtiene las horas de la fecha
                    $Horas = $Horo_fin - $Horo_ini;
                    // si no hubo horas, no hay rendimiento
                    if ($Horas == 0) {
                        $rend = 'S/R';
                        $rendimiento = $rend;
                    } else {
                        // calcula litros de la fecha, partido por horas de la fecha, obtiene litros por hora, para esta fecha
                        $rend = $Litros_maqf / $Horas;
                        $rendimiento = number_format($rend, 2);
                    }

                    // si la maquina registra rendimiento en kilometraje
                    if (true) {
                        // si no hubo litros de uso en el rango de fecha, no hay rendimiento
                        if ($Litros_maq == 0) {
                            $rend = 'S/R';
                            $rendimiento = $rend;
                        } else {
                            $rend = $Horas / ($Litros_maqf > 0 ? $Litros_maqf : 1);
                            $rendimiento = number_format($rend, 2);
                        }
                    }

                    echo "<td width='150'><div align='center' class='Estilo6'>" . $Horas . "</td>";
                    echo "<td width='150'><div align='center' class='Estilo6'>" . $rendimiento . "</td>";
                    $reg = mysqli_fetch_array($resultado);
                    echo "</tr>";
                }
                ?>
            </table>

            <table width="942" border="1" cellpadding=2 cellspacing=1 style="font-size: 11pt">
                <tr>
                    <?php
                    echo "<td width='60'><div align='center' class='Estilo6'></div></td>";//EN CADA CELDA SE COLOCA EL CONTENIDO DE REG
                    echo "<td width='60'><div align='left' class='Estilo6'></td>";
                    echo "<td width='60'><div align='left' class='Estilo6'></td>";
                    echo "<td width='120'><div align='left' class='Estilo6'></td>";
                    echo "<td width='200'><div align='left' class='Estilo6'>Total Litros</td>";
                    echo "<td width='50'><div align='left' class='Estilo6'>" . $Total_litros . "</td>";
                    echo "<td width='100'><div align='center' class='Estilo6'></td>";
                    echo "<td width='200'><div align='center' class='Estilo6'></td>";
                    echo "<td width='100'><div align='center' class='Estilo6'></td>";
                    ?>
                </tr>
                <tr>
                    <td width="64" height="51" Colspan="8">
                        <div align="center" class="Estilo5"><font face="verdana">&nbsp</font></div>
                    </td>
                    <td width="90">
                        <div align="center" class="Estilo6"><font face="verdana">&nbsp</font></div>
                    </td>
                </tr>

            </table>

            Detalle:

            <table width="942" border="1" cellpadding=2 cellspacing=1 style="font-size: 8pt">
                <tr>
                    <td width="64" height="51">
                        <div align="center" class="Estilo3 Estilo5"><font face="verdana">ID</font></div>
                    </td>
                    <td width="90">
                        <div align="center" class="Estilo6"><font face="verdana">Fecha</font></div>
                    </td>
                    <td width="20">
                        <div align="center" class="Estilo5"><span class="Estilo2">Faena</span></div>
                    </td>
                    <td width="20">
                        <div align="center" class="Estilo6"><font face="verdana">Maq.</font></div>
                    </td>
                    <td width="20">
                        <div align="center" class="Estilo6"><font face="verdana">P.P.U.</font></div>
                    </td>
                    <td width="250">
                        <div align="center" class="Estilo5"><span class="Estilo6">Operador</span></div>
                    </td>
                    <td width="40">
                        <div align="center" class="Estilo6"><font face="verdana">Turno</font></div>
                    </td>
                    <td width="40">
                        <div align="center" class="Estilo5"><span class="Estilo2">Disp.</span></div>
                    </td>
                    <td width="120">
                        <div align="center" class="Estilo5"><span class="Estilo2">Ubicacion</span></div>
                    </td>
                    <td width="120">
                        <div align="center" class="Estilo5"><span class="Estilo2">Litros</span></div>
                    </td>
                    <td width="120">
                        <div align="center" class="Estilo5"><span class="Estilo2">Horometro</span></div>
                    </td>
                </tr>
                <?php
                $sql = "select * from ingresos where faena=$faena and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "'";
                $resultado = mysqli_query($db, $sql);//REALIZA LA CONSULTA
                $reg = mysqli_fetch_array($resultado);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                $Litros_maq = 0;
                $Total_litros = 0;

                while ($reg) {
                    $fec = explode("-", $reg[1]);
                    ?>
                    <tr>
                        <?php
                        echo "<td width='64'><div align='center' class='Estilo6'>" . $reg[0] . "</div></td>";//EN CADA CELDA SE COLOCA EL CONTENIDO DE REG
                        echo "<td width='90'><div align='center' class='Estilo6'>" . $fec[2] . "-" . $fec[1] . "-" . $fec[0] . "</td>";
                        echo "<td width='40'><div align='center' class='Estilo6'>" . $reg[2] . "</td>";
                        echo "<td width='40'><div align='center' class='Estilo6'>" . $reg[3] . "</td>";
                        $maqui = $reg[3];

                        echo "<td width='70'><div align='center' class='Estilo6'>PATENTE</td>";
                        echo "<td width='106'><div align='left' class='Estilo6'>OPERADOR</td>";
                        echo "<td width='121'><div align='center' class='Estilo6'>" . $reg[4] . "</td>";
                        echo "<td width='147'><div align='center' class='Estilo6'>" . $reg[9] . "</td>";
                        echo "<td width='181'><div align='center' class='Estilo6'>" . $reg[6] . "</td>";
                        echo "<td width='120'><div align='center' class='Estilo6'>" . $reg[7] . "</td>";
                        echo "<td width='120'><div align='center' class='Estilo6'>" . $reg[8] . "</td>";
                        $Total_litros = $Total_litros + $reg[7];

                        $reg = mysqli_fetch_array($resultado);
                        ?>
                    </tr>
                    <?php
                }
                ?>
            </table>

            <table width="942" border="1" cellpadding=2 cellspacing=1 style="font-size: 12pt">
                <tr>
                    <?php
                    echo "<td width='64'><div align='center' class='Estilo6'>&nbsp</div></td>";//EN CADA CELDA SE COLOCA EL CONTENIDO DE REG
                    echo "<td width='90'><div align='center' class='Estilo6'>&nbsp</td>";
                    echo "<td width='71'><div align='center' class='Estilo6'>&nbsp</td>";
                    echo "<td width='106'><div align='left' class='Estilo6'>&nbsp</td>";
                    echo "<td width='121'><div align='center' class='Estilo6'>&nbsp</td>";
                    echo "<td width='147'><div align='center' class='Estilo6'>&nbsp</td>";
                    echo "<td width='181'><div align='center' class='Estilo6'>Total de Litros</td>";
                    echo "<td width='120'><div align='center' class='Estilo6'>" . $Total_litros . "</td>";
                    echo "<td width='120'><div align='center' class='Estilo6'>&nbsp</td>";
                    ?>
                </tr>
            </table>

            <?php
        } //FINALIZA LA TABLA
        elseif (empty($_GET['select1'])) {
            $fecha_d = explode("-", $_GET['fecha_d']);
            if (empty($_GET['fecha_h'])) {
                $date = date("d-m-Y");
                $fecha_h = explode("-", $date);
            } else {
                $fecha_h = explode("-", $_GET['fecha_h']);
            }
            $maquina = mysqli_real_escape_string($db, $_GET['maquina']);
            ?>

            Resumen de todas  las cargas de la Maquina $maquina entre las fechas <?php echo "$fecha_d[0]-$fecha_d[1]-$fecha_d[2]"; ?> y <?php echo "$fecha_h[0]-$fecha_h[1]-$fecha_h[2]"; ?>

            <?php
            $sqli = "select * from comb2.ingresos where num_maquina=$maquina and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "' ORDER BY fecha ASC";
            $resultadoi = mysqli_query($db, $sqli);//REALIZA LA CONSULTA
            $regi = mysqli_fetch_array($resultadoi);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
            $Litrosi = $regi[7];

            $sql = "select * from comb2.ingresos where num_maquina=$maquina and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "' ORDER BY fecha ASC";
            $resultado = mysqli_query($db, $sql);//REALIZA LA CONSULTA
            $reg = mysqli_fetch_array($resultado);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
            $Total_litros = 0;
            $Litros_maq = 0;
            while ($reg) {
                $Total_litros = $Total_litros + $reg[7];
                $reg = mysqli_fetch_array($resultado);
            }
            ?>
            <table width="942" border="1" cellpadding=2 cellspacing=1 style="font-size: 10pt">
                <tr>
                    <td width="60" height="51">
                        <div align="center" class="Estilo3 Estilo5"><font face="verdana">Fanea</font></div>
                    </td>
                    <td width="60" height="51">
                        <div align="center" class="Estilo3 Estilo5"><font face="verdana">ID</font></div>
                    </td>
                    <td width="60">
                        <div align="left" class="Estilo6"><font face="verdana">PPU</font></div>
                    </td>
                    <td width="120">
                        <div align="left" class="Estilo5"><span class="Estilo6"><font face="verdana">Tipo</font></span>
                        </div>
                    </td>
                    <td width="20">
                        <div align="left" class="Estilo6"><font face="verdana">Marca</font></div>
                    </td>
                    <td width="200">
                        <div align="left" class="Estilo5"><span class="Estilo2">Modelo</span></div>
                    </td>
                    <td width="100">
                        <div align="center" class="Estilo5"><span class="Estilo2">Litros</span></div>
                    </td>
                    <td width="60">
                        <div align="left" class="Estilo5"><span class="Estilo2">Horas Trabajadas</span></div>
                    </td>
                    <td width="60">
                        <div align="left" class="Estilo5"><span class="Estilo2">Rendimiento</span></div>
                    </td>
                </tr>

                <?php
                $sql = "SELECT DISTINCT faena FROM comb2.ingresos where num_maquina=$maquina ORDER By faena ASC";
                $resultado = mysqli_query($db, $sql);//REALIZA LA CONSULTA
                $reg = mysqli_fetch_array($resultado);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                while ($reg) {
                    ?>
                    <tr>
                        <?php
                        //$sql1 = "select * from comb2.par_maquinas where ID_MAQUINA=$maquina";
                        //$resultado1 = mysqli_query($db, $sql1);//REALIZA LA CONSULTA
                        //$reg1 = mysqli_fetch_array($resultado1);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                        echo "<td width='60'><div align='center' class='Estilo6'>" . $reg[0] . "</div></td>";//EN CADA CELDA SE COLOCA EL CONTENIDO DE REG
                        echo "<td width='60'><div align='center' class='Estilo6'>ID</div></td>";//EN CADA CELDA SE COLOCA EL CONTENIDO DE REG
                        echo "<td width='60'><div align='left' class='Estilo6'>PPU</td>";
                        echo "<td width='60'><div align='left' class='Estilo6'>TIPO</td>";
                        echo "<td width='120'><div align='left' class='Estilo6'>MARCA</td>";
                        echo "<td width='20'><div align='left' class='Estilo6'>MODELO</td>";
                        echo "<td width='200'><div align='center' class='Estilo6'>" . $Total_litros . "</td>";
                        $Horo_ini = 0;
                        $sql = "select horometro from comb2.ingresos where faena=$reg[0] and num_maquina=$maquina  and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "' ORDER BY horometro ASC LIMIT 1";
                        $resultado4 = mysqli_query($db, $sql);//REALIZA LA CONSULTA
                        $reg4 = mysqli_fetch_array($resultado4);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                        $Horo_ini = $reg4[0];
                        //echo $reg4[0];

                        $Horo_fin = 0;
                        $sql = "select horometro from comb2.ingresos where faena=$reg[0] and num_maquina=$maquina and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "' ORDER BY horometro DESC LIMIT 1";
                        $resultado5 = mysqli_query($db, $sql);//REALIZA LA CONSULTA
                        $reg5 = mysqli_fetch_array($resultado5);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                        $Horo_fin = $reg5[0];
                        // echo $reg[0];
                        $Horas = $Horo_fin - $Horo_ini;
                        if ($Horas == 0) {
                            $rend = 'S/R';

                            $rendimiento = $rend;
                        } else {
                            $rend = ($Total_litros - $Litrosi) / $Horas;
                            $rendimiento = number_format($rend, 2);
                        }
                        if (true) {//$reg[6] == 'Camioneta' or $reg[6] == 'Furgon') {
                            if ($Litros_maq == 0) {
                                $rend = 'S/R';
                                $rendimiento = $rend;
                            } else {
                                $rend = $Horas / ($Total_litros - $Litrosi);
                                $rendimiento = number_format($rend, 2);
                            }
                        }

                        echo "<td width='200'><div align='center' class='Estilo6'>" . $Horas . "</td>";
                        echo "<td width='100'><div align='center' class='Estilo6'>" . $rendimiento . "</td>";
                        $reg = mysqli_fetch_array($resultado);
                        ?>
                    </tr>
                    <?php
                }
                ?>
            </table>

            <table width="942" border="1" cellpadding=2 cellspacing=1 style="font-size:11pt">
                <tr>
                    <td width='60'>
                        <div align='center' class='Estilo6'></div>
                    </td>
                    <td width='60'>
                        <div align='left' class='Estilo6'>
                    </td>
                    <td width='60'>
                        <div align='left' class='Estilo6'>
                    </td>
                    <td width='120'>
                        <div align='left' class='Estilo6'>
                    </td>
                    <td width='200'>
                        <div align='left' class='Estilo6'>Total Litros
                    </td>
                    <td width='50'>
                        <div align='left' class='Estilo6'><?php echo $Total_litros; ?></td>
                    <td width='100'>
                        <div align='center' class='Estilo6'>
                    </td>
                    <td width='200'>
                        <div align='center' class='Estilo6'>
                    </td>
                    <td width='100'>
                        <div align='center' class='Estilo6'>
                    </td>
                </tr>
                <tr>
                    <td width="64" height="51" Colspan="8">
                        <div align="center" class="Estilo5"><font face="verdana">&nbsp</font></div>
                    </td>
                    <td width="90">
                        <div align="center" class="Estilo6"><font face="verdana">&nbsp</font></div>
                    </td>
                </tr>
            </table>

            <table width="942" border="1" cellpadding=2 cellspacing=1 style="font-size: 8pt">
                <tr>
                    <td width="64" height="51">
                        <div align="center" class="Estilo3 Estilo5"><font face="verdana">ID</font></div>
                    </td>
                    <td width="90">
                        <div align="center" class="Estilo6"><font face="verdana">Fecha</font></div>
                    </td>
                    <td width="167">
                        <div align="center" class="Estilo5"><span class="Estilo2">Faena</span></div>
                    </td>
                    <td width="20">
                        <div align="center" class="Estilo6"><font face="verdana">Maquina</font></div>
                    </td>
                    <td width="250">
                        <div align="center" class="Estilo5"><span class="Estilo6">Operador</span></div>
                    </td>
                    <td width="40">
                        <div align="center" class="Estilo6"><font face="verdana">Turno</font></div>
                    </td>
                    <td width="40">
                        <div align="center" class="Estilo5"><span class="Estilo2">Dispensador</span></div>
                    </td>
                    <td width="120">
                        <div align="center" class="Estilo5"><span class="Estilo2">Ubicacion</span></div>
                    </td>
                    <td width="120">
                        <div align="center" class="Estilo5"><span class="Estilo2">Litros</span></div>
                    </td>
                    <td width="120">
                        <div align="center" class="Estilo5"><span class="Estilo2">Horometro</span></div>
                    </td>
                </tr>
                <?php
                $sql = "select * from comb2.ingresos where num_maquina=$maquina and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "' ORDER BY horometro DESC";
                $resultado = mysqli_query($db, $sql);//REALIZA LA CONSULTA
                $reg = mysqli_fetch_array($resultado);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                $Total_litros = 0;
                while ($reg) {
                    ?>
                    <tr>
                        <?php
                        $fec = explode("-", $reg[1]);
                        echo "<td width='64'><div align='center' class='Estilo6'>" . $reg[0] . "</div></td>";//EN CADA CELDA SE COLOCA EL CONTENIDO DE REG
                        echo "<td width='90'><div align='center' class='Estilo6'>" . $fec[2] . "-" . $fec[1] . "-" . $fec[0] . "</td>";
                        echo "<td width='71'><div align='center' class='Estilo6'>" . $reg[2] . "</td>";
                        echo "<td width='70'><div align='center' class='Estilo6'>" . $reg[3] . "</td>";
                        //$sql2 = "select APELLIDO_P, NOMBRES from par_traba where RUT='$reg[5]'";
                        //$resultado2 = mysqli_query($db, $sql2);//REALIZA LA CONSULTA
                        //$reg2 = mysqli_fetch_array($resultado2);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                        echo "<td width='106'><div align='left' class='Estilo6'>OPERADOR</td>";
                        echo "<td width='121'><div align='center' class='Estilo6'>" . $reg[4] . "</td>";
                        echo "<td width='147'><div align='center' class='Estilo6'>" . $reg[9] . "</td>";
                        echo "<td width='181'><div align='center' class='Estilo6'>" . $reg[6] . "</td>";
                        echo "<td width='120'><div align='center' class='Estilo6'>" . $reg[7] . "</td>";
                        echo "<td width='120'><div align='center' class='Estilo6'>" . $reg[8] . "</td>";
                        $Total_litros = $Total_litros + $reg[7];
                        $reg = mysqli_fetch_array($resultado);
                        ?>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
        } else {
            //echo "Todas  las Cargas de la Maquina en la Faena entre las fechas";
            //echo ".$faena." ".$num_maquina.";
            //{
            $fecha_d = explode("-", $_GET['fecha_d']);
            if (empty($_GET['fecha_h'])) {
                $date = date("d-m-Y");
                $fecha_h = explode("-", $date);
            } else {
                $fecha_h = explode("-", $_GET['fecha_h']);
            }
            $faena = mysqli_real_escape_string($db, $_GET['select1']);
            $maquina = mysqli_real_escape_string($db, $_GET['maquina']);

            ?>

            Resumen de todas  las cargas de la Maquina $maquina en la Faena $faena entre las fechas <?php echo "$fecha_d[0]-$fecha_d[1]-$fecha_d[2]"; ?> y <?php echo "$fecha_h[0]-$fecha_h[1]-$fecha_h[2]"; ?>

            <?php
            $sql = "select * from comb2.ingresos where num_maquina=$maquina and faena=$faena and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "' ORDER BY fecha ASC";
            $resultado = mysqli_query($db, $sql);//REALIZA LA CONSULTA
            $reg = mysqli_fetch_array($resultado);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
            $Total_litros = 0;
            while ($reg) {
                $Total_litros = $Total_litros + $reg[7];
                $reg = mysqli_fetch_array($resultado);
            }
            ?>
            <table width="942" border="1" cellpadding=2 cellspacing=1 style="font-size: 10pt">
                <tr>
                    <td width="60" height="51">
                        <div align="center" class="Estilo3 Estilo5"><font face="verdana">ID</font></div>
                    </td>
                    <td width="60">
                        <div align="left" class="Estilo6"><font face="verdana">PPU</font></div>
                    </td>
                    <td width="120">
                        <div align="left" class="Estilo5"><span class="Estilo6"><font face="verdana">Tipo</font></span>
                        </div>
                    </td>
                    <td width="20">
                        <div align="left" class="Estilo6"><font face="verdana">Marca</font></div>
                    </td>
                    <td width="200">
                        <div align="left" class="Estilo5"><span class="Estilo2">Modelo</span></div>
                    </td>
                    <td width="100">
                        <div align="center" class="Estilo5"><span class="Estilo2">Litros</span></div>
                    </td>
                    <td width="60">
                        <div align="left" class="Estilo5"><span class="Estilo2">Horas Trabajadas</span></div>
                    </td>
                    <td width="60">
                        <div align="left" class="Estilo5"><span class="Estilo2">Rendimiento</span></div>
                    </td>
                </tr>

                <?php
                //$sql = "select * from comb2.par_maquinas where ID_MAQUINA=$maquina";
                //$resultado = mysqli_query($db, $sql);//REALIZA LA CONSULTA
                //$reg = mysqli_fetch_array($resultado);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                while ($reg) {
                    ?>
                    <tr>
                        <?php
                        echo "<td width='60'><div align='center' class='Estilo6'>ID</div></td>";//EN CADA CELDA SE COLOCA EL CONTENIDO DE REG
                        echo "<td width='60'><div align='left' class='Estilo6'>PPU</td>";
                        echo "<td width='60'><div align='left' class='Estilo6'>TIPO</td>";
                        echo "<td width='120'><div align='left' class='Estilo6'>MARCA</td>";
                        echo "<td width='20'><div align='left' class='Estilo6'>MODEL</td>";
                        echo "<td width='200'><div align='center' class='Estilo6'>" . $Total_litros . "</td>";
                        $Horo_ini = 0;
                        $sql = "select horometro from ingresos where num_maquina=$reg[0] and faena=$faena and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "' ORDER BY horometro ASC LIMIT 1";
                        $resultado4 = mysqli_query($db, $sql);//REALIZA LA CONSULTA
                        $reg4 = mysqli_fetch_array($resultado4);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                        $Horo_ini = $reg4[0];
                        //echo $reg4[0];

                        $Horo_fin = 0;
                        $sql = "select horometro from comb2.ingresos where num_maquina=$reg[0] and faena=$faena and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "' ORDER BY horometro DESC LIMIT 1";
                        $resultado5 = mysqli_query($db, $sql);//REALIZA LA CONSULTA
                        $reg5 = mysqli_fetch_array($resultado5);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                        $Horo_fin = $reg5[0];
                        // echo $reg[0];
                        $Horas = $Horo_fin - $Horo_ini;
                        if ($Horas == 0) {
                            $rend = 'S/R';
                            $rendimiento = $rend;
                        } else {
                            $rend = $Total_litros / $Horas;
                            $rendimiento = number_format($rend, 2);
                        }
                        if ($reg[6] == 'Camioneta' or $reg[6] == 'Furgon') {
                            if ($Litros_maq == 0) {
                                $rend = 'S/R';
                                $rendimiento = $rend;
                            } else {
                                $rend = $Horas / $Total_litros;
                                $rendimiento = number_format($rend, 2);
                            }
                        }

                        echo "<td width='200'><div align='center' class='Estilo6'>" . $Horas . "</td>";
                        echo "<td width='100'><div align='center' class='Estilo6'>" . $rendimiento . "</td>";
                        $reg = mysqli_fetch_array($resultado);
                        ?>
                    </tr>
                    <?php
                }
                ?>
            </table>

            <table width="942" border="1" cellpadding=2 cellspacing=1 style="font-size: 11pt">
                <tr>
                    <td width='60'>
                        <div align='center' class='Estilo6'></div>
                    </td>
                    <td width='60'>
                        <div align='left' class='Estilo6'>
                    </td>
                    <td width='60'>
                        <div align='left' class='Estilo6'>
                    </td>
                    <td width='120'>
                        <div align='left' class='Estilo6'>
                    </td>
                    <td width='200'>
                        <div align='left' class='Estilo6'>Total Litros
                    </td>
                    <td width='50'>
                        <div align='left' class='Estilo6'><?php echo $Total_litros; ?></td>
                    <td width='100'>
                        <div align='center' class='Estilo6'>
                    </td>
                    <td width='200'>
                        <div align='center' class='Estilo6'>
                    </td>
                    <td width='100'>
                        <div align='center' class='Estilo6'>
                    </td>
                </tr>
                <tr>
                    <td width="64" height="51" Colspan="3">
                        <div align="center" class="Estilo5"><font face="verdana">&nbsp</font></div>
                    </td>
                    <td width="90">
                        <div align="center" class="Estilo6"><font face="verdana">&nbsp</font></div>
                    </td>
                </tr>
            </table>

            <table width="942" border="1" cellpadding=2 cellspacing=1 style="font-size: 8pt">
                <tr>
                    <td width="64" height="51">
                        <div align="center" class="Estilo3 Estilo5"><font face="verdana">ID</font></div>
                    </td>
                    <td width="90">
                        <div align="center" class="Estilo6"><font face="verdana">Fecha</font></div>
                    </td>
                    <td width="167">
                        <div align="center" class="Estilo5"><span class="Estilo2">Faena</span></div>
                    </td>
                    <td width="20">
                        <div align="center" class="Estilo6"><font face="verdana">Maquina</font></div>
                    </td>
                    <td width="250">
                        <div align="center" class="Estilo5"><span class="Estilo6">Operador</span></div>
                    </td>
                    <td width="40">
                        <div align="center" class="Estilo6"><font face="verdana">Turno</font></div>
                    </td>
                    <td width="40">
                        <div align="center" class="Estilo5"><span class="Estilo2">Dispensador</span></div>
                    </td>
                    <td width="120">
                        <div align="center" class="Estilo5"><span class="Estilo2">Ubicacion</span></div>
                    </td>
                    <td width="120">
                        <div align="center" class="Estilo5"><span class="Estilo2">Litros</span></div>
                    </td>
                    <td width="120">
                        <div align="center" class="Estilo5"><span class="Estilo2">Horometro</span></div>
                    </td>
                </tr>
                <?php
                $sql = "select * from comb2.ingresos where num_maquina=$maquina and faena=$faena and fecha BETWEEN '" . $fecha_d[2] . "-" . $fecha_d[1] . "-" . $fecha_d[0] . "' AND '" . $fecha_h[2] . "-" . $fecha_h[1] . "-" . $fecha_h[0] . "' ORDER BY horometro DESC";
                $resultado = mysqli_query($db, $sql);//REALIZA LA CONSULTA
                $reg = mysqli_fetch_array($resultado);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                $Total_litros = 0;
                while ($reg) {
                    ?>
                    <tr>
                        <?php
                        $fec = explode("-", $reg[1]);
                        echo "<td width='64'><div align='center' class='Estilo6'>" . $reg[0] . "</div></td>";//EN CADA CELDA SE COLOCA EL CONTENIDO DE REG
                        echo "<td width='90'><div align='center' class='Estilo6'>" . $fec[2] . "-" . $fec[1] . "-" . $fec[0] . "</td>";
                        echo "<td width='71'><div align='center' class='Estilo6'>" . $reg[2] . "</td>";
                        echo "<td width='70'><div align='center' class='Estilo6'>" . $reg[3] . "</td>";
                        //$sql2 = "select APELLIDO_P, NOMBRES from par_traba where RUT='$reg[5]'";
                        //$resultado2 = mysqli_query($db, $sql2);//REALIZA LA CONSULTA
                        //$reg2 = mysqli_fetch_array($resultado2);//LA VARIABLE $REG GUARDA LOS REGISTROS DE LA CONSULTA REALIZADA
                        echo "<td width='106'><div align='left' class='Estilo6'>OPERADOR</td>";
                        echo "<td width='121'><div align='center' class='Estilo6'>" . $reg[4] . "</td>";
                        echo "<td width='147'><div align='center' class='Estilo6'>" . $reg[9] . "</td>";
                        echo "<td width='181'><div align='center' class='Estilo6'>" . $reg[6] . "</td>";
                        echo "<td width='120'><div align='center' class='Estilo6'>" . $reg[7] . "</td>";
                        echo "<td width='120'><div align='center' class='Estilo6'>" . $reg[8] . "</td>";
                        $reg = mysqli_fetch_array($resultado);
                        ?>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
        }
    }
    ?>
        <div id="imFooterBg">
            <div id="imFooter">
            </div>
        </div>
        <span class="imHidden"><a href="#imGoToCont" title="Lea esta página de nuevo">Regreso al contenido</a> |
            <a href="#imGoToMenu" title="Lea este sitio de nuevo">Regreso al menu principal</a></span>
    </body>
    </html>
    <?php
}
?>
