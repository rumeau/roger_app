/**
 * Created by jean on 12-05-16.
 */

module.exports = {
    params: ['value'],

    bind: function () {
        $(this.el).datepicker({
            dateFormat: 'dd/mm/yy'
        });
    },
    update: function (val) {
      var p = /[0-9]{2}\/[0-9]{2}\/[0-9]{2}/;
      if (this.params.value != '' && p.test(this.params.value)) {
        $(this.el).val(this.params.value);
      }
    }
};
