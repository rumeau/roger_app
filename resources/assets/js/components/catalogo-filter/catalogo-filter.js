module.exports = {
    data: function() {
        return {
            operation: '',
            operations: [
                {
                    text: 'ad', value: 1
                }, {
                    text: 'vc', value: 2
                }
            ],
            work_shift: '',
            work_shifts: [],
            date_from: '',
            date_to: '',
            order_number: '',
            bill_number: '',
            provider: '',
            dispenser: '',
            dispensers: [],
            operator: '',
            operators: [],
            equipment: '',
            cc: '',
           // cc: [],
            
            filtering: false
        };
    },
    
    props: {
        url: '',
    },
    
    ready: function() {
        var that = this;
        
        $("#catalogosfilter").jsGrid({
            width: "100%",
            height: "430px",
            pageSize: 10,
        
            filtering: false,
            editing: false,
            sorting: true,
            paging: true,
            autoload: true,
            pageLoading: true,
            controller: {
                loadData: function(filter) {
                    if (that.filtering == undefined || that.filtering !== true) {
                        filter.operation    = that.operation;
                        filter.work_shift   = that.work_shift;
                        filter.date_from    = that.date_from;
                        filter.date_to      = that.date_to;
                        filter.order_number = that.order_number;
                        filter.bill_number  = that.bill_number;
                        filter.provider     = that.provider;
                        filter.dispenser    = that.dispenser;
                        filter.operator     = that.operator;
                        filter.equipment    = that.equipment;
                        filter.cc           = that.cc;
                    }
                    
                    return $.ajax({
                        type: "GET",
                        url: that.url,
                        data: filter
                    });
                }
            },
        
            fields: [
               
                
                { name: "name", title: "Tipo catálogo", type: "text", width: 70 },
                { name: "n1", title: "Tipo Solicitud", type: "text", width: 70 },
                { name: "n2", title: "Plataforma", type: "text", width: 70 },
                { name: "n3", title: "Sistema", type: "text", width: 70 },
                { name: "n4", title: "Sub-sistema", type: "text", width: 70 },
                { name: "n5", title: "Elemento", type: "text", width: 70 },
                { name: "n6", title: "Acción", type: "text", width: 70 },
                
            ]
        });
    },
    
    methods: {
        filterSearch: function($event) {
            $event.preventDefault();
            
            this.filtering = true;
            
            $('#catalogosfilter').jsGrid('search', {
                operation: this.operation,
                work_shift: this.work_shift,
                date_from: this.date_from,
                date_to: this.date_to,
                order_number: this.order_number,
                bill_number: this.bill_number,
                //provider: this.provider,
                dispenser: this.dispenser,
                operator: this.operator,
                equipment: this.equipment,
                cc: this.cc,
            });
        },
        
        clearFilters: function($event) {
            $event.preventDefault();
            
            this.filtering    = true;
            this.operation    = '';
            this.work_shift   = '';
            this.date_from    = '';
            this.date_to      = '';
            this.dispenser    = '';
            this.operator     = '';
            this.cc           = '';
            this.equipment    = '';
            
            $('#catalogosfilter').jsGrid("search");
        },
        
        operationChanged: function() {
            this.work_shift  = '';
            this.work_shifts = [];
            this.dispenser   = '';
            this.dispensers  = [];
            if (this.operation != '') {
                //actualizar turnos
                this.$http.get(
                    '/ajax/search-filters/work-shift-by-operation', {
                        operation_id: this.operation
                    }, function(data){
                        this.work_shifts = data;
                    }
                );
            }
        },
    }
};