module.exports={
    data: function(){
        return{
            show: false,
            
            level_1         : '',
            level_1s        : [],
            level_2         : '',
            level_2s        : [],
            level_3         : '',
            level_3s        : [],
            level_4         : '',
            level_4s        : [],
            level_5         : '',
            level_5s        : [],
            level_6         : '',
            level_6s        : [],
            operation       : '',
            operations      : [],
            ubication       : '',
            ubications      : [],
            affected:       '',
            affecteds:      [],
            responsible: '',
            responsibles: [],
            enterprise: '',
            enterprises: [],
            order_number: '',
            showAlert   : false,
            message     : '',
            alertType   : 'success',
            errors      : {},
            componentes: []
        };    
    },
    
    ready: function(){
        var that = this;
        $('#seleccionComponentes').on('show.bs.modal', function(){
            that.componentes = [];
            that.$http.get('/ajax/load-components/' + that.affected, {}, function(data){
                that.componentes = data;
            });
        });
    },
    
    filters: {
        equipmentName: function(value, type) {
             return type == 'App\Models\Equipo' ? value.cc : value.name;
        },
        equipmentBrand: function(value, type)
        {
            return type == 'App\Models\Equipo' ? value.patent : value.brand.name;
        },
        equipmentPatent: function(value, type)
        {
            return type == 'App\Models\Equipo' ? value.patent : value.patent;
        },
        equipmentModel: function(value, type)
        {
            return type == 'App\Models\Equipo' ? value.brand : value.model.name;
        },
        equipmentSerial: function(value, type)
        {
            return type == 'App\Models\Equipo' ? value.model : value.serial_number;
        },
        equipmentFamily: function(value, type)
        {
            return type == 'App\Models\Equipo' ? value.category.name : value.category.name;
        }
    },
    
    methods:{
        onSubmit: function($event){
          $event.preventDefault();
          
          this.$http.post($event.target.getAttribute('action'), new FormData($event.target), function($data){
              this.showAlert = true;
              this.message = $data.message;
              this.alertType = $data.status;
              this.errors = [];
              if($data.success){
                  if(typeof $data.redirect != 'undefined'){
                      window.location.href  = $data.redirect;
                  } else {
                        this.ubication  = '';
                        this.equipment  = '';
                        this.operator   = '';
                        this.counter    = '';
                        this.hour_meter = '';
                        this.liters     = '';
                        window.scrollTo(0,0);
                  }
              }else{
                  this.errors = $data.errors;    
              }
          })
          .error(function($data) {
              this.errors = $data;
          });
        },
        
        
        
        
        
        
        requestChange: function(){
            //this.level_1    = '';
            this.level_2    = '';
            this.level_2s   = [];
            
            //if (this.level_1 != '') {
                //actualizar turnos
                this.$http.get(
                    '/ajax/search-filters/level_2-by-level_1', {
                        type_request_id: this.level_1
                    },
                    function(data){
                        this.level_2s = data;
                    }
                );
                
            //}
        },
        
        platformChange: function() {
            this.level_3    = '';
            this.level_3s   = [];
            
            //if (this.level_1 != '') {
                //actualizar turnos
                this.$http.get(
                    '/ajax/search-filters/level_3-by-level_2', {
                        type_request_id: this.level_1,
                        platform_id: this.level_2
                        
                    },
                    function(data){
                        this.level_3s = data;
                    }
                );
        },
        
        systemChange: function() {
            this.level_4    = '';
            this.level_4s   = [];
            
            //if (this.level_1 != '') {
                //actualizar turnos
                this.$http.get(
                    '/ajax/search-filters/level_4-by-level_3', {
                        type_request_id:    this.level_1,
                        platform_id:        this.level_2,
                        n3_system_id:       this.level_3,
                        
                    },
                    function(data){
                        this.level_4s = data;
                    }
                );
        },
        
        subSystemChange: function(){
            this.level_5    = '';
            this.level_5s   = [];
            this.$http.get(
                    '/ajax/search-filters/level_5-by-level_4', {
                        type_request_id:    this.level_1,
                        platform_id:        this.level_2,
                        n3_system_id:       this.level_3,
                        n4_sub_system_id:   this.level_4,
                    },
                    function(data){
                        this.level_5s = data;
                    }
                );
        },
        
        n5Change: function(){
            this.level_6    = '';
            this.level_6s   = [];
            this.$http.get(
                    '/ajax/search-filters/level_6-by-level_5', {
                        type_request_id:    this.level_1,
                        platform_id:        this.level_2,
                        n3_system_id:       this.level_3,
                        n4_sub_system_id:   this.level_4,
                        n5:                 this.level_5,
                    },
                    function(data){
                        this.level_6s = data;
                    }
            );
        },
        
        operationChanged: function(){
            
        },
        
    }
};