module.exports = {
    data: function() {
        return {
            operation:      '',
            operations:     [
                {
                    text: 'ad', value: 1
                }, {
                    text: 'vc', value: 2
                }
            ],
            work_shift:     '',
            work_shifts:    [],
            date_from:      '',
            date_to:        '',
            order_number:   '',
            bill_number:    '',
            provider:       '',
            dispenser:      '',
            dispensers:     [],
            operator:       '',
            operators:      [],
            equipment:      '',
            cc:             '',
            responsible:    '',
            productiveArea: '',
            productive_areas: [],
            productive_area: '',
            
            filtering: false
        };
    },
    
    props: {
        url: '',
    },
    
    ready: function() {
        var that = this;
        
        $("#consumofilter").jsGrid({
            width: "100%",
            height: "430px",
            pageSize: 10,
        
            filtering: false,
            editing: false,
            sorting: true,
            paging: true,
            autoload: true,
            pageLoading: true,
            controller: {
                loadData: function(filter) {
                   // if (that.filtering == undefined || that.filtering !== true) {
                        filter.operation        = that.operation;
                        filter.work_shift       = that.work_shift;
                        filter.date_from        = that.date_from;
                        filter.date_to          = that.date_to;
                        filter.order_number     = that.order_number;
                        filter.bill_number      = that.bill_number;
                        filter.provider         = that.provider;
                        filter.dispenser        = that.dispenser;
                        filter.operator         = that.operator;
                        filter.equipment        = that.equipment;
                        filter.cc               = that.cc;
                        filter.responsible      = that.responsible;
                        filter.productive_area  = that.productive_area;
                 //   }
                    
                    return $.ajax({
                        type: "GET",
                        url: that.url,
                        data: filter
                    });
                }
            },
        
            fields: [
                { name: "id", type: "control", width: 30, cellRenderer: function(value, item) {
                    return '<td align="left"><a href="consumption/'+value+'/edit" class="btn btn-sm btn-default" data-toggle="tooltip" data-title="Editar"><span class="glyphicon glyphicon-pencil"></span></a></td>';
                } },
                /*
                { name: "id", type: "control", width: 30, cellRenderer: function(value, item) {
                    return '<td><a href="consumption/'+value+'/delete" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash"></span></a></td>';
                } },*/
                { name: "operation", title: "Faena", type: "text", width:70 },
                { name: "work_shift", title: "Turno", type: "text", width: 30 },
                { name: "dispenser", title: "Dispensador", type: "text", width: 70 },
                /*
                { name: "productive_area", title: "Area. Productiva", type: "text", width:70, cellRenderer: function (value, item) {
                    console.log(item);
                    if (typeof item.productive_area === 'undefined' || item.productive_area === null){
                        return '<td></td>';
                    }else{
                        return '<td>' + value.name + '</td>';
                    }
                } },*/
                { name: "order_number", title: "Folio", type: "text", width: 35 },
                { name: "cc", title: "CC", type: "text", width: 40, align: "right" },
                { name: "patent", title: "Patente", type: "text", width: 60, align: "right", cellRenderer: function (value, item) {
                    return '<td><code>' + value + '</code></td>';
                } },
                { name: "type", title: "Tipo", type: "text", width: 70 },
                { name: "brand", title: "Marca", type: "text", width: 70 },
                { name: "model", title: "Modelo", type: "text", width: 70 },
                { name: "operator", title: "Operador", type: "text", width: 70 },
                { name: "load_date", title: "Fecha carga", type: "text", width: 70, align: "center", cellRenderer: function (value, item) {
                    var MyDate = new Date(value);
                    var MyDateString;

                    MyDateString = ('0' + MyDate.getDate()).slice(-2) + '/' + ('0' + (MyDate.getMonth()+1)).slice(-2) + '/'
                                + MyDate.getFullYear();
                    
                    return '<td>' + MyDateString + '</td>';
                } },
                { name: "liters", title: "Litros", type: "text", width: 40 },
                { name: "counter", title: "Contador", type: "text", width: 55 },
                { name: "hour_meter", title: "Horómetro", type: "text", width: 55 },
            ]
        });
    },
    
    methods: {
        filterSearch: function($event) {
            $event.preventDefault();
            
            this.filtering = true;
            
            $('#consumofilter').jsGrid('search', {
                operation:          this.operation,
                work_shift:         this.work_shift,
                date_from:          this.date_from,
                date_to:            this.date_to,
                order_number:       this.order_number,
                bill_number:        this.bill_number,
                //provider:         this.provider,
                dispenser:          this.dispenser,
                operator:           this.operator,
                equipment:          this.equipment,
                cc:                 this.cc,
                responsible:        this.responsible,
                productive_area:    this.productive_area
            });
        },
        
        clearFilters: function($event) {
            $event.preventDefault();
            
            this.filtering          = true;
            this.operation          = '';
            this.work_shift         = '';
            this.date_from          = '';
            this.date_to            = '';
            this.dispenser          = '';
            this.operator           = '';
            this.cc                 = '';
            this.equipment          = '';
            this.order_number       = '';
            this.responsible        = '';
            this.productive_area    = '';
            
            $('#consumofilter').jsGrid("search");
        },
        
        operationChanged: function() {
            this.work_shift  = '';
            this.work_shifts = [];
            this.dispenser   = '';
            this.dispensers  = [];
            this.productive_area = '';
            this.productive_areas = [];
            if (this.operation != '') {
                //actualizar turnos
                this.$http.get('/ajax/search-filters/work-shift-by-operation?operation_id=' + this.operation)
                    .then(function (response){
                        this.work_shifts = response.data;
                    });
                /*
                this.$http.get(
                    '/ajax/search-filters/dispenser-by-operation-tct', {
                        operation_id: this.operation
                    }, function(data) {
                        this.dispensers = data;
                    }
                );
                this.$http.get(
                    '/ajax/search-filters/productiveAreas-by-operation', {
                        operation_id: this.operation
                    },
                    function(data){
                        this.productive_areas = data;
                    }
                );*/
            }
        },
        
        exportExcel: function() {
            window.location.href = '/consumption?export=1&' + $.param({
                    operation:        this.operation,
                    work_shift:       this.work_shift,
                    date_from:        this.date_from,
                    date_to:          this.date_to,
                    dispenser:        this.dispenser,
                    operator:         this.operator,
                    cc:               this.cc,
                    equipment:        this.equipment,
                    order_numer:      this.order_number,
                    responsible:      this.responsible,
                    productive_area:  this.productive_area
                });
        },
    }
};