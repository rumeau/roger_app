module.exports = {
    data: function() {
        return {
            operation: '',
            operations: [
                {
                    text: 'ad', value: 1
                }, {
                    text: 'vc', value: 2
                }
            ],
            enterprise: '',
            enterprises: [],
            date_from: '',
            date_to: '',
                
            filtering: false
        };
    },
    
    props: {
        url: '',
    },

    ready: function() {
        // jsGrid trata 'this', como un objeto de jsGrid, por eso para mantener a vue dentro de jsGrid lo guardo
        // en 'that' de manera temporal.
        var that = this;
        
        // Carga la grilla con los datos
        $("#initdispensadorfilter").jsGrid({
            width: "100%",
            height: "430px",
            pageSize: 10,
            filtering: false,
            editing: false,
            sorting: true,
            paging: true,
            autoload: true,
            pageLoading: true,
            controller: {
                loadData: function(filter) {
                    return $.ajax({
                        type: "GET",
                        url: that.url,
                        data: filter
                    });
                }
            },
            
            fields: [
                { name: "id", type: "control", width: 20, cellRenderer: function(value, item) {
                    return '<td><a href="#" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-pencil"></span></a></td>';
                } },
                { name: "id", type: "control", width: 20, cellRenderer: function(value, item) {
                    return '<td><a href="#" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash"></span></a></td>';
                } },
                { name: "operation", title: "Faena", type: "text", width:25, cellRenderer: function (value, item) {
                    return '<td>' + value.name + '</td>';
                } },
                { name: "dispenser", title: "Faena", type: "text", width:25, cellRenderer: function (value, item) {
                    return '<td>' + value.name + '</td>';
                } },
                { name: "previous_balance", title: "Saldo anterior", type: "number", width: 20 },
                { name: "current_balance", title: "Saldo actual", type: "number", width: 20 },
                { name: "liters", title: "Litros cargados", type: "number", width: 20 },
                { name: "concept", title: "Concepto", type: "text", width:25, cellRenderer: function (value, item) {
                    
                    if(value == null){
                        return '<td></td>';
                    }else{
                        return '<td>' + value.name + '</td>';
                    }
                } },
                { name: "date_residue", title: "Fecha", type: "number", width: 30 },
                
                
            ]
        });
    },

    methods: {
        
    }
};