var swal = require('sweetalert'),
    moment = require('moment');

module.exports = {
    http: {
        headers: {
            'X-CSRF-TOKEN': document.querySelector('#metatoken').getAttribute('content')
        }
    },

    data: function() {
        return {
            data: {
                operation_id: '',
                operator_id: '',
                work_shift_id: '',
                loaded_origin_id: '',
                loaded_destination_id: '',
                responsible_id: '',
                counter: '',
                hour_meter: '',
                liters: '',
                load_date: '',
                order_number: ''
            },

            operators: [],
            work_shifts: [],
            dispenser_origins: [],
            responsibles: [],

            
            showAlert : false,
            message : '',
            alertType : 'success',
            errors: {},
            
            isEdit: false
        };    
    },
    
    computed: {
        dispenser_destinations: function() {
            var dispenser_origin = this.data.loaded_origin_id;
            return _.filter(this.dispenser_origins, function(item, i) {
                return item.value != dispenser_origin;
            });
        }
    },
    
    props: ['edit'],
    
    ready: function() {
        if (this.edit != '' && typeof this.edit != 'undefined') {
            this.isEdit = true;
            this.$http({
                url: this.edit,
                method: 'GET'
            })
                .then(function (response) {
                    this.data.operation_id           = response.data.operation_id;
                    this.operationChanged();
                    this.data.load_date              = moment(response.data.load_date).format('DD/MM/YYYY');
                    this.data.responsible_id         = response.data.responsible_id;
                    this.data.work_shift_id          = response.data.work_shift_id;
                    this.data.loaded_origin_id       = response.data.loaded_origin_id;
                    this.data.loaded_destination_id  = response.data.loaded_destination_id;
                    this.data.operator_id            = response.data.operator_id;
                    this.data.counter                = response.data.counter;
                    this.data.hour_meter             = response.data.hour_meter;
                    this.data.order_number           = response.data.order_number;
                    this.data.liters                 = response.data.liters;
                }, function (response) {

                });
        }
    },

    methods:{
        onSubmit: function($event) {
            this.$http({
                url: $event.target.getAttribute('action'),
                method: this.isEdit ? 'PUT' : 'POST',
                data: this.data
            })
                .then(function (response) {
                    this.showAlert = true;
                    this.message = response.data.message;
                    this.alertType = response.data.status;
                    this.errors = [];
                    if (response.data.success) {
                        if (typeof response.data.redirect != 'undefined') {
                            window.location.href  = response.data.redirect;
                        } else {
                            this.data.operation_id           = '';
                            this.data.load_date              = '';
                            this.data.responsible_id         = '';
                            this.data.order_number           = '';
                            this.data.loaded_origin_id       = '';
                            this.data.loaded_destination_id  = '';
                            this.data.operator_id            = '';
                            this.data.counter                = '';
                            this.data.hour_meter             = '';
                            this.data.liters                 = '';
                            this.data.work_shift_id          = '';
                            window.scrollTo(0,0);
                        }
                    } else {
                        this.errors = response.data.errors;
                    }
                }, function (response) {
                    this.errors = response.data;
                });
        },
        
        operationChanged: function() {
            this.data.work_shift_id         = '';
            this.data.loaded_origin_id      = '';
            this.data.loaded_destination_id = '';
            //this.data.counter = '';
            //this.data.hour_meter = '';
            //this.data.liters = '';

            this.work_shifts = [];
            this.dispensers  = [];
            this.equipments  = [];
            this.operators   = [];

            if (this.operation_id != '') {
                //actualizar turnos
                this.$http.get('/ajax/search-filters/work-shift-by-operation?operation_id=' + this.data.operation_id)
                    .then(function (response) {
                        this.work_shifts = response.data;
                    });

                this.$http.get('/ajax/search-filters/dispenser-by-operation?operation_id=' + this.data.operation_id)
                    .then(function (response) {
                        this.dispenser_origins = response.data;
                    });

                this.$http('/ajax/search-filters/dispenser-by-operation_destination?operation_id=' + this.data.operation_id)
                    .then(function (response) {
                        this.dispenser_destinations = response.data;
                    });

                this.$http('/ajax/search-filters/operators-by-operation?operation_id=' + this.data.operation_id)
                    .then(function (response) {
                        this.operators = response.data;
                    });
            }
        },

        checkDestination: function () {
            if (this.data.loaded_origin_id == this.data.loaded_destination_id) {
                this.data.loaded_destination_id = '';
            }
        }
    }      
};