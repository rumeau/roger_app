module.exports={
    data: function(){
        return{
            operation           : '',
            equipment           : '',
            enterprise          : '',
            operations          : [],
            equipments          : [],
            category            : '',
            categorys           : [],
            type                : '',
            types               : [],
            brand               : '',
            brands              : [],
            model               : '',
            models              : [],
            typeidentification  : '',
            owner               : '',
            
            showAlert           : false,
            message             : '',
            alertType           : 'success',
            errors              : {},
        };    
    },
    
    props: ['edit'],
    
    ready: function(){
        if (this.edit != '' && typeof this.edit != 'undefined'){
            this.$http.get(this.edit, {}, function(data){
               this.enterprise_id = data.enterprise_id; 
               this.enterpriseChanged();
               this.operation = data.operation_id; 

               this.category_id = data.category_id;
               this.actualizaTipos();
               this.type = data.type_id;
               
               this.brand = data.brand_id;
               this.actualizaModelos();
               this.model = data.model_id;
            });
        }
    },
    
    methods:{
        onSubmit: function($event){
          $event.preventDefault();
          
          this.$http.post($event.target.getAttribute('action'), new FormData($event.target), function($data){
              this.showAlert = true;
              this.message = $data.message;
              this.alertType = $data.status;
              this.errors = [];
              if($data.success){
                  if(typeof $data.redirect != 'undefined'){
                      window.location.href  = $data.redirect;
                  } else {
                        this.liters          = '';
                        this.productive_area = '';
                        window.scrollTo(0,0);
                  }
              }else{
                  this.errors = $data.errors;    
              }
          })
          .error(function($data) {
              this.errors = $data;
          });
        },
        
        actualizaTipos:function(){
            //Al seleccionar la familia, a travès de una llamada Ajax despliegue sólo
            //los tipos asociados a la familia previamente seleccionada.
            this.$http.get('/ajax/search-filters/types-by-family', {
                category_id: this.category
                }, function(data) {
                    this.types = data;
                }
            );
        },
        
        actualizaModelos:function(){
            this.$http.get('/ajax/search-filters/models-by-brand', {
                brand_id: this.brand
                }, function(data) {
                    this.models = data;
                }
            );
        },
        
        enterpriseChanged: function(){
            this.operation = '';
            this.equipment = '';
            
            this.$http.get('/ajax/search-filters/operations-by-enterprise', {
                enterprise_id: this.enterprise
                }, function(data){
                    this.operations = data;
                }
            );
            
            this.$http.get('/ajax/search-filters/equipment-by-enterprise', {
                enterprise_id: this.enterprise
                }, function(data) {
                    this.equipments = data;
                }
            );
        },
    }
};