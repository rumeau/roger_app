module.exports = {
    data: function() {
        return {
            operation: '',
            operations: [
                {
                    text: 'ad', value: 1
                }, {
                    text: 'vc', value: 2
                }
            ],
            date_from: '',
            date_to: '',
            
            filtering: false
        };
    },
    
    props: {
        url: '',
    },
    
    ready: function() {
        var that = this;
        
        $("#faenafilter").jsGrid({
            width: "100%",
            height: "430px",
            pageSize: 10,
        
            filtering: false,
            editing: false,
            sorting: true,
            paging: true,
            autoload: true,
            pageLoading: true,
            controller: {
                loadData: function(filter) {
                    return $.ajax({
                        type: "GET",
                        url: that.url,
                        data: filter
                    });
                }
            },
        
            fields: [
                { name: "id", type: "control", width: 35, cellRenderer: function(value, item) {
                    return '<td><a href="empresa/'+value+'/edit" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-pencil"></span></a></td>';
                } },
                { name: "id", type: "control", width: 35, cellRenderer: function(value, item) {
                    return '<td><a href="empresa/'+value+'/delete" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash"></span></a></td>';
                } },
                { name: "name", title: "Nombre", type: "text", width: 70 },
            ]
        });
    },
    
    methods: {
    }
};