module.exports = {
    data: function() {
            return {
                operation           : '',
                operations: [
                    {
                        text: 'ad', value: 1
                    }, {
                        text: 'vc', value: 2
                    }
                ],
                enterprise          : '',
                enterprises         : [],
                category            : '',
                type                : '',
                cc                  : '',
                patent              : '',
                equipment           : '',
                equipmentFilters    : '',
                brand               : '',
                model               : '',
                models              : [],
                condition           : '',
                active              : '',
                lease               : false,
                
                filtering: false
            };
    },
    
    props: {
        url: '',
    },

    ready: function() {
        // jsGrid trata 'this', como un objeto de jsGrid, por eso para mantener a vue dentro de jsGrid lo guardo
        // en 'that' de manera temporal.
        var that = this;
        
        // Carga la grilla con los datos
        $("#equipmentFilters").jsGrid({
            width: "100%",
            height: "430px",
            pageSize: 10,
            filtering: false,
            editing: false,
            sorting: true,
            paging: true,
            autoload: true,
            pageLoading: true,
            controller: {
                loadData: function(filter) {
                    if (that.filtering == true) {
                        filter.enterprise   = that.enterprise;
                        filter.operation    = that.operation;
                        filter.cc           = that.cc;
                        filter.equipment    = that.equipment;
                        filter.patent       = that.patent;
                        filter.category     = that.category;
                        filter.type         = that.type;
                        filter.condition    = that.condition;
                        filter.active       = that.active;
                        filter.brand        = that.brand;
                        filter.model        = that.model;
                        filter.lease        = that.lease;
                    }
                    
                    return $.ajax({
                        type: "GET",
                        url: that.url,
                        data: filter
                    });
                }
            },
            
            fields: [
                { name: "id", type: "control", width: 20, cellRenderer: function(value, item) {
                    return '<td><a href="equipo/'+value+'/edit" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-pencil"></span></a></td>';
                } },
                //LA OPCIÒN DE ELIMINAR DEBE QUEDAR PERFILADA PARA QUE SEA REALIZADA SOLO POR UN ADMINISTRADOR
                { name: "id", type: "control", width: 20, cellRenderer: function(value, item) {
                    return '<td><a href="loan/'+value+'/delete" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash"></span></a></td>';
                } },
                { name: "enterprise", title: "Empresa", type: "text", width:80, cellRenderer: function (value, item) {
                    return '<td>' + value.name + '</td>';
                } },
                { name: "operation", title: "Faena", type: "text", width:80, cellRenderer: function (value, item) {
                    return '<td>' + value.name + '</td>';
                } },
                { name: "cc", title: "CC", type: "text", width:30 },
                { name: "patent", title: "Patente", type: "text", width:45, cellRenderer: function(value, item) {
                    return '<td><code>' + value + '</code></td>'; 
                } },
                { name: "category", title: "Familia", type: "text", width:55, cellRenderer: function(value, item) {
                    return '<td>' + value.name + '</td>';
                } },
                { name: "type", title: "Tipo", type: "text", width:45, cellRenderer: function(value, item) {
                    return '<td>' + value.name + '</td>'; 
                } },
                { name: "brand", title: "Marca", type: "text", width:45, cellRenderer: function(value, item) {
                    return '<td>' + value.name + '</td>'; 
                } },
                { name: "model", title: "Modelo", type: "text", width:70, cellRenderer: function(value, item) {
                    return '<td>' + value.name + '</td>'; 
                } },
                { name: "status", title: "Condición", type: "text", width:30, cellRenderer: function(value, item) {
                    return '<td>' + value.name + '</td>'; 
                } },
            ]
        });
    },

    methods: {
        filterSearch: function($event) {
            $event.preventDefault();
            
            this.filtering = true;
            
            $('#equipmentFilters').jsGrid('search', {
                enterprise  : this.enterprise,
                operation   : this.operation,
                category    : this.category,
                type        : this.type,
                cc          : this.cc,
                equipment   : this.equipment,
                active      : this.active,
                brand       : this.brand,
                model       : this.model,
                condition   : this.condition,
                patent      : this.patent,
                lease       : this.lease,
            });
        },
        
        clearFilters: function($event) {
            $event.preventDefault();
            
            this.filtering      = true;
            this.enterprise     = '';
            this.operation      = '';
            this.category       = '';
            this.type           = '';
            this.cc             = '';
            this.equipment      = '';
            this.brand          = '';
            this.model          = '';
            this.condition      = '';
            this.patent         = '';
            this.lease          = '';
            this.active         = '';
            
            $('#equipmentFilters').jsGrid("search");
        },
        
        enterpriseChanged: function() {
            // resetea el form
            this.operation  ='';
            this.operations = [];
            
            // Carga los campos dependientes
            if (this.enterprise != '') {
                //actualizar faenas
                this.$http.get(
                    'ajax/search-filters/operations-by-enterprise', {
                        enterprise_id: this.enterprise
                    },
                    function(data){
                        this.operations = data;
                    }
                );
            }
        },
        
        brandChanged: function() {
            this.model  ='';
            this.models = [];
            
            // Carga los campos dependientes
            if (this.brand != '') {
                //actualizar faenas
                this.$http.get(
                    'ajax/search-filters/models-by-brand', {
                        brand_id: this.brand
                    },
                    function(data){
                        this.models = data;
                    }
                );
            }
        },
        
        export: function($event) {
            $event.preventDefault();
            var form = $($event.target).closest('form');
            
            var data = {
                'enterprise'            : this.enterprise,
                'operation'             : this.operation,
                'cc'                    : this.cc,
                'patent'                : this.patent,
                'typeidentification'    : this.typeidentification,
                'category'              : this.category,
                'type'                  : this.type,
                'brand'                 : this.brand,
                'model'                 : this.model,
                'serial_number'         : this.serial_number,
                'engine'                : this.engine,
                'year'                  : this.year,
                'lease'                 : this.lease,
                'owner'                 : this.owner,
                'active'                : this.active
            };
            
            window.location.href = '/equipo?export=1&pageSize=1000&' + $.param(data);
        },
    }
};