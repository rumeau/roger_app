module.exports={
    data: function(){
        return{
            show: false,
            
            operation           : '',
            operations          : [],
            work_shift          : '',
            work_shifts         : [],
            dispenser           : '',
            dispensers          : [],
            ubication           : '',
            ubications          : [],
            dispenser_intern    : '',
            dispenser_interns   : [],
            responsible         : '',
            responsibles        : [],
            enterprise          : '',
            enterprises         : [],
            order_number        : '',
            external            : '',
            externals           : [],
            init_operation      : '',
            init_operations     : [],
            dispenser_destiny   : '',
            dispenser_destinys  : [],
            operator            : '',
            operators           : [],
            
            showAlert           : false,
            message             : '',
            alertType           : 'success',
            errors              : {},
        };    
    },
    
    props: ['edit'],
           // chargeables:''
    //},
    
    methods:{
        onSubmit: function($event){
          $event.preventDefault();
          
          this.$http.post($event.target.getAttribute('action'), new FormData($event.target), function($data){
              this.showAlert    = true;
              this.message      = $data.message;
              this.alertType    = $data.status;
              this.errors       = [];
              if($data.success){
                  if(typeof $data.redirect != 'undefined'){
                      window.location.href  = $data.redirect;
                  } else {
                        window.scrollTo(0,0);
                  }
              }else{
                  this.errors = $data.errors;    
              }
          })
          .error(function($data) {
              this.errors = $data;
          });
        },
        
        operationChangedInternLoan: function(){
            this.dispenser_intern  = '';
            this.dispenser_interns = [];
            this.$http.get(
                '/ajax/search-filters/operations-by-enterprise-for-interloan', {
                    init_operation: this.init_operation
                },
                function(data){
                    this.dispenser_interns = data;
                }
            );
        },
        
        operationChangedInternLoanDestiny: function(){
            this.dispenser_destiny  = '';
            this.dispenser_destinys = [];
            this.operator           = '';
            this.operators          = [];
            
            this.$http.get(
                '/ajax/search-filters/operations-by-enterprise-for-interloan-destiny', {
                    operation: this.operation
                },
                function(data) {
                    this.dispenser_destinys = data;    
                }
            );
            this.$http.get(
                    '/ajax/search-filters/operators-by-operation', {
                        operation_id: this.operation
                    },
                    function(data){
                        this.operators = data;
                    }
            );
        },
        
        enterpriseChanged: function(){
          this.operation    = '';
          this.operations   = [];
          this.$http.get(
                '/ajax/search-filters/operations-by-enterprise-loan', {
                    enterprise_id: this.enterprise
                },
                function(data){
                    this.operations = data;
                }
            );
        },
        
        /*
        operationChanged: function(){
            this.work_shift  = '';
            this.work_shifts = [];
            this.dispenser   = '';
            this.dispensers  = [];
            this.ubication   = '';
            this.ubications  = [];
            this.external    = '';
            this.externals   = [];
            this.responsible = '';
            this.responsibles = [];

            if (this.operation != '') {
                //actualizar turnos
                this.$http.get(
                    '/ajax/search-filters/work-shift-by-operation', {
                        operation_id: this.operation
                    },
                    function(data){
                        this.work_shifts = data;
                    }
                );
                this.$http.get(
                    '/ajax/search-filters/dispenser-by-operation-tct', {
                        operation_id: this.operation
                    },
                    function(data){
                        this.dispensers = data;
                    }
                );
                this.$http.get(
                    '/ajax/search-filters/operators-by-operation', {
                        operation_id: this.operation
                    },
                    function(data){
                        this.operators = data;
                    }
                );
                this.$http.get(
                    '/ajax/search-filters/locations-by-operation', {
                        operation_id: this.operation
                    },
                    function(data){
                        this.ubications = data;
                    }
                );
                this.$http.get(
                    '/ajax/search-filters/external-by-operation', {
                        operation_id: this.operation  
                    },
                    function(data) {
                        this.externals = data;
                    }
                );
            }
        },*/
        
        
    },
};