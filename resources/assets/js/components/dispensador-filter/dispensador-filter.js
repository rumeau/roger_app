module.exports = {
    data: function() {
        return {
            operation: '',
            operations: [],
            dispenser: '',
            dispensers: [],
            enterprise: '',
            
            filtering: false
        };
    },
    
    //props
    props: {
        //la url a la que hacer la llamada ajax para cargar la grilla
        url: '',
    },
    
    ready: function() {
        // jsGrid trata 'this', como un objeto de jsGrid, por eso para mantener a vue dentro de jsGrid lo guardo
        // en 'that' de manera temporal.
        var that = this;
        
        // Carga la grilla con los datos
        $("#newdispensadorfilters").jsGrid({
            width: "100%",
            height: "430px",
            pageSize: 10,

            filtering: false,
            editing: false,
            sorting: true,
            paging: true,
            autoload: true,
            pageLoading: true,
            controller: {
                loadData: function(filter) {
                    return $.ajax({
                        type: "GET",
                        url: that.url,
                        data: filter
                    });
                }
            },

            fields: [
                { name: "id", type: "control", width: 10, cellRenderer: function(value, item) {
                    return '<td><a href="compra/'+value+'/edit" class="btn btn-sm btn-default" data-toggle="tooltip" data-title="Editar"><span class="glyphicon glyphicon-pencil"></span></a></td>';
                } },
                { name: "id", type: "control", width: 10, cellRenderer: function(value, item) {
                    return '<td><a href="compra/'+value+'/delete" class="btn btn-default btn-sm" data-toggle="tooltip" data-title="Eliminar"><span class="glyphicon glyphicon-trash"></span></a></td>';
                } },
                { name: "enterprise", title: "Empresa", type: "text", width:80, cellRenderer: function(value, item){
                    return '<td>' + value.name + '</td>';
                } },
                { name: "name", title: "Nombre", type: "text", width:80 },
                { name: "capacity", title: "Capacidad (lts.)", type: "text", width:80 },
                { name: "current_balance", title: "Saldo actual (lts.)", type: "text", width:80 },
                //{ name: "operation", title: "Faena", type: "text", width:80, cellRenderer: function(value, item){
                //    return '<td>' + value.name + '</td>';
                //} },
            ]
        });
    },
    
    methods: {
        // Al filtrar
        filterSearch: function($event) {
            $event.preventDefault();
            
            this.filtering = true;
            
            $('#newdispensadorfilters').jsGrid('search', {
                operation: this.operation,
                enterprise: this.enterprise,
                dispenser: this.dispenser
            });
        },
        
        // Limpiar filtros
        clearFilters: function($event) {
            $event.preventDefault();
            
            this.filtering  = true;
            this.operation  = '';
            this.dispenser  = '';
            this.enterprise = '';
            
            $('#newdispensadorfilters').jsGrid("search");
        },
        
        // Cuando se selecciona una epresa
        enterpriseChanged: function() {
            // resetea el form
            this.operation  ='';
            this.operations = [];
            this.dispenser  = '';
            this.dispensers = [];
            
            // Carga los campos dependientes
            if (this.enterprise != '') {
                //actualizar faenas
                this.$http.get(
                    'ajax/search-filters/operations-by-enterprise', {
                        enterprise_id: this.enterprise
                    },
                    function(data){
                        this.operations = data;
                    }
                );
                
                //llamada Ajax para cargar los dispensadores asociados a la empresa seleccionada
                //ajax/search-filters/dispensers-by-enterprise. Se debe definir también en el routes.php
                //ajax/search-filters/dispensers-by-enterprise. Corresponde a una ruta, dentro del directorio del sitio.
                //"dispensers", es un método que debe estar definido en el modelo (en este caso en Empresa).
                this.$http.get(
                    'ajax/search-filters/dispensers-by-enterprise', {
                        enterprise_id: this.enterprise
                    },
                    function(data) {
                        this.dispensers = data;
                    }
                );
            }
        }
    }
};
