module.exports = {
    data: function() {
        return {
            filtering       : false,

            jsgrid: null
        };
    },

    props: {
        url: ''
    },

    ready: function() {
        var that = this;

        this.jsgrid = $("#personasFilter");

        this.jsgrid.jsGrid({
            width: "100%",
            height: "430px",
            pageSize: 10,

            filtering: false,
            editing: false,
            sorting: true,
            paging: true,
            autoload: true,
            pageLoading: true,
            controller: {
                loadData: function(filter) {
                    if (that.filtering == true) {
                        filter.name         = that.name;
                        filter.rut          = that.rut;
                    }

                    return $.ajax({
                        type: "GET",
                        url: that.url,
                        data: filter
                    });
                }
            },

            fields: [
                //EL BOTÓN MODIFICAR, DEBE ESTAR HABILITADO PARA LA PERSONA QUE ACTUALIZA ALGUNOS DATOS, POSTERIRO A LA RECEPCIÓN EN FAENA
                { name: "id", type: "control", width: 30, cellRenderer: function(value, item) {
                    return '<td><a href="persona/' + value + '/edit" class="btn btn-sm btn-default" data-toggle="tooltip" data-title="Editar"><span class="glyphicon glyphicon-pencil"></span></a></td>';
                } },
                { name: "id", type: "control", width: 30, cellRenderer: function(value, item) {
                    return '<td><a href="persona/' + value + '/delete" class="btn btn-default btn-sm" data-toggle="tooltip" data-title="Eliminar"><span class="glyphicon glyphicon-trash"></span></a></td>';
                } },
                { name: "name", title: "Nombre", type: "text", width:50 },
                { name: "rut", title: "RUT", type: "rut", width: 50 }
            ]
        });
    },

    methods: {
        filterSearch: function($event) {
            $event.preventDefault();

            this.filtering = true;

            this.jsgrid.jsGrid('search', this.filterObject());
        },

        clearFilters: function($event) {
            $event.preventDefault();

            this.filtering    = true;
            this.name = '';
            this.rut = '';

            this.jsgrid.jsGrid("search");
        },

        exportExcel: function () {
            window.location.href = '/compra?export=1&' + $.param(this.filterObject());
        },

        filterObject: function () {
            return {
                name: this.name,
                rut: this.rut
            };
        }
    }
};
