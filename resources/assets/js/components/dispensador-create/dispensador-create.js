module.exports={
    data: function(){
        return{
            show: false,
            
            operation   : '',
            equipment   : '',
            enterprise  : '',
            operations  : [],
            equipments  : [],
            responsible : ''
        };    
    },
    
    ready: function(){
        this.operationChanged();
    },
    
    methods:{
        onSubmit: function($event){
          $event.preventDefault();
          
          this.$http.post($event.target.getAttribute('action'), new FormData($event.target), function($data){
              this.showAlert = true;
              this.message = $data.message;
              this.alertType = $data.status;
              this.errors = [];
              if($data.success){
                  if(typeof $data.redirect != 'undefined'){
                      window.location.href  = $data.redirect;
                  } else {
                        this.ubication = '';
                        this.equipment = '';
                        this.counter = '';
                        this.hour_meter = '';
                        this.liters = '';
                        window.scrollTo(0,0);
                  }
              }else{
                  this.errors = $data.errors;    
              }
          })
          .error(function($data) {
              this.errors = $data;
          });
        },
        
        muestraCamion: function($event){
            console.log($event.target);
            console.log($event.target.checked);
            if($event.target.checked){
                this.show = true;
            }else{
                this.show = false;
            }
            
        },
        
        enterpriseChanged: function(){
            this.operation = '';
            this.equipment = '';
            
            this.$http.get('/ajax/search-filters/operations-by-enterprise', {
                enterprise_id: this.enterprise
                }, function(data){
                    this.operations = data;
                }
            );
            
            this.$http.get('/ajax/search-filters/equipment-by-enterprise', {
                enterprise_id: this.enterprise
            }, function(data) {
                this.equipments = data;
            }
            );
        }
    }
};