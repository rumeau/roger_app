module.exports={
    data: function(){
        return{
            operation: '',
            equipment: '',
            enterprise: '',
            operations: [],
            equipments: [],
        };    
    },
    
    methods:{
        
        actualizaTipos:function(){
            //Al seleccionar la familia, a travès de una llamada Ajax despliegue sólo
            //los tipos asociados a la familia previamente seleccionada.
            this.$http.get('/ajax/search-filters/types-by-family', {
                category_id: this.category
                }, function(data) {
                    this.types = data;
                }
            );
        },
    }
};