var moment = require('moment');

module.exports = {
    http: {
        headers: {
            'X-CSRF-TOKEN': document.querySelector('#metatoken').getAttribute('content')
        }
    },

    data: function() {
        return{
            show: false,

            data: {
                operation_id       : '',
                work_shift_id      : '',
                loaded_origin_id      : '',
                loaded_destination_id : '',
                operator_id        : '',
                ubication_id       : '',
                responsible_id     : '',
                order_number    : '',
                counter         : '',
                liters          : '',
                hour_meter      : '',
                //productive_areas: [],
                //productive_area : '',
                last_hour_meter : '',
                load_date: '',
                task: 'save_n_new'
            },

            operations      : [],
            work_shifts     : [],
            dispensers      : [],
            equipments      : [],
            operators       : [],
            ubications      : [],
            responsibles    : [],

            showAlert       : false,
            message         : '',
            alertType       : 'success',
            errors          : {},

            isEdit: false
        };
    },

    props: ['edit'],

    ready: function() {
        if (this.edit != '' && typeof this.edit != 'undefined'){
            this.isEdit = true;

            this.$http.get(this.edit)
                .then(function (response){
                    this.data.operation_id           = response.data.operation_id;
                    this.operationChanged();
                    this.$nextTick(function () {
                        this.data.load_date              = moment(response.data.load_date).format('DD/MM/YYYY');
                        this.data.responsible_id         = response.data.responsible_id;
                        this.data.work_shift_id          = response.data.work_shift_id;
                        //this.productive_area            = data.productive_area_id;
                        this.data.ubication_id           = response.data.ubication_id;
                        this.data.loaded_origin_id       = response.data.loaded_origin_id;
                        this.data.loaded_destination_id  = response.data.loaded_destination_id;
                        this.data.operator_id            = response.data.operator_id;
                        this.data.liters                 = response.data.liters;
                        this.data.counter                = response.data.counter;
                        this.data.hour_meter             = response.data.hour_meter;
                        this.data.responsible_id         = response.data.responsible_id;
                        this.data.order_number           = response.data.order_number;
                        this.lastHourMeter();
                    });
                });
        }
    },

    methods:{
        onSubmit: function($event){
            this.$http({
                url: $event.target.getAttribute('action'),
                method: this.isEdit ? 'PUT' : 'POST',
                data: this.data
            })
                .then(function( response) {
                    this.showAlert = true;
                    this.message = response.data.message;
                    this.alertType = response.data.status;
                    this.errors = [];
                    if (response.data.success) {
                        if(typeof response.data.redirect != 'undefined') {
                            window.location.href  = response.data.redirect;
                        } else {
                            //this.data.operation_id = '';
                            //this.data.load_date = '';
                            //this.data.responsible_id = '';
                            //this.data.work_shift_id = '';
                            this.data.ubication_id       = '';
                            //this.data.loaded_origin_id = '';
                            //this.data.loaded_destination_id = '';
                            this.data.order_number = '';
                            this.data.operator_id = '';
                            this.data.counter = '';
                            this.data.hour_meter = '';
                            this.data.last_hour_meter = '';
                            this.lastHourMeter();
                            this.data.liters = '';

                            window.scrollTo(0,0);
                        }
                    } else {
                        this.errors = response.data.errors;
                    }
                }, function(response) {
                    this.errors = response.data;
                });
        },
        /*
        muestraCamion: function($event){
            console.log($event.target);
            console.log($event.target.checked);
            if($event.target.checked){
                this.show = true;
            }else{
                this.show = false;
            }

        },*/

        operationChanged: function(){
            //this.data.responsible_id = '';
            this.data.work_shift_id  = '';
            this.data.loaded_origin_id = '';
            this.data.loaded_destination_id   = '';
            this.data.operator_id    = '';
            this.data.ubication_id   = '';

            //this.responsibles= [];
            this.work_shifts = [];
            this.dispensers  = [];
            this.equipments  = [];
            this.operators   = [];
            this.ubications  = [];
            //this.productive_area = '';
            //this.productive_areas = [];
            if (this.data.operation_id != '') {
                //actualizar turnos
                this.$http.get('/ajax/search-filters/work-shift-by-operation?operation_id=' + this.data.operation_id)
                    .then(function (response){
                        this.work_shifts = response.data;
                    });

                this.$http.get('/ajax/search-filters/dispenser-by-operation-tct?operation_id=' + this.data.operation_id)
                    .then(function (response) {
                        this.dispensers = response.data;
                    });

                this.$http.get('/ajax/search-filters/equipment-by-operation?operation_id=' + this.data.operation_id)
                    .then(function (response) {
                        this.equipments = response.data;
                    });

                this.$http.get('/ajax/search-filters/operators-by-operation?operation_id=' + this.data.operation_id)
                    .then(function (response) {
                        this.operators = response.data;
                    });

                this.$http.get('/ajax/search-filters/locations-by-operation?operation_id=' + this.data.operation_id)
                    .then(function (response) {
                        this.ubications = response.data;
                    });

                /*
                this.$http.get(
                    '/ajax/search-filters/productiveAreas-by-operation', {
                        operation_id: this.operation
                    },
                    function(data){
                        this.productive_areas = data;
                    }
                );
                this.$http.get(
                    '/ajax/search-filters/responsible-by-enterprise', {
                        enterprise_id: this.enterprise
                    },
                    function(data){
                        this.responsibles = data;
                    }
                );*/
            }
        },

        lastHourMeter: function(){
            this.data.last_hour_meter    = '';
            this.$http.get('/ajax/search-filters/last-hour-meter-by-equipment?loaded_destination_id=' + this.data.loaded_destination_id)
                .then(function(response) {
                    this.data.last_hour_meter = response.data.hour_meter;
                });
        }
    }
};
