import Vue from 'vue'
import VueResource from 'vue-resource'
import swal from 'sweetalert'
import $ from 'jquery'
import 'jquery-ui'

import 'bootstrap'
import 'bootstrap-select'
import 'eonasdan-bootstrap-datetimepicker'
import 'jsgrid/dist/jsgrid.js'

import Datepicker from './directives/datepicker.js'
import vSelect from 'vue-select'
import CompraFilter from './components/compra/filter.vue'
import CompraForm from './components/compra/form.vue'
import DispensadorFilter from './components/carga/filter.vue'
import DispensadorForm from './components/carga/form.vue'

import PrestamoFilter from './components/loan/filter.vue'
import PrestamoForm from './components/loan/form.vue'

import WorkShiftFilter from './components/workShift/filter.vue'
import WorkShiftForm from './components/workShift/form.vue'

import ReportEquipment from './components/report/equipment.vue'

let RutField = function (config) {
    jsGrid.Field.call(this, config);
};
RutField.prototype = new jsGrid.Field({
  css: "rut-field",            // redefine general property 'css'
  align: "right",              // redefine general property 'align'

  itemTemplate: function (value) {
    while (/(\d+)(\d{3})/.test(value.toString())) {
        value = value.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
    }

    return value;
  }
});

jsGrid.fields.rut = RutField;

Vue.use(VueResource);
Vue.config.debug = true;

Vue.directive('datepicker', Datepicker)
Vue.component('v-select', vSelect);
Vue.component('vue-datetime-picker', require("vue-datetime-picker/src/vue-datetime-picker"));

Vue.http.interceptors.push(function () {
    return {
        request: function (request) {
            return request;
        },

        response: function (response) {
            if (response.status == 403) {
                swal('Error', 'No tiene autorización para realizar esta operación', 'error');
            } else if (response.status == 404) {
                swal('Error', 'No se ha encontrado el recurso solicitado', 'error');
            } else if (response.status == 400) {
                if (response.data.title != undefined) {
                    swal(response.data.title, response.data.message, 'error');
                }
            }

            return response;
        }

    };
});

new Vue({
    el: '#app',

    data: {
        // Valores de cada select (dropdown)
        empresa: '',
        faena: '',
        proveedor: '',
        tipo: '',
        marca: '',
        modelo: '',
        dispensador: '',
        familia: '',
        turno: '',
        operador: '',
        concepto: '',
        equipo: '',
        externo: '',
        ubicacion: '',

        // Opciones de cada dropdown
        // empresas: [], esto no se define por que las empresas ya estan cargadas en la vista.

        // estas opciones se cargaran con ajax, pero en un inicio estan vacias
        tipos: [],
        modelos: [],
        faenas: [],
        proveedores: [],
        dispensadores: [],
        turnos: [],
        operadores: [],
        equipos: [],
        externos: [],
        ubicaciones: [],
        dispensador_dest: '',
        dispensador_orig: '',

        muestraDiv: true
    },

    props: {
        empresaSeleccionada: null
    },

    components: {
        CompraFilter,
        CompraForm,
        DispensadorFilter,
        DispensadorForm,
        'newdispensadorfilters': require('./components/dispensador-filter/dispensador-filter'),
        'dispensadorform': require('./components/dispensador-create/dispensador-create'),
        'equipmentfilters': require('./components/equipo-filter/equipo-filter'),
        'equipmentform': require('./components/equipo-create/equipo-create'),
        'consumofilter': require('./components/consumo-filter/consumo-filter'),
        'consumoform': require('./components/consumo-create/consumo-create'),

        PrestamoFilter,
        PrestamoForm,

        WorkShiftFilter,
        WorkShiftForm,
        'loanformintern': require('./components/loanintern-create/loanintern-create'),
        'empresafilter': require('./components/empresa-filter/empresa-filter'),
        'empresaform': require('./components/empresa-create/empresa-create'),
        'faenafilter': require('./components/faena-filter/faena-filter'),
        'faenaform': require('./components/faena-create/faena-create'),
        'initdispensadorfilter': require('./components/initdispensador-filter/initdispensador-filter'),
        'catalogosfilter': require('./components/catalogo-filter/catalogo-filter'),
        'catalogosform': require('./components/catalogo-create/catalogo-create'),
        'otform': require('./components/ordentrabajo-create/ordentrabajo-create'),
        'otfilter': require('./components/ordentrabajo-filter/ordentrabajo-filter'),
        'personaform': require('./components/persona/create'),
        'personafilter': require('./components/persona/filter'),

        "vue-datetime-picker": require("vue-datetime-picker/src/vue-datetime-picker"),

        ReportEquipment
    },

    ready: function () {
        $('.selectpicker').selectpicker();

        $('body').tooltip({
            selector: '[data-toggle="tooltip"]',
            container: 'body'
        });

        $(document).on('keypress', '.onlynumbers', function (event) {
            if (event.charCode >= 48 && event.charCode <= 57) {
                return true;
            }

            return !!(event.keyCode == 46 || event.keyCode == 44);
        });

        if (this.empresaSeleccionada != null) {
            this.empresa = this.empresaSeleccionada;
            this.actualizaDropdownAlSeleccionarEmpresa();
        }
    },
});
