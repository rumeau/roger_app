@extends('layout')
 @section('content')
<div class="page-header">
    <h1>Combustible <small>Registro de cargas a dispensadores</small></h1>
</div>

<form action="{{ route('fueldispenser.store') }}" method="post" class="form-horizontal">
    {!! csrf_field() !!}
    
    <div class="form-group">
        <label class="control-label col-md-2">Faena</label>
        <div class="col-md-6">
            {!! Form::select('operation_id', [], Null, [
                'class'=>'form-control',
                'placeholder' => 'Seleccione Faena',
                'options'=>'faenas',
                'v-on' => 'change: actualizaDropdownAlSeleccionarFaena',
                'v-model' => 'faena']) !!}
        </div>
    </div>
</form>
@endsection