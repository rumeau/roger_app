@extends('layout')
@section('content')

<div class="page-header">
    <h1>Ingreso de equipo <small>Editar</small></h1>
</div>

<equipmentform inline-template edit="{{ route('equipo.edit', ['equipos' => $equipos->id]) }}">
    {!! Form::model($equipos, [
        'v-on'    => 'submit: onSubmit',
        'route'   => ['equipo.update', 'equipos' => $equipos->id],
        'class'   => 'form-horizontal',
        'method'  => 'PUT' ]) !!}
        
        @include('equipos.form')
        
    {!! Form::close() !!}
</equipmentform>
@endsection