    <equipmentfilters inline-template data-url="{{ route('equipo.index') }}">
        <form action="" class="form-horizontal">
        <fieldset>
            <legend>Filtros de Búsqueda</legend>
            
            <div class="pull-right">
                <a class="btn btn-primary" href="{{route('equipo.create')}}">+ Nuevo</a>
            </div>
            
            <div class="form-group">
                <label class="control-label col-md-2">Empresa</label>
                <div class="col-xs-2">
                    {!! Form::select('enterprise_id', $selectEmpresas, Null, [
                        'class'        =>'form-control',
                        'placeholder'  => 'Seleccione Empresa',
                        'v-on'         => 'change: enterpriseChanged',
                        'v-model'      => 'enterprise']) !!}
                </div>
                <label class="control-label col-md-2">Faena</label>
                <div class="col-xs-2">
                    {!! Form::select('operation_id', $selectFaenas, Null, [
                        'class'        => 'form-control',
                        'placeholder'  => 'Seleccione Faena',
                        'v-model'      => 'operation' ]) !!}
                </div>
                <div class="col-md-2">
                    <div class="checkbox checkbox-primary">
                        {!! Form::checkbox('active', null, null, [
                            'class'          => 'checkbox', 
                            'v-model'        => 'active' ]) !!}
                            <label><strong>Equipos inactivos</strong></label>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-md-2">Centro de costo</label>
                <div class="col-md-2">
                    {!! Form::text('cc', null, [
                        'class'        => 'form-control', 
                        'v-model'      => 'cc' ]) !!}
                </div>
                <label class="control-label col-md-2">Equipo</label>
                <div class="col-md-2">
                    {!! Form::select('equipment', $selectEquipos, null, [
                        'class'          => 'form-control',
                        'placeholder'    => 'Seleccione Equipo', 
                        'v-model'        => 'equipment' ]) !!}
                </div>
                <label class="control-label col-md-2" width="150">Patente</label>
                <div class="col-md-2">
                    {!! Form::text('patent', null, [
                        'class'        => 'form-control', 
                        'v-model'      => 'patent' ]) !!}
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-md-2">Familia</label>
                <div class="col-xs-2">
                    {!! Form::select('category_id', $selectFamilias, Null, [
                        'class'       =>'form-control',
                        'placeholder' => 'Seleccione Familia',
                        'v-model'     => 'category' ]) !!}
                </div>
                <label class="control-label col-md-2">Tipo</label>
                <div class="col-xs-2">
                    {!! Form::select('type_id', $selectTipos, Null, [
                        'class'        => 'form-control',
                        'placeholder'  => 'Seleccione Tipo',
                        'v-model'      => 'type']) !!}
                </div>
                <label class="control-label col-md-2">Condición</label>
                <div class="col-xs-2">
                    {!! Form::select('status_id', $selectStatus, Null, [
                        'class'        => 'form-control',
                        'placeholder'  => 'Seleccione condición',
                        'v-model'      => 'condition']) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">Marca</label>
                <div class="col-xs-2">
                    {!! Form::select('brand_id', $selectMarcas, Null, [
                        'class'       => 'form-control',
                        'placeholder' => 'Seleccione marca',
                        'v-on'        => 'change: brandChanged',
                        'v-model'     => 'brand' ]) !!}
                </div>
                <label class="control-label col-md-2">Modelo</label>
                <div class="col-xs-2">
                    {!! Form::select('model_id', [], Null, [
                        'class'        => 'form-control',
                        'placeholder'  => 'Seleccione modelo',
                        'v-model'      => 'model',
                        'options'      => 'models' ]) !!}
                </div>
                <div class="col-md-2">
                    <div class="checkbox checkbox-primary">
                        {!! Form::checkbox('lease', 1, false, [
                            'class'          => 'checkbox',
                            'v-model'        => 'lease' ]) !!}
                            <label><strong>Arriendo</strong></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="pull-right">
                    <a class="btn btn-primary" href="#" v-on="click: export">Excel</a>
                    <a class="btn btn-primary" href="#" v-on="click: clearFilters">Limpiar filtros</a>
                    <a class="btn btn-primary" href="#" v-on="click: filterSearch">Buscar</a>
                </div>
            </div>
        </fieldset>
        </form>
    </equipmentfilters>
            