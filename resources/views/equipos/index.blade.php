@extends('layout')
@section('content')

    <h1>Listado de equipos Serfocol</h1>
    
    @include('equipos.filters')
    
    <div id="equipmentFilters"></div>
    
@endsection