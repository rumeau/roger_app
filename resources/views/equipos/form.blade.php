
    <div class="form-group@{{ errors.enterprise_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Empresa</label>
        <div class="col-xs-4">
            {!! Form::select('enterprise_id', $selectEmpresas, Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione empresa',
                    'v-on'         => 'change: enterpriseChanged',
                    'v-model'      => 'enterprise' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.enterprise_id" v-text="errors.enterprise_id"></p>
    </div>
    <div class="form-group@{{ errors.operation_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Faena</label>
        <div class="col-xs-4">
            {!! Form::select('operation_id', [], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione faena',
                    'options'      => 'operations', 
                    'v-model'      => 'operation']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.operation_id" v-text="errors.operation_id"></p>
    </div>
    <div class="form-group@{{ errors.cc ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Centro de Costo</label>
        <div class="col-xs-2">
            {!! Form::text('cc', Null, [
                    'class'    => 'form-control']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.cc" v-text="errors.cc"></p>
    </div>
    <div class="form-group@{{ errors.patent ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Patente</label>
        <div class="col-xs-2">
            {!! Form::text('patent', Null, [
                    'class'    => 'form-control']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.patent" v-text="errors.patent"></p>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Tipo Identificación</label>
        <div class="col-xs-4">
            {!! Form::select('type_identification_id', $selectTipoIdentif, Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione Tipo Identificación',
                    'v-model'      => 'typeidentification' ]) !!}
        </div>
    </div>
    <div class="form-group@{{ errors.category_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Familia</label>
        <div class="col-xs-2">
            {!! Form::select('category_id', $selectFamilias, Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione Familia', 
                    'v-on'         => 'change: actualizaTipos', 
                    'v-model'      => 'category']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.category_id" v-text="errors.category_id"></p>
    </div>
    <div class="form-group@{{ errors.type_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Tipo</label>
        <div class="col-md-2">
            {!! Form::select('type_id', [], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione Tipo', 
                    'options'      => 'types',
                    'v-model'      => 'type']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.type_id" v-text="errors.type_id"></p>
    </div>
    <div class="form-group@{{ errors.brand_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Marca</label>
        <div class="col-xs-2">
            {!! Form::select('brand_id', $selectMarcas, Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione Marca', 
                    'v-on'         => 'change: actualizaModelos', 
                    'v-model'      => 'brand']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.brand_id" v-text="errors.brand_id"></p>
    </div>
    <div class="form-group@{{ errors.model_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Modelo</label>
        <div class="col-xs-2">
            {!! Form::select('model_id', [], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione Modelo', 
                    'options'      => 'models', 
                    'v-model'      => 'model' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.model_id" v-text="errors.model_id"></p>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Serie motor</label>
        <div class="col-xs-2">
            {!! Form::text('serial_number', Null, [
                    'class'    => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Serie chasis</label>
        <div class="col-xs-2">
            {!! Form::text('engine', Null, [
                    'class'    => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Año</label>
        <div class="col-xs-2">
            {!! Form::selectYear('year', '1960', '2050', '2015', [
                    'class'    => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2"></label>
        <div class="col-md-6">
            <div class="checkbox checkbox-primary">
                {!! Form::hidden('lease', 0, null) !!}
                {!! Form::checkbox('lease', 1, null, [
                        'class'    => 'form-control']) !!}
                <label><strong>¿Es arrendado?</strong></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Propietario</label>
        <div class="col-xs-4">
            {!! Form::select('owner_id', $selectPropietario, Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione propietario',
                    'v-model'      => 'owner' ]) !!}
        </div>
    </div>
     <div class="form-group">
        <label class="control-label col-md-2">Estatus</label>
        <div class="col-xs-2">
            {!! Form::select('active', ['1' => 'Activo', '0' => 'Inactivo'], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione estatus' ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Condición</label>
        <div class="col-xs-2">
            {!! Form::select('status_id', $selectStatus, Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione condición' ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Combustible</label>
        <div class="col-xs-4">
            {!! Form::select('fuel', config('serfocol.form.combustiblesSelect'), null, [
                    'class'        => 'form-control',
                    'placeholder'  => 'Seleccione combustible utilizado' ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2"></label>
        <div class="col-md-2">
            <div class="checkbox checkbox-primary">
                {!! Form::hidden('dispenser', 0, null) !!}
                {!! Form::checkbox('dispenser', 1, null, [
                        'class'    => 'form-control']) !!}
                <label><strong>¿El equipo, es dispensador de combustible?</strong></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Capacidad estanque (lts.)</label>
        <div class="col-xs-2">
            {!! Form::text('fuel_tank_capacity', Null, [
                    'class'    => 'form-control']) !!}
        </div>
    </div>
    
    <div class="form-group">
        <p class="col-md-offset-2 col-md-10">
            <a class="btn btn-default" href="{{ route('equipo.index') }}" >&larr; Cancelar</a>
            <button type="submit" class="btn btn-primary" value="save" name="task">Guardar y salir</button>
            <!--
            <button type="submit" class="btn btn-primary" value="save_n_new" name="task">Guardar y continuar</button>
            -->
        </p>
    </div>