@extends('layout')
@section('content')
<div class="page-header">
    <h1>Ingreso de equipo</small></h1>
</div>

<equipmentform inline-template>
    <div class="alert alert-@{{alertType}} alert-dismissible" role="alert" v-show="showAlert" v-html="message"></div>
        {!! Form::open(['v-on' => 'submit: onSubmit' ,'route' => 'equipo.store', 'class' => 'form-horizontal']) !!}
            @include('equipos.form')
        {!! Form::close() !!}
</equipmentform>
@endsection