@extends('layout')

@section('content')

    <div class="page-header">
        <h1>Sistema de control de combustible</h1>
    </div>

    <div class="col-md-4 col-md-offset-4">
        <div class="well">
            <form action="{{ url('/select-enterprise') }}" method="POST">
                {!! csrf_field() !!}
                
                <p>Seleccione una empresa para continuar</p>

                <div class="form-group">
                    {!! Form::select('enterprise', $enpterprises, null, [
                            'class'        => 'form-control', 
                            'placeholder'  => 'Seleccione una Empresa' ]) !!}
                </div>

                <div class="form-group">
                    <p class="text-center">
                        <button class="btn btn-primary" type="submit">Seleccionar</button>
                    </p>
                </div>
            </form>


        </div>
    </div>



@endsection