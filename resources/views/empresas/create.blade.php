@extends('layout')
 @section('content')
<div class="page-header">
    <h1>Empresas</h1>
</div>

{!! Form::open(['route' => 'empresa.store', 'class' => 'form-horizontal']) !!}

    @include('empresas.form')
    
{!! Form::close() !!}

 @endsection