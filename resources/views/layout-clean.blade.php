<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SGP ver.2</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="{{asset('css/app.css')}}">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<link rel="stylesheet" href="{{ asset('css/app.css') }}">

</head>

<body id='app'>
    
    <div class="container">@yield('content')</div>

<script type="text/javascript">
    var $;
    var jQuery;
</script>
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
</body>
</html>