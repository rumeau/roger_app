@extends('layout')
@section('content')

    <h1>Reportes de gestión</h1>

    @include('reportes.equipo.filters')

    @if ((bool) $detail)
        @include('reportes.equipo.detalle')
    @else
        @include('reportes.equipo.resumen')
    @endif

@endsection