<h1>Filtros del Reporte por Equipo @if ((bool) $detail)
        / <small> Detalle</small>
    @endif
</h1>
<report-equipment inline-template>
    <form action="" class="form-horizontal container-fluid" method="GET">
        <input type="hidden" name="detail" value="{{ (int) $detail }}">
        <div class="form-group">
            <label for="operation" class="control-label col-md-2">Fanea</label>
            <div class="col-md-2">
                {!! Form::select('operation', \App\Repositories\OperationRepository::forConsumption($enterprise)->lists('name', 'id'), $operationId, [
                    'class' => 'form-control',
                    'placeholder' => '-- Seleccione una faena --',
                    'v-model' => 'operation_id',
                    '@change' => 'updateEquipments',
                ]) !!}
            </div>

            <label for="equipment" class="control-label col-md-2">Equipo</label>
            <div class="col-md-2">
                <select name="equipment" class="col-md-2 form-control" v-model="equipment_id" v-el:equipments>
                    <option value="">-- Seleccione un equipo --</option>
                    @foreach ($equipments as $eq)
                        <option value="{{ $eq->id }}" data-operation="{{ $eq->operation_id }}" {{ $equipmentId == $eq->id ? ' selected' : '' }}>{{ $eq->patent }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="date" class="control-label col-md-2">Fecha desde</label>
            <div class="col-md-2">
                <div class='input-group date datepicker'>
                    {!! Form::text('from', $from, [ 'class' => 'form-control datepicker', 'v-datepicker']) !!}
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <label for="date" class="control-label col-md-2">Fecha hasta</label>
            <div class="col-md-2">
                <div class='input-group date datepicker'>
                    {!! Form::text('to', $to, [ 'class' => 'form-control datepicker', 'v-datepicker']) !!}
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group">
            @if ((bool) $detail)
                <a href="{{ action('ReportController@equipment', ['from' => $from, 'to' => $to, 'operation' => $operationId, 'equipment' => $equipmentId]) }}" class="btn btn-primary">Ver Resumen</a>
            @else
                <a href="{{ action('ReportController@equipment', ['from' => $from, 'to' => $to, 'detail' => true, 'operation' => $operationId, 'equipment' => $equipmentId]) }}" class="btn btn-primary">Ver Detalle</a>
            @endif
            <a href="{{ action('ReportController@index', ['from' => $from, 'to' => $to]) }}" class="btn btn-primary">Por Empresa</a>
                <a href="{{ action('ReportController@operation', ['from' => $from, 'to' => $to, 'operation' => $operationId]) }}" class="btn btn-primary">Por Faena</a>

            <div class="pull-right">
                <a class="btn btn-primary" href="{{ action('ReportController@equipment', ['from' => $from, 'to' => $to, 'operation' => $operationId, 'equipment' => $equipmentId, 'detail' => $detail, 'export' => 1]) }}">Excel</a>
                <button class="btn btn-primary" type="submit">Buscar</button>
            </div>
        </div>
    </form>
</report-equipment>