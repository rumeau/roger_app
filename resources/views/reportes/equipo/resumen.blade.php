@if ($equipment instanceof \App\Models\Equipo)
<h2>Resumen de todas las cargas del equipo {{ $equipment->patent }} entre las fechas {{ $from }} y {{ $to }}</h2>
@endif
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Faena</th>
        <th>PPU</th>
        <th>Patente</th>
        <th class="col-sm-3">Tipo</th>
        <th class="col-sm-2">Marca</th>
        <th class="col-sm-2">Modelo</th>
        <th class="text-right">Litros</th>
        <th class="text-right">Horas Trabajadas</th>
        <th class="text-right">Rendimiento</th>
    </tr>
    </thead>

    <tbody>
    @if ($paginator && $paginator->count())
        <?php $sumLiters = 0; ?>
        @foreach ($paginator as $eq)
            <?php $sumLiters += $eq->liters; ?>
            <tr>
                <td>{{ $eq->operation_name }}</td>
                <td>PPU</td>
                <td>{{ $eq->patent }}</td>
                <td>{{ $eq->type }}</td>
                <td>{{ $eq->brand }}</td>
                <td>{{ $eq->model }}</td>
                <td class="text-right">{{ number_format($eq->liters, 0, ',', '.') }}</td>
                <td class="text-right">{{ $eq->hour_meter }}</td>
                <td class="text-right">
                    @if (empty($eq->counter) || $eq->counter == 0)
                        {{ number_format($eq->hour_meter / $eq->liters, 2, ',', '.') }}
                    @else
                        {{ number_format($eq->counter / $eq->liters, 2, ',', '.') }}
                    @endif
                </td>
            </tr>
        @endforeach
        <tr>
            <th colspan="6">&nbsp;</th>
            <th class="text-right">Total Litros</th>
            <th colspan="2">&nbsp;</th>
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>
            <td class="text-right">{{ number_format($total, 0, ',', '.')  }}</td>
            <td colspan="2">&nbsp;</td>
        </tr>
    @else
        <tr>
            <td colspan="10">Por favor seleccione un rango de fechas y un equipo</td>
        </tr>
    @endif
    </tbody>

    <tfoot>
    @if ($paginator && $paginator->count())
        <tr>
            <td colspan="9" class="text-center">{!! $paginator->appends(['from' => $from, 'to' => $to, 'operation' => $operationId, 'equipment' => $equipmentId])->render() !!}</td>
        </tr>
    @endif
    </tfoot>

</table>