@extends('layout')
@section('content')
    
    <h1>Reportes de gestión</h1>
    
    @include('reportes.empresa.filters')

    @if ((bool) $detail)
        @include('reportes.empresa.detalle')
    @else
        @include('reportes.empresa.resumen')
    @endif

 @endsection