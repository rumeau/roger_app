<h1>Filtros del Reporte @if ((bool) $detail)
    / <small> Detalle</small>
    @endif
</h1>
<form action="" class="form-horizontal container-fluid" method="GET">
    <div class="form-group">
        <label for="date" class="control-label col-md-2">Fecha desde</label>
        <div class="col-md-2">
            <div class='input-group date datepicker'>
                {!! Form::text('from', $from, [ 'class' => 'form-control datepicker', 'v-datepicker']) !!}
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
        <label for="date" class="control-label col-md-2">Fecha hasta</label>
        <div class="col-md-2">
            <div class='input-group date datepicker'>
                {!! Form::text('to', $to, [ 'class' => 'form-control datepicker', 'v-datepicker']) !!}
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        @if ((bool) $detail)
            <a href="{{ action('ReportController@index', ['from' => $from, 'to' => $to]) }}" class="btn btn-primary">Ver Resumen</a>
        @else
            <a href="{{ action('ReportController@index', ['from' => $from, 'to' => $to, 'detail' => true]) }}" class="btn btn-primary">Ver Detalle</a>
        @endif
        <a href="{{ action('ReportController@operation') }}" class="btn btn-primary">Por Faena</a>
            <a href="{{ action('ReportController@equipment') }}" class="btn btn-primary">Por Equipo</a>

        <div class="pull-right">
            <a class="btn btn-primary" href="{{ action('ReportController@index', ['from' => $from, 'to' => $to, 'detail' => $detail, 'export' => 1]) }}">Excel</a>
            <button class="btn btn-primary" type="submit">Buscar</button>
        </div>
    </div>
</form>