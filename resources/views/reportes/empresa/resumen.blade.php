<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th class="col-sm-1">ID Faena</th>
        <th class="col-sm-5">Faena</th>
        <th class="col-sm-2 text-right">Litros</th>
        <th class="col-sm-2 text-right">% Porcentaje</th>
        <th class="col-sm-2">Valorizado</th>
    </tr>
    </thead>

    <tbody>
    @if ($paginator && $paginator->count())
        <?php $sumLiters = 0; ?>
        @foreach ($paginator as $operation)
            <?php $sumLiters += $operation->operation_liters; ?>
            <tr>
                <td>{{ $operation->operation_id }}</td>
                <td><a href="{{ action('ReportController@operation', [
                    'operation' => $operation->operation_id,
                    'from'      => $from,
                    'to'        => $to,
                    ]) }}">{{ $operation->operation_name }}</a></td>
                <td class="text-right">{{ number_format($operation->operation_liters, 0, ',','.')  }}</td>
                <td class="text-right">{{ number_format($operation->operation_percentage, 2, ',', '.') }}</td>
                <td>&nbsp;</td>
            </tr>
        @endforeach
        <tr>
            <th colspan="2">&nbsp;</th>
            <th class="text-right">Total Litros</th>
            <th class="text-right">Total %</th>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td class="text-right">{{ number_format($total, 0, ',', '.') }}</td>
            <td class="text-right">{{ $total == 0 ? '0' : number_format(($sumLiters*100)/$total, 2, ',', '.') }}</td>
            <td>&nbsp;</td>
        </tr>
    @else
        <tr>
            <td colspan="5">Por favor seleccione un rango de fechas</td>
        </tr>
    @endif
    </tbody>

    <tfoot>
    @if ($paginator && $paginator->count())
        <tr>
            <td colspan="5" class="text-center">{!! $paginator->appends(['from' => $from, 'to' => $to])->render() !!}</td>
        </tr>
    @endif
    </tfoot>

</table>