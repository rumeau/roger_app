<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th class="">ID</th>
        <th>Fecha</th>
        <th class="col-sm-2">Faena</th>
        <th class="col-sm-1">Maquina</th>
        <th class="col-sm-3">Operador</th>
        <th class="">Turno</th>
        <th class="col-sm-2">Dispensador</th>
        <th>Ubicación</th>
        <th class="text-right">Litros</th>
        <th class="text-right">Horómetro</th>
    </tr>
    </thead>

    <tbody>
    @if ($paginator && $paginator->count())
        <?php $sumLiters = 0; ?>
        @foreach ($paginator as $chargeable)
            <?php $sumLiters += $chargeable->liters; ?>
            <tr>
                <td>{{ $chargeable->id }}</td>
                <td>{{ $chargeable->load_date }}</td>
                <td>{{ $chargeable->operation_name }}</td>
                <td>{{ $chargeable->equipment_name }}</td>
                <td>{{ $chargeable->operator_name }}</td>
                <td>{{ $chargeable->work_shift }}</td>
                <td>{{ $chargeable->dispenser }}</td>
                <td>{{ $chargeable->ubication }}</td>
                <td class="text-right">{{ number_format($chargeable->liters, 0, ',', '.') }}</td>
                <td class="text-right">{{ number_format($chargeable->hour_meter, 0, ',', '.') }}</td>
            </tr>
        @endforeach
        <tr>
            <th colspan="8">&nbsp;</th>
            <th class="text-right">Total Litros</th>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <td colspan="8">&nbsp;</td>
            <td class="text-right">{{ $sumLiters }}</td>
            <td>&nbsp;</td>
        </tr>
    @else
        <tr>
            <td colspan="10">Por favor seleccione un rango de fechas</td>
        </tr>
    @endif
    </tbody>

    <tfoot>
    @if ($paginator && $paginator->count())
        <tr>
            <td colspan="10" class="text-center">{!! $paginator->appends(['from' => $from, 'to' => $to, 'detail' => $detail])->render() !!}</td>
        </tr>
    @endif
    </tfoot>

</table>