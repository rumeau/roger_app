@if ($operation instanceof \App\Models\Operation)
<h2>Resumen de todas las cargas de la faena {{ $operation->name }} entre las fechas {{ $from }} y {{ $to }}</h2>
@endif
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>ID Equipo</th>
        <th>PPU</th>
        <th>Patente</th>
        <th class="col-sm-3">Tipo</th>
        <th class="col-sm-2">Marca</th>
        <th class="col-sm-2">Modelo</th>
        <th class="text-right">Litros</th>
        <th class="text-right">Porcentaje</th>
        <th class="text-right">Horas Trabajadas</th>
        <th class="text-right">Rendimiento</th>
    </tr>
    </thead>

    <tbody>
    @if ($paginator && $paginator->count())
        <?php $sumLiters = 0; ?>
        @foreach ($paginator as $equipment)
            <?php $sumLiters += $equipment->liters; ?>
            <tr>
                <td>{{ $equipment->id }}</td>
                <td>PPU</td>
                <td><a href="{{ action('ReportController@equipment', ['from' => $from, 'to' => $to, 'operation' => $operationId, 'equipment' => $equipment->id]) }}">{{ $equipment->patent }}</a></td>
                <td>{{ $equipment->type }}</td>
                <td>{{ $equipment->brand }}</td>
                <td>{{ $equipment->model }}</td>
                <td class="text-right">{{ number_format($equipment->liters, 0, ',', '.') }}</td>
                <td class="text-right">{{ number_format($equipment->equipment_percentage, 2, ',', '.') }}%</td>
                <td class="text-right">{{ $equipment->hour_meter }}</td>
                <td class="text-right">
                    @if (empty($equipment->counter) || $equipment->counter == 0)
                        {{ number_format($equipment->hour_meter / $equipment->liters, 2, ',', '.') }}
                    @else
                        {{ number_format($equipment->counter / $equipment->liters, 2, ',', '.') }}
                    @endif
                </td>
            </tr>
        @endforeach
        <tr>
            <th colspan="6">&nbsp;</th>
            <th class="text-right">Total Litros</th>
            <th class="text-right">Total %</th>
            <th colspan="2">&nbsp;</th>
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>
            <td class="text-right">{{ number_format($total, 0, ',', '.')  }}</td>
            <td class="text-right">{{ number_format(($sumLiters*100)/$total, 2, ',', '.') }}</td>
            <td colspan="2">&nbsp;</td>
        </tr>
    @else
        <tr>
            <td colspan="10">Por favor seleccione un rango de fechas y una faena</td>
        </tr>
    @endif
    </tbody>

    <tfoot>
    @if ($paginator && $paginator->count())
        <tr>
            <td colspan="10" class="text-center">{!! $paginator->appends(['from' => $from, 'operation' => $operationId, 'to' => $to])->render() !!}</td>
        </tr>
    @endif
    </tfoot>

</table>