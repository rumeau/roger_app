<h1>Filtros del Reporte por Faena @if ((bool) $detail)
        / <small> Detalle</small>
    @endif
</h1>
<form action="" class="form-horizontal container-fluid" method="GET">
    <div class="form-group">
        <label for="operation" class="control-label col-md-2">Fanea</label>
        <div class="col-md-2">
            {!! Form::select('operation', \App\Repositories\OperationRepository::forConsumption($enterprise)->lists('name', 'id'), $operationId, [
                'class' => 'form-control',
                'placeholder' => '-- Seleccione una faena --',
            ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="date" class="control-label col-md-2">Fecha desde</label>
        <div class="col-md-2">
            <div class='input-group date datepicker'>
                {!! Form::text('from', $from, [ 'class' => 'form-control datepicker', 'v-datepicker']) !!}
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
        <label for="date" class="control-label col-md-2">Fecha hasta</label>
        <div class="col-md-2">
            <div class='input-group date datepicker'>
                {!! Form::text('to', $to, [ 'class' => 'form-control datepicker', 'v-datepicker']) !!}
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        @if ((bool) $detail)
            <a href="{{ action('ReportController@operation', ['from' => $from, 'to' => $to, 'operation' => $operationId]) }}" class="btn btn-primary">Ver Resumen</a>
        @else
            <a href="{{ action('ReportController@operation', ['from' => $from, 'to' => $to, 'detail' => true, 'operation' => $operationId]) }}" class="btn btn-primary">Ver Detalle</a>
        @endif
        <a href="{{ action('ReportController@index', ['from' => $from, 'to' => $to]) }}" class="btn btn-primary">Por Empresa</a>
            <a href="{{ action('ReportController@equipment', ['from' => $from, 'to' => $to, 'operation' => $operationId]) }}" class="btn btn-primary">Por Equipo</a>

        <div class="pull-right">
            <a class="btn btn-primary" href="{{ action('ReportController@operation', ['from' => $from, 'to' => $to, 'operation' => $operationId, 'detail' => $detail, 'export' => 1]) }}">Excel</a>
            <button class="btn btn-primary" type="submit">Buscar</button>
        </div>
    </div>
</form>