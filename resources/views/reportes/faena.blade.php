@extends('layout')
@section('content')

    <h1>Reportes de gestión</h1>

    @include('reportes.faena.filters')

    @if ((bool) $detail)
        @include('reportes.faena.detalle')
    @else
        @include('reportes.faena.resumen')
    @endif

@endsection