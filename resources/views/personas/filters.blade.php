<personafilter inline-template data-url="{{ route('persona.index') }}">
    <form action="" class="form-horizontal">
        <fieldset>
            <legend>Filtros de Búsqueda</legend>

            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('persona.create') }}"><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="javascript:;" @click="exportExcel">Excel</a>
                        <a class="btn btn-primary" href="javascript:;" @click="clearFilters">Limpiar filtros</a>
                        <a class="btn btn-primary" href="javascript:;" @click="filterSearch">Buscar</a>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
</personafilter>