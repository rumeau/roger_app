@extends('layout')

@section('content')

    <h1>Personas</h1>

    @include('personas.filters')

    <div id="personasFilter"></div>

@endsection