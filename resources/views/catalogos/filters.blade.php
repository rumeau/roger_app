    <catalogosfilter inline-template >
        <form action="" class="form-horizontal">
        <fieldset>
            <legend>Filtros de Búsqueda</legend>
            
            <div class="pull-right">
                <a class="btn btn-primary" >+ Nuevo</a>
            </div>
            
            <div class="form-group">
                <label for="operation" class="control-label col-md-2">Tipo catálogo</label>
                <div class="col-xs-2">
                    {!! Form::select('catalogue_type_id', $selectTipoCatalogo, null, [
                        'class'        => 'form-control',
                        'placeholder'  => 'Seleccione tipo catálogo',
                        'v-on'         => 'change: operationChanged',
                        'v-model'      => 'catalogue_type' ]) !!}
            </div>
            
            <!--
            <div class="form-group">
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{route('catalogo.create')}}">Excel</a>
                    <a class="btn btn-primary" href="#" v-on="click: clearFilters">Limpiar filtros</a>
                    <a class="btn btn-primary" href="#" v-on="click: filterSearch">Buscar</a>
                </div>
            </div>-->
        </fieldset>
        </form>
    </catalogosfilter>