@extends('layout')
@section('content')

<div class="page-header">
    <h1>Inicializador de dispensadores</h1>
</div>

<form action="{{ route('initdispensador.store') }}" method="post" class="form-horizontal">
    {!! csrf_field() !!}
    <!--
    <div>
        {!! Form::hidden('enterprise_id', current_enterprise()->id, [
            'class'=>'form-control'
            ]) !!}
    </div>-->
    <div class="form-group">
        <label class="control-label col-md-2">Empresa</label>
        <div class="col-md-6">
            {!! Form::select('enterprise_id', $selectEmpresas, Null, [
                'class'        => 'form-control',
                'placeholder'  => 'Seleccione Empresa',
                'v-on'         => 'change: actualizaDropdownAlSeleccionarEmpresa',
                'v-model'      => 'empresa']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Faena</label>
        <div class="col-md-6">
            {!! Form::select('operation_id', [], Null, [
                'class'=>'form-control',
                'placeholder' => 'Seleccione Faena',
                'v-on' => 'change: actualizaDropdownAlSeleccionarFaena',
                'options'    => 'faenas',
                'v-model' => 'faena']) !!}
        </div>
    </div>
   
    <div class="form-group{{ $errors->has('fechaCarga') ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Fecha carga</label>
        <div class="col-xs-2">
            {!! Form::date('date_residue', Null, ['class'=>'form-control']) !!}
        </div>
        @if ($errors->has('date_residue'))
            {!! $errors->first('date_residue', '<p class="help-block col-md-10 col-md-offset-2">:message</p>') !!}
        @endif
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Dispensador</label>
        <div class="col-md-6">
            {!! Form::select('fuel_dispenser_id', [], Null, [
                'class'       =>'form-control',
                'placeholder' => 'Seleccione dispensador',
                'options'     => 'dispensadores',
                'v-model'     => 'dispensador'
            ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Saldo inicial</label>
        <div class="col-md-6">
            {!! Form::text('beginning_residue', Null, [
                'class'=>'form-control'
            ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Capacidad estanque</label>
        <div class="col-md-6">
            {!! Form::text('capacity', Null, [
                'class'=>'form-control'
            ]) !!}
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>

 @endsection