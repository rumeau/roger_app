<initdispensadorfilter inline-template data-url="{{ route('initdispensador.index') }}">
    <form action="" class="form-horizontal">
        <fieldset>
            <legend>Filtros de Búsqueda</legend>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{route('initdispensador.create')}}">+ Nuevo</a>
            </div>
            
            <div class="form-group">
                <label for="operation" class="control-label col-md-2">Faena</label>
                <div class="col-xs-2">
                    {!! Form::select('operation_id', $selectFaenas, null, [
                        'class'        => 'form-control',
                        'placeholder'  => 'Seleccione Faena',
                        
                        'v-model'      => 'operation' ]) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Fecha desde</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('date', null, [
                            'class'    => 'form-control datepicker' ]) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <label for="date" class="control-label col-md-2">Fecha hasta</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('invoiceDate', null, [
                            'class'     => 'form-control datepicker' ]) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            
            <div class="pull-right">
                <a class="btn btn-primary" href="{{route('initdispensador.create')}}">Excel</a>
                <a class="btn btn-primary" href="#" >Limpiar filtros</a>
                <a class="btn btn-primary" href="#" >Buscar</a>
            </div>
        </fieldset>
    </form>
</initdispensadorfilter>