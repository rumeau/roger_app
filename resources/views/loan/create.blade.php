@extends('layout')
@section('content')
 
<div class="page-header">
    <h1>Préstamo de combustible</h1>
</div>

<prestamo-form inline-template >
    <div class="alert alert-@{{alertType}} alert-dismissible" role="alert" v-show="showAlert" v-html="message"></div>
    {!! Form::open(['@submit.prevent' => 'onSubmit' ,'route' => 'loan.store', 'class' => 'form-horizontal']) !!}
    
        @include('loan.form')
    
    {!! Form::close() !!}
</prestamo-form>
 @endsection