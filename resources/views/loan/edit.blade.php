@extends('layout')

@section('content')

    <div class="page-header">
        <h1>Préstamo de combustible <small> &gt; Editar</small></h1>
    </div>

    <prestamo-form inline-template edit="{{ route("loan.edit", ['loan' => $loan->id]) }}">
        {!! Form::open([
            '@submit.prevent' => 'onSubmit',
            'route'       => ['loan.update', 'loan' => $loan->id],
            'class'       => 'form-horizontal',
            'method'      => 'PUT' ]) !!}

            @include('loan.form', ['loan' => $loan])

        {!! Form::close() !!}
    </prestamo-form>
@endsection