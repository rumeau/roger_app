<prestamo-filter inline-template url="{{ route('loan.index') }}">
    <form action="" class="form-horizontal">
        <fieldset>
            <legend>Filtros de Búsqueda</legend>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('loan.create') }}"><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
            </div>
            <div class="form-group">
                <label for="operation" class="control-label col-md-2">Faena</label>
                <div class="col-xs-2">
                    {!! Form::select('operation', \App\Repositories\OperationRepository::forLoan($enterprise)->lists('name', 'id'), null, [
                            'class'          => 'form-control',
                            'placeholder'    => 'Seleccione Faena',
                            '@change'        => 'updateWorkShifts',
                            'v-model'        => 'operation' ]) !!}
                </div>
                <label for="date" class="control-label col-md-2">Turno</label>
                <div class="col-xs-2">
                    <select name="work_shift" class="form-control" v-model="work_shift">
                        <option value="">Seleccione turno</option>
                        <option v-for="option in work_shifts" v-bind:value="option.value">@{{ option.text }}</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Fecha desde</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('date', null, [
                                'class'          => 'form-control',
                                'v-datepicker',
                                'v-model'        => 'date_from' ]) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <label for="date" class="control-label col-md-2">Fecha hasta</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('invoiceDate', null, [
                                'class'         => 'form-control',
                                'v-datepicker',
                                'v-model'       => 'date_to' ]) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Nº Guía</label>
                <div class="col-md-2">
                        {!! Form::text('order_number', null, [
                                'class'      => 'form-control onlynumbers',
                                'v-model'    => 'order_number' ]) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Dispensador</label>
                <div class="col-md-2">
                    {!! Form::select('dispenser', \App\Repositories\EnterpriseRepository::dispensersList($enterprise), null, [
                            'class'        => 'form-control',
                            'placeholder'  => 'Seleccione dispensador',
                            'v-model'      => 'dispenser' ]) !!}
                </div>
                <label for="date" class="control-label col-md-2">Empresa externa</label>
                <div class="col-md-2">
                    {!! Form::select('external', \App\Repositories\EnterpriseRepository::externalEnterprisesList($enterprise), null, [
                            'class'        => 'form-control',
                            'placeholder'  => 'Seleccione externo',
                            'v-model'      => 'external' ]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="pull-left">
                        <a class="btn btn-primary" href="" data-toggle="modal" data-target="#registroDevolucion"><span class="glyphicon glyphicon-plus"></span> Registrar devolución</a>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="javascript:;" @click="exportExcel">Excel</a>
                        <a class="btn btn-primary" href="javascript:;" @click="clearFilters">Limpiar filtros</a>
                        <a class="btn btn-primary" href="javascript:;" @click="filterSearch">Buscar</a>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
        
    <div class="modal fade" role="dialog" aria-labelledby="gridSystemModalLabel" id="registroDevolucion">
        <div class="modal-dialog" role="document">
            <form action="{{ route('loan.return') }}" class="form-horizontal" @submit.prevent="registerReturn">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel">Registro de devoluciones</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nº Guía</th>
                                    <th>Empresa</th>
                                    <th>Litros</th>
                                    <th>Fecha</th>
                                 </tr>
                            </thead>
                            <tbody>
                                <tr v-for="return in returns">
                                    <td width="30">@{{ return.order_number }}</td>
                                    <td width="45">@{{ return.external_enterprise }}</td>
                                    <td width="30">@{{ return.liters }}</td>
                                    <td width="40">@{{ formattedDate(return.load_date) }}</td>
                                </tr>
                                <tr>
                                    <td width="30" colspan="2">Total</td>
                                    <td width="30"><strong>@{{ totalLitros }}</strong></td>
                                    <td width="40"></td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="prestamosSeleccionados"></div>
                        <div class="form-group">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-4">
                                <div class="checkbox checkbox-primary">
                                    {!! Form::checkbox('return_type', '1', null, [
                                            'class'  => 'checkbox',
                                            '@click' => 'isamountStatus' ]) !!}
                                    <label><strong>Registrar devolución en dinero.</strong></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Ingresar monto $</label>
                            <div class="col-md-4">
                                {!! Form::text('amount', Null, [
                                        'class'        => 'form-control',
                                        'v-model'      => 'returnAmount',
                                        'v-bind:disabled' => '!isamount' ]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <!--<a href="/ajax/chargeables/fuelreturn" class="btn btn-default btn-confirm" >
                        <span class="glyphicon glyphicon-thumbs-up"></span></a>-->
                        <button class="btn btn-primary" type="submit">Guardar</button>
                    </div>
                </div><!-- /.modal-content -->
            </form>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</prestamo-filter>