@extends('layout')
@section('content')
    <div class="page-header">
        <h1>Cargas ><small> a dispensadores</small></h1>
    </div>

    <table class="table table-bordered table-responsive table-hover">
    <thead>
        <tr>
            <td>Empresa</td>
            <td>Faena</td>
            <td>Concepto</td>
            <td>Fecha carga</td>
            <td>N° Guía</td>
            <td>Turno</td>
            <td>Dispensador origen</td>
            <td>Dispensador destino</td>
            <td>Operador</td>
            <td>Contador</td>
            <td>Horómetro</td>
            <td>Litros</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($compras as $compra)
        <tr>
            <td width='60'><div align='center' class='Estilo6'>{{$compra->enterprise->name}}</div></td>
            <td width='60'><div align='center' class='Estilo6'>{{$compra->operation->name}}</div></td>
            <td width='60'><div align='center' class='Estilo6'>{{$compra->concept->name}}</div></td>
            <td width='60'><div align='center' class='Estilo6'>{{$compra->load_date}}</div></td>
            <td width='60'><div align='center' class='Estilo6'>{{$compra->order_number}}</div></td>
            <td width='60'><div align='center' class='Estilo6'>{{$compra->work_shift->name}}</div></td>
            <td width='60'><div align='center' class='Estilo6'>{{$compra->dispenserorigen->name}}</div></td>
            <td width='60'><div align='center' class='Estilo6'>{{$compra->dispenserdestino->name}}</div></td>
            <td width='60'><div align='center' class='Estilo6'>{{$compra->operator->name." ".$compra->operator->primary_last_name}}</div></td>
            <td width='60'><div align='center' class='Estilo6'>{{$compra->counter}}</div></td>
            <td width='60'><div align='center' class='Estilo6'>{{$compra->hour_meter}}</div></td>
            <td width='60'><div align='center' class='Estilo6'>{{$compra->liters}}</div></td>
        </tr>
        @endforeach
    </tbody>
    </table>
@endsection