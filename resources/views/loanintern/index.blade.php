@extends('layout')
@section('content')
    
    <h1>Préstamos internos (entre faenas Serfocol)</h1>
    
    @include('loanintern.filters')

    <div id="loaninternfilter"></div>

@endsection