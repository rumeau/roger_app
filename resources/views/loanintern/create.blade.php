@extends('layout')
@section('content')
<div class="page-header">
    <h1>Préstamo de combustible (faenas Serfocol)</h1>
</div>

<loanformintern inline-template >
    <div class="alert alert-@{{alertType}} alert-dismissible" role="alert" v-show="showAlert" v-html="message"></div>
    {!! Form::open(['v-on'    => 'submit: onSubmit',
                    'route'   => 'loanintern.store', 
                    'class'   => 'form-horizontal' ]) !!}
    
    @include('loanintern.form', ['chargeables' => null])
    
    {!! Form::close() !!}
</loanformintern>
 @endsection