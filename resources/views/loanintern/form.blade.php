    <!--<div>
        {!! Form::hidden('enterprise_id', current_enterprise()->id, [
                'class'        => 'form-control',
                'v-model'      => 'enterprise' ]) !!}
    </div>-->
    <h3>Antedentes origen del combustible</h3>
    <div class="form-group@{{ errors.init_operation ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Faena origen</label>
        <div class="col-xs-4">
            {!! Form::select('init_operation', $selectFaenas, Null, [
                'class'        => 'form-control',
                'placeholder'  => 'Seleccione faena origen',
                'v-on'         => 'change: operationChangedInternLoan',
                'v-model'      => 'init_operation' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.init_operation" v-text="errors.init_operation"></p>
    </div>
    <div class="form-group@{{ errors.loaded_origin_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Dispensador origen</label>
        <div class="col-xs-4">
            {!! Form::select('loaded_origin_id', [], Null, [
                    'class'        => 'form-control',
                    'placeholder'  => 'Seleccione dispensador origen',
                    'options'      => 'dispenser_interns',
                    'v-model'      => 'dispenser_intern' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.loaded_origin_id" v-text="errors.loaded_origin_id"></p>
    </div>
    <div class="form-group@{{ errors.load_date ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Fecha préstamo</label>
        <div class="col-xs-2">
            {!! Form::date('load_date', Null, ['class' => 'form-control' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.load_date" v-text="errors.load_date"></p>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Turno</label>
        <div class="col-xs-4">
            {!! Form::select('work_shift_id', [], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione Turno', 
                    'options'      => 'work_shifts', 
                    'v-model'      => 'work_shift' ]) !!}
        </div>
    </div>
    <div class="form-group@{{ errors.order_number ? ' has-error' : '' }}">
        <label class="control-label col-md-2">N° Guía</label>
        <div class="col-xs-2">
            {!! Form::text('order_number', Null, [
                    'class'        => 'form-control',
                    'v-model'      => 'order_number' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.order_number" v-text="errors.order_number"></p>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Ubicación</label>
        <div class="col-xs-2">
            {!! Form::select('ubication_id', [], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione Ubicación', 
                    'options'      => 'ubications', 
                    'v-model'      => 'ubication' ]) !!}
        </div>
    </div>
    
    <div class="form-group@{{ errors.responsible_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Responsable carga</label>
        <div class="col-xs-4">
            {!! Form::select('responsible_id', $selectResponsables, Null, [
                    'class'        => 'form-control',
                    'placeholder'  => 'Seleccione responsable',
                    'v-model'      => 'responsible' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.responsible_id" v-text="errors.responsible_id"></p>
    </div>
    <div class="form-group@{{ errors.counter ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Contador</label>
        <div class="col-xs-2">
            {!! Form::text('counter', Null, ['class' => 'form-control' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.counter" v-text="errors.counter"></p>
    </div>
    <div class="form-group@{{ errors.liters ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Litros</label>
        <div class="col-xs-2">
            {!! Form::text('liters', Null, ['class' => 'form-control' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.liters" v-text="errors.liters"></p>
    </div>
    <h3>Antedentes destino del combustible</h3>
    <div class="form-group@{{ errors.enterprise_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Empresa destino</label>
        <div class="col-xs-4">
            {!! Form::select('enterprise_id', $selectEmpresas, Null, [
                'class'        => 'form-control',
                'placeholder'  => 'Seleccione empresa destino',
                'v-on'         => 'change: enterpriseChanged',
                'v-model'      => 'enterprise' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.enterprise_id" v-text="errors.enterprise_id"></p>
    </div>
    <div class="form-group@{{ errors.operation_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Faena destino</label>
        <div class="col-xs-4">
            {!! Form::select('operation_id', [], Null, [
                'class'        => 'form-control',
                'placeholder'  => 'Seleccione faena destino',
                'options'      => 'operations',
                'v-model'      => 'operation', 
                'v-on'         => 'change: operationChangedInternLoanDestiny' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.operation_id" v-text="errors.operation_id"></p>
    </div>
    <div class="form-group@{{ errors.loaded_destination_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Dispensador destino</label>
        <div class="col-xs-4">
            {!! Form::select('loaded_destination_id', [], Null, [
                    'class'        => 'form-control',
                    'placeholder'  => 'Seleccione dispensador destino',
                    'options'      => 'dispenser_destinys',
                    'v-model'      => 'dispenser_destiny' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.loaded_destination_id" v-text="errors.loaded_destination_id"></p>
    </div>
    <div class="form-group@{{ errors.operator_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Operador</label>
        <div class="col-xs-4">
            {!! Form::select('operator_id', [], Null, [
                'class'       => 'form-control',
                'placeholder' => 'Seleccione operador',
                'options'     => 'operators',
                'v-model'     => 'operator' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.operator_id" v-text="errors.operator_id"></p>
    </div>
    <div class="form-group">
        <p class="col-md-offset-2 col-md-10">
            <a href="{{ route('loanintern.index') }}" class="btn btn-default">&larr; Cancelar</a>
            <button type="submit" class="btn btn-primary" value="save" name="task">Guardar y salir</button>
        </p>
    </div>