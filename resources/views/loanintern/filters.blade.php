<loaninternfilter inline-template>
    <form action="" class="form-horizontal">
        <fieldset>
            <legend>Filtros de Búsqueda</legend>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{route('loanintern.create')}}">+ Nuevo</a>
            </div>
            <div class="form-group">
                <label for="operation" class="control-label col-md-2">Faena</label>
                <div class="col-xs-2">
                    {!! Form::select('operation', $selectFaenas, null, [
                            'class'          => 'form-control',
                            'placeholder'    => 'Seleccione Faena',
                            'v-model'        => 'operation' ]) !!}
                </div>
                <label for="date" class="control-label col-md-2">Nº Guía</label>
                <div class="col-md-2">
                        {!! Form::text('order_number', null, [
                                'class'      => 'form-control', 
                                'v-model'    => 'order_number' ]) !!}
                </div>
                <!--
                <label for="date" class="control-label col-md-2">Turno</label>
                <div class="col-xs-2">
                    {!! Form::select('work_shift', $selectTurnos, null, [
                            'class'          => 'form-control',
                            'placeholder'    => 'Seleccione Turno',
                            'v-model'        => 'work_shift' ]) !!}
                </div>-->
            </div>
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Empresa destino</label>
                <div class="col-md-2">
                    {!! Form::select('dispenser', $selectEmpresaDestino, null, [
                            'class'        => 'form-control',
                            'placeholder'  => 'Seleccione empresa',
                            'v-model'      => 'dispenser' ]) !!}
                </div>
                <label for="date" class="control-label col-md-2">Faena destino</label>
                <div class="col-md-2">
                    {!! Form::select('external', $selectFaenaDestino, null, [
                            'class'        => 'form-control',
                            'placeholder'  => 'Seleccione faena',
                            'v-model'      => 'external' ]) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Fecha desde</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('date', null, [
                                'class'          => 'form-control datepicker',
                                'v-model'        => 'date_from' ]) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <label for="date" class="control-label col-md-2">Fecha hasta</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('invoiceDate', null, [
                                'class'         => 'form-control datepicker',
                                'v-model'       => 'date_to' ]) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                
            </div>
            <!--
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Dispensador</label>
                <div class="col-md-2">
                    {!! Form::select('dispenser', $selectDispensadores, null, [
                            'class'        => 'form-control',
                            'placeholder'  => 'Seleccione dispensador',
                            'v-model'      => 'dispenser' ]) !!}
                </div>
            </div>-->
            <div class="form-group">
                <div class="pull-left">
                    <a class="btn btn-primary" href="" data-toggle="modal" data-target="#registroDevolucion">+ Registrar devolución</a>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{route('loan.create')}}">Excel</a>
                    <a class="btn btn-primary" href="#" v-on="click: clearFilters">Limpiar filtros</a>
                    <a class="btn btn-primary" href="#" v-on="click: filterSearch">Buscar</a>
                </div>
            </div>
        </fieldset>
    </form>
        
    <div class="modal fade" role="dialog" aria-labelledby="gridSystemModalLabel" id="registroDevolucion">
        <div class="modal-dialog" role="document">
        {!! Form::open(['route' => 'loan.return', 'class' => 'form-horizontal'])  !!}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Registro de devoluciones</h4>
                </div>
                <div class="modal-body">
                    <script id="prestamosSeleccionadosTemplate" type="x-tmpl-mustache">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nº Guía</th>
                                <th>Empresa</th>
                                <th>Litros</th>
                                <th>Fecha</th>
                             </tr>
                        </thead>
                        <tbody>
                            @{{#loans}}
                            <tr>
                                <td width="30">
                                @{{order_number}}
                                    <input type="hidden" name="chargeables[]" value="@{{ id }}" />
                                </td>
                                <td width="45">@{{loaded_destination.name}}</td>
                                <td width="30">@{{liters}}</td>
                                <td width="40">@{{load_date}}</td>
                            </tr>
                            @{{/loans}}
                            <tr>
                                <td width="30" colspan="2">Total</td>
                                <td width="30"><strong>@{{totalLitros}}</strong></td>
                                <td width="40"></td>
                            </tr>
                        </tbody>
                    </table>
                    </script>
                          
                    <div class="prestamosSeleccionados"></div>
                        <div class="form-group">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-4">
                                <div class="checkbox checkbox-primary">
                                    {!! Form::checkbox('return_type', '1', null, [
                                            'class'        => 'checkbox',
                                            'v-on'         => 'click: isamountStatus' ]) !!}
                                    <label><strong>Registrar devolución en dinero.</strong></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Ingresar monto $</label>
                            <div class="col-md-4">
                                {!! Form::text('amount', Null, [
                                        'class'        => 'form-control',
                                        'v-model'      => 'amount',
                                        'v-attr'       => 'disabled: !isamount' ]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <!--<a href="/ajax/chargeables/fuelreturn" class="btn btn-default btn-confirm" >
                        <span class="glyphicon glyphicon-thumbs-up"></span></a>-->
                        <button class="btn btn-primary" type="submit">Guardar</button>
                    </div>
        {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</loaninternfilter>