@extends('layout')
@section('content')

<div class="page-header">
    <h1>Configuración</h1>
</div>
<!--
<div class="row">
    <div class="col-md-2">
        <div class="panel panel-default">
          <div class="panel-heading">Empresa</div>
          <div class="panel-body"><img src="../img/icon empresa.png"/></div>
        </div>
    </div>
     <div class="col-md-2">
        <div class="panel panel-default">
          <div class="panel-heading">Faenas</div>
          <div class="panel-body"><img src="../img/icon fabrica.png"/></div>
        </div>
    </div>
</div>
-->
<table width="150" class="table table-bordered table-responsive table-hover">
        <tr>
            <td width="60">Empresa</td>
            <td width="60">Faenas</td>
            <td>Ubicaciones</td>
        </tr>
        <tr>
            <td><a href="empresa"><img src="../img/icon empresa.png"></img></td>
            <td><a href="faena"><img src="../img/icon fabrica.png"></img></td>
            <td><img src="../img/icon ubicacion.png"></img></td>
        </tr>
</table>

<table class="table table-bordered table-responsive table-hover">
        <tr>
            <td>Categoria</td>
            <td>Tipo</td>
            <td>Marca</td>
            <td>Modelo</td>
        </tr>
        <tr>
            <td><a href="catalogo"><img src="../img/icon fabrica.png"></img></td>
            <td><a href="ordentrabajo"><img src="../img/icon fabrica.png"></img></td>
            <td></td>
            <td></td>
        </tr>
</table>

<table class="table table-bordered table-responsive table-hover">
        <tr>
            <td>Equipo</td>
            <td>Personas</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><a href="equipo"><img src="../img/icon tractor.png"></a></td>
            <td><a href="persona"><img src="../img/icon casco.png"></a></td>
            <td></td>
            <td></td>
        </tr>
</table>

<table class="table table-bordered table-responsive table-hover">
        <tr>
            <td>Dispensadores</td>
            <td>Iniciar dispensadores</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><a href="newdispensador"><img src="../img/icon dispensador.png"></a></td>
            <td><a href="initdispensador"><img src="../img/icon pistola.png"></a></td>
            <td></td>
            <td></td>
        </tr>
</table>
@endsection