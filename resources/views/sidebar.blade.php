<!-- Sidebar -->
<div id="sidebar-wrapper">
    <h1 class="logo">
        <a href="{{ url('/') }}">Sistema de control de combustible</a>
    </h1>
    
    {!! $SideBar->asUl( array('class' => 'sidebar-nav') ) !!}
</div>
<!-- /#sidebar-wrapper -->