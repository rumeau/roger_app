@extends('layout-clean')

@section('content')

<div class="auth-container">
    
    <div class="page-header">
        <h1>Sistema de control de combustible</h1>
    </div>
    
    <form method="post" action="/password/email">
        <h2>Reestablecer Contraseña</h2>
        <hr />
    
        {!! csrf_field() !!}
    
        <div class="form-group">
            <label class="control-label" for="email">Email</label>
            <input type="email" name="email" value="{{ old('email') }}" class="form-control">
        </div>
    
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Solicitar cambio de clave</button>
        </div>
    </form>
    
</div>

@endsection