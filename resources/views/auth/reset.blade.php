@extends('layout-clean')

@section('content')

<div class="auth-container">
    
    <div class="page-header">
        <h1>Sistema de control de combustible</h1>
    </div>
    
    <form method="POST" action="/password/reset">
        <h2>Iniciar Sesión</h2>
        <hr />
        
        {!! csrf_field() !!}

        <div class="form-group">
            <label class="control-label" for="email">Email</label>
            <input type="email" name="email" value="{{ old('email') }}" class="form-control">
        </div>

        <div class="form-group">
            <label class="control-label" for="password">Contraseña</label>
            <input type="password" name="password" class="form-control">
        </div>

        <div class="form-group">
            <label class="control-label" for="password_confirm">Confirmar Contraseña</label>
            <input type="password" name="password_confirmation" class="form-control">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Reestablecer Contraseña</button>
        </div>
    </form>
    
</div>

@endsection