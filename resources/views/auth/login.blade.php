@extends('layout-clean')

@section('content')

<div class="auth-container">
    
    <div class="page-header">
        <h1>Sistema de control de combustible</h1>
    </div>
    
    <form method="post" action="/auth/login">
        <h2>Iniciar Sesión</h2>
        <hr />
        
        {!! csrf_field() !!}
    
        <div class="form-group">
            <label class="control-label" for="email">Nombre de Usuario</label>
            <input type="text" name="login" value="{{ old('login') }}" class="form-control">
        </div>
        
        <div class="form-group">
            <label class="control-label" for="password">Contraseña</label>
            <input type="password" name="password" id="password" class="form-control">
        </div>
    
        <div class="form-group">
            <input type="checkbox" name="remember"> Recuérdame
        </div>
        
        <div class="form-group">
            <a href="{{ url('password/email') }}">Olvide mi Contraseña</a>
        </div>
    
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Ingresar</button>
        </div>
    </form>
</div>

@endsection