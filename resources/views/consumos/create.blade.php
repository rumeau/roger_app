@extends('layout')

@section('content')
    <div class="page-header">
        <h1>Consumo de equipos <small>Nuevo</small></h1>
    </div>

    <consumoform inline-template >
        <div class="alert alert-@{{alertType}} alert-dismissible" role="alert" v-show="showAlert" v-html="message" v-cloak></div>
        {!! Form::open(['@submit.prevent' => 'onSubmit' ,'route' => 'consumption.store', 'class' => 'form-horizontal']) !!}

            @include('consumos.form')

        {!! Form::close() !!}
    </consumoform>
@endsection