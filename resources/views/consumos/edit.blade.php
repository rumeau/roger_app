@extends('layout')
@section('content')

<div class="page-header">
    <h1>Consumo de equipos <small>Editar</small></h1>
</div>

<consumoform  inline-template edit="{{ route('consumption.edit', ['consumos' => $consumo->id]) }}">
    {!! Form::open([
        '@submit.prevent' => 'onSubmit',
        'route'   => ['consumption.update', 'consumos' => $consumo->id],
        'class'   => 'form-horizontal',
        'method'  => 'PUT']) !!}
        
        @include('consumos.form', ['consumo' => $consumo])
        
    {!! Form::close() !!}
</consumoform>
@endsection