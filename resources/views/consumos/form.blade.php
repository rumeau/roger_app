    <div class="form-group@{{ errors.operation_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Faena</label>
        <div class="col-md-4">
            {!! Form::select('operation_id', \App\Repositories\OperationRepository::forConsumption($enterprise)->lists('name', 'id'), null, [
                    'class'        => 'form-control',
                    'placeholder'  => 'Seleccione Faena',
                    '@change'      => 'operationChanged',
                    'v-model'      => 'data.operation_id' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.operation_id" v-text="errors.operation_id"></p>
    </div>

    <div class="form-group@{{ errors.load_date ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Fecha carga</label>
        <div class="col-md-2">
            {!! Form::text('load_date', null, ['class' => 'form-control', 'v-model' => 'data.load_date', 'v-datepicker']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.load_date" v-text="errors.load_date"></p>
    </div>
    <div class="form-group@{{ errors.responsible_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Responsable de la carga</label>
        <div class="col-md-4">
            {!! Form::select('responsible_id', \App\Repositories\EnterpriseRepository::responsiblesList($enterprise), Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione responsable',
                    'v-model'      => 'data.responsible_id' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.responsible_id" v-text="errors.responsible_id"></p>
    </div>
    <div class="form-group@{{ errors.work_shift_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Turno</label>
        <div class="col-md-2">
            <select name="work_shift_id" class="form-control" v-model="data.work_shift_id">
                <option value="">Seleccione Turno</option>
                <option v-for="option in work_shifts" v-bind:value="option.value">@{{ option.text }}</option>
            </select>
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.work_shift_id" v-text="errors.work_shift_id"></p>
    </div>
    <!--
    <div class="form-group">
        <label class="control-label col-md-2">Area productiva</label>
        <div class="col-md-4">
            {!! Form::select('productive_area_id', [], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione área productiva', 
                    'options'      => 'productive_areas', 
                    'v-model'      => 'productive_area']) !!}
        </div>
    </div>
    -->
    <div class="form-group">
        <label class="control-label col-md-2">Ubicación</label>
        <div class="col-md-4">
            <select name="ubication_id" class="form-control" v-model="data.ubication_id">
                <option value="">Seleccione ubicación</option>
                <option v-for="option in ubications" v-bind:value="option.value">@{{ option.text }}</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">N° Folio</label>
        <div class="col-md-2">
            {!! Form::text('order_number', Null, [
                'class'       => 'form-control onlynumbers',
                'v-model'     => 'data.order_number' ]) !!}
        </div>
    </div>
    <div class="form-group@{{ errors.loaded_origin_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Dispensador</label>
        <div class="col-md-4">
            <select name="loaded_origin_id" class="form-control" v-model="data.loaded_origin_id">
                <option value="">Seleccione dispensador</option>
                <option v-for="option in dispensers" v-bind:value="option.value">@{{ option.text }}</option>
            </select>
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.loaded_origin_id" v-text="errors.loaded_origin_id"></p>
    </div>
    <div class="form-group@{{ errors.loaded_destination_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Equipo</label>
        <div class="col-md-4">
            <select name="loaded_destination_id" class="form-control" v-model="data.loaded_destination_id" @change="lastHourMeter">
                <option value="">Seleccione equipo</option>
                <option v-for="option in equipments" v-bind:value="option.value">@{{ option.text }}</option>
            </select>
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.loaded_destination_id" v-text="errors.loaded_destination_id"></p>
    </div>
    <div class="form-group@{{ errors.operator_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Operador</label>
        <div class="col-md-4">
            <select name="operator_id" class="form-control" v-model="data.operator_id">
                <option value="">Seleccione operador</option>
                <option v-for="option in operators" v-bind:value="option.value">@{{ option.text }}</option>
            </select>
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.operator_id" v-text="errors.operator_id"></p>
    </div>
    <div class="form-group@{{ errors.counter ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Contador equipo dispensador</label>
        <div class="col-md-2">
            {!! Form::text('counter', null, [
                    'class'      => 'form-control onlynumbers',
                    'v-model'    => 'data.counter' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.counter" v-text="errors.counter"></p>
    </div>
    <div class="form-group@{{ errors.hour_meter ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Horómetro equipo</label>
        <div class="col-md-2">
            {!! Form::text('hour_meter', null, [
                    'class'      => 'form-control onlynumbers',
                    'v-model'    => 'data.hour_meter' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.hour_meter" v-text="errors.hour_meter"></p>
        
        <label class="control-label col-md-2"><i>Ultimo horómetro registrado</i></label>
        <div class="col-md-2">
            {!! Form::text('last_hour_meter', null, [
                    'class'      => 'form-control onlynumbers',
                    'v-model'    => 'data.last_hour_meter',
                    'readonly'    => true ]) !!}
        </div>
    </div>
    <div class="form-group@{{ errors.liters ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Litros</label>
        <div class="col-md-2">
            {!! Form::text('liters', null, [
                    'class'    => 'form-control onlynumbers',
                    'v-model'  => 'data.liters' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.liters" v-text="errors.liters"></p>
    </div>
    <div class="form-group">
        <p class="col-md-offset-2 col-md-10">
            <a href="{{ route('consumption.index') }}" class="btn btn-default">&larr; Cancelar</a>
            @if (isset($consumo))
                <button type="submit" class="btn btn-success" value="save" name="task">Guardar</button>
            @else
                <button type="submit" class="btn btn-success" value="save" name="task" @click="data.task = 'save'">Guardar y salir</button>
                <button type="submit" class="btn btn-success" value="save_n_new" name="task" @click="data.task = 'save_n_new'">Guardar y continuar</button>
            @endif
        </p>
    </div>