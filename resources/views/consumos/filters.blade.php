    <consumofilter inline-template  data-url="{{ route('consumption.index') }}">
        <form action="" class="form-horizontal">
        <fieldset>
            <legend>Filtros de Búsqueda</legend>
            
            <div class="pull-right">
                <a class="btn btn-primary" href="{{route('consumption.create', ['concepto' => 3])}}"><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
            </div>
            
            <div class="form-group">
                <label for="operation" class="control-label col-md-2">Faena</label>
                <div class="col-md-2">
                    {!! Form::select('operation_id', \App\Repositories\OperationRepository::forConsumption($enterprise)->lists('name', 'id'), null, [
                        'class'         => 'form-control',
                        'v-model'       => 'operation',
                        'placeholder'   => 'Seleccione Faena',
                        '@change'       => 'operationChanged', ]) !!}
                </div>
                <label for="date" class="control-label col-md-2">Turno</label>
                <div class="col-xs-2">
                    <select name="work_shift_id" class="form-control" v-model="work_shift">
                        <option value="">Seleccione turno</option>
                        <option v-for="option in work_shifts" v-bind:value="option.value">@{{ option.text }}</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Fecha desde</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('date', null, [
                            'class'      => 'form-control',
                            'v-model'    => 'date_from',
                             'v-datepicker']) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <label for="date" class="control-label col-md-2">Fecha hasta</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('invoiceDate', null, [
                            'class'      => 'form-control',
                            'v-model'    => 'date_to',
                             'v-datepicker']) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div><!--
                <label for="date" class="control-label col-md-2">Area productiva</label>
                <div class="col-md-2">
                    {!! Form::select('productive_area_id', \App\Repositories\EnterpriseRepository::productiveAreasList($enterprise), null, [
                        'class'         => 'form-control',
                        'v-model'       => 'productive_area',
                        'placeholder'   => 'Seleccione Area productiva',
                         ]) !!}
                </div>-->
            </div>
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Dispensador</label>
                <div class="col-md-2">
                    {!! Form::select('dispenser_id', \App\Repositories\EnterpriseRepository::dispensersList($enterprise), null, [
                        'class'         => 'form-control',
                        'v-model'       => 'dispenser',
                        'placeholder'   => 'Seleccione dispensador' ]) !!}
                </div>
                <label for="date" class="control-label col-md-2">Operador</label>
                <div class="col-md-2">
                    {!! Form::select('operator_id', \App\Repositories\EnterpriseRepository::operatorsList($enterprise), null, [
                        'class'         => 'form-control',
                        'v-model'       => 'operator',
                        'placeholder'   => 'Seleccione operador' ]) !!}
                </div>
                <label class="control-label col-md-2">Nª Folio</label>
                <div class="col-md-2">
                    {!! Form::text('order_number', null, [
                        'class'        => 'form-control onlynumbers',
                        'v-model'      => 'order_number' ]) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Centro de costo</label>
                <div class="col-md-2">
                    {!! Form::text('cc', null, [
                        'class'         => 'form-control onlynumbers',
                        'v-model'       => 'cc' ]) !!}
                </div>
                <label for="date" class="control-label col-md-2">Equipo</label>
                <div class="col-md-2">
                    {!! Form::select('equipment', \App\Repositories\EnterpriseRepository::equipmentsList($enterprise), null, [
                        'class'         => 'form-control',
                        'placeholder'   => 'Seleccione equipo',
                        'v-model'       => 'equipment' ]) !!}
                </div>
                <label for="date" class="control-label col-md-2">Responsable</label>
                <div class="col-md-2">
                    {!! Form::select('responsible_id', \App\Repositories\EnterpriseRepository::responsiblesList($enterprise), null, [
                        'class'         => 'form-control',
                        'placeholder'   => 'Seleccione responsable',
                        'v-model'       => 'responsible' ]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="javascript:;" @click="exportExcel">Excel</a>
                        <a class="btn btn-primary" href="javascript:;" @click="clearFilters">Limpiar filtros</a>
                        <a class="btn btn-primary" href="javascript:;" @click="filterSearch">Buscar</a>
                    </div>
                </div>
            </div>
        </fieldset>
        </form>
    </consumofilter>