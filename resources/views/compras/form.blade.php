    <div class="form-group@{{ errors.operation_id ? ' has-error' : '' }}" xmlns="http://www.w3.org/1999/html">
        <label class="control-label col-md-2">Faena</label>
        <div class="col-md-4">
            {!! Form::select('operation_id', \App\Repositories\OperationRepository::forBuy($enterprise)->lists('name', 'id'), null, [
                'class'        => 'form-control',
                'placeholder'  => 'Seleccione Faena',
                '@change'      => 'actualizaProvider',
                'v-model'      => 'data.operation_id']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.operation_id" v-text="errors.operation"></p>
    </div>
    
    <div class="form-group@{{ errors.load_date ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Fecha compra</label>
        <div class="col-md-2">
            {!! Form::text('load_date', null, ['class'=>'form-control', 'v-model' => 'data.load_date', 'v-datepicker', 'autocomplete' => 'false']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.load_date" v-text="errors.load_date"></p>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">N° Pedido</label>
        <div class="col-md-2">
            {!! Form::text('order_number', null, ['class'=>'form-control onlynumbers', 'v-model' => 'data.order_number']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">N° Factura</label>
        <div class="col-md-2">
            {!! Form::text('bill_number', null, ['class'=>'form-control onlynumbers', 'v-model' => 'data.bill_number']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.loaded_origin_id" v-text="errors.loaded_origin_id"></p>
    </div>
    <div class="form-group@{{ errors.loaded_origin_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Proveedor</label>
        <div class="col-md-4">
            <select name="loaded_origin_id" class="form-control" v-model="data.loaded_origin_id">
                <option value="">Seleccione Proveedor</option>
                <option v-for="option in providers" v-bind:value="option.value">@{{ option.text }}</option>
            </select>
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.loaded_origin_id" v-text="errors.loaded_origin_id"></p>
    </div>
    <div  class="form-group@{{ errors.loaded_destination_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Dispensador destino</label>
        <div class="col-md-4">
            <select name="loaded_destination_id" class="form-control" v-model="data.loaded_destination_id">
                <option value="">Seleccione dispensador de destino</option>
                <option v-for="option in dispensers" v-bind:value="option.value">@{{ option.text }}</option>
            </select>
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.loaded_destination_id" v-text="errors.loaded_destination_id"></p>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Precio Neto</label>
        <div class="col-md-2">
            {!! Form::text('net_price', null, ['class'=>'form-control onlynumbers', 'v-model' => 'data.net_price', 'number']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.net_price" v-text="errors.net_price"></p>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">IEC (monto $)</label>
        <div class="col-md-2">
            {!! Form::text('fuel_tax', null, ['class'=>'form-control onlynumbers', 'v-model' => 'data.fuel_tax', 'number']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.fuel_tax" v-text="errors.fuel_tax"></p>
    </div>
    <div  class="form-group@{{ errors.liters ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Litros</label>
        <div class="col-md-2">
            {!! Form::text('liters', null, ['class'=>'form-control onlynumbers', 'v-model' => 'data.liters', 'number']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.liters" v-text="errors.liters"></p>
    </div>
    <div class="form-group">
        <p class="col-md-offset-2 col-md-10">
            <a href="{{ route('compra.index') }}" class="btn btn-default">&larr; Cancelar</a>
            <button type="submit" class="btn btn-primary" value="save" name="task">Guardar y salir</button><!--
            <button type="submit" class="btn btn-primary" value="save_n_new" name="task">Guardar y continuar</button>-->
        </p>
    </div>
