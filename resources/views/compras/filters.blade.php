    <compra-filter inline-template data-url="{{ route('compra.index') }}">
        <form action="" class="form-horizontal">
        <fieldset>
            <legend>Filtros de Búsqueda</legend>
            
            <div class="pull-right">
                <a class="btn btn-primary" href="{{route('compra.create',['concepto'=>1])}}"><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
            </div>
            
            <div class="form-group">
                <label for="operation" class="control-label col-md-2">Faena</label>
                <div class="col-xs-2">
                    {!! Form::select('operation_id', \App\Repositories\OperationRepository::forBuy($enterprise)->lists('name', 'id'), null, [
                        'class'        => 'form-control',
                        'placeholder'  => 'Seleccione Faena',
                        '@change'      => 'operationChanged',
                        'v-model'      => 'operation']) !!}
                </div><!--
                <label class="control-label col-md-2">Turno</label>
                <div class="col-xs-2">
                    {!! Form::select('work_shift_id', [], Null, [
                        'class'        => 'form-control', 
                        'placeholder'  => 'Seleccione Turno', 
                        'options'      => 'work_shifts', 
                        'v-model'      => 'work_shift']) !!}
                </div>-->
                <label class="control-label col-md-2"></label>
                <div class="col-md-2">
                    <div class="checkbox checkbox-primary">
                    {!! Form::checkbox('received', null, null, [
                        'class'    => 'checkbox',
                        'v-model'  => 'received' ]) !!}
                    <label><strong>Pendiente recepción</strong></label>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Fecha de compra desde</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('date', null, [
                            'class'        => 'form-control',
                            'v-model'      => 'date_from',
                            'v-datepicker',
                            ]) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <label for="date" class="control-label col-md-2">Fecha de compra hasta</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('invoiceDate', null, [
                        'class'            => 'form-control',
                        'v-model'          => 'date_to',
                        'v-datepicker',
                        ]) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label for="order_number" class="control-label col-md-2">Nº pedido</label>
                <div class="col-md-2">
                    {!! Form::text('order_number', null, [
                        'class'    => 'form-control',
                        'v-model'  => 'order_number',
                    ]) !!}
                </div>
                <label for="bill_number" class="control-label col-md-2">Nº factura</label>
                <div class="col-md-2">
                    {!! Form::text('bill_number', null, [
                        'class'   => 'form-control',
                        'v-model' => 'bill_number',
                    ]) !!}
                </div>
            </div>
            
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Proveedor</label>
                <div class="col-md-2">
                    {!! Form::select('provider_id', \App\Repositories\EnterpriseRepository::dispensersList($enterprise), null, [
                        'class'       => 'form-control',
                        'placeholder' => 'Seleccione Proveedor',
                        'v-model'     => 'provider',
                    ]) !!}
                </div>
                <label for="date" class="control-label col-md-2">Dispensador</label>
                <div class="col-md-2">
                    {!! Form::select('dispenser', \App\Repositories\EnterpriseRepository::dispensersList($enterprise), null, [
                        'class'       => 'form-control',
                        'placeholder' => 'Seleccione Dispensador',
                        'v-model'     => 'dispenser',
                    ]) !!}
                </div>
                <!--<label for="date" class="control-label col-md-2">Responsable</label>
                <div class="col-md-2">
                    {!! Form::select('responsible_id', \App\Repositories\EnterpriseRepository::responsiblesList($enterprise), null, [
                        'class'       => 'form-control',
                        'placeholder' => 'Seleccione Responsable',
                        'v-model'     => 'responsible',
                    ]) !!}
                </div>
                -->
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="javascript:;" @click="exportExcel">Excel</a>
                        <a class="btn btn-primary" href="javascript:;" @click="clearFilters">Limpiar filtros</a>
                        <a class="btn btn-primary" href="javascript:;" @click="filterSearch">Buscar</a>
                    </div>
                </div>
            </div>
        </fieldset>
        </form>
    </compra-filter>