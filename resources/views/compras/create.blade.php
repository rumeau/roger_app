@extends('layout')

@section('content')
    <div class="page-header">
        <h1>Compras de combustible</h1>
    </div>
    <compra-form inline-template>
        <div class="alert alert-@{{alertType}} alert-dismissible" role="alert" v-show="showAlert" v-html="message" v-cloak></div>

        {!! Form::open(['@submit.prevent' => 'onSubmit', 'route' => 'compra.store', 'class' => 'form-horizontal', 'method' => 'POST']) !!}

        @include('compras.form')

        {!! Form::close() !!}
    </compra-form>
@endsection