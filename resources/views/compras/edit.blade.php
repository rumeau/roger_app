@extends('layout')
@section('content')

<div class="page-header">
    <h1>Compras de combustible <small>Editar</small></h1>
</div>

<compra-form  inline-template edit="{{ route('compra.edit', ['compra' => $compra->id]) }}">
    {!! Form::open([
        '@submit.prevent'   => 'onSubmit',
        'route'     => ['compra.update', 'compra' => $compra->id], 
        'class'     => 'form-horizontal',
        'method'    => 'POST' ]) !!}
        
        @include('compras.form')
        
    {!! Form::close() !!}
</compra-form>
@endsection