@extends('layout')
 @section('content')
<div class="page-header">
    <h1>Orden de trabajo</h1>
</div>
<otform inline-template >
    <div class="alert alert-@{{alertType}} alert-dismissible" role="alert" v-show="showAlert" v-html="message"></div>
{!! Form::open(['v-on' => 'submit: onSubmit' ,'route' => 'ordentrabajo.store', 'class' => 'form-horizontal']) !!}
    
    @include('ordentrabajo.form')
    
{!! Form::close() !!}
</otform>
@endsection