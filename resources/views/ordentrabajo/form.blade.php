
    <div>
        {!! Form::hidden('enterprise_id', current_enterprise()->id) !!}
    </div>
    <div class="form-group@{{ errors.operation_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Faena</label>
        <div class="col-md-4">
            {!! Form::select('operation_id', $selectFaenas, Null, [
                    'class'        => 'form-control',
                    'placeholder'  => 'Seleccione Faena',
                    'v-on'         => 'change: operationChanged',
                    'v-model'      => 'operation' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.operation_id" v-text="errors.operation_id"></p>
    </div>
    <div>
        {!! Form::hidden('concept_id', $selectConceptos->id, []) !!}
    </div>
    <div class="form-group@{{ errors.attention_date ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Fecha atención</label>
        <div class="col-md-2">
            {!! Form::date('attention_date', Null, ['class'=>'form-control']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.attention_date" v-text="errors.attention_date"></p>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Usuario afectado</label>
        <div class="col-md-4">
            {!! Form::select('affected_id', $selectAfectado, Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione usuario', 
                    
                    'v-model'      => 'affected' ]) !!}
        </div>
        <a class="glyphicon glyphicon-tags" href="#" data-toggle="modal" data-target="#seleccionComponentes"></a>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Descripción</label>
        <div class="col-md-4">
            {!! Form::textarea('description', Null, [
                    'class'        => 'form-control' ]) !!}
        </div>
        <!--<p class="help-block col-md-10 col-md-offset-2" v-show="errors.affected_id" v-text="errors.affected_id"></p>-->
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Actividad</label>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Tipo solicitud</label>
        <div class="col-md-4">
            {!! Form::select('type_request_id', $selectNivel1, Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione tipo de solicitud',
                    'v-on'         => 'change: requestChange',
                    'v-model'      => 'level_1' ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Plataforma</label>
        <div class="col-md-4">
            {!! Form::select('platform_id', [], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione plataforma',
                    'v-on'         => 'change: platformChange',
                    'v-model'      => 'level_2',
                    'options'      => 'level_2s' ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Sistema</label>
        <div class="col-md-4">
            {!! Form::select('n3_system_id', [], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione sistema',
                    'v-on'         => 'change: systemChange',
                    'v-model'      => 'level_3',
                    'options'      => 'level_3s' ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Sub-sistema</label>
        <div class="col-md-4">
            {!! Form::select('n4_sub_system_id', [], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione sub-sistema',
                    'v-on'         => 'change: subSystemChange',
                    'v-model'      => 'level_4',
                    'options'      => 'level_4s' ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Nivel 5</label>
        <div class="col-md-4">
            {!! Form::select('n5', [], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione nivel 5',
                    'v-on'         => 'change: n5Change',
                    'v-model'      => 'level_5',
                    'options'      => 'level_5s' ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Nivel 6</label>
        <div class="col-md-4">
            {!! Form::select('n6', [], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione nivel 6',
                    'v-model'      => 'level_6',
                    'options'      => 'level_6s' ]) !!}
        </div>
    </div>
    <div class="form-group">
        <p class="col-md-offset-2 col-md-10">
            <a href="{{ route('ordentrabajo.index') }}" class="btn btn-default">&larr; Cancelar</a>
            <button type="submit" class="btn btn-primary" value="save" name="task">Guardar y salir</button>
            <button type="submit" class="btn btn-primary" value="save_n_new" name="task">Guardar y continuar</button>
        </p>
    </div>
    
    
    <div class="modal fade" role="dialog" aria-labelledby="gridSystemModalLabel" id="seleccionComponentes">
        <div class="modal-dialog" role="document">
        {!! Form::open(['route' => 'ordentrabajo.return', 'class' => 'form-horizontal'])  !!}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Componentes asociados por usuario</h4>
                </div>
                <div class="modal-body">
                    
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Patente</th>
                                <th>Nombre</th>
                                <th>Marca</th>
                                <th>Modelo</th>
                                <th>N° de serie</th>
                                <th>Familia</th>
                             </tr>
                        </thead>
                        <tbody>
                            
                            <tr v-repeat="componentes">
                                <td width="45" v-text="component | equipmentPatent component_type"></td>
                                <td width="30" v-text="component | equipmentName component_type"></td>
                                <td width="45" v-text="component | equipmentBrand component_type"></td>
                                <td width="30" v-text="component | equipmentModel component_type"></td>
                                <td width="40" v-text="component | equipmentSerial component_type"></td>
                                <td width="40" v-text="component | equipmentFamily component_type"></td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <!--<a href="/ajax/chargeables/fuelreturn" class="btn btn-default btn-confirm" >
                        <span class="glyphicon glyphicon-thumbs-up"></span></a>-->
                        <button class="btn btn-primary" type="submit">Guardar</button>
                    </div>
        {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->