<faenaform inline-template>
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Nombre</label>
        <div class="col-xs-4">
            {!! Form::text('name', Null, [
                    'class'        => 'form-control' ]) !!}
        </div>
        @if ($errors->has('name'))
            {!! $errors->first('name', '<p class="help-block col-md-10 col-md-offset-2">:message</p>') !!}
        @endif
    </div>
    <div class="form-group{{ $errors->has('rut') ? ' has-error' : '' }}">
        <label class="control-label col-md-2">R.U.T</label>
        <div class="col-xs-2">
            {!! Form::text('rut', Null, [
                    'class'        => 'form-control' ]) !!}
        </div>
        @if ($errors->has('rut'))
            {!! $errors->first('rut', '<p class="help-block col-md-10 col-md-offset-2">:message</p>') !!}
        @endif
    </div>
    <div class="form-group{{ $errors->has('region_id') ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Región</label>
        <div class="col-xs-4">
            {!! Form::select('region_id', $selectRegiones, Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione región', 
                    'options'      => 'regions' ]) !!}
        </div>
        @if ($errors->has('region_id'))
            {!! $errors->first('region_id', '<p class="help-block col-md-10 col-md-offset-2">:message</p>') !!}
        @endif
    </div>
    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Dirección</label>
        <div class="col-xs-4">
            {!! Form::text('address', Null, [
                    'class'        => 'form-control' ]) !!}
        </div>
        @if ($errors->has('address'))
            {!! $errors->first('address', '<p class="help-block col-md-10 col-md-offset-2">:message</p>') !!}
        @endif
    </div>
    <div class="form-group{{ $errors->has('commune') ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Comuna</label>
        <div class="col-xs-4">
            {!! Form::text('commune', Null, [
                    'class'        => 'form-control' ]) !!}
        </div>
        @if ($errors->has('commune'))
            {!! $errors->first('commune', '<p class="help-block col-md-10 col-md-offset-2">:message</p>') !!}
        @endif
    </div>
    <div class="form-group">
        <p class="col-md-offset-2 col-md-10">
            <a href="{{ route('empresa.index') }}" class="btn btn-default">&larr; Cancelar</a>
            <button type="submit" class="btn btn-primary" value="save" name="task">Guardar y salir</button><!--
            <button type="submit" class="btn btn-primary" value="save_n_new" name="task">Guardar y continuar</button>-->
        </p>
    </div>
</faenaform>