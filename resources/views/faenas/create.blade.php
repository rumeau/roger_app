@extends('layout')
 @section('content')
<div class="page-header">
    <h1>Faenas</h1>
</div>

{!! Form::open(['route' => 'faena.store', 'class' => 'form-horizontal']) !!}

    @include('faenas.form')
    
{!! Form::close() !!}

 @endsection