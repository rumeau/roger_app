<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="_token" id="metatoken" content="{{ csrf_token() }}">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SGP ver.2</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="{{asset('css/app.css')}}">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<link rel="stylesheet" href="{{ asset('css/app.css') }}">

</head>

<body id='app'>
    
<div id="wrapper">

        @include('sidebar')

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                @include('enterprise-selection')
                
                @include('flash::message')
                
                @yield('content')
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Menu Toggle Script -->
    <script type="text/javascript">
    var $, jQuery;
    </script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
    @yield('inlinescript')

</body>
</html>