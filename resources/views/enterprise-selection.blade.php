@if (\Session::has('user_enterprise'))
<nav class="navbar navbar-default navbar-fixed-top serfocol-fixed-container">
  <div class="">
    <p class="navbar-text">Ud. esta trabajando con la empresa: <strong>{{ current_enterprise()->name }}</strong>
     - <a href={{ url('select-enterprise') }}>[Cambiar de empresa]</a></p>
  </div>
  </nav>
@endif