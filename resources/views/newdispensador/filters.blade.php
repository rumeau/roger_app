    <newdispensadorfilters inline-template data-url="{{route('newdispensador.index')}}">
        <form action="" class="form-horizontal">
        <fieldset>
            <legend>Filtros de Búsqueda</legend>
            
            <div class="pull-right">
                <a class="btn btn-primary" href="{{route('newdispensador.create')}}">+ Nuevo</a>
            </div>
            
            <div class="form-group">
                <label class="control-label col-md-2">Empresa</label>
                <div class="col-xs-2">
                    {!! Form::select('enterprise_id', $selectEmpresas, Null, [
                        'class'       => 'form-control',
                        'placeholder' => 'Seleccione Empresa',
                        'v-on'        => 'change: enterpriseChanged',
                        'v-model'     => 'enterprise']) !!}
                </div>
                <label class="control-label col-md-2">Faena</label>
                <div class="col-xs-2">
                    {!! Form::select('operation_id', [], NULL, [
                        'class'        => 'form-control',
                        'placeholder'  => 'Seleccione faena',
                        'v-model'      => 'operation',
                        'options'      => 'operations', ]) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Dispensador</label>
                <div class="col-md-2">
                    {!! Form::select('dispenser', [], null, [
                        'class'        => 'form-control',
                        'placeholder'  => 'Seleccione dispensador',
                        'v-model'      => 'dispenser',
                        'options'      => 'dispensers', ]) !!}
                </div>
                
                <label  class="control-label col-md-2"></label>
                <div class="col-md-2">
                    <div class="checkbox checkbox-primary">
                        {!! Form::checkbox('active', null, null, [
                            'class'          => 'checkbox' ]) !!}
                            <label><strong>Dispensadores inactivos</strong></label>
                    </div>
                </div>
            </div>
            
            <div class="pull-right">
                <a class="btn btn-primary" href="{{route('newdispensador.create')}}">Excel</a>
                <a class="btn btn-primary" href="#" v-on="click: clearFilters">Limpiar filtros</a>
                <a class="btn btn-primary" href="#" v-on="click: filterSearch">Buscar</a>
            </div>
          
        </fieldset>
        </form>
    </newdispensadorfilters>