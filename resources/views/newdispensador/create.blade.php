@extends('layout')
@section('content')
 
<div class="page-header">
    <h1>Nuevo Dispensador</h1>
</div>

{!! Form::open(['route' => 'newdispensador.store', 'class' => 'form-horizontal']) !!}
    
    @include('newdispensador.form')
    
{!! Form::close() !!}

 @endsection