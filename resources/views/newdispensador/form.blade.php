<dispensadorform inline-template>
    <div>
        {!! Form::hidden('enterprise_id', current_enterprise()->id, [
                'class'=>'form-control'
            ]) !!}
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Empresa</label>
        <div class="col-xs-4">
            {!! Form::select('enterprise_id', $selectEmpresas, Null, [
                'class'         =>'form-control',
                'placeholder'   => 'Seleccione Empresa',
                'v-on'          => 'change: enterpriseChanged',
                'v-model'       => 'enterprise']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Faenas que atiende</label>
        <div class="col-xs-4">
            {!! Form::select('operation_id', [], Null, [
                'class'         => 'form-control',
                'options'       => 'operations',
                'v-model'       => 'operation',
                'multiple'      => 'multiple',]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Tipo dispensador</label>
        <div class="col-xs-4">
            {!! Form::select('type_fuel_dispenser_id', $selectTipoDispensadores, Null, [
                'class'         =>'form-control',
                'placeholder'   => 'Seleccione tipo dispensador',
                'v-model'       => 'type_fuel_dispenser' ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2"></label>
        <div class="col-md-6">
            <div class="checkbox checkbox-primary">
                {!! Form::checkbox('truck', 1, false, [
                    'class'    => 'checkbox',
                    'v-on'     => 'click: muestraCamion']) !!}
                <label><strong>¿Es dispensador móvil (camión)?</strong></label>
            </div>
        </div>
    </div>
    <div class="form-group" v-if="show">
        <label class="control-label col-md-2">Equipo</label>
        <div class="col-md-2">
            {!! Form::select('dispenser_equipo_id', [], null, [
                'class'          => 'form-control',
                'v-model'        => 'equipment',
                'placeholder'    => 'Seleccione Equipo',
                'options'        => 'equipments', ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Nombre</label>
        <div class="col-xs-4">
            {!! Form::text('name', Null, [
                'class'    => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Capacidad (lts.)</label>
        <div class="col-xs-2">
            {!! Form::text('capacity', Null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Saldo inicial (lts.)</label>
        <div class="col-xs-2">
            {!! Form::text('beginning_balance', Null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2"></label>
        <div class="col-md-6">
            <div class="checkbox checkbox-primary">
                {!! Form::checkbox('internal', 1, false, [
                        'class'=>'checkbox' ]) !!}
                <label><strong>Dispensador propio</strong></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2"></label>
        <div class="col-xs-2">
            <div class="checkbox checkbox-primary">
                {!! Form::checkbox('administration', 1, false, ['class'=>'checkbox']) !!}
                <label><strong>Administra Serfocol</strong></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2"></label>
        <div class="col-xs-2">
            <div class="checkbox checkbox-primary">
                {!! Form::checkbox('require_counter', 1, false, ['class'=>'checkbox']) !!}
                <label><strong>Requiere contador</strong></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2"></label>
        <div class="col-xs-2">
            <div class="checkbox checkbox-primary">
                {!! Form::checkbox('require_liters', 1, false, ['class'=>'checkbox']) !!}
                <label><strong>Requiere litros</strong></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2"></label>
        <div class="col-xs-2">
            <div class="checkbox checkbox-primary">
                {!! Form::checkbox('active', 1, false, ['class'=>'checkbox']) !!}
                <label><strong>Activo</strong></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <p class="col-md-offset-2 col-md-10">
            <a href="{{ route('newdispensador.index') }}" class="btn btn-default">&larr; Cancelar</a>
            <button type="submit" class="btn btn-primary" value="save" name="task">Guardar y salir</button>
            <button type="submit" class="btn btn-primary" value="save_n_new" name="task">Guardar y continuar</button>
        </p>
    </div>
</dispensadorform>