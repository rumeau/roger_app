@extends('layout')
 @section('content')
<div class="page-header">
    <h1>Apertura de turnos <small></small></h1>
</div>
<turnosform inline-template >
    <div class="alert alert-@{{alertType}} alert-dismissible" role="alert" v-show="showAlert" v-html="message"></div>
{!! Form::open(['v-on' => 'submit: onSubmit' ,'route' => 'turnos.store', 'class' => 'form-horizontal']) !!}
    
    @include('turnos.form', ['chargeables' => null])
    
{!! Form::close() !!}
</turnosform>
@endsection