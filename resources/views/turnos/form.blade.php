
    <div>
        {!! Form::hidden('enterprise_id', current_enterprise()->id) !!}
    </div>
    <div class="form-group@{{ errors.operation_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Faena</label>
        <div class="col-md-2">
            {!! Form::select('operation_id', $selectFaenas, Null, [
                    'class'        => 'form-control',
                    'placeholder'  => 'Seleccione Faena',
                    'v-on'         => 'change: operationChanged',
                    'v-model'      => 'operation' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.operation_id" v-text="errors.operation_id"></p>
        <label class="control-label col-md-2">Fecha</label>
        <div class="col-md-2">
            {!! Form::date('load_date', Null, ['class'=>'form-control']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.load_date" v-text="errors.load_date"></p>
    </div>
    <div class="form-group@{{ errors.responsible_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Responsable</label>
        <div class="col-md-4">
            {!! Form::select('responsible_id', $selectResponsible, Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione responsable', 
                    'v-model'      => 'responsible' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.responsible_id" v-text="errors.responsible_id"></p>
    </div>
    <div class="form-group@{{ errors.work_shift_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Jornada</label>
        <div class="col-md-2">
            {!! Form::select('work_shift_id', [], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione Turno', 
                    'options'      => 'work_shifts', 
                    'v-model'      => 'work_shift']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.work_shift_id" v-text="errors.work_shift_id"></p>
    </div>
    <!--
    <div class="form-group">
        <label class="control-label col-md-2">Area productiva</label>
        <div class="col-md-4">
            {!! Form::select('productive_area_id', [], Null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione área productiva', 
                    'options'      => 'productive_areas', 
                    'v-model'      => 'productive_area']) !!}
        </div>
    </div>
    -->
    <div class="form-group">
        <label class="control-label col-md-2">Litros inicio</label>
        <div class="col-md-2">
            {!! Form::text('order_number', Null, [
                'class'       => 'form-control',
                'v-model'     => 'order_number' ]) !!}
        </div>
        <label class="control-label col-md-2">Litros término</label>
        <div class="col-md-2">
            {!! Form::text('order_number', Null, [
                'class'       => 'form-control',
                'v-model'     => 'order_number' ]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Contador inicio</label>
        <div class="col-md-2">
            {!! Form::text('order_number', Null, [
                'class'       => 'form-control',
                'v-model'     => 'order_number' ]) !!}
        </div>
        <label class="control-label col-md-2">Contador término</label>
        <div class="col-md-2">
            {!! Form::text('order_number', Null, [
                'class'       => 'form-control',
                'v-model'     => 'order_number' ]) !!}
        </div>
    </div>
    <div class="form-group@{{ errors.liters ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Litros total turno</label>
        <div class="col-xs-2">
            {!! Form::text('liters', Null, [
                    'class'    => 'form-control',
                    'v-model'  => 'liters' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.liters" v-text="errors.liters"></p>
    </div>
    <div class="form-group">
        <p class="col-md-offset-2 col-md-10">
            <a href="{{ route('turnos.index') }}" class="btn btn-default">&larr; Cancelar</a>
            <button type="submit" class="btn btn-primary" value="save" name="task">Iniciar turno y Salir</button>
            <button type="submit" class="btn btn-primary" value="save_n_new" name="task">Iniciar turno y Agregar consumos</button>
        </p>
    </div>