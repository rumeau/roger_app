    <turnosfilter inline-template  data-url="{{ route('turnos.index') }}">
        <form action="" class="form-horizontal">
        <fieldset>
            <legend>Filtros de Búsqueda</legend>
            
            <div class="pull-right">
                <a class="btn btn-primary" href="{{route('turnos.create')}}">+ Nuevo</a>
            </div>
            
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Fecha desde</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('date', null, [
                            'class'      => 'form-control datepicker',
                            'v-model'    => 'date_from' ]) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <label for="date" class="control-label col-md-2">Fecha hasta</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('invoiceDate', null, [
                            'class'      => 'form-control datepicker', 
                            'v-model'    => 'date_to' ]) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            
                <div class="pull-right">
                    <a class="btn btn-primary" href="#" v-on="click: export">Excel</a>
                    <a class="btn btn-primary" href="#" v-on="click: clearFilters">Limpiar filtros</a>
                    <a class="btn btn-primary" href="#" v-on="click: filterSearch">Buscar</a>
                </div>
           
        </fieldset>
        </form>
    </turnosfilter>