<dispensador-filter inline-template>
    <form action="" class="form-horizontal">
        <fieldset>
            <legend>Filtros de Búsqueda</legend>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{route('dispensadores.create')}}"><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
            </div>
            <div class="form-group">
                <label for="operation" class="control-label col-md-2">Faena</label>
                <div class="col-md-2">
                    {!! Form::select('operation_id', \App\Repositories\OperationRepository::forCharge($enterprise)->lists('name', 'id'), null, [
                        'class'        => 'form-control',
                        'placeholder'  => 'Seleccione Faena',
                        'v-model'      => 'operation_id' ]) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="date" class="control-label col-md-2">Fecha carga desde</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('date', null, [
                            'class'        => 'form-control',
                            'v-model'      => 'date_from',
                            'v-datepicker']) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <label for="date" class="control-label col-md-2">Fecha carga hasta</label>
                <div class="col-md-2">
                    <div class='input-group date datepicker'>
                        {!! Form::text('invoiceDate', null, [
                        'class'            => 'form-control',
                        'v-model'          => 'date_to',
                         'v-datepicker']) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="loaded_origin" class="control-label col-md-2">Dispensador origen</label>
                <div class="col-md-2">
                    {!! Form::select('loaded_origin', \App\Repositories\EnterpriseRepository::dispensersList($enterprise), null, [
                        'class'        => 'form-control',
                        'placeholder'  => 'Seleccione dispensador de origen',
                        'v-model'      => 'loaded_origin' ]) !!}
                </div>
                <label for="loaded_destination" class="control-label col-md-2">Dispensador destino</label>
                <div class="col-md-2">
                    {!! Form::select('loaded_destination', \App\Repositories\EnterpriseRepository::dispensersList($enterprise), null, [
                        'class'         => 'form-control',
                        'placeholder'   => 'Seleccione dispensador de destino',
                        'v-model'       => 'loaded_destination' ]) !!}
                </div>
                <label for="date" class="control-label col-md-2">Responsable</label>
                <div class="col-md-2">
                    {!! Form::select('responsible_id', \App\Repositories\EnterpriseRepository::responsiblesList($enterprise), null, [
                        'class'         => 'form-control',
                        'placeholder'   => 'Seleccione responsable',
                        'v-model'       => 'responsible' ]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="javascript:;" @click="exportExcel">Excel</a>
                        <a class="btn btn-primary" href="javascript:;" @click="clearFilters">Limpiar filtros</a>
                        <a class="btn btn-primary" href="javascript:;" @click="filterSearch">Buscar</a>
                    </div>
                </div>
            </<div>
        </fieldset>
    </form>
</dispensador-filter>