    <div class="form-group@{{ errors.operation_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Faena</label>
        <div class="col-md-4">
            {!! Form::select('operation_id', \App\Repositories\OperationRepository::forCharge($enterprise)->lists('name', 'id'), null, [
                    'class'        => 'form-control',
                    'placeholder'  => 'Seleccione Faena',
                    '@change'      => 'operationChanged',
                    'v-model'      => 'data.operation_id',
                    'disabled'     => in_array('operation_id', $readonly)]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.operation_id" v-text="errors.operation_id"></p>
    </div>

    <div class="form-group@{{ errors.load_date ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Fecha carga</label>
        <div class="col-md-2">
            {!! Form::text('load_date', null, ['class'=>'form-control', 'v-model' => 'data.load_date', 'v-datepicker']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.load_date" v-text="errors.load_date"></p>
    </div>
    <div class="form-group@{{ errors.responsible_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Responsable de la carga</label>
        <div class="col-md-4">
            {!! Form::select('responsible_id', \App\Repositories\EnterpriseRepository::responsiblesList($enterprise), null, [
                    'class'        => 'form-control', 
                    'placeholder'  => 'Seleccione responsable', 
                    'v-model'      => 'data.responsible_id' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.responsible_id" v-text="errors.responsible_id"></p>
    </div>
    <div class="form-group@{{ errors.work_shift_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Turno</label>
        <div class="col-md-2">
            <select name="work_shift_id" class="form-control" v-model="data.work_shift_id">
                <option>Seleccione Turno</option>
                <option v-for="option in work_shifts" v-bind:value="option.value">@{{ option.text }}</option>
            </select>
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.work_shift_id" v-text="errors.work_shift_id"></p>
    </div>
    <div class="form-group@{{ errors.order_number ? ' has-error' : '' }}">
        <label class="control-label col-md-2">N° Guía</label>
        <div class="col-md-2">
            {!! Form::text('order_number', null, ['class'=>'form-control onlynumbers', 'v-model' => 'data.order_number']) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.order_number" v-text="errors.order_number"></p>
    </div>
    <div class="form-group@{{ errors.loaded_origin_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Dispensador origen</label>
        <div class="col-md-4">
            <select name="loaded_origin_id" class="form-control" v-model="data.loaded_origin_id" @change="checkDestination" disabled="{{ in_array('loaded_origin_id', $readonly) ? 'true' : 'false' }}">
                <option value="">Seleccione dispensador de origen</option>
                <option v-for="option in dispenser_origins" v-bind:value="option.value">@{{ option.text }}</option>
            </select>
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.loaded_origin_id" v-text="errors.loaded_origin_id"></p>
    </div>
    <div class="form-group@{{ errors.loaded_destination_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Dispensador destino</label>
        <div class="col-md-4">
            <select name="loaded_destination_id" class="form-control" v-model="data.loaded_destination_id" disabled="{{ in_array('loaded_origin_id', $readonly) ? 'true' : 'false' }}">
                <option value="">Seleccione dispensador de destino</option>
                <option v-for="option in dispenser_destinations" v-bind:value="option.value">@{{ option.text }}</option>
            </select>
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.loaded_destination_id" v-text="errors.loaded_destination_id"></p>
    </div>
    <div class="form-group@{{ errors.operator_id ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Operador</label>
        <div class="col-md-4">
            <select name="operator_id" class="form-control" v-model="data.operator_id">
                <option value="">Seleccione operador</option>
                <option v-for="option in operators" v-bind:value="option.value">@{{ option.text }}</option>
            </select>
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.operator_id" v-text="errors.operator_id"></p>
    </div>
    
    <div class="form-group@{{ errors.counter ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Contador equipo dispensador origen</label>
        <div class="col-md-2">
            {!! Form::text('counter', null, [
                    'class'      => 'form-control onlynumbers',
                    'v-model'    => 'data.counter' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.counter" v-text="errors.counter"></p>
    </div>
    <div class="form-group@{{ errors.hour_meter ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Horómetro equipo</label>
        <div class="col-md-2">
            {!! Form::text('hour_meter', null, [
                    'class'      => 'form-control onlynumbers',
                    'v-model'    => 'data.hour_meter' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.hour_meter" v-text="errors.hour_meter"></p>
    </div>
    <div class="form-group@{{ errors.liters ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Litros</label>
        <div class="col-md-2">
            {!! Form::text('liters', null, [
                    'class'    => 'form-control onlynumbers',
                    'v-model'  => 'data.liters' ]) !!}
        </div>
        <p class="help-block col-md-10 col-md-offset-2" v-show="errors.liters" v-text="errors.liters"></p>
    </div>
    <div class="form-group">
        <p class="col-md-offset-2 col-md-10">
            <a href="{{ route('dispensadores.index') }}" class="btn btn-default">&larr; Cancelar</a>
            <button type="submit" class="btn btn-primary" value="save" name="task">Guardar y salir</button>
            <!--
            <button type="submit" class="btn btn-primary" value="save_n_new" name="task">Guardar y continuar</button>
            -->
        </p>
    </div>