@extends('layout')

@section('content')

    <div class="page-header">
        <h1>Cargas a dispensadores <small> > Editar</small></h1>
    </div>

    <dispensador-form inline-template edit="{{ route("dispensadores.edit", ['dispensadores' => $charge->id]) }}">
        {!! Form::open([
            '@submit.prevent' => 'onSubmit',
            'route'       => ['dispensadores.update', 'dispensadores' => $charge->id],
            'class'       => 'form-horizontal',
            'method'      => 'POST' ]) !!}

            @include('dispensadores.form', ['readonly' => ['operation_id', 'loaded_origin_id', 'loaded_destination_id']])

        {!! Form::close() !!}
    </dispensador-form>
 @endsection