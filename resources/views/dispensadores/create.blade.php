@extends('layout')

@section('content')
    <div class="page-header">
        <h1>Cargas a dispensadores <small> > Nuevo</small></h1>
    </div>

    <dispensador-form inline-template>
        <div class="alert alert-@{{alertType}} alert-dismissible" role="alert" v-show="showAlert" v-html="message" v-cloak></div>

        {!! Form::open(['@submit.prevent' => 'onSubmit' ,'route' => 'dispensadores.store', 'class' => 'form-horizontal', 'method' => 'POST']) !!}
            @include('dispensadores.form')
        {!! Form::close() !!}

    </dispensador-form>
@endsection