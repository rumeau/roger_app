@if (\Request::query('concepto') == 1)
    <div class="form-group{{ $errors->has('order_number') ? ' has-error' : '' }}">
        <label class="control-label col-md-2">N° Pedido</label>
        <div class="col-md-6">
            {!! Form::text('order_number', Null, ['class'=>'form-control']) !!}
        </div>
        @if ($errors->has('order_number'))
            {!! $errors->first('order_number', '<p class="help-block col-md-10 col-md-offset-2">:message</p>') !!}
        @endif
    </div>
    <div class="form-group{{ $errors->has('bill_number') ? ' has-error' : '' }}">
        <label class="control-label col-md-2">N° Factura</label>
        <div class="col-md-6">
            {!! Form::text('bill_number', Null, ['class'=>'form-control']) !!}
        </div>
        @if ($errors->has('bill_number'))
            {!! $errors->first('bill_number', '<p class="help-block col-md-10 col-md-offset-2">:message</p>') !!}
        @endif
    </div>
    <div class="form-group{{ $errors->has('origin') ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Proveedor</label>
        <div class="col-md-6">
            {!! Form::select('loaded_origin_id', [], Null, ['class'=>'form-control', 'placeholder' => 'Seleccione Proveedor', 'options'=>'proveedores', 'v-model' => 'proveedor']) !!}
        </div>
        @if ($errors->has('origin'))
            {!! $errors->first('origin', '<p class="help-block col-md-10 col-md-offset-2">:message</p>') !!}
        @endif
    </div>
    <div class="form-group{{ $errors->has('destination') ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Dispensador destino</label>
        <div class="col-md-6">
            {!! Form::select('loaded_destination_id', [], Null, [
                'class'       =>'form-control',
                'placeholder' => 'Seleccione dispensador de destino',
                'options'     => 'dispensadores',
                'v-model'     => 'dispensador']) !!}
        </div>
        @if ($errors->has('destination'))
            {!! $errors->first('destination', '<p class="help-block col-md-10 col-md-offset-2">:message</p>') !!}
        @endif
    </div>
    <div class="form-group{{ $errors->has('net_price') ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Precio Neto</label>
        <div class="col-md-6">
            {!! Form::text('net_price', Null, ['class'=>'form-control']) !!}
        </div>
        @if ($errors->has('net_price'))
            {!! $errors->first('net_price', '<p class="help-block col-md-10 col-md-offset-2">:message</p>') !!}
        @endif
    </div>
    
    <div class="form-group{{ $errors->has('fuel_tax') ? ' has-error' : '' }}">
        <label class="control-label col-md-2">IEC (monto $)</label>
        <div class="col-md-6">
            {!! Form::text('fuel_tax', Null, ['class'=>'form-control']) !!}
        </div>
        @if ($errors->has('fuel_tax'))
            {!! $errors->first('fuel_tax', '<p class="help-block col-md-10 col-md-offset-2">:message</p>') !!}
        @endif
    </div>

@elseif (\Request::query('concepto') == 2)
<!--
<div v-if="concepto=='1'"></div>-->
<!--<div v-if="concepto=='2'">-->
    <div class="form-group">
        <label class="control-label col-md-2">Turno</label>
        <div class="col-md-6">
            {!! Form::select('work_shift_id', [], Null, [
            'class'=>'form-control', 
            'placeholder' => 'Seleccione Turno', 
            'options'=>'turnos', 
            'v-model' => 'turno']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">N° Guía</label>
        <div class="col-md-6">
            {!! Form::text('order_number', Null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Dispensador origen</label>
        <div class="col-md-6">
            {!! Form::select('loaded_origin_id', [], Null, [
                'class'       =>'form-control',
                'placeholder' => 'Seleccione dispensador de origen',
                'options'     => 'dispensadores',
                'v-model'     => 'dispensador_orig']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Dispensador destino</label>
        <div class="col-md-6">
            {!! Form::select('loaded_destination_id', [], Null, [
                'class'       =>'form-control',
                'placeholder' => 'Seleccione dispensador de destino',
                'options'     => 'dispensadores_dest_options',
                'v-model'     => 'dispensador_dest']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Operador</label>
        <div class="col-md-6">
            {!! Form::select('operator_id', [], Null, [
                'class'       => 'form-control',
                'placeholder' => 'Seleccione operador',
                'options'     => 'operadores',
                'v-model'     => 'operador'])!!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Contador</label>
        <div class="col-md-6">
            {!! Form::text('counter', Null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Horómetro</label>
        <div class="col-md-6">
            {!! Form::text('hour_meter', Null, ['class'=>'form-control']) !!}
        </div>
    </div>
<!--</div>-->
@endif
@if (\Request::query('concepto') == 3)
<!--<div v-if="concepto=='3'">-->
    <div class="form-group">
        <label class="control-label col-md-2">Turno</label>
        <div class="col-md-6">
            {!! Form::select('work_shift_id', [], Null, [
            'class'=>'form-control', 
            'placeholder' => 'Seleccione Turno', 
            'options'=>'turnos', 
            'v-model' => 'turno']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Ubicación</label>
        <div class="col-md-6">
            {!! Form::select('ubication_id', [], Null, [
            'class'=>'form-control', 
            'placeholder' => 'Seleccione Ubicación', 
            'options'=>'ubicaciones', 
            'v-model' => 'ubicacion']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Dispensador</label>
        <div class="col-md-6">
            {!! Form::select('loaded_origin_id', [], Null, [
                'class'       =>'form-control',
                'placeholder' => 'Seleccione dispensador',
                'options'     => 'dispensadores',
                'v-model'     => 'dispensador_orig']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Equipo</label>
        <div class="col-md-6">
            {!! Form::select('loaded_destination_id', [], Null, [
                'class'       => 'form-control',
                'placeholder' => 'Seleccione equipo',
                'options'     => 'equipos',
                'v-model'     => 'equipo']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Operador</label>
        <div class="col-md-6">
            {!! Form::select('operator_id', [], Null, [
                'class'       => 'form-control',
                'placeholder' => 'Seleccione operador',
                'options'     => 'operadores',
                'v-model'     => 'operador'])!!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Contador</label>
        <div class="col-md-6">
            {!! Form::text('counter', Null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Horómetro</label>
        <div class="col-md-6">
            {!! Form::text('hour_meter', Null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>
@endif
<div v-if="concepto=='4'">
    
</div>
<div v-if="concepto==5">
    <div class="form-group">
        <label class="control-label col-md-2">Turno</label>
        <div class="col-md-6">
            {!! Form::select('work_shift_id', [], Null, [
            'class'=>'form-control', 
            'placeholder' => 'Seleccione Turno', 
            'options'=>'turnos', 
            'v-model' => 'turno']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">N° Guía</label>
        <div class="col-md-6">
            {!! Form::text('order_number', Null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Dispensador</label>
        <div class="col-md-6">
            {!! Form::select('loaded_origin_id', [], Null, [
                'class'       =>'form-control',
                'placeholder' => 'Seleccione dispensador',
                'options'     => 'dispensadores',
                'v-model'     => 'dispensador_orig',
                'v-attr'      => 'disabled=faena']) !!}
        </div>
    </div>
    <div class="form-group" v-show="muestraDiv">
        <label class="control-label col-md-2">Externo</label>
        <div class="col-md-6">
            {!! Form::select('loaded_destination_id', [], Null, [
                'class'       =>'form-control',
                'placeholder' => 'Seleccione empresa externa',
                'options'     => 'externos',
                'v-model'     => 'externo']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Contador</label>
        <div class="col-md-6">
            {!! Form::text('counter', Null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>
<div v-if="concepto=='6'">
    <div class="form-group">
        <label class="control-label col-md-2">Turno</label>
        <div class="col-md-6">
            {!! Form::select('work_shift_id', [], Null, [
            'class'=>'form-control', 
            'placeholder' => 'Seleccione Turno', 
            'options'=>'turnos', 
            'v-model' => 'turno']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">N° Guía</label>
        <div class="col-md-6">
            {!! Form::text('order_number', Null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Dispensador origen</label>
        <div class="col-md-6">
            {!! Form::select('loaded_origin_id', [], Null, [
                'class'       =>'form-control',
                'placeholder' => 'Seleccione dispensador de origen',
                'options'     => 'dispensadores',
                'v-model'     => 'dispensador_orig']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Dispensador destino</label>
        <div class="col-md-6">
            {!! Form::select('loaded_destination_id', [], Null, [
                'class'       =>'form-control',
                'placeholder' => 'Seleccione dispensador de destino',
                'options'     => 'dispensadores_dest_options',
                'v-model'     => 'dispensador_dest']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Contador</label>
        <div class="col-md-6">
            {!! Form::text('counter', Null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>