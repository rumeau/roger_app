
    <div class="form-group">
        <label class="control-label col-md-2">Faena</label>
        <div class="col-md-6">
    @if (\Request::query('concepto') == 1)
            {!! Form::select('operation_id', current_enterprise() != null ? current_enterprise()->operations()->where('to_buy', '=', 1)->get()->lists('name', 'id') : [], Null, [
                'class'=>'form-control',
                'placeholder' => 'Seleccione Faena',
                'v-on' => 'change: actualizaDropdownAlSeleccionarFaena',
                'v-model' => 'faena']) !!}
    @elseif (\Request::query('concepto') == 2 || \Request::query('concepto') == 3)
            {!! Form::select('operation_id', current_enterprise() != null ? current_enterprise()->operations()->where('external', '<>', 1)->get()->lists('name', 'id') : [], Null, [
                'class'=>'form-control',
                'placeholder' => 'Seleccione Faena',
                'v-on' => 'change: actualizaDropdownAlSeleccionarFaena',
                'v-model' => 'faena']) !!}
    
    @endif
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2">Concepto</label>
        <div class="col-md-6">
            {!! Form::select('concept_id', $selectConceptos, $selected = $selectConceptos, [
                'class'       => 'form-control',
                'v-model'     => 'concepto']) !!}
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('fechaCarga') ? ' has-error' : '' }}">
        <label class="control-label col-md-2">Fecha carga</label>
        <div class="col-xs-2">
            {!! Form::date('load_date', Null, ['class'=>'form-control']) !!}
        </div>
        @if ($errors->has('load_date'))
            {!! $errors->first('load_date', '<p class="help-block col-md-10 col-md-offset-2">:message</p>') !!}
        @endif
    </div>
    
    <!-- campos variables-->
    @include('charges.form-concept')
    
    <div class="form-group">
        <label class="control-label col-md-2">Litros</label>
        <div class="col-md-6">
            {!! Form::text('liters', Null, ['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>