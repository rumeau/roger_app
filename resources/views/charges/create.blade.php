@extends('layout')
 @section('content')
<div class="page-header">
    <h1>Combustible > <small> Compras y movimientos.</small></h1>
</div>

<form action="{{ route('chargeables.store') }}" method="post" class="form-horizontal">
    {!! csrf_field() !!}
    
    @include('charges.form')
</form>
 @endsection