<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HelpersTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testNumberCl()
    {
        $number = '11.111.111,11';
        $shouldBe = 11111111.11;
        $formatted = number_cl($number);
        
        $this->assertEquals($formatted, $shouldBe);
    }
}
