<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ModelChargeableTest extends TestCase
{
    use DatabaseTransactions;
    
    public function testValidationConcept1Form()
    {
        $this->visit('chargeables/create?concepto=1')
            ->select('8', 'operation_id')
            ->press('Guardar')
            ->shouldSee('Filtros de Búsqueda'); // asumiendo que guardo y volvio al index
    }
    
    public function testValidationConcept1()
    {
        $userData = [
            'concept_id' => 1,
            'operation_id' => '8',
            'net_price' => '376,56567',
            'liters' => 20,
            'fuel_tax' => '12,48654',
            'loaded_origin_id' => '3',
            'loaded_destination_id' => '1',
            'operator_id' => '6',
            'counter' => 1,
            'hour_meter' => 2,
            //...
            ];
            
        $user = \App\Models\User::find(6); // rrumeau

        $this->actingAs($user); //actuando como Roger
        
        $repo = new \App\Repositories\ChargeableRepository();
        $this->withSession(['user_enterprise' => 3]); // Paso la empresa en duro, para test
        
        $create = $repo->create($userData);
        
        //$this->seeInDatabase('chargeables', []); // probar que el campo creado exista en la base de datos
         
        $this->assertInstanceOf(\App\Models\Chargeable::class, $create);
        
    }
}
