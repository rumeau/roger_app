<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaPersons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // Tabla 'persons'
        Schema::create('persons', function (Blueprint $table) {
            $table->engine = 'InnoDb';

            $table->increments('id');
            $table->integer('operation_id')->unsigned();
            $table->integer('job_title_id')->unsigned()->nullable();
            $table->string('rut');
            $table->char('dv', 1);
            $table->string('name');
            $table->string('primary_last_name')->nullable();
            $table->string('second_last_name')->nullable();
            $table->integer('created_by')->unsigned();
            $table->timestamps();
        });

        Schema::table('persons', function (Blueprint $table) {
            $table->foreign('operation_id')->references('id')->on('operations');
            $table->foreign('job_title_id')->references('id')->on('job_titles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('persons');
    }
}
