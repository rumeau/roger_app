<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaOperationWorkShift extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // Tabla 'operation_work_shift'
        Schema::create('operation_work_shift', function (Blueprint $table) {
            $table->engine = 'InnoDb';

            $table->integer('operation_id')->unsigned();
            $table->integer('work_shift_id')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->timestamps();
        });

        Schema::table('operation_work_shift', function (Blueprint $table) {
            $table->foreign('operation_id')->references('id')->on('operations');
            $table->foreign('work_shift_id')->references('id')->on('work_shifts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('operation_work_shift');
    }
}
