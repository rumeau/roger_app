<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaEquipments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Tabla 'equipments'
        Schema::create('equipments', function (Blueprint $table) {
            $table->engine = 'InnoDb';

            $table->increments('id');
            $table->integer('operation_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->integer('brand_id')->unsigned();
            $table->integer('model_id')->unsigned();
            $table->integer('owner_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('type_identification_id')->unsigned();
            $table->integer('fuel_tank_capacity')->nullable();
            $table->string('cc');
            $table->string('patent');
            $table->string('serial_number')->nullable();
            $table->string('engine')->nullable();
            $table->integer('year')->unsigned()->nullable();
            $table->integer('lease');
            $table->integer('active')->default(1);
            $table->integer('fuel');
            $table->integer('dispenser')->default(0);
            $table->integer('created_by')->unsinged();
            $table->timestamps();
        });

        Schema::table('equipments', function (Blueprint $table) {
            $table->foreign('operation_id')->references('id')->on('operations');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('model_id')->references('id')->on('models');
            $table->foreign('owner_id')->references('id')->on('owners');
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->foreign('type_identification_id')->references('id')->on('id_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('equipments');
    }
}
