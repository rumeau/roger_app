<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaDispenserOperation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // Tabla 'dispenser_operation'
        Schema::create('dispenser_operation', function (Blueprint $table) {
            $table->engine = 'InnoDb';

            $table->integer('operation_id')->unsigned();
            $table->integer('fuel_dispenser_id')->unsigned();
        });

        Schema::table('dispenser_operations', function (Blueprint $table) {
            $table->foreign('operation_id')->references('id')->on('operations');
            $table->foreign('fuel_dispenser_id')->references('id')->on('fuel_dispensers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('dispenser_operation');
    }
}
