<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Tabla 'types'
        Schema::create('types', function (Blueprint $table) {
            $table->engine = 'InnoDb';

            $table->increments('id');
            $table->integer('family_id')->unsigned();
            $table->string('code');
            $table->string('name');
            $table->integer('created_by')->unsigned();

            $table->timestamps();
        });

        Schema::table('types', function (Blueprint $table) {
            $table->foreign('family_id')->references('id')->on('families');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('types');
    }
}
