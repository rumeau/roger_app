<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaOperations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // Tabla 'operations'
        Schema::create('operations', function (Blueprint $table) {
            $table->engine = 'InnoDb';

            $table->increments('id');
            $table->integer('enterprise_id')->unsigned();
            $table->integer('business_area');
            $table->string('name');
            $table->integer('external');
            $table->integer('supervisor_id')->unsigned(); // todo asigar referencia
            $table->integer('shift_id');
            $table->integer('active')->default(1);
            $table->integer('visible')->default(1);
            $table->integer('created_by')->unsigned();
            $table->timestamps();
        });

        Schema::table('operations', function (Blueprint $table) {
            $table->foreign('enterprise_id')->references('id')->on('enterprises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('operations');
    }
}
