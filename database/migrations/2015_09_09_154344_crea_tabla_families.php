<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaFamilies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Tabla 'categories'
        Schema::create('categories', function (Blueprint $table) {
            $table->engine = 'InnoDb';

            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->integer('created_by')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schemma::drop('categories');
    }
}
