<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaWorkShift extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // Tabla 'work_shift'
        Schema::create('work_shifts', function (Blueprint $table) {
            $table->engine = 'InnoDb';

            $table->increments('id');
            $table->string('code')->nullable();
            $table->string('name');
            $table->string('working_day')->nullable();
            $table->string('start_day')->nullable();
            $table->string('term_dey')->nullable();
            $table->time('start_hour')->nullable();
            $table->time('term_hour')->nullable();
            $table->integer('lunch_time')->nullable();
            $table->time('start_lunch_time')->nullable();
            $table->time('term_lunch_time')->nullable();
            $table->integer('created_by')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('work_shift');
    }
}
