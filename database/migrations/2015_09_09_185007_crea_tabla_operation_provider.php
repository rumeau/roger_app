<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaOperationProvider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Tabla 'operation_equipments'
        Schema::create('operation_provider', function (Blueprint $table) {
            $table->engine = 'InnoDb';

            $table->integer('operation_id')->unsigned();
            $table->integer('provider_id')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->timestamps();
        });

        Schema::table('operation_provider', function (Blueprint $table) {
            $table->foreign('operation_id')->references('id')->on('operations');
            $table->foreign('provider_id')->references('id')->on('providers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('operation_provider');
    }
}
