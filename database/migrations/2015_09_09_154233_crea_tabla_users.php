<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // Tabla 'users'
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDb';

            $table->increments('id');
            $table->integer('person_id')->unsigned();
            $table->string('login');
            $table->string('password');
            $table->integer('created_by')->unsigned();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('person_id')->references('id')->on('persons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
