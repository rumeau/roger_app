<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaFuelDispenser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // Tabla 'fuel_dispensers'
        Schema::create('fuel_dispensers', function (Blueprint $table) {
            $table->engine = 'InnoDb';
            
            $table->increments('id');
            $table->integer('ubication_id')->unsigned();
            $table->integer('type_fuel_dispenser_id')->unsigned();
            $table->integer('equipment_id')->unsigned();
            $table->string('name');
            $table->integer('capacity');
            $table->integer('internal');
            $table->integer('administration');
            $table->integer('require_counter');
            $table->integer('require_liters');
            $table->integer('created_by')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('fuel_dispensers');
    }
}
