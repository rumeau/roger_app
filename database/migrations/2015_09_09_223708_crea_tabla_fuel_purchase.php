<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaFuelPurchase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // Tabla 'fuel_purchase'
        Schema::create('fuel_purchases', function (Blueprint $table) {
            $table->engine = 'InnoDb';

            $table->increments('id');
            $table->integer('enterprise_id')->unsigned();
            $table->integer('operation_id')->unsigned();
            $table->integer('concept_id')->unsigned();
            $table->integer('provider_id')->unsigned();
            $table->integer('fuel_dispenser_id')->unsigned();
            $table->timestamp('load_date');
            $table->integer('order_number');
            $table->integer('bill_number')->nullable();
            $table->integer('liters');
            $table->decimal('net_price', 10, 5);
            $table->decimal('net_value', 10, 5);
            $table->decimal('fuel_tax', 10, 5);
            $table->decimal('tax', 10, 5);
            $table->decimal('total_amount', 10, 5);
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });

        Schema::table('fuel_purchases', function (Blueprint $table) {
            $table->foreign('enterprise_id')->references('id')->on('enterprises');
            $table->foreign('operation_id')->references('id')->on('operations');
            $table->foreign('concept_id')->references('id')->on('concepts');
            $table->foreign('provider_id')->references('id')->on('providers');
            $table->foreign('fuel_dispenser_id')->references('id')->on('fuel_dispenser');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('fuel_purchases');
    }
}
