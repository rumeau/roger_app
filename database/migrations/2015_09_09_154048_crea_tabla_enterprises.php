<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaEnterprises extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // Tabla 'enterprises'
        Schema::create('enterprises', function (Blueprint $table) {
            $table->engine = 'InnoDb';

            $table->increments('id');
            $table->integer('region_id')->unsigned();
            $table->string('name');
            $table->string('rut');
            $table->string('city');
            $table->string('address')->nullable();
            $table->integer('created_by')->unsigned();
            $table->timestamps();
        });

        Schema::table('enterprises', function (Blueprint $table) {
            $table->foreign('region_id')->references('id')->on('regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('enterprises');
    }
}
