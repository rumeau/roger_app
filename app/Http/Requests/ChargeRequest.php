<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChargeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'operation_id'          => 'required|exists:operation,id',
            'concept_id'            => 'required|exists:concept,id',
            'load_date'             => 'required|date',
            'loaded_origin_id'      => 'required',
            'loaded_destination_id' => 'required',
            'work_shift_id'         => 'required_if:concept_id,2,3,5,6|exists:work_shift,id',
            //'responsible_id'      => 'required|exists:person,id',
            'operator_id'         => 'required|exists:person,id',
            'ubication_id'          => 'required_if:concept_id,3,5|exists:locations,id',
            'order_number'          => 'required_if:concept_id,1,2,5,6',
            'bill_number'           => 'required_if:concept_id,1',
            'liters'                => 'required',
            'counter'               => 'required_if:concept_id,2,5,6',
            'hour_meter'            => 'required_if:concept_id,2,3',
            'net_price'             => 'required_if:concept_id,1',
            'fuel_tax'              => 'required_if:concept_id,1',
        ];

        if (user()->enterprises->count() > 1) {
            $rules['enterprise_id'] = 'required|exists:enterprise,id';
        }

        return $rules;
    }
}