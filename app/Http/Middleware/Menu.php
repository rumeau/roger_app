<?php

namespace App\Http\Middleware;

use Closure;

class Menu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Menu::make('SideBar', function($menu){
            if (\Auth::check()) { // primero valida que el usuario tiene sesion
                if  (in_array(user()->id, [7, 8])) {
                    //dd(user());
                    $menu->add('Consumos por equipo', 'consumption');
                    $menu->add('Salir', 'auth/logout');
                } elseif(user()->id == 9) {
                    $menu->add('Compras de combustible', 'compra');
                    $menu->add('Carga dispensadores', 'dispensadores');
                    $menu->add('Consumos por equipo', 'consumption');
                    $menu->add('Préstamos a externos', 'loan');
                    $menu->add('Salir', 'auth/logout');
                }else{
                    $menu->add('Inicio', '/');
                    $menu->add('Configuración', 'configuracion');
                    $menu->add('Administración', '#');
                    $menu->add('Turnos', 'turnos');
                    $menu->add('Compras de combustible', 'compra');
                    $menu->add('Carga dispensadores', 'dispensadores');
                    $menu->add('Consumos por equipo', 'consumption');
                    $menu->add('Préstamos a externos', 'loan');
                    $menu->add('Préstamos internos', 'loanintern');
                    $menu->add('Reportes de gestión', 'reportes');
                    $menu->add('Salir', 'auth/logout');
                }
            }
        });
        
        return $next($request);
    }
}
