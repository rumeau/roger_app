<?php

namespace App\Http\Middleware;

use Closure;

class ForceEnterpriseSelection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Session::has('user_enterprise')) {
            if (user()->enterprises->contains(\Session::get('user_enterprise'))) {
                view()->share('enterprise', current_enterprise());
                return $next($request);
            } else {
                \Session::remove('user_enterprise');
            }
        }

        // no hay empresa seleccionada aun

        // si solo tiene una empresa
        if (user()->enterprises->count() == 1) {
            // se selecciona automaticamente
            \Session::set('user_enterprise', user()->enterprises->first()->id);
            view()->share('enterprise', current_enterprise());
            return $next($request);
        } else {
            // Si tiene mas de una empresa, ir a seleccion de empresas
            return redirect(url('/select-enterprise'));
        }
    }
}
