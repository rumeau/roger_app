<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
*/
Route::get('/home', function ()  {
    return redirect('/');
});

//Route::get('/', 'HomeController@getIndex');
// Todo esto queda bajo el middleware 'auth'

// Route::get('/', function () {
//     return view('home.index');
    
//     //$personas = \App\Persona::all();
//     //$equipo = \App\Models\Equipo::find(1);
//     //$dispensador = \App\Models\Dispensador::find(1);

//     /*--------------------------------------------------*/
//     if ($elUserSeleccionoConcepto == 'carga') {
//         $origClass = '\App\Models\Dispensador';
//         $destClass = '\App\Models\Equipo';
//     } else {
//         // ...
//     }

//     // ... luego
//     $orig = $origClass::find($idSeleccionadoPorUserDeOrigen);
//     $dest = $destClass::find($idSeleccionadoPorUserDeDestino);
//     /*--------------------------------------------------*/
//     // ---> Todo eso lo puedes meter en un metodo aparte, en el modelo o en la clase que corresponda

//     $charge = new \App\Models\Chargeables();
//     //$charge->destination('dispensador_origen');
//     $charge->concept_id = 1;
//     // desde Chargeable, asigno origen y destino
//     // asi el codigo es mas entendible
//     //$charge->destination()->associate($equipo);
//     $charge->destination()->associate($dispensador); // igual sirve
//     //$charge->origin()->associate($dispensador);
//     $charge->origin()->associate($equipo); // igual sirve
//     // es necesario guardar los cambios
//     $charge->save();

//     //$charge->loaded_origin_id = 1;
//     //$charge->loaded_destination_id = 1;
//     //$charge->loaded_origin_type = 'App\Equipo';
//     //$charge->loaded_destination_type = 'App\Equipo';
//     //dd($charge);

//     //$equipo->chargeables()->save($charge);
//     $dispensador->chargeables('origen')->save($charge);
// });

// Rutas publicas
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group([
    'middleware' => ['auth']
], function() {

    Route::get('select-enterprise', 'Auth\AuthController@enterpriseSelect');
    Route::post('select-enterprise', 'Auth\AuthController@enterpriseSelected');

});

// Este grupo usa un middleware para proteger las rutas para que el usuario inicie sesion antes de acceder
Route::group([
    'middleware' => [
        'auth',
        'force.user.selection'
    ]
], function() {
    Route::get('/', function() {
        return view('home.index');
    });
    
    Route::resource('equipo', 'EquiposController'); //controlador para la adminisración de los equipos (máquinas)
    Route::resource('compra', 'PurchaseController'); //controlador para compra de combustible
    Route::post('compra/{id}/confirm', ['as' => 'compra.confirm', 'uses' => 'PurchaseController@confirm']);
    Route::resource('dispensadores', 'ChargeController'); //controlador para carga de dispensadores
    Route::resource('consumption', 'ConsumptionController'); //controlador para los consumos de los equipos
    Route::resource('loan', 'LoanController'); //controlador para prestamos de combustibles a externos
    Route::resource('loanintern', 'PrestamoInternoController'); //controlador para prestamos de combustibles a internos
    Route::post('loan/return', ['as' => 'loan.return', 'uses' => 'LoanController@return']);
    Route::resource('chargeables', 'ChargeablesController'); //controlador para relaciones polimorficas
    Route::resource('ubicacion', 'UbicacionesController'); //controlador para el mantenedor de ubicaciones (por faenea)
    Route::resource('configuracion', 'ConfiguracionController'); //controlador para el menu de configuración
    Route::resource('newdispensador', 'NewDispensadorController'); //controlador para el ingreso/mantenedor de dispensadores
    Route::resource('initdispensador', 'InitDispensadorController'); //controlador para iniciar saldo de dispensadores por faena
    Route::resource('empresa', 'EmpresasController');
    Route::resource('faena', 'FaenasController');
    Route::resource('catalogo', 'CatalogosController');
    Route::resource('ordentrabajo', 'OrdenTrabajoController');
    Route::resource('persona', 'PersonasController');
    Route::post('ordentrabajo/return', ['as' => 'ordentrabajo.return', 'uses' => 'LoanController@returnOt']);
    
    Route::resource('turnos', 'WorkShiftController');
    Route::get('reportes', 'ReportController@index');
    Route::get('reportes/faena', 'ReportController@operation');
    Route::get('reportes/equipo', 'ReportController@equipment');
    
    // Routes para AJAX
    Route::get('ajax/search-filters/work-shift-by-operation', 'Ajax\SearchFiltersController@workShiftByOperation');
    Route::get('ajax/search-filters/operations-by-enterprise', 'Ajax\SearchFiltersController@operationsByEnterprise');
    Route::get('ajax/search-filters/dispensers-by-enterprise', 'Ajax\SearchFiltersController@dispenserByEnterprise');
    Route::get('ajax/search-filters/equipment-by-enterprise', 'Ajax\SearchFiltersController@equipmentByEnterprise');
    Route::get('ajax/search-filters/types-by-family', 'Ajax\SearchFiltersController@typesByFamily');
    Route::get('ajax/search-filters/models-by-brand', 'Ajax\SearchFiltersController@modelsByBrands');
    Route::get('ajax/search-filters/provider-by-operation', 'Ajax\SearchFiltersController@providerByOperation');
    Route::get('ajax/search-filters/dispenser-by-operation', 'Ajax\SearchFiltersController@dispenserByOperation');
    Route::get('ajax/search-filters/dispenser-by-operation-tct', 'Ajax\SearchFiltersController@dispenserByOperationAddTct');
    Route::get('ajax/search-filters/equipment-by-operation', 'Ajax\SearchFiltersController@equipmentByOperation');
    Route::get('/ajax/search-filters/operators-by-operation', 'Ajax\SearchFiltersController@operatorsByOperation');
    Route::get('/ajax/search-filters/locations-by-operation', 'Ajax\SearchFiltersController@locationsByOperation');
    Route::get('/ajax/search-filters/external-by-operation', 'Ajax\SearchFiltersController@externalByOperation');
    Route::get('/ajax/search-filters/responsible-by-enterprise', 'Ajax\SearchFiltersController@responsibleByEnterprise');
    Route::get('ajax/search-filters/dispenser-by-operation_destination', 'Ajax\SearchFiltersController@dispenserByOperationDestination');
    Route::get('/ajax/search-filters/operations-by-enterprise-for-interloan', 'Ajax\SearchFiltersController@dispenserByOperationForInternLoan');
    
    Route::get('/ajax/search-filters/operations-by-enterprise-loan', 'Ajax\SearchFiltersController@operationsByEnterpriseLoan');
    Route::get('/ajax/search-filters/operations-by-enterprise-for-interloan-destiny', 'Ajax\SearchFiltersController@dispenserByOperationForInternLoanDestiny');
    
    //Route::get('ajax/search-filters/productiveAreas-by-operation', 'Ajax\SearchFiltersController@productiveAreaByOperation');
    
    Route::get('/ajax/search-filters/last-hour-meter-by-equipment', 'Ajax\SearchFiltersController@lastHourMeterByEquipment');
    Route::get('/ajax/load-components/{id}', 'Ajax\SearchFiltersController@componentsByResponsible');
    
    Route::get('ajax/search-filters/affected-by-enterprise', 'Ajax\SearchFiltersController@affectedByEnterprise');
    Route::get('/ajax/search-filters/level_2-by-level_1', 'Ajax\SearchFiltersController@level_2ByLevel_1');
    Route::get('/ajax/search-filters/level_3-by-level_2', 'Ajax\SearchFiltersController@level_3ByLevel_2');
    Route::get('/ajax/search-filters/level_4-by-level_3', 'Ajax\SearchFiltersController@level_4ByLevel_3');
    Route::get('/ajax/search-filters/level_5-by-level_4', 'Ajax\SearchFiltersController@level_5ByLevel_4');
    Route::get('/ajax/search-filters/level_6-by-level_5', 'Ajax\SearchFiltersController@level_6ByLevel_5');
    
    Route::get('/ajax/chargeables/{id}/confirm', 'Ajax\ChargeablesController@confirm');
    Route::get('/ajax/chargeables/fuelreturn', 'Ajax\ChargeablesController@fuelreturn');
});