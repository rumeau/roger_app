<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PrestamoInternoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $selectFaenas        = \App\Models\Faena::where('enterprise_id', '=', current_enterprise()->id)
                                    ->where('external', '=', 0)
                                    ->where('to_buy', '=', 1)
                                    ->where('active', '=', 1)
                                    ->lists('name', 'id');
        
        $selectEmpresaDestino = \App\Models\Empresa::lists('name', 'id');
        
        $selectFaenaDestino  = \App\Models\Faena::
                                    //where('enterprise_id', '=', current_enterprise()->id)
                                    where('external', '=', 0)
                                    ->where('to_buy', '=', 1)
                                    ->where('active', '=', 1)
                                    ->lists('name', 'id');
            
        $prestamos           = \App\Models\Chargeable::with('enterprise', 'operation', 'concept', 'provider', 'destination', 
                                                            'origin', 'dispenser', 'work_shift', 'returnLoan')
                                    ->where('concept_id', '=', 5)
                                    ->where('enterprise_id', '=', current_enterprise()->id);
                                    //->paginate(25);

        //$prestamos = $prestamos->paginate($request->query('pageSize', 25), ['*'], 'pageIndex');
        
        $selectTurnos        = \App\Models\Turno::join('operation_work_shift', 'work_shift.id', '=', 'operation_work_shift.work_shift_id')
                                    ->join('operation', 'operation.id', '=', 'operation_work_shift.operation_id')
                                    ->select('work_shift.name', 'work_shift.id')
                                    ->where('operation.enterprise_id', '=', current_enterprise()->id)
                                    ->where('work_shift.active', '=', 1)
                                    ->groupBy('work_shift.id')
                                    ->lists('name', 'id');
                            
        $selectDispensadores = \App\Models\Empresa::find(current_enterprise()->id)
                                    ->dispensers()
                                    ->where('active', '=', 1)
                                    ->lists('name', 'id');
                                    
        $selectExternas      = \App\Models\EmpresaExterna::where('enterprise_id', '=', current_enterprise()->id)
                                    ->where('active', '=', 1)
                                    ->lists('name', 'id');
                                
        $this->applySearchFilters($request, $prestamos);
        $prestamos = $prestamos->paginate($request->query('pageSize', 25), ['*'], 'pageIndex');

        if ($request->ajax()) {
            return [
                'data'          => $prestamos->getCollection(),
                'itemsCount'    => $prestamos->count()
            ];
        }

        return view('loanintern.index')->with(array(
            "selectFaenas"          => $selectFaenas,
            "selectEmpresaDestino"  => $selectEmpresaDestino,
            "selectFaenaDestino"    => $selectFaenaDestino,
            "selectTurnos"          => $selectTurnos,
            "selectDispensadores"   => $selectDispensadores,
            "selectExternas"        => $selectExternas,
		    "prestamos"             => $prestamos
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $selectFaenas        = \App\Models\Faena::where('enterprise_id', '=', current_enterprise()->id)
                                    ->where('external', '=', 0)
                                    ->where('to_buy', '=', 1)
                                    ->where('active', '=', 1)
                                    ->lists('name', 'id');
                                    
        $selectEmpresas      = \App\Models\Empresa::where('id', '<>', 6)->lists('name', 'id');
                                    
        $selectResponsables  = \App\Models\Empresa::find(current_enterprise()->id)
                                    ->operator()
                                    ->where('its_responsible', '=', 1)
                                    ->where('person.active', '=', 1)
                                    ->get([\DB::raw("CONCAT(`name`,' ',`primary_last_name`,' ',`second_last_name`) as fullName"), 'person.id'])
                                    ->lists('fullName', 'id');
                                    
        $selectConceptos     = \App\Models\Concepto::where('id', '=', 5)->first();
        
        return view('loanintern.create')->with([
            'selectEmpresas'        => $selectEmpresas,
            'selectFaenas'          => $selectFaenas,
            'selectResponsables'    => $selectResponsables,
            'selectConceptos'       => $selectConceptos
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    
    protected function applySearchFilters(Request $request, &$query)
    {
        if ($request->has('operation')) {
            $query->where('operation_id', '=', $request->input('operation'));
        }
        
        if ($request->has('work_shift')) {
            $query->where('work_shift_id', '=', $request->input('work_shift'));
        }
        
        if ($request->has('date_from') && $request->has('date_to')) {
            $from = Carbon::createFromFormat('d-m-Y', $request->input('date_from'))->hour(0)->minute(0)->second(0);
            $to   = Carbon::createFromFormat('d-m-Y', $request->input('date_to'))->hour(23)->minute(59)->second(59);
            $query->where(function($q) use ($from, $to) {
                $q->whereDate('load_date', '>=', $from)
                    ->whereDate('load_date', '<=', $to);
            });
        } elseif ($request->has('date_from') && !$request->has('date_to')) {
            $from = Carbon::createFromFormat('d-m-Y', $request->input('date_from'))->hour(0)->minute(0)->second(0);
            $query->whereDate('load_date', '>=', $from);
        } elseif (!$request->has('date_from') && $request->has('date_to')) {
            $to   = Carbon::createFromFormat('d-m-Y', $request->input('date_to'))->hour(23)->minute(59)->second(59);
            $query->whereDate('load_date', '<=', $to);
        }
        
        if ($request->has('order_number')) {
            $query->where('order_number', '=', $request->input('order_number'));
        }
        
        if ($request->has('dispenser')) {
            $query->where('loaded_origin_id', '=', $request->input('dispenser'));
        }
        
        if ($request->has('external')) {
            $query->where('loaded_destination_id', '=', $request->input('external'));
        }
        
        // Sorting
        if ($request->has('sortField')) {
            $sortField = $request->input('sortField', 'load_date');
            switch ($sortField) {
                case 'operation' :
                    $sortField = 'operation.name';
                    $query->with(['operation' => function($q) use ($request) {
                        $q->orderBy('name', $request->input('sortOrder', 'asc'));
                    }]);
                    break;
                    
                case 'loaded_origin':
                case 'loaded_destination':
                    $sortField = 'chargeables.' . $sortField . '_id';
                    $query->orderBy($sortField, $request->input('sortOrder', 'asc'));
                    break;
                    
                default:
                    $sortField = 'chargeables.' . $sortField;
                    $query->orderBy($sortField, $request->input('sortOrder', 'asc'));
                    break;
            }
        }
        
        return $query;
    }
}
