<?php
namespace App\Http\Controllers;

use App\Events\ChargeReturned;
use App\Events\ChargeReturnedMoney;
use App\Http\Requests;
use App\Models\Chargeable;
use App\Models\DevolucionPrestamo;
use App\Models\Dispensador;
use App\Repositories\ChargeableRepository;
use App\Repositories\EnterpriseRepository;
use Carbon\Carbon;
use Flash;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class LoanController extends Controller
{
    protected $enterprise;

    /**
     * PrestamoController constructor.
     */
    public function __construct()
    {
        $this->enterprise = current_enterprise();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param ChargeableRepository $chargeableRepository
     * @return array|bool|\Illuminate\View\View
     */
    public function index(Request $request, ChargeableRepository $chargeableRepository)
    {
        $prestamos = $chargeableRepository->getLoansByEnterpriseJoined($this->enterprise);
        $this->applySearchFilters($request, $prestamos);
        
        if ($request->query('export', false) == '1') {
            $this->exportExcel($prestamos);
            return true;
        }
        
        $prestamos->select([
            'chargeables.id as id',
            'chargeables.returns_id as returns_id',
            'operation.name as operation',
            'chargeables.load_date as load_date',
            'work_shift.code as work_shift',
            'chargeables.order_number as order_number',
            'fuel_dispenser.name as dispenser',
            'external_enterprise.name as external_enterprise',
            'chargeables.liters as liters',
            'return_type.name as return_type'
        ]);
        $paginator = $prestamos->paginate($request->query('pageSize', 25), ['*'], 'pageIndex');

        if ($request->ajax()) {
            return [
                'data'          => $paginator->items(),
                'itemsCount'    => $paginator->total()
            ];
        }

        return view('loan.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('loan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @param ChargeableRepository $chargeableRepository
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request, ChargeableRepository $chargeableRepository)
    {
        $this->validate($request, $this->rules(), [
            'loaded_origin_id.required'             => 'El campo Dispensador origen es obligatorio.',
            'loaded_destination_id.required'        => 'El campo Externo es obligatorio.',
            'load_date.required'                    => 'El campo Fecha préstamo es obligatorio.',
            'order_number.required'                 => 'El campo Nº de guía es obligatorio.' ]);
        
        //CAMPOS QUE VIENEN DIRECTAMENTE DEL FORMULARIO
        $data = $request->only(
            'operation_id',
            'load_date',
            'work_shift_id',
            'order_number',
            'ubication_id',
            'loaded_origin_id',
            'loaded_destination_id',
            'responsible_id',
            'counter',
            'liters'
        );

        $data['enterprise_id'] = $this->enterprise->id;
        $data['concept_id'] = Chargeable::CONCEPTO_PRESTAMO;

        /**
         * @var Dispensador $origin
         */
        $origin = EnterpriseRepository::dispensers($this->enterprise)
            ->where('fuel_dispenser.id', $data['loaded_origin_id'])
            ->first();
        $liters = raw_number($data['liters']);

        if ($liters > $origin->current_balance) {
            return response()->json([
                'title' => 'Error!',
                'message' => 'Esta prestando mas litros de los disponibles en el dispensador',
            ], 400);
        }

        $chargeableRepository->createLoan($data);

        Flash::success('El préstamo ha sido registrada');
        
        if ($request->input('task', 'save_n_new') == 'save_n_new') {
            // crear otro
            return response()->json([
                'message'       => 'El préstamo ha sido registrada',
                'success'       => true,
                'status'        => 'success',
            ]);
        } elseif ($request->input('task', 'save_n_new') == 'save') {
            // volver al index luego de guardar
            //return redirect(route('loan.index'));
            
            return response()->json([
                'message'       => 'El préstamo ha sido registrada',
                'success'       => true,
                'status'        => 'success',
                'redirect'      => route('loan.index'),
                ]);
                
        }

        return [];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @param ChargeableRepository $chargeableRepository
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\View\View
     */
    public function edit(Request $request, $id, ChargeableRepository $chargeableRepository)
    {
        $loan = $chargeableRepository->findLoanOrFail($id);

        if ($request->ajax()) {
            return $loan;
        }

        return view('loan.edit', ['loan' => $loan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @param ChargeableRepository $chargeableRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id, ChargeableRepository $chargeableRepository)
    {
        $loan = $chargeableRepository->findLoanOrFail($id);

        $this->validate($request, $this->rules(), [
            'loaded_origin_id.required'             => 'El campo Dispensador origen es obligatorio.',
            'loaded_destination_id.required'        => 'El campo Externo es obligatorio.',
            'load_date.required'                    => 'El campo Fecha préstamo es obligatorio.',
            'order_number.required'                 => 'El campo Nº de guía es obligatorio.' ]);

        //CAMPOS QUE VIENEN DIRECTAMENTE DEL FORMULARIO
        $data = $request->only(
            //'operation_id',
            'load_date',
            'work_shift_id',
            'order_number',
            //'ubication_id',
            //'loaded_origin_id',
            //'loaded_destination_id',
            'responsible_id',
            'counter',
            'liters'
        );

        $origin         = $loan->origin;
        $originalLiters = raw_number($loan->liters);
        $newLiters      = raw_number($data['liters']);
        $diff           = $originalLiters - $newLiters;
        // Sobran litros (devolver)
        if ($diff > 0)
        {
            // si el origen no puede recibir los litros de vuelta
            if (($diff + $origin->current_balance) > $origin->capacity) {
                // todo revisar esto, si estan devolviendo, no es logico rechazar la devolucion por que no cabe en mi dispensador para almacenarla a si?
                return response()->json([
                    'title'     => 'Error!',
                    'message'   => 'Se estan intentando devolver mas litros de los que el dispensador de origen puede volver a recibir',
                ], 400);
            }
        }
        // faltan litros (sacar mas del dispensador)
        elseif ($diff < 0)
        {
            // Si el dispensador no tiene disponible los litros que se intentan sacar
            if (-$diff > $origin->current_balance)
            {
                return response()->json([
                    'title' => 'Error!',
                    'message' => 'Esta intentando prestar mas litros que los que el dispensador tiene disponible',
                ], 400);
            }
        }

        $chargeableRepository->updateLoan($loan, $data);

        Flash::success('El préstamo ha sido actualizado');

        return response()->json([
            'message'       => 'El préstamo ha sido actualizado',
            'success'       => true,
            'status'        => 'success',
            'redirect'      => route('loan.index'),
        ]);
    }

    /**
     * Devolucion de prestamo
     *
     * @param Request $request
     * @param ChargeableRepository $chargeableRepository
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function return(Request $request, ChargeableRepository $chargeableRepository)
    {
        $loanIdsData    = $request->input('loans', []);
        $loanIds        = array_pluck($loanIdsData, 'id');
        $loans          = Chargeable::whereIn('id', $loanIds)->get();
        if (count($loanIds) != count($loans)) {
            return response()->json([
                'title' => 'Error!',
                'message' => 'Llamada inválida!',
            ], 400);
        }

        $data = $request->only([
            'isAmount',
            'amount',
        ]);

        $return = $chargeableRepository->returnLoans($loans, $data);

        return response()->json([
            'success' => true,
            'title' => 'Éxito!',
            'message' => 'La devolución ha sido registrada',
            'returnName' => $return->returnType->name,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        //
    }

    public function rules()
    {
        $rules = [
            'operation_id'          => 'required|exists:operation,id',
            'load_date'             => 'required|date_format:d/m/Y',
            'work_shift_id'         => 'required|exists:work_shift,id',
            'order_number'          => 'required',
            //'ubication_id'          => 'required',
            'loaded_origin_id'      => 'required',
            'loaded_destination_id' => 'required',
            'responsible_id'        => 'required',
            //'operator_id'         => 'required|exists:person,id',
            'counter'               => 'required',
            'liters'                => 'required',
        ];
        return $rules;
    }

    /**
     * @param Request $request
     * @param Builder|\Illuminate\Database\Query\Builder $query
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    protected function applySearchFilters(Request $request, &$query)
    {
        if ($request->input('operation', '') != '') {
            $query->where('chargeables.operation_id', '=', $request->input('operation'));
        }
        
        if ($request->input('work_shift', '') != '') {
            $query->where('chargeables.work_shift_id', '=', $request->input('work_shift'));
        }
        
        if ($request->input('date_from', '') != '' && $request->input('date_to', '') != '') {
            $from = Carbon::createFromFormat('d/m/Y', $request->input('date_from'))->format('Y-m-d');
            $to   = Carbon::createFromFormat('d/m/Y', $request->input('date_to'))->format('Y-m-d');
            $query->where(function($q) use ($from, $to) {
                /**
                 * @var \Illuminate\Database\Query\Builder $q
                 */
                $q->whereDate('chargeables.load_date', '>=', $from)
                    ->whereDate('chargeables.load_date', '<=', $to);
            });
        } elseif ($request->input('date_from', '') != '' && $request->input('date_to', '') == '') {
            $from = Carbon::createFromFormat('d/m/Y', $request->input('date_from'))->format('Y-m-d');
            $query->whereDate('chargeables.load_date', '>=', $from);
        } elseif ($request->input('date_from', '') == '' && $request->input('date_to', '') != '') {
            $to   = Carbon::createFromFormat('d/m/Y', $request->input('date_to'))->format('Y-m-d');
            $query->whereDate('chargeables.load_date', '<=', $to);
        }
        
        if ($request->input('order_number', '') != '') {
            $query->where('chargeables.order_number', '=', $request->input('order_number'));
        }
        
        if ($request->input('dispenser', '') != '') {
            $query->where('chargeables.loaded_origin_id', '=', $request->input('dispenser'));
        }
        
        if ($request->input('external', '') != '') {
            $query->where('chargeables.loaded_destination_id', '=', $request->input('external'));
        }
        
        // Sorting
        if ($request->input('sortField', '') != '') {
            $sortField = $request->input('sortField', 'load_date');
            switch ($sortField) {
                case 'operation' :
                    $query->orderBy('operation.name', $request->input('sortOrder', 'asc'));
                    break;

                case 'work_shift':
                    $query->orderBy('work_shift.code', $request->input('sortOrder', 'asc'));
                    break;

                case 'dispenser':
                    $query->orderBy('fuel_dispenser.name', $request->input('sortOrder', 'asc'));
                    break;

                case 'external_enterprise':
                    $query->orderBy('external_enterprise.name', $request->input('sortOrder', 'asc'));
                    break;

                case 'return_type':
                    $query->orderBy('return_type.name', $request->input('sortOrder', 'asc'));
                    break;

                case 'returns_id':
                    $query->orderBy('chargeables.returns_id', $request->input('sortOrder', 'asc'));
                    break;
                    
                default:
                    $sortField = 'chargeables.' . $sortField;
                    $query->orderBy($sortField, $request->input('sortOrder', 'asc'));
                    break;
            }
        }
        
        return $query;
    }

    /**
     * @param Builder|\Illuminate\Database\Query\Builder $prestamos
     */
    private function exportExcel($prestamos)
    {
        $prestamos->select([
            \DB::raw('CASE COALESCE(chargeables.returns_id, 0) WHEN 0 THEN "No" ELSE "Si" END as return_name'),
            \DB::raw('return_type.name as return_type'),
            'operation.name as operation',
            \DB::raw('chargeables.load_date as load_date'),
            'work_shift.code as work_shift',
            \DB::raw('chargeables.order_number as order_number'),
            'fuel_dispenser.name as dispenser',
            \DB::raw('external_enterprise.name as external_enterprise'),
            'chargeables.liters as liters',
        ]);

        $items = collect();
        foreach ($prestamos->get() as $item) {
            $liters = str_replace('.', '', $item->liters);
            $liters = str_replace(',', '.', $liters);
            $new = [
                'Registra Devolución'   => $item->return_name,
                'Tipo de Devolución'    => $item->return_type,
                'Faena'                 => $item->operation,
                'Fecha Préstamo'        => $item->load_date->format('d-m-Y'),
                'Turno'                 => $item->work_shift,
                'Nº Guía'               => $item->order_number,
                'Dispensador'           => $item->dispenser,
                'Empresa Externa'       => $item->external_enterprise,
                'Litros'                => number_format($liters, 0, ',', '.'),
            ];

            $items->push($new);
        }

        $this->export('Consulta_prestamosexternos_', $items);
    }
}