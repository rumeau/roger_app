<?php
namespace App\Http\Controllers;

use App\Models\Chargeable;
use App\Models\Dispensador;
use App\Models\Empresa;
use App\Models\Equipo;
use App\Repositories\ChargeableRepository;
use App\Repositories\EnterpriseRepository;
use Carbon\Carbon;
use Flash;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ChargeController extends Controller
{
    /**
     * @var Empresa
     */
    protected $enterprise;

    public function __construct()
    {
        $this->enterprise = current_enterprise();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param ChargeableRepository $chargeableRepository
     * @return array|bool|\Illuminate\View\View
     */
    public function index(Request $request, ChargeableRepository $chargeableRepository)
    {
        $charges = $chargeableRepository->getChargesByEnterpriseJoined($this->enterprise);
        $this->applySearchFilters($request, $cargasdisp);
        /**
         * Compras de combustible realizadas (concept_id = 1, es por compra),
         * a través de la empresa seleccionada
         */
        if ($request->query('export', 0) == '1') {
            $this->exportExcel($cargasdisp);
            return true;
        }

        $charges->select([
            'chargeables.id as id',
            'operation.name as operation',
            'origin.name as origin',
            'destination.name as destination',
            'chargeables.load_date as load_date',
            'chargeables.liters as liters',
            \DB::raw('CONCAT(person.primary_last_name, \' \', person.second_last_name, \', \', person.name) as responsible'),
        ]);

        $paginator = $charges->paginate($request->query('pageSize', 25), ['*'], 'pageIndex');
        
        if ($request->ajax()) {
            return [
                'data'       => $paginator->items(),
                'itemsCount' => $paginator->total()
            ];
        }
        
        return view('dispensadores.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return $this|array
     */
    public function create()
    {
        // Devuelve el formulario para crear
        return view('dispensadores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param ChargeableRepository $chargeableRepository
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function store(Request $request, ChargeableRepository $chargeableRepository)
    {
        $this->validate($request, $this->rules(), [
            'loaded_origin_id.required'             => 'El campo Dispensador origen es obligatorio.',
            'loaded_destination_id.required'        => 'El campo Dispensador destino es obligatorio.',
            'load_date.required'                    => 'El campo Fecha carga es obligatorio.' ]);
        
        $data = $request->only(
            'operation_id',
            'load_date',
            'order_number',
            'loaded_origin_id',
            'loaded_destination_id',
            'operator_id',
            'responsible_id',
            'work_shift_id',
            'liters',
            'counter',
            'hour_meter'
        );

        /**
         * @var Dispensador $origin
         * @var Dispensador  $destination
         */
        $origin = EnterpriseRepository::dispensers($this->enterprise)
            ->where('fuel_dispenser.id', $data['loaded_origin_id'])
            ->first();
        $destination = EnterpriseRepository::dispensers($this->enterprise)
            ->where('fuel_dispenser.id', $data['loaded_destination_id'])
            ->first();

        if (!$origin instanceof Dispensador || $origin->current_balance < raw_number($data['liters'])) {
            return response()->json([
                'title' => 'Error!',
                'message' => 'Esta cargando mas litros de los disponibles en el dispensador',
            ], 400);
        }

        if (!empty($destination->capacity) && ($destination->capacity - $destination->current_balance) < raw_number($data['liters'])) {
            return response()->json([
                'title' => 'Error!',
                'message' => 'El equipo que esta cargando no tiene capacidad suficiente para recibir esta cantidad de carga.',
            ], 400);
        }

        $data['enterprise_id'] = $this->enterprise->id;
        $data['concept_id'] = Chargeable::CONCEPTO_CARGA;
        $data['created_by'] = user()->id;
        $data['created_at'] = time();

        //$loadDispensador = \App\Models\CargaDispensador::create($data);
        $chargeableRepository->createCharge($data);

        if ($request->input('task', 'save') == 'save_n_new') {
            // crear otro
            return response()->json([
                'message'       => 'La carga ha sido registrada',
                'success'       => true,
                'status'        => 'success',
                ]);
        } elseif ($request->input('task', 'save') == 'save') {
            // volver al index luego de guardar
            return response()->json([
                'message'       => 'La carga ha sido registrada',
                'success'       => true,
                'status'        => 'success',
                'redirect'      => route('dispensadores.index'),
            ]);
        }

        return [];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @param ChargeableRepository $chargeableRepository
     * @return Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\View\View
     */
    public function edit(Request $request, $id, ChargeableRepository $chargeableRepository)
    {
        $charge = $chargeableRepository->findChargeOrFail($id);

        if ($request->ajax()){
            return $charge;
        }

        // Devuelve el formulario para crear
        return view('dispensadores.edit', [
            'charge' => $charge,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @param ChargeableRepository $chargeableRepository
     * @return \Illuminate\Http\JsonResponse
     * @todo se pueden actualizar todos los campos?
     */
    public function update(Request $request, $id, ChargeableRepository $chargeableRepository)
    {
        /**
         * @var Chargeable $charge
         */
        $charge = $chargeableRepository->findChargeOrFail($id);

        $this->validate($request, $this->rules(), [
            'loaded_origin_id.required'             => 'El campo Dispensador origen es obligatorio.',
            'loaded_destination_id.required'        => 'El campo Dispensador destino es obligatorio.',
            'load_date.required'                    => 'El campo Fecha carga es obligatorio.' ]);
        
        $data = $request->only(
            //'operation_id',
            'load_date',
            'order_number',
            //'loaded_origin_id',
            //'loaded_destination_id',
            'operator_id',
            'responsible_id',
            'work_shift_id',
            'liters',
            'counter',
            'hour_meter'
        );

        $origin         = $charge->origin;
        $destination    = $charge->destination;
        $originalLiters = raw_number($charge->liters);
        $newLiters      = $data['liters'];
        $diff           = $newLiters - $originalLiters;
        if ($diff > 0) {
            if ($diff > $origin->current_balance) {
                return response()->json(['Status' => 'ERROR', 'title' => 'Error!', 'message' => 'Esta cargando mas litros de los disponibles en el dispensador',], 400);
            } elseif ($diff > ($destination->capacity - $destination->current_balance)) {
                return response()->json(['Status' => 'ERROR', 'title' => 'Error!', 'message' => 'Esta cargando mas litros de los que el dispensador de destino puede recibir',], 400);
            }
        } elseif ($diff < 0) {
            if (-$diff > $destination->current_balance) {
                return response()->json(['Status' => 'ERROR', 'title' => 'Error!', 'message' => 'Esta devolviendo mas litros de los que el dispensador de destino puede devolver',], 400);
            } elseif ((-$diff + $origin->current_balance) > $origin->capacity) {
                return response()->json(['Status' => 'ERROR', 'title' => 'Error!', 'message' => 'Esta devolviendo mas litros de los que el dispensador de origen puede recibir',], 400);
            }
        }

        $chargeableRepository->updateCharge($charge, $data);

        Flash::success('El registro ha sido modificado.');

        return response()->json([
            'status'     => 'success',
            'success'    => true,
            'message'    => 'El registro ha sido modificado.',
            'redirect'   => route('dispensadores.index'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param ChargeableRepository $chargeableRepository
     * @return \Illuminate\Http\JsonResponse
     * @throws void
     */
    public function destroy($id, ChargeableRepository $chargeableRepository)
    {
        /**
         * @var Chargeable $charge
         */
        $charge = $chargeableRepository->findChargeOrFail($id);

        $this->authorize('destroy', $charge);

        $origin         = $charge->origin;
        // Valida que exista la cantidad a devolver en el destino
        $destination    = $charge->destination;
        if ($destination->current_balance < raw_number($charge->liters)) {
            return response()->json(['Status' => 'ERROR', 'title' => 'Error', 'message' => 'El balance actual del dispensador de destino es insuficiente para restaurarla al origen'], 400);
        }

        $liters = raw_number($charge->liters);
        if (($liters + $origin->current_balance) > $origin->capacity) {
            return response()->json(['Status' => 'ERROR', 'title' => 'Error!', 'message' => 'Esta devolviendo mas litros de los que el dispensador de origen puede recibir',], 400);
        }

        $return = $chargeableRepository->deleteCharge($charge);

        if ($return) {
            return response()->json(['Status' => 'OK']);
        } else {
            return response()->json(['Status' => 'ERROR', 'title' => 'Error', 'message' => 'No se ha podido eliminar esta compra'], 400);
        }
    }

    /**
     * @param Request $request
     * @param Builder|\Illuminate\Database\Query\Builder $query
     * @return mixed
     */
    protected function applySearchFilters(Request $request, &$query)
    {
        if ($request->input('operation_id', '') != '') {
            $query->where('chargeables.operation_id', '=', $request->input('operation_id'));
        }

        /**
         * @var int $from
         * @var int $to
         */
        if ($request->input('date_from', '') != '' && $request->input('date_to', '') != '') {
            $from = Carbon::createFromFormat('d/m/Y', $request->input('date_from'))->format('Y-m-d');
            $to   = Carbon::createFromFormat('d/m/Y', $request->input('date_to'))->format('Y-m-d');
            $query->where(function($q) use ($from, $to) {
                /**
                 * @var \Illuminate\Database\Query\Builder $q
                 */
                $q->whereDate('chargeables.load_date', '>=', $from)
                    ->whereDate('chargeables.load_date', '<=', $to);
            });
        } elseif ($request->input('date_from', '') != '' && $request->input('date_to', '') == '') {
            $from = Carbon::createFromFormat('d/m/Y', $request->input('date_from'))->format('Y-m-d');
            $query->whereDate('chargeables.load_date', '>=', $from);
        } elseif ($request->input('date_from', '') == '' && $request->input('date_to', '') != '') {
            $to   = Carbon::createFromFormat('d/m/Y', $request->input('date_to'))->format('Y-m-d');
            $query->whereDate('chargeables.load_date', '<=', $to);
        }
        
        if ($request->input('order_number', '') != '') {
            $query->where('chargeables.order_number', '=', $request->input('order_number'));
        }
        
        if ($request->input('loaded_origin', '') != '') {
            $query->where('chargeables.loaded_origin_id', '=', $request->input('loaded_origin'));
        }
        
        if ($request->input('loaded_destination', '') != '') {
            $query->where('chargeables.loaded_destination_id', '=', $request->input('loaded_destination'));
        }
        
        if ($request->input('responsible', '') != '') {
            $query->where('chargeables.responsible_id', '=', $request->input('responsible'));
        }
        
        // Sorting
        if ($request->input('sortField', '') != '') {
            $sortField = $request->input('sortField', 'load_date');
            switch ($sortField) {
                case 'operation' :
                    $query->orderBy('operation.name', $request->input('sortOrder', 'asc'));
                    break;
                    
                case 'origin':
                    $query->orderBy('origin.name', $request->input('sortOrder', 'asc'));
                    break;

                case 'destination':
                    $query->orderBy('destination.name', $request->input('sortOrder', 'asc'));
                    break;

                case 'responsible':
                    $query->orderBy('person.primary_last_name', $request->input('sortOrder', 'asc'));
                    $query->orderBy('person.second_last_name', $request->input('sortOrder', 'asc'));
                    $query->orderBy('person.name', $request->input('sortOrder', 'asc'));
                    break;

                default:
                    $query->orderBy('chargeables.' . $sortField, $request->input('sortOrder', 'asc'));
                    break;
            }
        }
        
        return $query;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'operation_id'          => 'required|exists:operation,id',
            'load_date'             => 'required|date_format:d/m/Y',
            'loaded_origin_id'      => 'required',
            'loaded_destination_id' => 'required',
            'responsible_id'        => 'required|exists:person,id',
            'operator_id'           => 'required|exists:person,id',
            'counter'               => 'sometimes',
            'hour_meter'            => 'sometimes',
            'liters'                => 'required',
        ];
        
        return $rules;
    }

    /**
     * @param Builder|\Illuminate\Database\Query\Builder $cargas
     */
    private function exportExcel($cargas)
    {
        $cargas->select([
            'operation.name as Faena',
            \DB::raw('origin.name as "Dispensador Origen"'),
            \DB::raw('destination.name as "Dispensador Destino"'),
            \DB::raw('DATE_FORMAT(chargeables.load_date, \'%d-%m-%Y\') as "Fecha Carga"'),
            'chargeables.liters as Litros',
            \DB::raw('CONCAT(person.primary_last_name, \' \', person.second_last_name, \', \', person.name) as Responsable'),
        ]);

        $data = $cargas->get();
        $this->export('Consulta_cargadispensadores_', $data);
    }
}