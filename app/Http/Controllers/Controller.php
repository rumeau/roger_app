<?php

namespace App\Http\Controllers;

use Excel;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    protected function export($filename, $data, $headers = [])
    {
        Excel::create($filename . date('dmY_His'), function($excel) use ($data, $headers)  {

            $excel->sheet('Consulta', function(LaravelExcelWorksheet $sheet) use ($data, $headers) {

                $sheet->fromArray($data);

            });

        })->download('xls');
    }
}