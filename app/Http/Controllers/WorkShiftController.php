<?php

namespace App\Http\Controllers;

use App\Models\Turno;
use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WorkShiftController extends Controller
{
    /**
     * @var Empresa
     */
    protected $enterprise;

    public function __construct()
    {
        $this->enterprise = current_enterprise();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $workShifts = Turno::query();

        $paginator = $workShifts->paginate($request->query('pageSize', 25), ['*'], 'pageIndex');

        if ($request->ajax()) {
            return [
                'data'       => $paginator->items(),
                'itemsCount' => $paginator->total()
            ];
        }

        return view('turnos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $selectEmpresas     = \App\Models\Empresa::all()->lists('name', 'id');
        
        $selectFaenas       = \App\Models\Faena::where('enterprise_id', '=', current_enterprise()->id)
                                    ->where('external', '<>', 1)
                                    ->where('visible', '=', 1)
                                    ->where('to_buy', '=', 1)
                                    ->lists('name', 'id');
                                    
        $selectConceptos    = \App\Models\Concepto::where('id', '=', 3)->first();
        
        $selectResponsible  = \App\Models\Empresa::find(current_enterprise()->id)
                                    ->operator()
                                    ->where('its_responsible', '=', 1)
                                    ->where ('person.active', '=', 1)
                                    ->orderBy('primary_last_name', 'ASC')
                                    ->get([\DB::raw("CONCAT(`primary_last_name`,' ',`second_last_name`,', ',`name`) as fullName"), 'person.id'])
                                    ->lists('fullName', 'id');
                            
        return view('turnos.create')->with([
            'selectEmpresas'        => $selectEmpresas,
            'selectFaenas'          => $selectFaenas,
            "selectResponsible"     => $selectResponsible,            
            'selectConceptos'       => $selectConceptos
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request, \App\Repositories\ChargeableRepository $repository)
    {
        $this->validate($request, $this->rules(), [
            'load_date.required'        => 'El campo Fecha carga es obligatorio.']);
        
        $data = $request->only(
            'enterprise_id',
		    'operation_id',
		    'operator_id',
		    'work_shift_id',
		    'loaded_origin_id',
		    'loaded_destination_id',
		    'ubication_id',
		    'responsible_id',
		    'liters',
		    'counter',
		    'hour_meter',
		    'load_date',
		    'order_number'
		);
		$data['concept_id'] = 3;
		$data['productive_area_id'] = 1;
        $repository->create($data);
        //flash()->success('El consumo ha sido registrada');
        
        if ($request->input('task', 'save_n_new') == 'save_n_new') {
            // crear otro
            return response()->json([
                'message'       => 'El consumo ha sido registrada',
                'success'       => true,
                'status'        => 'success',
                ]);
        } elseif ($request->input('task', 'save_n_new') == 'save') {
            // volver al index luego de guardar
            return response()->json([
                'message'       => 'El consumo ha sido registrado',
                'success'       => true,
                'status'        => 'success',
                'redirect'      => route('consumption.index'),
                ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {/*
        $consumos           = \App\Models\Chargeable::where('concept_id', '=', 3)
                                    ->where('chargeables.enterprise_id', '=', current_enterprise()->id)
                                    ->orderBy('load_date', 'DESC');*/
                                    
        $consumos           = \App\Models\Chargeable::findOrFail($id);
        
        $selectFaenas       = \App\Models\Faena::where('enterprise_id', '=', current_enterprise()->id)
                                    ->where('external', '<>', 1)
                                    ->lists('name', 'id');
                                    
        $selectResponsible  = \App\Models\Empresa::find(current_enterprise()->id)
                                    ->operator()
                                    ->where('its_responsible', '=', 1)
                                    ->where ('active', '=', 1)
                                    ->orderBy('primary_last_name', 'ASC')
                                    ->get([\DB::raw("CONCAT(`primary_last_name`,' ',`second_last_name`,', ',`name`) as fullName"), 'person.id'])
                                    ->lists('fullName', 'id');
                                    
        if ($request->ajax()){
            return $consumos;
        }
        
        return view('consumos.edit')->with([
            'consumos'          => $consumos,
            'selectFaenas'      => $selectFaenas,
            'selectConceptos'   => \App\Models\Concepto::where('id', '=', 3)->first(),
            'selectResponsible' => $selectResponsible
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules());
        //CAMPOS QUE VIENEN DIRECTAMENTE DEL FORMULARIO
        $data = $request->only(
            'enterprise_id',
		    'operation_id',
		    'operator_id',
		    'work_shift_id',
		    'loaded_origin_id',
		    'loaded_destination_id',
		    'ubication_id',
		    'responsible_id',
		    'liters',
		    'counter',
		    'hour_meter',
		    'load_date',
		    'order_number'
		);
		$data['concept_id'] = 3;
		
		$consumos = \App\Models\Chargeable::findOrFail($id);
		$consumos->update($data);
		
		flash()->success('El consumo ha sido modificado.');
		
        //return redirect(route('consumption.index'));
        return response()->json([
            'status'     => 'success',
            'success'    => true,
            'message'    => 'El consumo ha sido modificado.',
            'redirect'   => route('consumption.index'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    
    protected function applySearchFilters(Request $request, &$query)
    {
        if ($request->has('operation')) {
            $query->where('chargeables.operation_id', '=', $request->input('operation'));
        }
        
        if ($request->has('work_shift')) {
            $query->where('chargeables.work_shift_id', '=', $request->input('work_shift'));
        }
        
        if ($request->has('date_from') && $request->has('date_to')) {
            $from = Carbon::createFromFormat('d-m-Y', $request->input('date_from'))->hour(0)->minute(0)->second(0);
            $to   = Carbon::createFromFormat('d-m-Y', $request->input('date_to'))->hour(23)->minute(59)->second(59);
            $query->where(function($q) use ($from, $to) {
                $q->whereDate('chargeables.load_date', '>=', $from)
                    ->whereDate('chargeables.load_date', '<=', $to);
            });
        } elseif ($request->has('date_from') && !$request->has('date_to')) {
            $from = Carbon::createFromFormat('d-m-Y', $request->input('date_from'))->hour(0)->minute(0)->second(0);
            $query->whereDate('chargeables.load_date', '>=', $from);
        } elseif (!$request->has('date_from') && $request->has('date_to')) {
            $to   = Carbon::createFromFormat('d-m-Y', $request->input('date_to'))->hour(23)->minute(59)->second(59);
            $query->whereDate('chargeables.load_date', '<=', $to);
        }
        
        if ($request->has('order_number')) {
            $query->where('chargeables.order_number', '=', $request->input('order_number'));
        }
        
        if ($request->has('dispenser')) {
            $query->where('chargeables.loaded_origin_id', '=', $request->input('dispenser'));
        }
        
        if ($request->has('equipment')) {
            $query->where('chargeables.loaded_destination_id', '=', $request->input('equipment'));
        }
        
        if ($request->has('operator')) {
            $query->where('chargeables.operator_id', '=', $request->input('operator'));
        }
        
        if ($request->has('responsible')) {
            $query->where('chargeables.responsible_id', '=', $request->input('responsible'));
        }
        
        if ($request->has('productive_area')) {
            $query->where('chargeables.productive_area_id', '=', $request->input('productive_area'));
        }
        /*
        if ($request->has('cc')) {
            //$query->join('equipment', 'chargeables.loaded_destination_id', '=', 'equipment.id');
            $query->where('equipment.cc', '=', $request->input('cc'));
        }
        */
        // Sorting
        if ($request->has('sortField')) {
            $sortField = $request->input('sortField', 'load_date');
            switch ($sortField) {
                case 'operation' :
                    $sortField = 'operation.name';
                    $query->with(['operation' => function($q) use ($request) {
                        $q->orderBy('operation.name', $request->input('sortOrder', 'asc'));
                    }]);
                    break;
                    
                case 'loaded_origin':
                case 'loaded_destination':
                    $sortField = 'chargeables.' . $sortField . '_id';
                    $query->orderBy($sortField, $request->input('sortOrder', 'asc'));
                    break;
                    
                case 'equipment':
                    //dd('yy');
                    $sortField = 'equipment.name';
                    $query->with(['equipment' => function($q) use ($request) {
                        $q->orderBy('equipment.name', $request->input('sortOrder', 'asc'));
                    }]);
                    break;
                    
                default:
                    $sortField = 'chargeables.' . $sortField;
                    $query->orderBy($sortField, $request->input('sortOrder', 'asc'));
                    break;
            }
        }
        
        return $query;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'enterprise_id'         => 'required',
            'operation_id'          => 'required|exists:operation,id',
            'concept_id'            => 'required|exists:concept,id',
            'load_date'             => 'required|date',
            'loaded_origin_id'      => 'required',
            'loaded_destination_id' => 'required',
            'responsible_id'        => 'required|exists:person,id',
            'operator_id'           => 'required|exists:person,id',
            'order_number'          => 'required_if:concept_id,1,2,5,6',
            'bill_number'           => 'required_if:concept_id,1',
            'counter'               => 'required',
            'hour_meter'            => 'required',
            'liters'                => 'required'
        ];
        return $rules;
    }
}
