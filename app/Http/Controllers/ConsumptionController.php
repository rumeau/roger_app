<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Chargeable;
use App\Models\Dispensador;
use App\Models\Equipo;
use App\Repositories\ChargeableRepository;
use App\Repositories\EnterpriseRepository;
use Carbon\Carbon;
use Flash;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ConsumptionController extends Controller
{
    protected $enterprise;

    public function __construct()
    {
        $this->enterprise = current_enterprise();
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param ChargeableRepository $chargeableRepository
     * @return $this|array
     */
    public function index(Request $request, ChargeableRepository $chargeableRepository)
    {
        $consumos = $chargeableRepository->getConsumptionsByEnterpriseJoined($this->enterprise);
        $this->applySearchFilters($request, $consumos);

        /**
         * Consumos de combustible realizadas (concept_id = 3, es por consumo),
         * a través de la empresa seleccionada
         */
        if ($request->query('export', 0) == '1') {
            $this->exportExcel($consumos);
            return true;
        }

        $consumos->select([
            'chargeables.id as id',
            'chargeables.order_number as order_number',
            'chargeables.load_date as load_date',
            'chargeables.counter as counter',
            'chargeables.liters as liters',
            'chargeables.hour_meter as hour_meter',
            'operation.name as operation',
            'work_shift.code as work_shift',
            'fuel_dispenser.name as dispenser',
            'equipment.cc as cc',
            'equipment.patent as patent',
            'type.name as type',
            'brand.name as brand',
            'model.name as model',
            \DB::raw('CONCAT(person.primary_last_name, \' \', person.second_last_name, \', \', person.name) as operator')
        ]);

        $paginador = $consumos->paginate($request->query('pageSize', 25), ['*'], 'pageIndex');
            
        if ($request->ajax()) {
            return [
                'data'       => $paginador->items(),
                'itemsCount' => $paginador->total()
            ];
        } 
        
        return view('consumos.index', [
            "consumos"              => $paginador
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('consumos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @param ChargeableRepository $chargeableRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, ChargeableRepository $chargeableRepository)
    {
        $this->validate($request, $this->rules(), [
            'load_date.required'        => 'El campo Fecha carga es obligatorio.']);
        
        $data = $request->only(
            'operation_id',
            'operator_id',
            'work_shift_id',
            'loaded_origin_id',
            'loaded_destination_id',
            'ubication_id',
            'responsible_id',
            'liters',
            'counter',
            'hour_meter',
            'load_date',
            'order_number'
        );

        $data['enterprise_id'] = $this->enterprise->id;
        $data['concept_id'] = Chargeable::CONCEPTO_CONSUMO;
        $data['productive_area_id'] = 1;

        $validHourMeter = $this->validateHourMeter($data['hour_meter'], $data['loaded_destination_id'], $data['load_date']);
        if (is_array($validHourMeter)) {
            return response()->json($validHourMeter, 400);
        }

        /**
         * @var Dispensador $origin
         * @var Equipo $destination
         */
        $origin = EnterpriseRepository::dispensers($this->enterprise)
            ->where('fuel_dispenser.id', $data['loaded_origin_id'])
            ->first();
        $destination = EnterpriseRepository::equipments($this->enterprise)
            ->where('equipment.id', $data['loaded_destination_id'])
            ->first();
        
        $liters = raw_number($data['liters']);
        if ($liters > $origin->current_balance) {
            return response()->json([
                'title' => 'Error!',
                'message' => 'Esta cargando mas litros de los disponibles en el dispensador',
            ], 400);
        }
        
        if ($liters > $destination->fuel_tank_capacity) {
            return response()->json([
                'title' => 'Error!',
                'message' => 'Esta cargando mas litros de los que el equipo puede recibir',
            ], 400);
        }

        $chargeableRepository->createConsumption($data);

        Flash::success('El consumo ha sido registrada');
        
        if ($request->input('task', 'save_n_new') == 'save_n_new') {
            // crear otro
            return response()->json([
                'message'       => 'El consumo ha sido registrada',
                'success'       => true,
                'status'        => 'success',
                ]);
        } elseif ($request->input('task', 'save_n_new') == 'save') {
            // volver al index luego de guardar
            return response()->json([
                'message'       => 'El consumo ha sido registrado',
                'success'       => true,
                'status'        => 'success',
                'redirect'      => route('consumption.index'),
                ]);
        }

        return [];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @param ChargeableRepository $chargeableRepository
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\View\View|mixed|null|static
     */
    public function edit(Request $request, $id, ChargeableRepository $chargeableRepository)
    {                                    
        $consumo = $chargeableRepository->findConsumptionOrFail($id);

        if ($request->ajax()){
            return $consumo;
        }
        
        return view('consumos.edit', [
            'consumo' => $consumo
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @param ChargeableRepository $chargeableRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id, ChargeableRepository $chargeableRepository)
    {
        $consumption = $chargeableRepository->findConsumptionOrFail($id);
        $this->validate($request, $this->rules());
        
        //CAMPOS QUE VIENEN DIRECTAMENTE DEL FORMULARIO
        $data = $request->only(
            'operation_id', 
            'operator_id', 
            'work_shift_id', 
            'loaded_origin_id', 
            'loaded_destination_id', 
            'ubication_id', 
            'responsible_id', 
            'liters', 
            'counter', 
            'hour_meter', 
            'load_date', 
            'order_number'
        );

        $validHourMeter = $this->validateHourMeter($data['hour_meter'], $data['loaded_destination_id'], $data['load_date'], $consumption->id);
        if (is_array($validHourMeter)) {
            return response()->json($validHourMeter, 400);
        }
        
        $origin         = $consumption->origin;
        $destination    = $consumption->destination;
        $originalLiters = raw_number($consumption->liters);
        $newLiters      = raw_number($data['liters']);
        $diff           = $originalLiters - $newLiters;
        // Sobran litros (devolver)
        if ($diff > 0)
        {
            // si el origen no puede recibir los litros de vuelta
            if (($diff + $origin->current_balance) > $origin->capacity) {
                return response()->json([
                    'title'     => 'Error!',
                    'message'   => 'Se estan intentando devolver mas litros de los que el dispensador de origen puede volver a recibir',
                ], 400);
            } elseif ($diff > $destination->fuel_tank_capacity) {
                return response()->json([
                    'title' => 'Error!',
                    'message' => 'Se estan intentando devolver mas litros de los que el equipo tiene como máxima capacidad',
                ], 400);
            }
        }
        // faltan litros (sacar mas del dispensador)
        elseif ($diff < 0)
        {
            // Si el dispensador no tiene disponible los litros que se intentan sacar
            if (-$diff > $origin->current_balance)
            {
                return response()->json([
                    'title' => 'Error!',
                    'message' => 'Esta intentando cargar mas litros que los que el dispensador tiene disponible',
                ], 400);
            }
            // Si el equipo no puede recibir la cantidad que se intenta cargar (capacidad)
            elseif ($newLiters > $destination->fuel_tank_capacity)
            {
                return response()->json([
                    'title' => 'Error!',
                    'message' => 'Esta cargando mas litros de los que el equipo puede recibir',
                ], 400);
            }
        }

        $chargeableRepository->updateConsumption($consumption, $data);

        Flash::success('El consumo ha sido modificado.');

        //return redirect(route('consumption.index'));
        return response()->json([
            'status'     => 'success',
            'success'    => true,
            'message'    => 'El consumo ha sido modificado.',
            'redirect'   => route('consumption.index'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param Builder|\Illuminate\Database\Query\Builder $query
     * @return mixed
     */
    protected function applySearchFilters(Request $request, &$query)
    {
        if ($request->input('operation', '') != '') {
            $query->where('chargeables.operation_id', '=', $request->input('operation'));
        }
        
        if ($request->input('work_shift', '') != '') {
            $query->where('chargeables.work_shift_id', '=', $request->input('work_shift'));
        }
        
        if ($request->input('date_from', '') != '' && $request->input('date_to', '') != '') {
            $from = Carbon::createFromFormat('d-m-Y', $request->input('date_from'))->hour(0)->minute(0)->second(0);
            $to   = Carbon::createFromFormat('d-m-Y', $request->input('date_to'))->hour(23)->minute(59)->second(59);
            $query->where(function($q) use ($from, $to) {
                $q->whereDate('chargeables.load_date', '>=', $from)
                    ->whereDate('chargeables.load_date', '<=', $to);
            });
        } elseif ($request->input('date_from', '') != '' && $request->input('date_to', '') == '') {
            $from = Carbon::createFromFormat('d-m-Y', $request->input('date_from'))->hour(0)->minute(0)->second(0);
            $query->whereDate('chargeables.load_date', '>=', $from);
        } elseif ($request->input('date_from', '') == '' && $request->input('date_to', '') != '') {
            $to   = Carbon::createFromFormat('d-m-Y', $request->input('date_to'))->hour(23)->minute(59)->second(59);
            $query->whereDate('chargeables.load_date', '<=', $to);
        }
        
        if ($request->input('order_number', '') != '') {
            $query->where('chargeables.order_number', '=', $request->input('order_number'));
        }
        
        if ($request->input('dispenser', '') != '') {
            $query->where('chargeables.loaded_origin_id', '=', $request->input('dispenser'));
        }
        
        if ($request->input('equipment', '') != '') {
            $query->where('chargeables.loaded_destination_id', '=', $request->input('equipment'));
        }
        
        if ($request->input('operator', '') != '') {
            $query->where('chargeables.operator_id', '=', $request->input('operator'));
        }
        
        if ($request->input('responsible', '') != '') {
            $query->where('chargeables.responsible_id', '=', $request->input('responsible'));
        }
        /*
        if ($request->has('productive_area')) {
            $query->where('chargeables.productive_area_id', '=', $request->input('productive_area'));
        }
        */
        if ($request->input('cc', '') != '') {
            //$query->join('equipment', 'chargeables.loaded_destination_id', '=', 'equipment.id');
            $query->where('equipment.cc', '=', $request->input('cc'));
        }

        // Sorting
        if ($request->input('sortField', '') != '') {
            $sortField = $request->input('sortField', 'load_date');
            switch ($sortField) {
                case 'work_shift' :
                    $query->orderBy('work_shift.code', $request->input('sortOrder', 'asc'));
                    break;

                case 'operation' :
                    $query->orderBy('operation.name', $request->input('sortOrder', 'asc'));
                    break;
                    
                case 'dispenser':
                    $query->orderBy('fuel_dispenser.name', $request->input('sortOrder', 'asc'));
                    break;

                case 'cc':
                case 'patent':
                    $sortField = 'equipment.' . $sortField;
                    $query->orderBy($sortField, $request->input('sortOrder', 'asc'));
                    break;

                case 'brand':
                    $query->orderBy('brand.name', $request->input('sortOrder', 'asc'));
                    break;

                case 'model':
                    $query->orderBy('model.name', $request->input('sortOrder', 'asc'));
                    break;

                case 'type':
                    $query->orderBy('type.name', $request->input('sortOrder', 'asc'));
                    break;

                case 'operator':
                    $query->orderBy('person.primary_last_name', $request->input('sortOrder', 'asc'));
                    $query->orderBy('person.second_last_name', $request->input('sortOrder', 'asc'));
                    $query->orderBy('person.name', $request->input('sortOrder', 'asc'));
                    break;

                default:
                    $sortField = 'chargeables.' . $sortField;
                    $query->orderBy($sortField, $request->input('sortOrder', 'asc'));
                    break;
            }
        }
        
        return $query;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'operation_id'          => 'required|exists:operation,id',
            'load_date'             => 'required|date_format:d/m/Y',
            'loaded_origin_id'      => 'required',
            'loaded_destination_id' => 'required',
            'responsible_id'        => 'required|exists:person,id',
            'operator_id'           => 'required|exists:person,id',
            'order_number'          => 'required_if:concept_id,1,2,5,6',
            'bill_number'           => 'required_if:concept_id,1',
            'counter'               => 'required',
            'hour_meter'            => 'required',
            'liters'                => 'required'
        ];
        return $rules;
    }

    /**
     * @param Builder|\Illuminate\Database\Query\Builder $consumos
     */
    private function exportExcel($consumos)
    {
        $consumos->select([
            \DB::raw('chargeables.order_number as "Nº Orden"'),
            \DB::raw('DATE_FORMAT(chargeables.load_date, \'%d-%m-%Y\') as "Fecha Consumo"'),
            'chargeables.counter as Contador',
            'chargeables.liters as Litros',
            \DB::raw('chargeables.hour_meter as "Horómetro"'),
            'operation.name as Faena',
            'work_shift.code as Turno',
            'fuel_dispenser.name as Dispensador',
            'equipment.cc as CC',
            'equipment.patent as Patente',
            'type.name as Tipo',
            'brand.name as Marca',
            'model.name as Modelo',
            \DB::raw('CONCAT(person.primary_last_name, \' \', person.second_last_name, \', \', person.name) as Operador')
        ]);

        $data = $consumos->get();
        $this->export('Consulta_consumosporequipo_', $data);
    }

    protected function validateHourMeter($hourMeter, $destination, $date, $edit = false)
    {
        $date = Carbon::createFromFormat('d/m/Y', $date);

        if (!$edit) {
            $count = $this->enterprise->chargeables()
                ->where('loaded_destination_id', '=', $destination)
                ->where('hour_meter', $hourMeter)
                ->count();
            // Si este horometro ya existe, algo anda muy mal
            if ($count > 0) {
                return [
                    'title' => 'Error!',
                    'message' => 'El horómetro ingresado ya existe en los registros'
                ];
            }
        }

        $prevHourMeter = $this->enterprise->chargeables()
            ->where('loaded_destination_id', '=', $destination)
            ->where('load_date', '<', $date)
            ->orderBy('load_date', 'DESC')
            ->select('hour_meter');
        if ($edit) {
            $prevHourMeter = $prevHourMeter->where('id', '!=', $edit);
        }

        $prevHourMeter = $prevHourMeter->first();

        $nextHourMeter = $this->enterprise->chargeables()
            ->where('loaded_destination_id', '=', $destination)
            ->where('load_date', '>', $date)
            ->orderBy('load_date', 'ASC')
            ->select('hour_meter');
        if ($edit) {
            $nextHourMeter = $nextHourMeter->where('id', $edit);
        }
        $nextHourMeter = $nextHourMeter->first();

        // Si no hay horometros anteriores o siguientes registrados
        if (!$prevHourMeter instanceof Chargeable && !$nextHourMeter instanceof Chargeable) {
            return true;
        }

        // Si este horometro es menor que un horometro anterior, algo anda mal
        if ($prevHourMeter instanceof Chargeable && $hourMeter < $prevHourMeter->hour_meter) {
            return [
                'title' => 'Error!',
                'message' => 'El horómetro no puede ser inferior al horómetro anterior a esta fecha de carga',
            ];
        }

        // Si este horometros es mayor a un horometro siguientes, algo anda mal
        if ($nextHourMeter instanceof Chargeable && $hourMeter > $nextHourMeter->hour_meter) {
            return [
                'title' => 'Error!',
                'message' => 'El horómetro no puede ser superior algun horómetro siguiente a esta fecha de carga',
            ];
        }
    }
}
