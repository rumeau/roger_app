<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Chargeable;
use App\Models\Empresa;
use App\Repositories\ChargeableRepository;
use Carbon\Carbon;
use Flash;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    /**
     * @var Empresa
     */
    protected $enterprise;

    public function __construct()
    {
        $this->enterprise = current_enterprise();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param ChargeableRepository $chargeableRepository
     * @return array
     */
    public function index(Request $request, ChargeableRepository $chargeableRepository)
    {
        $compras = $chargeableRepository->getPurchasesByEnterpriseJoined($this->enterprise);
        $this->applySearchFilters($request, $compras);

        /**
         * Compras de combustible realizadas (concept_id = 1, es por compra),
         * a través de la empresa seleccionada
         */
        if ($request->query('export', 0) == '1') {
            $this->exportExcel($compras);
            return true;
        }

        $compras->select([
            'chargeables.id as id',
            'chargeables.received as received',
            'operation.name as operation',
            'chargeables.load_date as load_date',
            'chargeables.order_number as order_number',
            'chargeables.bill_number as bill_number',
            'provider.name as provider',
            'fuel_dispenser.name as dispenser',
            'chargeables.liters as liters',
            'chargeables.net_price as net_price',
            'chargeables.net_value as net_value',
            'chargeables.fuel_tax as fuel_tax',
            'chargeables.tax as tax',
            'chargeables.total_amount as total_amount'
        ]);

        $paginador = $compras->paginate($request->query('pageSize', 25), ['*'], 'pageIndex');

        if ($request->ajax()) {
            return [
                'data'       => $paginador->items(),
                'itemsCount' => $paginador->total()
            ];
        }

        return view('compras.index');
    }

    public function confirm($id, ChargeableRepository $chargeableRepository)
    {
        /**
         * @var Chargeable $chargeable
         */
        $purchase = $chargeableRepository->findPurchaseOrFail($id);

        $this->authorize('confirm', $purchase);

        $liters = raw_number($purchase->liters);
        $destination = $purchase->destination;
        if (($liters + $destination->current_balance) > $destination->capacity) {
            return response()->json(['Satus' => 'ERROR', 'title' => 'Error en la confirmación', 'message' => 'La cantidad de litros que esta confirmando al sumarse a la cantidad actual del dispensador, supera el limite de capacidad del dispensador.'], 400);
        }

        $chargeableRepository->confirmPurchase($purchase);

        return response()->json(['Satus' => 'OK']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        return view('compras.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @param ChargeableRepository $chargeableRepository
     * @return mixed
     */
    public function store(Request $request, ChargeableRepository $chargeableRepository)
    {
        $this->validate($request, $this->rules(), [
            'loaded_destination_id.required'        => 'El campo Dispensador destino es obligatorio.']);
        
        //CAMPOS QUE VIENEN DIRECTAMENTE DEL FORMULARIO
        $data = $request->only(
            'operation_id',
            'load_date',
            'order_number',
            'bill_number',
            'loaded_origin_id',
            'loaded_destination_id',
            'liters',
            'net_price',
            'fuel_tax'
        );

        $data['enterprise_id']  = $this->enterprise->id;
        $data['concept_id']     = Chargeable::CONCEPTO_COMPRA;
        $data['responsible_id'] = user()->id;
        //Al momento de crear el registro, se deja en "cero" de lo contrario, queda en "NULL", afectando al momento de filtrar.
        //$data['received']       = 0;

        $chargeableRepository->createPurchase($data);

        Flash::success('La compra ha sido registrada');

        /*
        if ($request->input('task', 'save_n_new') == 'save_n_new') {
            // crear otro
            return response()->json([
                'message'       => 'La compra ha sido registrada',
                'success'       => true,
                'status'        => 'success',
                ]);
        } else*/
        //if ($request->input('task', 'save_n_new') == 'save') {
            // volver al index luego de guardar
            return [
                'message'       => 'La compra ha sido registrada.',
                'success'       => true,
                'status'        => 'success',
                'redirect'      => route('compra.index'),
            ];
        //}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @param ChargeableRepository $chargeableRepository
     * @return mixed
     */
    public function edit(Request $request, $id, ChargeableRepository $chargeableRepository)
    {
        $purchase = $chargeableRepository->findPurchaseOrFail($id);

        if ($request->ajax()){
            return $purchase;
        }

        return view('compras.edit', [
            'compra' => $purchase,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @param ChargeableRepository $chargeableRepository
     * @return mixed
     */
    public function update(Request $request, $id, ChargeableRepository $chargeableRepository)
    {
        $purchase = $chargeableRepository->findPurchaseOrFail($id);

        $this->validate($request, $this->rules(), [
            'net_price.regex' => 'El precio neto no tiene un formato de moneda válido',
            'fuel_tax.regex' => 'El precio neto no tiene un formato de moneda válido',
        ]);
        
        //CAMPOS QUE VIENEN DIRECTAMENTE DEL FORMULARIO
        $data = $request->only(
            'operation_id',
            'load_date',
            'order_number',
            'bill_number',
            'loaded_origin_id',
            'loaded_destination_id',
            'liters',
            'net_price',
            'fuel_tax'
        );

        $chargeableRepository->updatePurchase($purchase, $data);

        Flash::success('La compra ha sido modificada.');

        return response()->json([
            'status'     => 'success',
            'success'    => true,
            'message'    => 'La compra ha sido modificada.',
            'redirect'   => route('compra.index'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param ChargeableRepository $chargeableRepository
     * @return mixed
     * @throws void
     */
    public function destroy($id, ChargeableRepository $chargeableRepository)
    {
        /**
         * @var Chargeable $chargeable
         */
        $bough = $chargeableRepository->findPurchaseOrFail($id);

        $this->authorize('destroy', $bough);

        $return = $chargeableRepository->deletePurchase($bough);
        if ($return) {
            return response()->json(['Status' => 'OK']);
        } else {
            return response()->json(['Status' => 'ERROR', 'title' => 'Error', 'message' => 'No se ha podido eliminar esta compra'], 400);
        }
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'operation_id'          => 'required|exists:operation,id',
            'load_date'             => 'required|date_format:d/m/Y',
            'loaded_origin_id'      => 'required|exists:provider,id',
            'loaded_destination_id' => 'required|exists:fuel_dispenser,id',
            'order_number'          => 'required|numeric',
            'bill_number'           => 'required|numeric',
            'liters'                => 'required|numeric',
            'net_price'             => 'required|regex:/[0-9]{1,2}([,.][0-9]{1,2})?/',
            'fuel_tax'              => 'required|regex:/[0-9]{1,2}([,.][0-9]{1,2})?/',
        ];

        return $rules;
    }

    /**
     * @param Request $request
     * @param Builder|\Illuminate\Database\Query\Builder $query
     * @return Builder
     */
    protected function applySearchFilters(Request $request, &$query)
    {
        if ($request->input('operation', '') != '') {
            $query->where('operation_id', '=', $request->input('operation'));
        }
        
        if ($request->input('work_shift', '') != '') {
            $query->where('work_shift_id', '=', $request->input('work_shift'));
        }
        
        if ($request->input('date_from', '') != '' && $request->input('date_to', '') != '') {
            $from = Carbon::createFromFormat('d/m/Y', $request->input('date_from'))->format('Y-m-d');
            $to   = Carbon::createFromFormat('d/m/Y', $request->input('date_to'))->format('Y-m-d');
            $query->where(function($q) use ($from, $to) {
                /**
                 * @var Builder|\Illuminate\Database\Query\Builder $q
                 */
                $q->whereDate('load_date', '>=', $from);
                $q->whereDate('load_date', '<=', $to);
            });
        } elseif ($request->input('date_from', '') != '' && $request->input('date_to', '') == '') {
            $from = Carbon::createFromFormat('d/m/Y', $request->input('date_from'))->format('Y-m-d');
            $query->whereDate('load_date', '>=', $from);
        } elseif ($request->input('date_from', '') == '' && $request->input('date_to', '') != '') {
            $to   = Carbon::createFromFormat('d/m/Y', $request->input('date_to'))->format('Y-m-d');
            $query->whereDate('load_date', '<=', $to);
        }
        
        if ($request->input('order_number', '') != '') {
            $query->where('order_number', '=', $request->input('order_number'));
        }
        
        if ($request->input('bill_number', '') != '') {
            $query->where('bill_number', '=', $request->input('bill_number'));
        }
        
        if ($request->input('provider', '') != '') {
            $query->where('loaded_origin_id', '=', $request->input('provider'));
        }
        
        if ($request->input('dispenser', '') != '') {
            $query->where('loaded_destination_id', '=', $request->input('dispenser'));
        }
        
        if ($request->input('received', '') != '' && $request->input('received', 'false') == 'true') {
            $query->where('received', '=', 0);
        }

        /*
        if ($request->input('responsible', '') != '') {
            $query->where('responsible_id', '=', $request->input('responsible'));
        }
        */
        
        // Sorting
        if ($request->input('sortField', '') != '') {
            $sortField = $request->input('sortField', 'chargeables.load_date');
            switch ($sortField) {
                case 'operation' :
                    $query->orderBy('operation.name', $request->input('sortOrder', 'asc'));
                    break;
                    
                case 'provider':
                    $query->orderBy('provider.name', $request->input('sortOrder', 'asc'));
                    break;

                case 'dispenser':
                    $query->orderBy('fuel_dispenser.name', $request->input('sortOrder', 'asc'));
                    break;
                    
                default:
                case 'received':
                    $query->orderBy('chargeables.' . $sortField, $request->input('sortOrder', 'asc'));
                    break;
            }
        }
        
        return $query;
    }

    /**
     * @param Builder|\Illuminate\Database\Query\Builder $compras
     */
    private function exportExcel($compras)
    {
        $compras->select([
            'chargeables.received as received',
            'operation.name as operation',
            \DB::raw('DATE_FORMAT(chargeables.load_date, \'%d-%m-%Y\') as load_date'),
            'chargeables.order_number as order_number',
            'chargeables.bill_number as bill_number',
            'provider.name as provider',
            'fuel_dispenser.name as dispenser',
            'chargeables.liters as liters',
            'chargeables.net_price as net_price',
            'chargeables.net_value as net_value',
            'chargeables.fuel_tax as fuel_tax',
            'chargeables.tax as tax',
            'chargeables.total_amount as total',
        ]);

        $items = collect();
        foreach ($compras->get() as $item) {
            $netPrice = str_replace('.', '', $item->net_price);
            $netPrice = str_replace(',', '.', $netPrice);
            $new = [
                'Pendiente Recepcion' => $item->received == '1' ? 'No' : 'Si',
                'Faena'         => $item->operation,
                'Fecha Compra'  => $item->load_date,
                'Nº Pedido'     => $item->order_number,
                'Nº Factura'    => $item->bill_number,
                'Proveedor'     => $item->provider,
                'Dispensador'   => $item->dispenser,
                'Litros'        => $item->liters,
                'Precio Neto'   => number_format($netPrice, 5, ',', '.'),
                'Valor Neto'    => number_format(round($item->net_value), 0, ',', '.'),
                'IEC'           => number_format(round($item->fuel_tax), 0, ',', '.'),
                'IVA'           => number_format(round($item->tax), 0, ',', '.'),
                'Total'         => number_format(round($item->total), 0, ',', '.'),
            ];

            $items->push($new);
        }

        $this->export('Consulta_compradecombustible_', $items);
    }
}
