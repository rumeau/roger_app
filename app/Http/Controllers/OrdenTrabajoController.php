<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrdenTrabajoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $selectFaenas       = \App\Models\Faena::where('enterprise_id', '=', current_enterprise()->id)
                                    ->where('external', '<>', 1)
                                    ->where('visible', '=', 1)
                                    ->lists('name', 'id');
                                 
        $selectConceptos    = \App\Models\Concepto::where('id', '=', 3)->first();
        
        
        $selectAfectado     = \App\Models\Empresa::find(current_enterprise()->id)
                                    //where('id', '=', )
                                    ->afectado()
                                    ->get([\DB::raw("CONCAT(`primary_last_name`,' ',`second_last_name`,', ',`person`.`name`) as fullName"), 'person.id'])
                                    ->lists('fullName', 'id');
        
        
        $selectResponsible  = \App\Models\Empresa::find(current_enterprise()->id)
                                    ->operator()
                                    ->where('its_responsible', '=', 1)
                                    ->where ('active', '=', 1)
                                    ->orderBy('primary_last_name', 'ASC')
                                    ->get([\DB::raw("CONCAT(`primary_last_name`,' ',`second_last_name`,', ',`name`) as fullName"), 'person.id'])
                                    //->get([\DB::raw("CONCAT(`name`,' ',`primary_last_name`,' ',`second_last_name`) as fullName"), 'person.id'])
                                    ->lists('fullName', 'id');
                                    
        $selectNivel1       = \App\Models\TipoSolicitud::all()
                                    ->lists('name', 'id');
                            
        return view('ordentrabajo.create')->with([
            //'selectEmpresas'        => $selectEmpresas
            'selectFaenas'          => $selectFaenas,
            'selectAfectado'        => $selectAfectado,
            "selectResponsible"     => $selectResponsible,            
            'selectConceptos'       => $selectConceptos,
            'selectNivel1'          => $selectNivel1
        ]);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function returnOt(Request $request){
        $componentes = \App\Models\Componente::where('responsible_id', '=', 190)->get();
        
        return $componentes;
    }
}
