<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Chargeable;
use App\Models\Equipo;
use App\Repositories\EnterpriseRepository;
use App\Repositories\OperationRepository;
use App\Repositories\EquipmentRepository;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * @var \App\Models\Empresa|null
     */
    protected $enterprise;

    /**
     * ReportController constructor.
     */
    public function __construct()
    {
        $this->enterprise = current_enterprise();
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param EnterpriseRepository $enterpriseRepository
     * @return \Illuminate\View\View
     */
    public function index(Request $request, EnterpriseRepository $enterpriseRepository)
    {
        $dateFrom = $request->query('from', '');
        $dateTo   = $request->query('to', '');

        $paginator  = false;
        $total      = 0;
        $detail     = (bool) $request->query('detail', 0);
        if (!empty($dateFrom) && !empty($dateTo)) {
            $from   = Carbon::createFromFormat('d/m/Y', $dateFrom);
            $to     = Carbon::createFromFormat('d/m/Y', $dateTo);
            $total = $enterpriseRepository->consumptions($this->enterprise)
                ->whereBetween('load_date', [$from->format('Y-m-d'), $to->format('Y-m-d')])
                ->sum('liters');

            if (!$detail) {
                $query = $enterpriseRepository->reportConsumption($this->enterprise, $from, $to, $total);
            } else {
                $query = $enterpriseRepository->reportDetailConsumption($this->enterprise, $from, $to);
            }

            if ($request->query('export', 0) == 1) {
                return $this->exportEnterprise($query, $detail, $total);
            } else {
                $paginator = $query->paginate(25);
            }
        }
        
        return view('reportes.index', [
            'noDate'    => !$dateFrom && !$dateTo,
            'from'      => $dateFrom,
            'to'        => $dateTo,
            'paginator' => $paginator,
            'total'     => $total,
            'detail'    => $detail,
        ]);
    }

    /**
     * @param Builder $builder
     * @param int $detail
     * @param int $total
     */
    protected function exportEnterprise(Builder $builder, $detail = 0, $total = 0)
    {
        if (!(bool) $detail) {
            $builder->select([
                \DB::raw('operation.name as "Nombre Faena"'),
                \DB::raw('operation.id as "ID Faena"'),
                \DB::raw('COALESCE(SUM(chargeables.liters), 0) as Litros'),
                \DB::raw('CONCAT(((COALESCE(SUM(chargeables.liters),0) * 100) / ' . $total . '), \'%\') as Porcentaje')
            ]);

            $data = $this->toExportableArrayData($builder->get());
            return $this->export('Reporte_empresa_', $data);
        } else {
            $builder->select([
                'chargeables.id as ID',
                \DB::raw('DATE_FORMAT(chargeables.load_date, \'%d-%m-%Y\') as "Fecha Carga"'),
                \DB::raw('operation.name as "Nombre Faena"'),
                \DB::raw('equipment.patent as "Patente"'),
                \DB::raw('CONCAT(person.primary_last_name, \' \', person.second_last_name, \', \', person.name) as Operador'),
                'work_shift.code as Turno',
                'fuel_dispenser.name as Dispensador',
                'locations.name as Ubicación',
                'chargeables.liters as Litros',
                'chargeables.hour_meter as Horometro',
            ]);
            $data = $this->toExportableArrayData($builder->get());
            return $this->export('Reporte_empresa_detalle_', $data);
        }
    }

    /**
     * @param Request $request
     * @param EnterpriseRepository $enterpriseRepository
     * @param OperationRepository $operationRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function operation(Request $request, EnterpriseRepository $enterpriseRepository, OperationRepository $operationRepository)
    {
        $dateFrom       = $request->query('from', '');
        $dateTo         = $request->query('to', '');
        $operationId    = $request->query('operation', false);

        $paginator      = false;
        $total          = 0;
        $detail         = (bool) $request->query('detail', 0);
        $operation      = null;
        if (!empty($dateFrom) && !empty($dateTo) && $operationId) {
            $operation  = $enterpriseRepository->findOperationOrFail($this->enterprise, $operationId);

            $from       = Carbon::createFromFormat('d/m/Y', $dateFrom);
            $to         = Carbon::createFromFormat('d/m/Y', $dateTo);
            $total      = $enterpriseRepository->consumptions($this->enterprise)
                ->where('operation_id', $operation->id)
                ->whereBetween('load_date', [$from->format('Y-m-d'), $to->format('Y-m-d')])
                ->sum('liters');

            if (!$detail) {
                $query = $operationRepository->reportConsumption($operation, $from, $to, $total);
            } else {
                $detail = true;
                $query = $operationRepository->reportDetailConsumption($operation, $from, $to);
            }

            if ($request->query('export', 0) == 1) {
                if (!$detail) {
                    return $this->exportOperation(
                        $operationRepository->reportConsumption($operation, $from, $to, $total, true),
                        false,
                        $operation,
                        $from,
                        $to,
                        $total
                    );
                } else {
                    return $this->exportOperation(
                        $operationRepository->reportDetailConsumption($operation, $from, $to),
                        true,
                        $operation,
                        $from,
                        $to,
                        $total
                    );
                }
            } else {
                $paginator = $query->paginate(10);
            }
        }

        return view('reportes.faena', [
            'noDate'    => !$dateFrom && !$dateTo,
            'from'      => $dateFrom,
            'to'        => $dateTo,
            'operationId' => $operationId,
            'operation' => $operation,
            'paginator' => $paginator,
            'total'     => $total,
            'detail'    => $detail,
        ]);
    }

    /**
     * @param Builder $builder
     * @param int $detail
     * @param $operation
     * @param Carbon $from
     * @param Carbon $to
     * @param int $total
     */
    protected function exportOperation(Builder $builder, $detail, $operation, Carbon $from, Carbon $to, $total = 0)
    {
        if ($total == 0) {
            $total = 1;
        }

        if (!(bool) $detail) {
            $builder->select([
                \DB::raw('equipment.id as "ID"'),
                // ppu
                \DB::raw('equipment.patent as "Patente"'),
                \DB::raw('type.name as "Tipo"'),
                \DB::raw('brand.name as "Marca"'),
                \DB::raw('model.name as "Modelo"'),
                \DB::raw('COALESCE(SUM(chargeables.liters), 0) as "Litros"'),
                \DB::raw('CONCAT(((COALESCE(SUM(chargeables.liters),0) * 100) / ' . $total . '), \'%\') as "Porcentaje"'),
            ])
                ->selectSub(function($query) use ($from, $to, $operation) {
                    /**
                     * @var Builder $query
                     */
                    $query->from('chargeables as charg2')
                        ->select([
                            \DB::raw('CASE MAX(charg2.counter) WHEN MIN(charg2.counter) THEN MAX(charg2.counter) ELSE COALESCE(MAX(charg2.counter) - MIN(charg2.counter), 0) END')
                        ])
                        ->whereBetween('charg2.load_date', [$from->format('Y-m-d'), $to->format('Y-m-d')])
                        ->where('charg2.operation_id', $operation->id)
                        ->where('charg2.concept_id', Chargeable::CONCEPTO_CONSUMO)
                        ->where('charg2.loaded_destination_id', \DB::raw('chargeables.loaded_destination_id'));
                }, 'Contador')
                ->selectSub(function($query) use ($from, $to, $operation) {
                    /**
                     * @var Builder $query
                     */
                    $query->from('chargeables as charg')
                        ->select([
                            \DB::raw('CASE MAX(charg.hour_meter) WHEN MIN(charg.hour_meter) THEN MAX(charg.hour_meter) ELSE COALESCE(MAX(charg.hour_meter) - MIN(charg.hour_meter), 0) END')
                        ])
                        ->whereBetween('charg.load_date', [$from->format('Y-m-d'), $to->format('Y-m-d')])
                        ->where('charg.operation_id', $operation->id)
                        ->where('charg.concept_id', Chargeable::CONCEPTO_CONSUMO)
                        ->where('charg.loaded_destination_id', \DB::raw('chargeables.loaded_destination_id'));
                }, 'Horometro');

            $data = $this->toExportableArrayData($builder->get());

            return $this->export('Reporte_empresa_', $data);
        } else {
            $builder->select([
                'chargeables.id as ID',
                \DB::raw('DATE_FORMAT(chargeables.load_date, \'%d-%m-%Y\') as "Fecha Carga"'),
                'operation.name as Faena',
                'equipment.patent as Patente',
                //ppu
                \DB::raw('CONCAT(person.primary_last_name, \' \', person.second_last_name, \', \', person.name) as Operador'),
                'work_shift.code as Turno',
                'fuel_dispenser.name as Dispensador',
                'locations.name as Ubicación',
                'chargeables.liters as Litros',
                'chargeables.hour_meter as Horometro',
            ]);

            $data = $this->toExportableArrayData($builder->get());
            return $this->export('Reporte_empresa_detalle_', $data);
        }
    }

    public function equipment(Request $request, EnterpriseRepository $enterpriseRepository, EquipmentRepository $equipmentRepository)
    {
        $dateFrom       = $request->query('from', '');
        $dateTo         = $request->query('to', '');
        $equipmentId    = $request->query('equipment', '');
        $operationId    = $request->query('operation', '');

        $paginator      = false;
        $total          = 0;
        $detail         = (bool) $request->query('detail', 0);
        $equipment      = null;
        if (!empty($dateFrom) && !empty($dateTo) && $equipmentId) {
            $equipment  = $enterpriseRepository->findEquipmentOrFail($this->enterprise, $equipmentId);

            $from       = Carbon::createFromFormat('d/m/Y', $dateFrom);
            $to         = Carbon::createFromFormat('d/m/Y', $dateTo);
            $total      = $enterpriseRepository->consumptions($this->enterprise)
                ->where('loaded_destination_id', $equipment->id)
                ->whereBetween('load_date', [$from->format('Y-m-d'), $to->format('Y-m-d')])
                ->sum('liters');

            if (!$detail) {
                $query = $equipmentRepository->reportConsumption($equipment, $from, $to, $total);
            } else {
                $detail = true;
                $query = $equipmentRepository->reportDetailConsumption($equipment, $from, $to);
            }

            if ($request->query('export', 0) == 1) {
                if ($detail) {
                    return $this->exportEquipment(
                        $query,
                        $detail,
                        $equipment,
                        $from,
                        $to
                    );
                } else {
                    return $this->exportEquipment(
                        $equipmentRepository->reportConsumption($equipment, $from, $to, $total, true),
                        $detail,
                        $equipment,
                        $from,
                        $to,
                        $total
                    );
                }
            } else {
                $paginator = $query->paginate(25);
            }
        }

        return view('reportes.equipo', [
            'noDate'    => !$dateFrom && !$dateTo,
            'from'      => $dateFrom,
            'to'        => $dateTo,
            'equipmentId' => $equipmentId,
            'operationId' => $operationId,
            'equipment' => $equipment,
            'equipments' => $this->enterprise->equipments()->orderBy('patent', 'ASC')->get(['patent', 'id', 'operation_id']),
            'paginator' => $paginator,
            'total'     => $total,
            'detail'    => $detail,
        ]);
    }

    /**
     * @param Builder $builder
     * @param int $detail
     * @param Equipo $equipment
     * @param Carbon $from
     * @param Carbon $to
     * @param int $total
     */
    protected function exportEquipment($builder, $detail = 0, $equipment, Carbon $from, Carbon $to, $total = 0)
    {
        if (!(bool) $detail) {
            $builder->select([
                \DB::raw('operation.name as "Nombre Faena"'),
                \DB::raw('operation.id as "ID Faena"'),
                \DB::raw('COALESCE(SUM(chargeables.liters), 0) as Litros'),
                \DB::raw('CONCAT(((COALESCE(SUM(chargeables.liters),0) * 100) / ' . $total . '), \'%\') as Porcentaje')
            ])
                ->selectSub(function($query) use ($from, $to, $equipment) {
                    /**
                     * @var Builder $query
                     */
                    $query->from('chargeables as charg2')
                        ->select([
                            \DB::raw('CASE MAX(charg2.counter) WHEN MIN(charg2.counter) THEN MAX(charg2.counter) ELSE COALESCE(MAX(charg2.counter) - MIN(charg2.counter), 0) END')
                        ])
                        ->whereBetween('charg2.load_date', [$from->format('Y-m-d'), $to->format('Y-m-d')])
                        ->where('charg2.loaded_destination_id', $equipment->id)
                        ->where('charg2.concept_id', Chargeable::CONCEPTO_CONSUMO)
                        ->where('charg2.loaded_destination_id', \DB::raw('chargeables.loaded_destination_id'));
                }, 'Contador')
                ->selectSub(function($query) use ($from, $to, $equipment) {
                    /**
                     * @var Builder $query
                     */
                    $query->from('chargeables as charg')
                        ->select([
                            \DB::raw('CASE MAX(charg.hour_meter) WHEN MIN(charg.hour_meter) THEN MAX(charg.hour_meter) ELSE COALESCE(MAX(charg.hour_meter) - MIN(charg.hour_meter), 0) END')
                        ])
                        ->whereBetween('charg.load_date', [$from->format('Y-m-d'), $to->format('Y-m-d')])
                        ->where('charg.loaded_destination_id', $equipment->id)
                        ->where('charg.concept_id', Chargeable::CONCEPTO_CONSUMO)
                        ->where('charg.loaded_destination_id', \DB::raw('chargeables.loaded_destination_id'));
                }, 'Horometro');

            $data = $this->toExportableArrayData($builder->get());
            return $this->export('Reporte_empresa_', $data);
        } else {
            $builder->select([
                'chargeables.id as ID',
                \DB::raw('DATE_FORMAT(chargeables.load_date, \'%d-%m-%Y\') as "Fecha Carga"'),
                'operation.name as Faena',
                'equipment.patent as Patente',
                //ppu
                \DB::raw('CONCAT(person.primary_last_name, \' \', person.second_last_name, \', \', person.name) as Operador'),
                'work_shift.code as Turno',
                'fuel_dispenser.name as Dispensador',
                'locations.name as Ubicacion',
                'chargeables.liters as Litros',
                'chargeables.hour_meter as Horometro',
            ]);
            $data = $this->toExportableArrayData($builder->get());
            return $this->export('Reporte_equipo_detalle_', $data);
        }
    }

    /**
     * @param $data
     * @return array
     */
    protected function toExportableArrayData($data)
    {
        $newData = [];
        foreach ($data as $row) {
            $newRow = [];
            foreach ($row as $column => $value) {
                $newRow[$column] = $value;
            }
            $newData[] = $newRow;
        }

        return $newData;
    }
}
