<?php

namespace App\Http\Controllers\Ajax;

use App\Models\Chargeable;
use App\Models\Empresa;
use App\Models\Faena;
use App\Models\Operation;
use Illuminate\Http\Request;

use App\Http\Requests;

class SearchFiltersController extends Controller
{
    /**
     * @var Empresa|null
     */
    protected $enterprise;

    public function __construct(Request $request)
    {
        parent::__construct($request);

        $this->enterprise = current_enterprise();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function workShiftByOperation(Request $request)
    {
        $operation = Faena::find($request->get('operation_id', null));
        if (!$operation instanceof Faena) {
            return [];
        }

        return $this->arrayPairNameId($operation->turnos);
    }
    
    public function operationsByEnterprise(Request $request)
    {
        $enterprise = Empresa::find($request->get('enterprise_id', null));
                            
        if (!$enterprise instanceof Empresa) {
            return [];
        }
        return $this->arrayPairNameId($enterprise->operationsFilter);
    }
    
    public function operationsByEnterpriseLoan(Request $request)
    {
        $enterprise = Empresa::find($request->get('enterprise_id', null));
                            
        if (!$enterprise instanceof Empresa) {
            return [];
        }
        return $this->arrayPairNameId($enterprise->operationsLoan);
    }
    
    public function dispenserByEnterprise(Request $request){
        $enterprise = Empresa::find($request->get('enterprise_id', null));
        if (!$enterprise instanceof Empresa) {
            return [];
        }
        
        return $this->arrayPairNameId($enterprise->dispensers);
    }
    /*
    public function dispenserByEnterpriseForInternLoan(Request $request){
        $enterprise = \App\Models\Empresa::find($request->get('enterprise_id', null));
        if (!$enterprise instanceof \App\Models\Empresa) {
            return [];
        }
        return $this->arrayPairNameId($enterprise->dispensers->where('type_fuel_dispenser_id', '<>', 3));
    }*/
    
    public function dispenserByOperationForInternLoan(Request $request){
        $operation = Faena::find($request->get('init_operation', null));
        $dispensers = $operation->dispensadores()
                        ->where('type_fuel_dispenser_id', '<>', 3)
                        ->where('active', '=', 1)
                        ->get();
        
        return $this->arrayPairNameId($dispensers);
    }
    
    public function dispenserByOperationForInternLoanDestiny(Request $request){
        $operation = Faena::find($request->get('operation', null));
        $dispensers = $operation->dispensadores()
                        //->join('fuel_dispenser', 'operation_dispenser.fuel_dispenser_id', '=', 'fuel_dispenser.id')
                        ->where('type_fuel_dispenser_id', '<>', 3)
                        ->where('active', '=', 1)
                        ->get();
        
        return $this->arrayPairNameId($dispensers);
    }
    
    public function equipmentByEnterprise(Request $request){
        $equipos    = \App\Models\Equipo::where('enterprise_id', '=', $request->get('enterprise_id', null))
                        ->where('active', '=', 1)
                        ->where('dispenser', '=', 1)
                        ->get();
        
        return $this->arrayPairNameId($equipos, 'id', 'patent');
    }
    
    public function equipmentByOperation(Request $request){
        $equipos    = \App\Models\Equipo::join('brand', 'equipment.brand_id', '=', 'brand.id')
                        ->join('model', 'equipment.model_id', '=', 'model.id')
                        ->join('type', 'equipment.type_id', '=', 'type.id')
                        ->where('equipment.enterprise_id', '=', current_enterprise()->id)
                        ->where('operation_id', '=', $request->get('operation_id', null))
                        ->where('equipment.active', '=', 1)
                        ->orderBy('type.name', 'asc')
                        ->get([\DB::raw('CONCAT("CC: ", equipment.cc, ", ", equipment.patent, ", - ", type.name, ", ", brand.name, ", ", model.name) AS equipos'), 'equipment.id']);
        
        return $this->arrayPairNameId($equipos, 'id', 'equipos');
    }
    
    public function operatorsByOperation(Request $request){
        $operators  = current_enterprise()->operators()
                        //->where('its_responsible', '=', 0)
                        ->where('operator_operation.operation_id', '=', $request->get('operation_id', null))
                        ->where('person.active', '=', 1)
                        ->orderBy('primary_last_name', 'ASC')
                        ->get([\DB::raw("CONCAT(`primary_last_name`,' ',`second_last_name`,', ',`name`) as fullName"), 'person.id']);
        
        return $this->arrayPairNameId($operators, 'id', 'fullName');
    }
     
    public function responsibleByEnterprise(Request $request){
        $responsibles  = Chargeable::join('operator_operation', 'chargeables.responsible_id', '=', 'operator_operation.id')
                        ->join('person', 'operator_operation.person_id', '=', 'person.id')
                        ->where('operator_operation.its_responsible', '=', 1)
                        ->get([\DB::raw("CONCAT(`name`,' ',`primary_last_name`,' ',`second_last_name`) as fullName"), 'person.id']);
                        //->lists('fullName', 'id');
        
        return $this->arrayPairNameId($responsibles, 'id', 'fullName');
    }
    /*
    public function productiveAreaByOperation(Request $request){
        $faenas  = \App\Models\Faena::find($request->get('operation_id', null));
        
        return $this->arrayPairNameId($faenas->productiveArea);
    }*/
    
    public function typesByFamily(Request $request){
        $types      = \App\Models\Tipo::where('category_id', '=', $request->get('category_id', null))
                            ->where('ci_type_id', '=', 1)
                            ->get();
        
        return $this->arrayPairNameId($types, 'id', 'name');
    }
    
    public function modelsByBrands(Request $request){
        $models     = \App\Models\Modelo::where('brand_id', '=', $request->get('brand_id', null))
                            ->get();
        
        return $this->arrayPairNameId($models);
    }
    
    public function providerByOperation(Request $request){
        $operation = Faena::find($request->get('operation_id', null));
        if (!$operation instanceof Faena) {
            return [];
        }
        
        return $this->arrayPairNameId($operation->proveedores);
    }
    
    public function locationsByOperation(Request $request){
        $operation = Faena::find($request->get('operation_id', null));
        if (!$operation instanceof Faena) {
            return [];
        }
        
        return $this->arrayPairNameId($operation->ubicaciones);
    }
    
    public function externalByOperation(Request $request){
        $operation = Faena::find($request->get('operation_id', null));
        if (!$operation instanceof Faena) {
            return [];
        }
        
        return $this->arrayPairNameId($operation->externos);
    }
    
    public function dispenserByOperation(Request $request){
        $operation = Faena::find($request->get('operation_id', null));
        if (!$operation instanceof Faena) {
            return [];
        }

        $dispensers = $operation->dispensadores()
                        ->where('fuel_dispenser_id', '<>', 3)
                        //->where('active', '=', 0)
                        ->get();
        
        return $this->arrayPairNameId($dispensers);
    }
    
    public function dispenserByOperationDestination(Request $request) {
        $operation = Operation::find($request->get('operation_id', null));
        if (!$operation instanceof Operation) {
            return [];
        }

        $dispensers = $operation->dispensadores()
                        ->where('fuel_dispenser_id', '<>', 3)
                        //->where('active', '=', 0)
                        ->get();
        
        return $this->arrayPairNameId($dispensers);
    }
    
    public function dispenserByOperationAddTct(Request $request){
        $operation = Faena::find($request->get('operation_id', null));
                            //->where('active', '=', 0);
        $dispensers = $operation->dispensadores()->get();
        
        return $this->arrayPairNameId($dispensers);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function lastHourMeterByEquipment(Request $request)
    {
        return $this->enterprise->chargeables()
            ->where('loaded_destination_id', '=', $request->get('loaded_destination_id', null))
            ->orderBy('load_date', 'DESC')
            ->select('hour_meter')
            ->first();
    }
    
    protected function arrayPairNameId($collection, $keyName = 'id', $nameName = 'name')
    {
        $data = [];
        foreach ($collection as $item) {
            $data[] = [
                'text'  => $item->{$nameName},
                'value' => $item->{$keyName},
            ];
        }
        
        return $data;
    }
    
    
    public function componentsByResponsible(Request $request, $affectedId){
        return \App\Models\Componente::where('responsible_id', '=', $affectedId)
                    ->get();
    }
    
    public function affectedByEnterprise(Request $request){
        $operation = Faena::find('enterprise_id', '=', current_enterprise()->id);
        $affected  = $operation->person();
        
        return $this->arrayPairNameId($affected);
    }
    
    public function level_2ByLevel_1(Request $request){
        $level_2 = \App\Models\Catalogo::join('platform', 'catalogue.platform_id', '=', 'platform.id')
                        ->where('catalogue.type_request_id', '=', $request->get('type_request_id', null))
                        ->groupBy('platform.id', 'platform.name')
                        ->select('platform.name', 'platform.id')->get();
                        
        return $this->arrayPairNameId($level_2);
    }
    
    public function level_3ByLevel_2(Request $request){
        $level_3 = \App\Models\Catalogo::join('n3_system', 'catalogue.n3_system_id', '=', 'n3_system.id')
                        ->where('catalogue.type_request_id', '=', $request->get('type_request_id', null))
                        ->where('catalogue.platform_id', '=', $request->get('platform_id', null))
                        ->groupBy('n3_system.id', 'n3_system.name')
                        ->select('n3_system.name', 'n3_system.id')->get();
                        
        return $this->arrayPairNameId($level_3);
    }
    
    public function level_4ByLevel_3(Request $request){
        $level_4 = \App\Models\Catalogo::join('n4_sub_system', 'catalogue.n4_sub_system_id', '=', 'n4_sub_system.id')
                        ->where('catalogue.n3_system_id', '=', $request->get('n3_system_id', null))
                        ->where('catalogue.platform_id', '=', $request->get('platform_id', null))
                        ->where('catalogue.type_request_id', '=', $request->get('type_request_id', null))
                        ->groupBy('n4_sub_system.id', 'n4_sub_system.name')
                        ->select('n4_sub_system.name', 'n4_sub_system.id')->get();
                        
        return $this->arrayPairNameId($level_4);
    }
    
    public function level_5ByLevel_4(Request $request){
        $level_5 = \App\Models\Catalogo::join('n5', 'catalogue.n5', '=', 'n5.id')
                        ->where('catalogue.n4_sub_system_id', '=', $request->get('n4_sub_system_id', null))
                        ->where('catalogue.n3_system_id', '=', $request->get('n3_system_id', null))
                        ->where('catalogue.platform_id', '=', $request->get('platform_id', null))
                        ->where('catalogue.type_request_id', '=', $request->get('type_request_id', null))
                        ->groupBy('n5.id', 'n5.name')
                        ->select('n5.name', 'n5.id')->get();
                        
        return $this->arrayPairNameId($level_5);
    }
    
    public function level_6ByLevel_5(Request $request){
        $level_6 = \App\Models\Catalogo::join('n6', 'catalogue.n6', '=', 'n6.id')
                        ->where('catalogue.n5', '=', $request->get('n5', null))
                        ->where('catalogue.n4_sub_system_id', '=', $request->get('n4_sub_system_id', null))
                        ->where('catalogue.n3_system_id', '=', $request->get('n3_system_id', null))
                        ->where('catalogue.platform_id', '=', $request->get('platform_id', null))
                        ->where('catalogue.type_request_id', '=', $request->get('type_request_id', null))
                        ->groupBy('n6.id', 'n6.name')
                        ->select('n6.name', 'n6.id')->get();
                        
        return $this->arrayPairNameId($level_6);
    }
}