<?php

namespace App\Http\Controllers\Ajax;

use App\Models\Chargeable;
use App\Repositories\ChargeableRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class ChargeablesController extends Controller
{
    public function confirm(Request $request, $id, ChargeableRepository $repository){
        $chargeable = Chargeable::findOrFail($id);
        
        $repository->update($chargeable, ['received' => 1]);
        
        //\event::dispatch(new )
        return response()->json(['Satus' => 'OK']);
    }
}
