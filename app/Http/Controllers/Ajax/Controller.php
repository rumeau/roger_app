<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    public function __construct(Request $request)
    {
        if (!\App::runningInConsole() && !$request->ajax()) {
            abort(400);
        }
    }
}
