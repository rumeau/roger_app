<?php

namespace App\Http\Controllers;

use App\Models\Chargeable;
use Illuminate\Http\Request;

use App\Http\Requests;

class ChargeablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //Asignar lista de familias a una variable
        $selectEmpresas = \App\Models\Empresa::all()->lists('name', 'id');
        $selectConceptos = \App\Models\Concepto::whereIn('id', array(1, 2, 3, 5, 6))->lists('name', 'id');
        //Asignar tipos a una variable

        $charges = Chargeable::paginate(25);

        // Devuelve el formulario para crear
        return view('charges.index')->with([
            'selectEmpresas'  => $selectEmpresas,
            'selectConceptos' => $selectConceptos,
            'charges'         => $charges,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
         // solo si es ajax
        if ($request->ajax()){
            switch ($request->query('task')){
                case 'getDispensadorO':
                    $faena = \App\Models\Faena::find($request->query('faena'));
                    $data = [];
                    if (!$faena instanceof \App\Models\Faena){
                        break;
                    }
                    if ($request->query('concepto') == 2){
                        $dispensadores = $faena->dispensadores()->where('id', '<>', 3);
                    }else{
                        $dispensadores = $faena->dispensadores;
                    }
                    foreach($dispensadores as $dispensador){
                        $data[] = ['text' => $dispensador->name, 'value' => $dispensador->id];
                    }
                    break;
                case 'getEquipos':
                    $faena = \App\Models\Faena::find($request->query('faena'));
                    $data = [];
                    if (!$faena instanceof \App\Models\Faena){
                        break;
                    }
                    $equipos = $faena->equipos;
                    //dd($equipos);
                    foreach($equipos as $equipo){
                        $tipo = \App\Models\Tipo::find($equipo->type_id);
                        //dd($request);
                        $data[] = ['text' => "CC: ".$equipo->cc." PATENTE: ".$equipo->patent." | ".$tipo->name, 
                        'value' => $equipo->id];
                    }
                break;
            }
            return $data;
        }
        //Asignar lista de familias a una variable
        //Deja seleccionado el concepto en cada mantenedor, según el menu seleccionado. De este modo el usuario
        //no debe seleccionar nada.
        $selectEmpresas = \App\Models\Empresa::all()->lists('name', 'id');
        $el_concepto = $request->query('concepto');
        if ($el_concepto == 1){
            $selectConceptos = \App\Models\Concepto::where('id', '=', 1)->lists('name', 'id');
        }elseif ($el_concepto == 2){
            $selectConceptos = \App\Models\Concepto::where('id', '=', 2)->lists('name', 'id');
        }else{
            $selectConceptos = \App\Models\Concepto::whereIn('id', array(1, 2, 3, 5, 6))->lists('name', 'id');
        }
        //Asignar tipos a una variable
        
        // Devuelve el formulario para crear
        return view('charges.create')->with([
            'selectEmpresas' => $selectEmpresas,
            'selectConceptos' => $selectConceptos
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(\App\Http\Requests\ChargeRequest $request, \App\Repositories\ChargeableRepository $repository)
    {
        $data = $request->only(
            'enterprise_id',
            'operation_id',
            'concept_id',
            'load_date',
            'loaded_origin_id',
            'loaded_destination_id',
            'loaded_origin_type',
            'loaded_destination_type',
            'work_shift_id',
            'responsible_id',
            'operator_id',
            'ubication_id',
            'order_number',
            'bill_number',
            'liters',
            'counter',
            'hour_meter',
            'net_price',
            'fuel_tax'
        );
        $repository->create($data);
        // mostrar un mensaje de exito, y volver al inicio
        //return redirect(route('chargeables.index'));
        if($request->input('concept_id') == 1) {
            return redirect(route('compra.index'));
        }elseif($request->input('concept_id') == 3) {
            return redirect(route('consumption.index'));
        }elseif($request->input('concept_id') == 5) {
            return redirect(route('loan.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
