<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $empresas = \App\Models\Empresa::with('region')->paginate(10);
        //dd($empresas);
        if ($request->ajax()) {
            return [
                'data'          => $empresas->getCollection(),
                'itemsCount'    => $empresas->total()
            ];
        }
        
        return view('empresas.index')->with([
            "empresas"             => $empresas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $selectRegiones = \App\Models\Region::all()
                            ->lists('name', 'id');
                            
        return view('empresas.create')->with([
            "selectRegiones"        => $selectRegiones,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request, \App\Repositories\ChargeableRepository $repository)
    {
        $this->validate($request, $this->rules());
        
        $data = $request->only(
            'name',
		    'rut',
		    'region_id',
		    'commune',
		    'address'
		);

        $data['created_by'] = user()->id;

        $newEnterprise = new \App\Models\Empresa($data);
        $newEnterprise->save();
        //return $newEnterprise;
        flash()->success('La empresa ha sido registrada');
        
        if ($request->input('task', 'save_n_new') == 'save') {
            // volver al index luego de guardar
            return redirect(route('empresa.index'));
        }
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'                  => 'required',
            'rut'                   => 'required',
            'region_id'             => 'required|exists:region,id',
            'address'               => 'required'
        ];
        return $rules;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
