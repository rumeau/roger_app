<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NewDispensadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //FILTRO DE BÚSQUEDA
        //llena el select, con el nombre de todas las empresas. Es el filtro principal.
        $selectEmpresas      = \App\Models\Empresa::all()->lists('name', 'id');
        
        $empresa             = current_enterprise()->id;//setea el "id" de la empresa seleccionada.
        
        $selectFaenas        = \App\Models\Faena::all();
        
        $dispensadores       = \App\Models\Dispensador::with('ubication', 'enterprise', 'operation');
        
        //$this->applySearchFilters($request, $dispensadores);
        $dispensadores = $dispensadores->paginate($request->query('pageSize', 25), ['*'], 'pageIndex');

        if ($request->ajax()) {
            return [
                'data' => $dispensadores->getCollection(),
                'itemsCount' => $dispensadores->count()
            ];
        }
        
        return view('newdispensador.index')->with([
            "selectEmpresas"            => $selectEmpresas,
            'selectFaenas'              => $selectFaenas,
            "dispensadores"             => $dispensadores
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        //carga el filtro inicila (principal), con las emoresas configuradas en el sistema
        $selectEmpresas     = \App\Models\Empresa::all()
                                    ->lists('name', 'id');
                                    
        $selectFaenas       = \App\Models\Faena::all();
        
        $selectTipoDispensadores = \App\Models\TipoDispensador::all()
                                    ->lists('name', 'id');
        
         return view('newdispensador.create')->with([
            'selectEmpresas'            => $selectEmpresas,
            'selectFaenas'              => $selectFaenas,
            "selectTipoDispensadores"   => $selectTipoDispensadores,
        ]);
    }

    public function store(Request $request, \App\Repositories\DispenserRepository $repository)
    {
        //$this->validate($request, $this->rules());
        //CAMPOS QUE VIENEN DIRECTAMENTE DEL FORMULARIO
        $data = $request->only(
            'enterprise_id',
		    'name',
		    'capacity',
		    'beginning_balance',
		    'internal',
		    'administration',
		    'require_counter',
		    'require_liters',
		    'active'
		);
		$data['created_by'] = 1;
		
        $repository->create($data);
        
        flash()->success('El dispensador ha sido registrado');
        
        if ($request->input('task', 'save_n_new') == 'save_n_new') {
            // crear otro
            return redirect(route('newdispensador.create'));
        } elseif ($request->input('task', 'save_n_new') == 'save') {
            // volver al index luego de guardar
            return redirect(route('newdispensador.index'));
        }
    }
    
    public function rules()
    {
        $rules = [
            'enterprise_id'             => 'required|exists:enterprise,id',
            'name'                      => 'required',
            'capacity'                  => 'required',
            'beginning_balance'         => 'required|numeric',
            'internal'                  => 'required',
            'administration'            => 'required|numeric',
            'require_counter'           => 'required|numeric',
            'require_liters'            => 'required|numeric',
            'active'                    => 'required|numeric',
            'created_by'                => 'required',
        ];
        return $rules;
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}