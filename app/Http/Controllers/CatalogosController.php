<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CatalogosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //
        $catalogos = \App\Models\Catalogo::join('catalogue_type', 'catalogue.catalogue_type_id', '=', 'catalogue_type.id')
                        ->join('type_request', 'catalogue.type_request_id', '=', 'type_request.id')
                        ->join('platform', 'catalogue.platform_id', '=', 'platform.id')
                        ->join('n3_system', 'catalogue.n3_system_id', '=', 'n3_system.id')
                        ->join('n4_sub_system', 'catalogue.n4_sub_system_id', '=', 'n4_sub_system.id')
                        ->join('n5','catalogue.n5', '=', 'n5.id')
                        ->join('n6','catalogue.n6', '=', 'n6.id')
                        ->select('catalogue_type.name', 'type_request.name as n1', 'platform.name as n2', 'n3_system.name as n3', 
                                 'n4_sub_system.name as n4', 'n5.name as n5', 'n6.name as n6');
                        
        $catalogos = $catalogos->paginate($request->query('pageSize', 25), ['*'], 'pageIndex');
        
        $selectTipoCatalogo = \App\Models\TipoCatalogo::all()->lists('name', 'id');
        
        if ($request->ajax()) {
            return [
                'data'       => $catalogos->getCollection(),
                'itemsCount' => $catalogos->total()
            ];
        }
        return view('catalogos.index')->with([
            "catalogos"             => $catalogos,
            "selectTipoCatalogo"    => $selectTipoCatalogo,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
