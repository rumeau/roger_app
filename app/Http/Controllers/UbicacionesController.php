<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UbicacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()){
            switch ($request->query('task')){
                case 'getUbicaciones':
                    $empresa = $request->query('empresa');
                    $ubicaciones     = \App\Models\Ubicacion::where('enterprise_id', '=', $empresa)
                            ->where('active', '=', 1)
                            ->distinct()
                            ->groupBy('name')
                            ->get();
                    $data            = [];
                    foreach ($ubicaciones as $ubicacion) {
                        $data[]  = ['text' => $ubicacion->name, 'value' => $ubicacion->id];
                    }
                break;
                case 'getDispensadores':
                default:
                    $empresa = $request->query('empresa');
                    $dispensadores = \App\Models\Dispensador::where('enterprise_id', '=', $empresa)->get();
                    $data = [];
                    foreach($dispensadores as $dispensador){
                       $data[] = ['text' => $dispensador->name, 'value' => $dispensador->id];
                    }
                break;
            }
            return $data;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
