<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ConfiguracionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('configuracion.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        // solo si es ajax
        if ($request->ajax()){
            switch ($request->query('task')){
                case 'getFaenas':
                default:
                    $empresa = $request->query('empresa');
                    $faenas = \App\Models\Faena::where('enterprise_id', '=', $empresa)->where('active', '=', 1)->get();
                    $data = [];
                    foreach ($faenas as $faena) {
                        $data[] = ['text' => $faena->name, 'value' => $faena->id];
                     }
                    break;
                case 'getProveedores':
                    //dd($request->query('faena'));
                    $faena = \App\Models\Faena::find($request->query('faena'));
                    $data = [];
                    if (!$faena instanceof \App\Models\Faena){
                        break;
                    }
                    $proveedores = $faena->proveedores;
                    foreach ($proveedores as $proveedor) {
                        $data[] = ['text' => $proveedor->name, 'value' => $proveedor->id];
                     }
                    break;
                case 'getDispensadorO':
                    $faena = \App\Models\Faena::find($request->query('faena'));
                    $data = [];
                    if (!$faena instanceof \App\Models\Faena){
                        break;
                    }
                    //if ($request->query('concepto') <> 1){
                        $dispensadores = $faena->dispensadores;
                    //}else{
                    //    $dispensadores = $faena->dispensadores()->where('fuel_dispenser_id', '<>', 3)->get();
                    //}
                    foreach($dispensadores as $dispensador){
                       $data[] = ['text' => $dispensador->name, 'value' => $dispensador->id];
                    }
                    break;
            }
            return $data;
        }
        //Asignar lista de familias a una variable
        $selectEmpresas = \App\Models\Empresa::all()->lists('name', 'id');
        $selectConceptos = \App\Models\Concepto::all()->lists('name', 'id');
            
        //Asignar tipos a una variable
        
        // Devuelve el formulario para crear
        return view('compras.create')->with([
            'selectEmpresas' => $selectEmpresas,
            'selectConceptos' => $selectConceptos,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(\App\Http\Requests\ComprasRequest $request)
    {
        $data = $request->only(
            'enterprise_id',
		    'operation_id',
		    'load_date',
		    'order_number',
		    'bill_number',
		    'provider_id',
		    'fuel_dispenser_id',
		    'concept_id',
		    'liters',
		    'net_price',
		    'fuel_tax'
		);
		$data['net_value'] = $data['liters']*number_cl($data['net_price']);
		$data['tax'] = round(($data['net_value']*0.19), 5, PHP_ROUND_HALF_UP);
		$data['created_by'] = 1;
		$data['created_at'] = time();
		$data['total_amount'] = $data['net_value']+$data['fuel_tax']+$data['tax'];
        $log = \App\Models\Compra::create($data);
        
        return redirect(route('compra.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
