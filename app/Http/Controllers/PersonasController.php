<?php

namespace App\Http\Controllers;

use App\Repositories\PersonRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PersonasController extends Controller
{
    /**
     * @var \App\Models\Empresa|null
     */
    protected $enterprise;

    public function __construct()
    {
        $this->enterprise = current_enterprise();
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param PersonRepository $personRepository
     * @return \Illuminate\View\View
     */
    public function index(Request $request, PersonRepository $personRepository)
    {
        $personas = $personRepository->getPersonsByEnterpriseJoined($this->enterprise);
        $this->applySearchFilters($request, $personas);

        $personas->select([
            'person.id as id',
            \DB::raw('CONCAT(person.primary_last_name, \' \', person.second_last_name, \', \', person.name) as name'),
            \DB::raw('CONCAT(person.rut, \'-\', dv) as rut'),
        ]);

        $paginador = $personas->paginate($request->query('pageSize', 25), ['*'], 'pageIndex');

        if ($request->ajax()) {
            return [
                'data'       => $paginador->items(),
                'itemsCount' => $paginador->total()
            ];
        }
        
        return view('personas.index');
    }

    /**
     * @param Request $request
     * @param Builder|\Illuminate\Database\Query\Builder $personas
     */
    public function applySearchFilters(Request $request, &$personas)
    {
        if ($request->input('sortField', '') != '') {
            switch ($request->input('sortField', '')) {
                case 'name':
                    $personas->orderBy('person.primary_last_name', $request->input('sortOrder', 'ASC'));
                    $personas->orderBy('person.second_last_name', $request->input('sortOrder', 'ASC'));
                    $personas->orderBy('person.name', $request->input('sortOrder', 'ASC'));
                    break;
                case 'rut':
                    $personas->orderBy(\DB::raw('CAST(person.rut as unsigned)'), $request->input('sortOrder', 'ASC'));
                    break;
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
