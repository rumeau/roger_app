<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EquiposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $selectEmpresas     = \App\Models\Empresa::all()->lists('name', 'id');
        
	    $selectFaenas       = \App\Models\Faena::all()->lists('name', 'id');
	    
		$selectFamilias     = \App\Models\Category::where('ci_type_id', '=', 1)
		                            ->lists('name', 'id');
		                                
		$selectTipos        = \App\Models\Type::where('ci_type_id', '=', 1)
		                            ->lists('name', 'id');

        $selectMarcas       = \App\Models\Marca::where('ci_type_id', '=', 1)
                                    ->lists('name', 'id');
                                        
        $selectStatus       = \App\Models\Status::all()->lists('name', 'id');
		
		$selectEquipos      = \App\Models\Equipo::join('brand', 'equipment.brand_id', '=', 'brand.id')
                                    ->join('model', 'equipment.model_id', '=', 'model.id')
                                    ->join('type', 'equipment.type_id', '=', 'type.id')
                                    //->where('equipment.enterprise_id', '=', current_enterprise()->id)
                                    ->select('equipment.id', \DB::raw('CONCAT("CC: ", equipment.cc, ", ", equipment.patent, ", - ", type.name, ", ", brand.name, ", ", model.name) AS equipos'))
                                    ->orderBy('type.name', 'asc')
                                    //->orderBy('equipment.patent', 'asc')
                                    ->lists('equipos', 'id');
                                    
	    $equipos            = \App\Models\Equipo::with('enterprise', 'operation', 'category', 'type', 'brand', 'model', 'status');
	                                  //->where('active', '=', 1);
	                                //->orderBy('patent', 'asc');
	    //                            ->where('patent', '=', 'BZPJ96-4')->get();
	    //dd($equipos);
	    $this->applySearchFilters($request, $equipos);
        
        $equipos = $equipos->paginate($request->query('pageSize', 25), ['*'], 'pageIndex');
        
        if ($request->ajax()) {
            return [
                'data'          => $equipos->getCollection(),
                'itemsCount'    => $equipos->total()
            ];
        }
        
        return view('equipos.index')->with(array(
		    "selectEmpresas"        => $selectEmpresas,
		    "selectFaenas"          => $selectFaenas,
		    "selectMarcas"          => $selectMarcas,
		    "selectFamilias"        => $selectFamilias,
		    "selectTipos"           => $selectTipos,
		    "selectEquipos"         => $selectEquipos,
		    //"selectTipoIdentif"     => $selectTipoIdentif,
		    //"selectPropietario"     => $selectPropietario,
		    "selectStatus"          => $selectStatus,
		    "equipos"               => $equipos
		));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $selectEmpresas     = \App\Models\Empresa::all()->lists('name', 'id');
        
        $selectFamilias     = \App\Models\Category::where('ci_type_id', '=', 1)
		                                ->lists('name', 'id');
		                                
        $selectMarcas       = \App\Models\Marca::where('ci_type_id', '=', 1)
                                        ->lists('name', 'id');
                                        
        $selectTipoIdentif  = \App\Models\TipoIdentif::all()->lists('name','id');
        $selectPropietario  = \App\Models\Propietario::all()->lists('name', 'id');
        $selectStatus       = \App\Models\Status::all()->lists('name', 'id');

        // Devuelve el formulario para crear
        return view('equipos.create')->with([
            'selectEmpresas'     => $selectEmpresas,
            'selectFamilias'     => $selectFamilias,
            'selectMarcas'       => $selectMarcas,
            'selectTipoIdentif'  => $selectTipoIdentif,
            'selectPropietario'  => $selectPropietario,
            'selectStatus'       => $selectStatus
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules());
        
        //CAMPOS QUE VIENEN DIRECTAMENTE DEL FORMULARIO
        $data = $request->only(
            'enterprise_id',
            'operation_id',
		    'cc',
		    'patent',
		    'type_identification_id',
		    'category_id',
		    'type_id',
		    'brand_id',
		    'model_id',
		    'serial_number',
		    'engine',
		    'year',
		    'lease',
		    'owner_id',
		    'active',
		    'status_id',
		    'fuel',
		    'dispenser',
		    'fuel_tank_capacity'
		);
		
		$data['created_by'] = user()->id;
		
		$newEquipo = new \App\Models\Equipo($data);
        $newEquipo->save();
		/*
		if ($request->input('task', 'save_n_new') == 'save') {
            // volver al index luego de guardar
            return redirect(route('equipo.index'));
        }
        */
        if ($request->input('task', 'save_n_new') == 'save') {
            // volver al index luego de guardar
            return response()->json([
                'message'       => 'El equipo ha sido registrado',
                'success'       => true,
                'status'        => 'success',
                'redirect'      => route('equipo.index'),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $selectFaenas       = \App\Models\Faena::where('enterprise_id', '=', current_enterprise()->id)
                                    ->where('external', '<>', 1)
                                    ->lists('name', 'id');
                                    
        $equipos            = \App\Models\Equipo::with('enterprise', 'operation', 'category', 'type', 'brand', 'model', 'status')
                                        ->findOrFail($id);
                                        
        $selectTipoIdentif  = \App\Models\TipoIdentif::all()->lists('name','id');
        
        $selectMarcas       = \App\Models\Marca::where('ci_type_id', '=', 1)
                                        ->lists('name', 'id');
        
        $selectFamilias     = \App\Models\Category::where('ci_type_id', '=', 1)
		                                ->lists('name', 'id');

        if ($request->ajax()){
            return $equipos;
        }

        return view('equipos.edit')->with([
            'equipos'           => $equipos,
            'selectTipoIdentif' => $selectTipoIdentif,
            'selectFamilias'    => $selectFamilias,
		    'selectMarcas'      => $selectMarcas,
            'selectPropietario' => \App\Models\Propietario::all()->lists('name', 'id'),
            'selectStatus'      => \App\Models\Status::all()->lists('name', 'id'),
            'selectEmpresas'    => \App\Models\Empresa::all()->lists('name', 'id'),
            'selectConceptos'   => \App\Models\Concepto::where('id', '=', 1)->first(),
            'selectFaenas'      => $selectFaenas,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules());
        //dd($request);
        //CAMPOS QUE VIENEN DIRECTAMENTE DEL FORMULARIO
        $data = $request->only(
            'enterprise_id',
            'operation_id',
		    'cc',
		    'patent',
		    'type_identification_id',
		    'category_id',
		    'type_id',
		    'brand_id',
		    'model_id',
		    'serial_number',
		    'engine',
		    'year',
		    'lease',
		    'owner_id',
		    'active',
		    'status_id',
		    'fuel',
		    'dispenser',
		    'fuel_tank_capacity'
		);
		
		//$data['active'] = $request('active');
		//if($data['active'] === 'false'){
		//    'checked = "unchecked"';
		//}else{
		//    'checked = "checked"';
		//}
		
		$data['created_by'] = user()->id;

		$equipo = \App\Models\Equipo::findOrFail($id);
		$equipo->update($data);
		
		flash()->success('El equipo ha sido modificado.');
		
        return response()->json([
            'status'     => 'success',
            'success'    => true,
            'message'    => 'El equipo ha sido modificado.',
            'redirect'   => route('equipo.index'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function rules(){
        $rules = [
            'enterprise_id'         => 'required',
            'operation_id'          => 'required',
            'cc'                    => 'required',
            'patent'                => 'required',
            'category_id'           => 'required',
            'type_id'               => 'required',
            'brand_id'              => 'required',
            'model_id'              => 'required',
            'active'                => 'required'
        ];
        return $rules;
    }
    
    protected function applySearchFilters(Request $request, &$query)
    {
        if ($request->has('enterprise')) {
            $query->where('enterprise_id', '=', $request->input('enterprise'));
        }
        
        if ($request->has('operation')) {
            $query->where('operation_id', '=', $request->input('operation'));
        }
        
        if ($request->has('category')) {
            $query->where('category_id', '=', $request->input('category'));
        }
        
        if ($request->has('type')) {
            $query->where('type_id', '=', $request->input('type'));
        }
        
        if ($request->has('equipment')) {
            $query->where('id', '=', $request->input('equipment'));
        }
        
        if ($request->has('active') && $request->input('active', 'false') == 'true') {
            $query->where('active', '=', 0);
        }else{
            $query->where('active', '=', 1);
        }
        
        if ($request->has('brand')) {
            $query->where('brand_id', '=', $request->input('brand'));
        }
        
        if ($request->has('model')) {
            $query->where('model_id', '=', $request->input('model'));
        }
        
        if ($request->has('condition')) {
            $query->where('status_id', '=', $request->input('condition'));
        }
        //echo $request->input('lease', 'false');
        
        // Si lease se ha marcado, filtrar los equipos donde lease = 1
        if ($request->has('lease') && $request->input('lease', 'false') == 'true') {
            //echo 0;
            $query->where('lease', '=', 1);
        }
        // Si lease no esta seleccionado, no se filtra por lease
        //else {
            //echo 1;
          //  $query->where('lease', '=', 0);
        //}
        /*
        if ($request->has('lease')){
            $query->where('lease', '=', $request->input('lease'));
        }*/
        
        $patent = $request->input('patent');
        
        if ($request->has('patent')) {
            $query->where('patent', 'LIKE', "%$patent%");
        }
        if ($request->has('cc')) {
            $query->where('cc', '=', $request->input('cc'));
        }

        // Sorting
        if ($request->has('sortField')) {
            $sortField = $request->input('sortField', 'load_date');
            switch ($sortField) {
                case 'operation' :
                    $sortField = 'operation.name';
                    $query->with(['operation' => function($q) use ($request) {
                        $q->orderBy('name', $request->input('sortOrder', 'asc'));
                    }]);
                    break;
                    
                case 'loaded_origin':
                case 'loaded_destination':
                    $sortField = 'chargeables.' . $sortField . '_id';
                    $query->orderBy($sortField, $request->input('sortOrder', 'asc'));
                    break;
                    
                default:
                    $sortField = 'chargeables.' . $sortField;
                    $query->orderBy($sortField, $request->input('sortOrder', 'asc'));
                    break;
            }
        }
        return $query;
    }
}