<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class InitDispensadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $empresa        = current_enterprise()->id;
        $selectEmpresas = \App\Models\Empresa::all()->lists('name', 'id');
        $selectFaenas   = \App\Models\Faena::where('enterprise_id', '=', $empresa)
                                ->where('visible', '=', 1)
                                ->lists('name');
        
        $dispensadores  = \App\Models\InitDispenser::with('operation', 'dispenser', 'concept');//->get();
                                //->lists('fuel_dispenser_id');
                                
        $this->applySearchFilters($request, $dispensadores);
        
        $dispensadores = $dispensadores->paginate($request->query('pageSize', 25), ['*'], 'pageIndex');
        
        if ($request->ajax()) {
            return [
                'data'       => $dispensadores->getCollection(),
                'itemsCount' => $dispensadores->total()
            ];
        }                            
                                
        //dd($dispensadores);
        return view('initdispensador.index')->with([
            "selectEmpresas"     => $selectEmpresas,
            "selectFaenas"       => $selectFaenas,
            "dispensadores"      => $dispensadores
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $selectEmpresas     = \App\Models\Empresa::all()->lists('name', 'id');
        
        
        $selectFaenas       = \App\Models\Faena::where('enterprise_id', '=', current_enterprise()->id)
                                    ->where('external', '<>', 1)
                                    ->where('visible', '=', 1)
                                    ->lists('name', 'id');
       
        return view('initdispensador.create')->with([
            'selectEmpresas'    => $selectEmpresas//,
            //'selectFaenas'      => $selectFaenas
        ]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->only(
		    'operation_id',
		    'fuel_dispenser_id',
		    'beginning_residue',
		    'date_residue',
		    'capacity'
		);
		$date['date_residue'] = time();
		$data['created_by'] = 1;
		$data['created_at'] = time();
        $initDispensador = \App\Models\InitDispenser::create($data);
        
        return redirect(route('initdispensador.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    
    protected function applySearchFilters(Request $request, &$query)
    {
        if ($request->has('operation')) {
            $query->where('operation_id', '=', $request->input('operation'));
        }
        
        if ($request->has('work_shift')) {
            $query->where('work_shift_id', '=', $request->input('work_shift'));
        }
        
        if ($request->has('date_from') && $request->has('date_to')) {
            $from = Carbon::createFromFormat('d-m-Y', $request->input('date_from'))->hour(0)->minute(0)->second(0);
            $to   = Carbon::createFromFormat('d-m-Y', $request->input('date_to'))->hour(23)->minute(59)->second(59);
            $query->where(function($q) use ($from, $to) {
                $q->whereDate('load_date', '>=', $from)
                    ->whereDate('load_date', '<=', $to);
            });
        } elseif ($request->has('date_from') && !$request->has('date_to')) {
            $from = Carbon::createFromFormat('d-m-Y', $request->input('date_from'))->hour(0)->minute(0)->second(0);
            $query->whereDate('load_date', '>=', $from);
        } elseif (!$request->has('date_from') && $request->has('date_to')) {
            $to   = Carbon::createFromFormat('d-m-Y', $request->input('date_to'))->hour(23)->minute(59)->second(59);
            $query->whereDate('load_date', '<=', $to);
        }
        
        if ($request->has('order_number')) {
            $query->where('order_number', '=', $request->input('order_number'));
        }
        
        if ($request->has('bill_number')) {
            $query->where('bill_number', '=', $request->input('bill_number'));
        }
        
        if ($request->has('provider')) {
            $query->where('loaded_origin_id', '=', $request->input('provider'));
        }
        
        if ($request->has('dispenser')) {
            $query->where('loaded_destination_id', '=', $request->input('dispenser'));
        }
        
        if ($request->has('received') && $request->input('received', 'false') == 'true') {
            $query->where('received', '=', $request->input('received'));
        }
        
        // Sorting
        if ($request->has('sortField')) {
            $sortField = $request->input('sortField', 'load_date');
            switch ($sortField) {
                case 'operation' :
                    $sortField = 'operation.name';
                    $query->with(['operation' => function($q) use ($request) {
                        $q->orderBy('name', $request->input('sortOrder', 'asc'));
                    }]);
                    break;
                    
                case 'loaded_origin':
                case 'loaded_destination':
                    $sortField = 'chargeables.' . $sortField . '_id';
                    $query->orderBy($sortField, $request->input('sortOrder', 'asc'));
                    break;
                    
                default:
                    $sortField = 'chargeables.' . $sortField;
                    $query->orderBy($sortField, $request->input('sortOrder', 'asc'));
                    break;
            }
        }
        
        return $query;
    }
}