<?php
namespace App\Exports;

class ConsumptionListExportHandler implements \Maatwebsite\Excel\Files\ExportHandler
{
    public function handle($export)
    {
        $export->getData();
        
        // work on the export
        return $export->sheet('sheetName', function($sheet) use ($export)
        {
            $q = $export->getData();
            $q->join('concept', 'concept_id', '=', 'concept.id');
            $q->join('fuel_dispenser', 'loaded_origin_id', '=', 'fuel_dispenser.id');
            $q->join('enterprise', 'chargeables.enterprise_id', '=', 'enterprise.id');
            $q->join('operation', 'chargeables.operation_id', '=', 'operation.id');
            $q->leftJoin('productive_area', 'chargeables.productive_area_id', '=', 'productive_area.id');
            $q->join('work_shift', 'chargeables.work_shift_id', '=', 'work_shift.id');
            $q->join('equipment', 'loaded_destination_id', '=', 'equipment.id');
            $q->join('type', 'type_id', '=', 'type.id');
            $q->join('brand', 'brand_id', '=', 'brand.id');
            $q->join('person', 'chargeables.operator_id', '=', 'person.id'); 
            $q->select('enterprise.name as Empresa', 'operation.name as Faena', 'concept.name as Concepto', 
                'fuel_dispenser.name as Dispensador', 'productive_area.name as Area productiva', 'work_shift.name as Turno', 
                'order_number as Folio',
                'equipment.cc as CC', 'equipment.patent as Equipo', 'type.name as Tipo', 'brand.name as Marca', 'load_date as Fecha', 
                'liters as Litros', 'counter as Contador', 'hour_meter as Horómetro', 
                \DB::raw("CONCAT(primary_last_name,' ',second_last_name,', ',person.name) as Operador"));
            
            $sheet->fromArray($q->get());
        })->export('xls');
    }

}