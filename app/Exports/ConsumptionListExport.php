<?php
namespace App\Exports;

class ConsumptionListExport extends \Maatwebsite\Excel\Files\NewExcelFile
{
    protected $data;
    
    public function setData($data)
    {
        $this->data = $data;
    }
    
    public function getData()
    {
        return $this->data;
    }
    
    public function getFilename()
    {
        return 'consumos-' . date('d_m_Y');
    }
}