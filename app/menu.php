<?php

Menu::make('SideBar', function($menu){
  //return \Auth::user();
  if (\Auth::check()) { // primero valida que el usuario tiene sesion
    //return \Auth::user();
    if(user()->id == 7 || user()->id == 8){
      //d(user());
        $menu->add('Consumos por equipo', 'consumption');
        $menu->add('Salir', 'auth/logout');
    }else{
      
      $menu->add('Inicio', '/');
      $menu->add('Configuración', 'configuracion');
      $menu->add('Administración', '#');
      $menu->add('Turnos', 'turnos');
      $menu->add('Compras de combustible', 'compra');
      $menu->add('Carga dispensadores', 'dispensadores');
      $menu->add('Consumos por equipo', 'consumption');
      $menu->add('Préstamos a externos', 'loan');
      $menu->add('Préstamos internos', 'loanintern');
      $menu->add('Reportes de gestión', 'reportes');
      $menu->add('Salir', 'auth/logout');
    }
  }
});