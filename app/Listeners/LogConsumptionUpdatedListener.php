<?php

namespace App\Listeners;

use App\Events\ChargeCreated;
use App\Events\ChargeUpdated;
use App\Models\Chargeable;
use App\Models\logBalance;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogConsumptionUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChargeCreated|ChargeUpdated  $event
     * @return void
     */
    public function handle($event)
    {
        $chargeable = $event->chargeable;
        $original   = $event->original;

        $dispenser = $chargeable->origin;
        
        $data = [
            //'enterprise_id'   => current_enterprise()->id,
            'operation_id'      => $chargeable->operation_id,
            'fuel_dispenser_id' => $dispenser->id,
            'previous_balance'  => $dispenser->current_balance, //SALDO DE COMBUSTIBLE INICIAL-ANTERIOR
            'current_balance'   => $dispenser->current_balance - $chargeable->liters, //SALDO DE COMBUSTIBLE ACTUAL DESPUES DE HABER SUMADO O RESTADO EL CONSUMO
            'liters'            => $chargeable->liters, //CANTIDAD DE LITROS TRANSADOS (CONSUMIDOS O AGREGADOS)
            'concept_id'        => $chargeable->concept_id, //CONCEPTO POR EL CUAL SE GENERÓ LA TRANSACCIÓN
            'chargeable_id'     => $chargeable->id, //IDE DE LA TRANSACCIÓN QUE RESPALDA EL REGISTRO DEL LOG
            'operation_type'    => 'RESTA', //TIPO DE OPERACIÓN, EN ESTE CASO ES UNA RESTA YA QUE EL CONCEPTO = CONSUMO, POR LO TANTO "SALE" COMBUSTIBLE DEL DISPENSADOR
            'created_by'        => user()->id,
            ];
        logBalance::create($data);
    }
}
