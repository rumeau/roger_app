<?php

namespace App\Listeners\ChargeReturned;

use App\Events\ChargeReturned;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateDispenserListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChargeReturned  $event
     * @return void
     */
    public function handle(ChargeReturned $event)
    {
        $chargeable = $event->chargeable;
        
        $dispenser = $chargeable->origin;
        $dispenser->current_balance = $dispenser->current_balance + $chargeable->liters;
        $dispenser->save();
    }
}