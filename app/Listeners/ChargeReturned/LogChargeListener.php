<?php

namespace App\Listeners\ChargeReturned;

use App\Events\ChargeReturned;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogChargeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChargeReturned  $event
     * @return void
     */
    public function handle(ChargeReturned $event)
    {
        $chargeable = $event->chargeable;
        
        $dispenser = $chargeable->origin;
        
        $data = [
            //'enterprise_id'   => current_enterprise()->id,
            'operation_id'      => $chargeable->operation_id,
            'fuel_dispenser_id' => $dispenser->id,
            'previous_balance'  => $dispenser->current_balance, //SALDO DE COMBUSTIBLE INICIAL-ANTERIOR
            'current_balance'   => $dispenser->current_balance + $chargeable->liters, //SALDO DE COMBUSTIBLE ACTUAL DESPUES DE HABER SUMADO O RESTADO EL CONSUMO
            'liters'            => $chargeable->liters, //CANTIDAD DE LITROS TRANSADOS (CONSUMIDOS O AGREGADOS)
            'concept_id'        => \App\Models\Chargeable::CONCEPTO_DEVOLUCION, //CONCEPTO POR EL CUAL SE GENERÓ LA TRANSACCIÓN
            'chargeable_id'     => $chargeable->id, //IDE DE LA TRANSACCIÓN QUE RESPALDA EL REGISTRO DEL LOG
            'operation_type'    => 'SUMA', //TIPO DE OPERACIÓN, EN ESTE CASO ES UNA RESTA YA QUE EL CONCEPTO = CONSUMO, POR LO TANTO "SALE" COMBUSTIBLE DEL DISPENSADOR
            'created_by'        => user()->id,
        ];
            
        \App\Models\logBalance::create($data);
    }
}
