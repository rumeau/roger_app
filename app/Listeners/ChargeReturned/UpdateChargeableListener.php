<?php

namespace App\Listeners\ChargeReturned;

use App\Events\ChargeReturned;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateChargeableListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChargeReturned  $event
     * @return void
     */
    public function handle(ChargeReturned $event)
    {
        $event->chargeable->returns_id = $event->loan->id;
        $event->chargeable->save();
    }
}
