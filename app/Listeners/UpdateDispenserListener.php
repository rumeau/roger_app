<?php

namespace App\Listeners;

use App\Events\ChargeCreated;
use App\Events\ChargeUpdated;
use App\Models\Chargeable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateDispenserListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChargeCreated|ChargeUpdated  $event
     * @return void
     */
    //función que permite la actualización de la columna "current_balance" (saldo actual) de la table "fuel_dispenser".
    //El objetivo es amntener siempre actualizada la "última" cantidad de combustible, a modo de saldo diponible en el dispensador.
    public function handle($event)
    {
        $chargeable = $event->chargeable;
        //concept_id = 1 > COMPRA. Suma combustible al dispensado de destino (cantidad que proviene desde el proveedor)
        if ($chargeable->concept_id == Chargeable::CONCEPTO_COMPRA) {
            $this->handleCompra($chargeable);
            
        //Carga, Consumo y préstamo implica una resta desde el dispensador de origen (cantidad de litros que sala para la carga)
        //La carga a dispensador (concept_id = 2), tiene la execepción que se debe actualizar (SUMA), la cantidad de litros entrantes
        //al dispensador de destino ($dispenserDestination).
        } elseif ($chargeable->concept_id == Chargeable::CONCEPTO_CONSUMO) {
            $this->handleConsumo($chargeable);
        } elseif ($chargeable->concept_id == Chargeable::CONCEPTO_PRESTAMO) {
            $this->handlePrestamo($chargeable);
        } elseif ($chargeable->concept_id == Chargeable::CONCEPTO_CARGA) {
            $this->handleCarga($chargeable);
        }
    }

    /**
     * @param Chargeable $chargeable
     */
    public function handleCompra(Chargeable $chargeable)
    {
        $dispenser = $chargeable->destination;
        $dispenser->current_balance = $dispenser->current_balance + $chargeable->liters;
        $dispenser->save();
    }

    
    public function handleConsumo(Chargeable $chargeable)
    {
        $dispenser = $chargeable->origin;
        $dispenser->current_balance = $dispenser->current_balance - $chargeable->liters;
        $dispenser->save();
    }

    public function handlePrestamo(Chargeable $chargeable)
    {
        $dispenser = $chargeable->origin;
        $dispenser->current_balance = $dispenser->current_balance - $chargeable->liters;
        $dispenser->save();
    }

    public function handleCarga(Chargeable $chargeable)
    {
        $dispenserDestination = $chargeable->destination;
        $dispenserDestination->current_balance = $dispenserDestination->current_balance + $chargeable->liters;
        $dispenserDestination->save();
        // todo descontar del origen
        $dispenserOrigin      = $chargeable->origin;
        $dispenserOrigin->current_balance = $dispenserOrigin->current_balance - $chargeable->liters;
        $dispenserOrigin->save();
    }
}
