<?php

namespace App\Listeners;

use App\Events\ChargeCreated;
use App\Events\ChargeUpdated;
use App\Models\Chargeable;
use App\Models\logBalance;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogChargeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChargeCreated|ChargeUpdated  $event
     * @return void
     */
    public function handle($event)
    {
        $chargeable = $event->chargeable;
        $task       = $event->task;
        $original   = $event->original;
        
        if ($chargeable->concept_id == Chargeable::CONCEPTO_COMPRA) {
            $this->handleCompra($chargeable);
        } elseif ($chargeable->concept_id == Chargeable::CONCEPTO_CARGA) {
            $this->handleCarga($chargeable, $task, $original);
        } elseif ($chargeable->concept_id == Chargeable::CONCEPTO_CONSUMO) {
            $this->handleConsumo($chargeable);    
        } elseif ($chargeable->concept_id == Chargeable::CONCEPTO_PRESTAMO) {
            $this->handlePrestamo($chargeable);
        }
    }
    
    public function handleCompra($chargeable)
    {
        $dispenser = $chargeable->destination;
        
        $data = [
            //'enterprise_id'     => current_enterprise()->id,
            'operation_id'      => $chargeable->operation_id,
            'fuel_dispenser_id' => $dispenser->id,
            'previous_balance'  => $dispenser->current_balance, //SALDO DE COMBUSTIBLE INICIAL-ANTERIOR
            'current_balance'   => $dispenser->current_balance + $chargeable->liters, //SALDO DE COMBUSTIBLE ACTUAL DESPUES DE HABER SUMADO O RESTADO EL CONSUMO
            'liters'            => $chargeable->liters, //CANTIDAD DE LITROS TRANSADOS (CONSUMIDOS O AGREGADOS)
            'concept_id'        => $chargeable->concept_id, //CONCEPTO POR EL CUAL SE GENERÓ LA TRANSACCIÓN
            'chargeable_id'     => $chargeable->id, //IDE DE LA TRANSACCIÓN QUE RESPALDA EL REGISTRO DEL LOG
            'operation_type'    => 'SUMA', //TIPO DE OPERACIÓN, EN ESTE CASO ES UNA RESTA YA QUE EL CONCEPTO = CONSUMO, POR LO TANTO "SALE" COMBUSTIBLE DEL DISPENSADOR
            'created_by'        => user()->id,
            ];
        logBalance::create($data);
    }

    /**
     * actualiza la tabla init_dispenser cuando la operación es "carga a dispensador".
     * Esta acción debe generar 2 registros en dicha tabla
     * ya que se deben actualziar el stock de ambos dispensadores
     * (restar litros de dispensador de origen, sumar litros al dispensador de destino)
     *
     * @param Chargeable $chargeable
     * @param string $task
     * @param array $original
     */
    public function handleCarga($chargeable, $task = 'CREATE', $original = [])
    {
        if ($task == 'UPDATE') {
            $chargeableLiters = str_replace('.', '', $chargeable->liters);
            $originalLiters   = str_replace('.', '', $original['liters']);
            if ($chargeableLiters == $originalLiters) {
                return;
            }
        }

        //seleccionar el dispensador de origen y de destino
        $dispenser            = $chargeable->origin;
        $dispenserDestination = $chargeable->destination;

        if ($task == 'DELETE') {
            $dispenser = $chargeable->destination;
            $dispenserDestination = $chargeable->origin;
        }

        $customLiters = false;
        if ($task == 'UPDATE') {
            if ($chargeableLiters < $original['liters']) {
                $customLiters = $original['liters'] - $chargeableLiters;
                $dispenser = $chargeable->destination;
                $dispenserDestination = $chargeable->origin;
            } elseif ($chargeableLiters > $original['liters']) {
                $customLiters = $chargeableLiters - $original['liters'];
            }
        }
        
        //en $data carga todos los datos del dispensador de origen, para hacer el insert en la tabla init_dispenser
        $this->logBalance($chargeable, $dispenser, 'RESTA', $customLiters);
        //en $data2 carga todos los datos del dispenbsador de destino, para hacer el insert en la tabla init_dispenser
        $this->logBalance($chargeable, $dispenserDestination, 'SUMA', $customLiters);
    }

    /**
     * @param $chargeable
     * @param $dispenser
     * @param string $operation
     * @param bool $customLiters
     */
    protected function logBalance($chargeable, $dispenser, $operation = 'SUMA', $customLiters = false)
    {
        $operation = strtoupper($operation);

        $liters = $chargeable->liters;
        if ($customLiters) {
            $liters = $customLiters;
            $currentBalance = $dispenser->current_balance + $customLiters;
            if ($operation == 'RESTA') {
                $currentBalance = $dispenser->current_balance - $customLiters;
            }
        } else {
            $currentBalance = $dispenser->current_balance + $chargeable->liters;
            if ($operation == 'RESTA') {
                $currentBalance = $dispenser->current_balance - $chargeable->liters;
            }
        }

        $data = [
            'operation_id'      => $chargeable->operation_id,
            'fuel_dispenser_id' => $dispenser->id,
            'previous_balance'  => $dispenser->current_balance, //SALDO DE COMBUSTIBLE INICIAL-ANTERIOR
            'current_balance'   => $currentBalance, //SALDO DE COMBUSTIBLE ACTUAL DESPUES DE HABER SUMADO O RESTADO EL CONSUMO
            'liters'            => $customLiters, //CANTIDAD DE LITROS TRANSADOS (CONSUMIDOS O AGREGADOS)
            'concept_id'        => $chargeable->concept_id, //CONCEPTO POR EL CUAL SE GENERÓ LA TRANSACCIÓN
            'chargeable_id'     => $chargeable->id, //ID DE LA TRANSACCIÓN QUE RESPALDA EL REGISTRO DEL LOG (tabla chargeables)
            'operation_type'    => $operation, //Suma la cantidad de combustible al dispensador de destino
            'created_by'        => user()->id,
        ];

        logBalance::create($data);
    }
    
    public function handleConsumo($chargeable)
    {
        $dispenser = $chargeable->origin;
        
        $data = [
            //'enterprise_id'   => current_enterprise()->id,
            'operation_id'      => $chargeable->operation_id,
            'fuel_dispenser_id' => $dispenser->id,
            'previous_balance'  => $dispenser->current_balance, //SALDO DE COMBUSTIBLE INICIAL-ANTERIOR
            'current_balance'   => $dispenser->current_balance - $chargeable->liters, //SALDO DE COMBUSTIBLE ACTUAL DESPUES DE HABER SUMADO O RESTADO EL CONSUMO
            'liters'            => $chargeable->liters, //CANTIDAD DE LITROS TRANSADOS (CONSUMIDOS O AGREGADOS)
            'concept_id'        => $chargeable->concept_id, //CONCEPTO POR EL CUAL SE GENERÓ LA TRANSACCIÓN
            'chargeable_id'     => $chargeable->id, //IDE DE LA TRANSACCIÓN QUE RESPALDA EL REGISTRO DEL LOG
            'operation_type'    => 'RESTA', //TIPO DE OPERACIÓN, EN ESTE CASO ES UNA RESTA YA QUE EL CONCEPTO = CONSUMO, POR LO TANTO "SALE" COMBUSTIBLE DEL DISPENSADOR
            'created_by'        => user()->id,
            ];
        logBalance::create($data);
    }
    
    public function handlePrestamo($chargeable){
        $dispenser = $chargeable->origin; //en caso de préstamo de combustible, se cambió "destination" por "origin"
        
        $data = [
            //'enterprise_id'   => current_enterprise()->id,
            'operation_id'      => $chargeable->operation_id,
            'fuel_dispenser_id' => $dispenser->id, //en caso de los préstamos la resta de combustible se debe realizar al dispensador de origen (origin)
                                                   //si se hace al de detino (destination), rescata el id de la empresa externa y no del dispensador
            'previous_balance'  => $dispenser->current_balance, //SALDO DE COMBUSTIBLE INICIAL-ANTERIOR
            'current_balance'   => $dispenser->current_balance - $chargeable->liters, //SALDO DE COMBUSTIBLE ACTUAL DESPUES DE HABER SUMADO O RESTADO EL CONSUMO
            'liters'            => $chargeable->liters, //CANTIDAD DE LITROS TRANSADOS (CONSUMIDOS O AGREGADOS)
            'concept_id'        => $chargeable->concept_id, //CONCEPTO POR EL CUAL SE GENERÓ LA TRANSACCIÓN
            'chargeable_id'     => $chargeable->id, //IDE DE LA TRANSACCIÓN QUE RESPALDA EL REGISTRO DEL LOG
            'operation_type'    => 'RESTA', //TIPO DE OPERACIÓN, EN ESTE CASO ES UNA RESTA YA QUE EL CONCEPTO = CONSUMO, POR LO TANTO "SALE" COMBUSTIBLE DEL DISPENSADOR
            'created_by'        => user()->id,
            ];

        logBalance::create($data);
    }
}
