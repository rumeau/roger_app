<?php

namespace App\Listeners\ChargeReturnedMoney;

use App\Events\ChargeReturnedMoney;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateChargeableListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChargeReturnedMoney  $event
     * @return void
     */
    public function handle(ChargeReturnedMoney $event)
    {
        $event->chargeable->returns_id = $event->loan->id;
        $event->chargeable->save();
    }
}
