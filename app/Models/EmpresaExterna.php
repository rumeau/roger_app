<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmpresaExterna
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chargeable[] $charger
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chargeable[] $loaded
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $enterprise_id
 * @property integer $operation_id
 * @property integer $third_code
 * @property integer $cc
 * @property string $name
 * @property integer $active
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmpresaExterna whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmpresaExterna whereEnterpriseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmpresaExterna whereOperationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmpresaExterna whereThirdCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmpresaExterna whereCc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmpresaExterna whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmpresaExterna whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmpresaExterna whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmpresaExterna whereCreatedAt($value)
 */
class EmpresaExterna extends Model implements CanBeAChargeInterface
{
    use CanBeAChargeTrait;

    protected $table = "external_enterprise";

    protected $primaryKey = 'id';

    /*
    public function external_enterprise()
    {
        return $this->belongsTo('App\Models\EmpresaExterna', 'operation_id');
    }*/
}
