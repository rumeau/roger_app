<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalogo
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $catalogue_type_id
 * @property string $name
 * @property integer $type_request_id
 * @property integer $platform_id
 * @property integer $n3_system_id
 * @property integer $n4_sub_system_id
 * @property integer $n5
 * @property integer $n6
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Catalogo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Catalogo whereCatalogueTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Catalogo whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Catalogo whereTypeRequestId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Catalogo wherePlatformId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Catalogo whereN3SystemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Catalogo whereN4SubSystemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Catalogo whereN5($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Catalogo whereN6($value)
 */
class Catalogo extends Model
{
    protected $table = "catalogue";
	
	protected $primaryKey = 'id';
}
