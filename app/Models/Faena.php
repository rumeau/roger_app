<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Faena
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Proveedor[] $proveedores
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Dispensador[] $dispensadores
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Turno[] $turnos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Persona[] $operadores
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EmpresaExterna[] $externos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ubicacion[] $ubicaciones
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AreaProductiva[] $productiveArea
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $enterprise_id
 * @property integer $business_area
 * @property string $name
 * @property integer $external
 * @property integer $supervisor_id
 * @property integer $shift_id
 * @property integer $to_buy
 * @property integer $active
 * @property integer $visible
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faena whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faena whereEnterpriseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faena whereBusinessArea($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faena whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faena whereExternal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faena whereSupervisorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faena whereShiftId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faena whereToBuy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faena whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faena whereVisible($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faena whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faena whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faena whereUpdatedAt($value)
 */
class Faena extends Model
{
    //
    protected $table = 'operation';

    protected $primaryKey = 'id';
    
    public function proveedores(){
		return $this->belongsToMany('App\Models\Proveedor','operation_provider', 'operation_id', 'provider_id');
	}
	
	public function dispensadores(){
		return $this->belongsToMany('App\Models\Dispensador', 'operation_dispenser', 'operation_id', 'fuel_dispenser_id');
	}
	
	public function turnos(){
		return $this->belongsToMany('App\Models\Turno', 'operation_work_shift', 'operation_id', 'work_shift_id');
	}
	
	public function operadores(){
		return $this->hasMany('App\Models\Persona', 'operation_id');
	}
	
	public function equipos(){
		return $this-> hasMany('App\Models\Equipo', 'operation_id');
	}
	
	public function externos(){
		return $this->hasMany('App\Models\EmpresaExterna', 'operation_id')->where('active', '=', 1);
	}
	
	public function ubicaciones(){
		return $this->hasMany('App\Models\Ubicacion', 'operation_id')->where('active', '=', 1);
	}
	
	public function productiveArea(){
		return $this->belongsToMany('App\Models\AreaProductiva', 'operation_productive_area', 'operation_id', 'productive_area_id')
					->where('productive_area.active', '=', 1)
					->orderBy('productive_area.name', 'asc');
	}
}
