<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Familia
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $ci_type_id
 * @property integer $code
 * @property string $name
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Familia whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Familia whereCiTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Familia whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Familia whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Familia whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Familia whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Familia whereUpdatedAt($value)
 */
class Familia extends Model
{
    //
	protected $table = "category";
	
	protected $primaryKey = 'id';
	
}