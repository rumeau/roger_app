<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PrestamoExterno
 *
 * @mixin \Eloquent
 */
class PrestamoExterno extends Model
{
    protected $table = "load_fuel_external_enterprise";
	
	protected $primaryKey = 'id';
}
