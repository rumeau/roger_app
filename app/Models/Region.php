<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Region
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name
 * @property string $created_by
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Region whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Region whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Region whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Region whereCreatedAt($value)
 */
class Region extends Model
{
    //
	protected $table = "region";
	
	protected $primaryKey = 'id';
	
}