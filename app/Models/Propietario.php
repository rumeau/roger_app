<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Propietario
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Propietario whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Propietario whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Propietario whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Propietario whereCreatedAt($value)
 */
class Propietario extends Model
{
    //
	protected $table = "owner";
	
	protected $primaryKey = 'id';
	
}