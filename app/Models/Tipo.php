<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Tipo
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $category_id
 * @property integer $ci_type_id
 * @property string $code
 * @property string $name
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tipo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tipo whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tipo whereCiTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tipo whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tipo whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tipo whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tipo whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tipo whereUpdatedAt($value)
 */
class Tipo extends Model
{
    //
	protected $table = "type";
	
	protected $primaryKey = 'id';
	
}