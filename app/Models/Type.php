<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Type
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $category_id
 * @property integer $ci_type_id
 * @property string $code
 * @property string $name
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Type whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Type whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Type whereCiTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Type whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Type whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Type whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Type whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Type whereUpdatedAt($value)
 */
class Type extends Model
{
    protected $table = "type";
	
	protected $primaryKey = 'id';
}
