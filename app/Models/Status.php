<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Status
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Status whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Status whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Status whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Status whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Status whereUpdatedAt($value)
 */
class Status extends Model
{
    //
	protected $table = "status_equipment";
	
	protected $primaryKey = 'id';
	
}