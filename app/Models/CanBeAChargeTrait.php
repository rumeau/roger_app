<?php

namespace App\Models;


trait CanBeAChargeTrait
{
    public function charger()
    {
        return $this->morphMany('App\Models\Chargeable', 'loaded_origin');
    }

    public function loaded()
    {
        return $this->morphMany('App\Models\Chargeable', 'loaded_destination');
    }
}