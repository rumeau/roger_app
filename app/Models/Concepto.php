<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Concepto
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Concepto whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Concepto whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Concepto whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Concepto whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Concepto whereCreatedAt($value)
 */
class Concepto extends Model
{
	protected $table = "concept";
	
	protected $primaryKey = 'id';
}
