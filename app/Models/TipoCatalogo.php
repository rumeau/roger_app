<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TipoCatalogo
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoCatalogo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoCatalogo whereName($value)
 */
class TipoCatalogo extends Model
{
    //
    protected $table = "catalogue_type";
	
	protected $primaryKey = 'id';
}
