<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DevolucionPrestamo
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $array_chargeables_id
 * @property integer $return_type_id
 * @property integer $total_returned_liters
 * @property integer $total_returned_money
 * @property string $returned_date
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DevolucionPrestamo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DevolucionPrestamo whereArrayChargeablesId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DevolucionPrestamo whereReturnTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DevolucionPrestamo whereTotalReturnedLiters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DevolucionPrestamo whereTotalReturnedMoney($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DevolucionPrestamo whereReturnedDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DevolucionPrestamo whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DevolucionPrestamo whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DevolucionPrestamo whereUpdatedAt($value)
 * @property-read \App\Models\ReturnType $returnType
 */
class DevolucionPrestamo extends Model
{
    const RETURN_LITERS = 1;
    const RETURN_MONEY  = 2;
    
    protected $primaryKey = 'id';

    protected $table = "return_loans";

    protected $casts = [
        'array_chargeables_id' => 'array'
    ];

    protected $dates = [
        'load_date',
    ];

    protected $fillable = [
        'array_chargeables_id',
        'return_type_id',
        'total_returned_liters',
        'total_returned_money',
        'returned_date',
        'created_by'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function returnType()
    {
        return $this->belongsTo('App\Models\ReturnType', 'return_type_id', 'id');
    }
}