<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Modelo
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $brand_id
 * @property string $code
 * @property string $name
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Modelo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Modelo whereBrandId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Modelo whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Modelo whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Modelo whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Modelo whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Modelo whereUpdatedAt($value)
 */
class Modelo extends Model
{
	protected $table = "model";
	
	protected $primaryKey = 'id';
	
}