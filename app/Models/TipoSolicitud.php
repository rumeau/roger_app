<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TipoSolicitud
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoSolicitud whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoSolicitud whereName($value)
 */
class TipoSolicitud extends Model
{
    //
    protected $table = 'type_request';

    protected $primaryKey = 'id';
}
