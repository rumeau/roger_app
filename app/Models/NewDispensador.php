<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\NewDispensador
 *
 * @property-read \App\Models\Faena $operation
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $enterprise_id
 * @property integer $ubication_id
 * @property integer $type_fuel_dispenser_id
 * @property string $name
 * @property integer $dispenser_equipo_id
 * @property string $dispenser_equipo_table
 * @property integer $capacity
 * @property integer $beginning_balance
 * @property integer $current_balance
 * @property integer $internal
 * @property integer $administration
 * @property integer $require_counter
 * @property integer $require_liters
 * @property integer $active
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereEnterpriseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereUbicationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereTypeFuelDispenserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereDispenserEquipoId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereDispenserEquipoTable($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereCapacity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereBeginningBalance($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereCurrentBalance($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereInternal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereAdministration($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereRequireCounter($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereRequireLiters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\NewDispensador whereUpdatedAt($value)
 */
class NewDispensador extends Model
{
    protected $table = "fuel_dispenser";

    protected $primaryKey = 'id';
    
    public function operation(){
		return $this->belongsTo('App\Models\Faena', 'operation_id');
	}
    
    public $timestamps = true;
	
	protected $dates = [
	    'created_at'
	];
	
	protected $fillable = [
	    'enterprise_id',
	    'name',
        'capacity',
        'beginning_balance',
        'internal',
        'administration',
        'require_counter',
        'require_liters',
        'active',
        'created_by',
        'created_at'
	];
}
