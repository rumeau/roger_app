<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Marca
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $ci_type_id
 * @property string $code
 * @property string $name
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Marca whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Marca whereCiTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Marca whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Marca whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Marca whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Marca whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Marca whereUpdatedAt($value)
 */
class Marca extends Model
{
	protected $table = "brand";
	
	protected $primaryKey = 'id';
	
}