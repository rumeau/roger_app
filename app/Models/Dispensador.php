<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Dispensador
 *
 * @property-read \App\Models\Ubicacion $ubication
 * @property-read \App\Models\Empresa $enterprise
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Faena[] $operation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chargeable[] $charger
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chargeable[] $loaded
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $enterprise_id
 * @property integer $ubication_id
 * @property integer $type_fuel_dispenser_id
 * @property string $name
 * @property integer $dispenser_equipo_id
 * @property string $dispenser_equipo_table
 * @property integer $capacity
 * @property integer $beginning_balance
 * @property integer $current_balance
 * @property integer $internal
 * @property integer $administration
 * @property integer $require_counter
 * @property integer $require_liters
 * @property integer $active
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereEnterpriseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereUbicationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereTypeFuelDispenserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereDispenserEquipoId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereDispenserEquipoTable($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereCapacity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereBeginningBalance($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereCurrentBalance($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereInternal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereAdministration($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereRequireCounter($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereRequireLiters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dispensador whereUpdatedAt($value)
 */
class Dispensador extends Model implements CanBeAChargeInterface
{
    use CanBeAChargeTrait;

    protected $table = "fuel_dispenser";

    protected $primaryKey = 'id';
    
    public function ubication(){
        return $this->belongsTo('App\Models\Ubicacion', 'ubication_id');
    }
    
    public function enterprise(){
        return $this->belongsTo('App\Models\Empresa', 'enterprise_id');
    }
    //SIRVE PARA RESCATAR EL NOMBRE DE LA FAENA A LA QUE ATIENDE EL DISPENSADOR, EXISTE UNA TABLA INTERMEDIA PARA LA RELACIÓN ENTRE
    //fue_dispenser y operation...operation_dispenser
    public function operation(){
        return $this->belongsToMany('App\Models\Faena', 'operation_dispenser', 'fuel_dispenser_id', 'operation_id');
    }
}