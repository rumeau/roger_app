<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Proveedor
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $rut
 * @property string $dv
 * @property string $name
 * @property integer $margin
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proveedor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proveedor whereRut($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proveedor whereDv($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proveedor whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proveedor whereMargin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proveedor whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proveedor whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proveedor whereUpdatedAt($value)
 */
class Proveedor extends Model
{
	protected $table = "provider";
	
	protected $primaryKey = 'id';
}