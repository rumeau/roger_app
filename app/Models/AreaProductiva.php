<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AreaProductiva
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name
 * @property integer $operation_id
 * @property integer $active
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AreaProductiva whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AreaProductiva whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AreaProductiva whereOperationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AreaProductiva whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AreaProductiva whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AreaProductiva whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\AreaProductiva whereUpdatedAt($value)
 */
class AreaProductiva extends Model
{
    //
    protected $table = "productive_area";
	
	protected $primaryKey = 'id';
}
