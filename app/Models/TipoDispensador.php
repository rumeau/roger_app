<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TipoDispensador
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name
 * @property integer $active
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoDispensador whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoDispensador whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoDispensador whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoDispensador whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoDispensador whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoDispensador whereUpdatedAt($value)
 */
class TipoDispensador extends Model
{
    //
	protected $table = "type_fuel_dispenser";
	
	protected $primaryKey = 'id';
	
}