<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Ubicacion
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $enterprise_id
 * @property integer $operation_id
 * @property string $code
 * @property string $name
 * @property integer $active
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ubicacion whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ubicacion whereEnterpriseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ubicacion whereOperationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ubicacion whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ubicacion whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ubicacion whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ubicacion whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ubicacion whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ubicacion whereUpdatedAt($value)
 */
class Ubicacion extends Model
{
    protected $table = "locations";
	
	protected $primaryKey = 'id';
}
