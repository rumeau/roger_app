<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TipoIdentif
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoIdentif whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoIdentif whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoIdentif whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TipoIdentif whereCreatedAt($value)
 */
class TipoIdentif extends Model
{
    //
	protected $table = "type_identification";
	
	protected $primaryKey = 'id';
	
}