<?php
/**
 * Created by PhpStorm.
 * User: jean
 * Date: 31-05-16
 * Time: 20:23
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ReturnType
 *
 * @property integer $id
 * @property string $abrev
 * @property string $name
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReturnType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReturnType whereAbrev($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReturnType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReturnType whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReturnType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReturnType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ReturnType extends Model
{
    protected $table = 'return_type';

    protected $primaryKey = 'id';
}