<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Chargeable
 *
 * @property-read \App\Models\Empresa $enterprise
 * @property-read \App\Models\Faena $operation
 * @property-read \App\Models\Concepto $concept
 * @property-read \App\Models\Proveedor $provider
 * @property-read \App\Models\Dispensador $dispenser
 * @property-read \App\Models\AreaProductiva $productiveArea
 * @property-read \App\Models\DevolucionPrestamo $returnloan
 * @property-read \App\Models\Turno $work_shift
 * @property-read \App\Models\Persona $operator
 * @property-read \App\Models\Equipo $equipment
 * @property-read \App\Models\Tipo $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\Eloquent|Dispensador|Proveedor $origin
 * @property-read \Illuminate\Database\Eloquent\Collection|\Eloquent|Dispensador|Proveedor $loaded_origin
 * @property-read \App\Models\Persona $responsible
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent|Dispensador|Proveedor|Equipo $destination
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent|Dispensador|Proveedor $loaded_destination
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $concept_id
 * @property integer $loaded_origin_id
 * @property integer $loaded_destination_id
 * @property string $loaded_origin_type
 * @property string $loaded_destination_type
 * @property integer $enterprise_id
 * @property integer $operation_id
 * @property integer $work_shift_id
 * @property integer $responsible_id
 * @property integer $operator_id
 * @property integer $ubication_id
 * @property \Carbon\Carbon $load_date
 * @property integer $order_number
 * @property integer $bill_number
 * @property integer $liters
 * @property integer $counter
 * @property integer $hour_meter
 * @property float $net_price
 * @property float $net_value
 * @property integer $fuel_tax
 * @property float $tax
 * @property integer $total_amount
 * @property integer $received
 * @property integer $returns_id
 * @property integer $productive_area_id
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereConceptId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereLoadedOriginId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereLoadedDestinationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereLoadedOriginType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereLoadedDestinationType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereEnterpriseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereOperationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereWorkShiftId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereResponsibleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereOperatorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereUbicationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereLoadDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereOrderNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereBillNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereLiters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereCounter($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereHourMeter($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereNetPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereNetValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereFuelTax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereTax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereTotalAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereReceived($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereReturnsId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereProductiveAreaId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chargeable whereUpdatedAt($value)
 */
class Chargeable extends Model
{
    const CONCEPTO_COMPRA = 1;
    const CONCEPTO_CARGA = 2;
    const CONCEPTO_CONSUMO = 3;
    const CONCEPTO_DEVOLUCION = 4;
    const CONCEPTO_PRESTAMO = 5;

    protected $table = "chargeables";

    protected $primaryKey = 'id';

    protected $fillable = [
        'enterprise_id',
        'operation_id',
        'concept_id',
        'load_date',
        'loaded_origin_id',
        'loaded_destination_id',
        'loaded_origin_type',
        'loaded_destination_type',
        'work_shift_id',
        'responsible_id',
        'operator_id',
        'ubication_id',
        'order_number',
        'bill_number',
        'provider_id',
        'fuel_dispenser_id',
        'concept_id',
        'liters',
        'counter',
        'hour_meter',
        'net_price',
        'net_value',
        'received',
        'fuel_tax',
        'tax',
        'total_amount',
        'created_by',
        'created_at',

        'productive_area_id'
    ];
    
    protected $dates = [
        'load_date'
    ];

    /**
     * @param $date
     */
    public function setLoadDateAttribute($date)
    {
        if (is_string($date)) {
            if (preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $date)) {
                $this->attributes['load_date'] = Carbon::createFromFormat('d/m/Y', $date);
                return;
            }
        }

        $this->attributes['load_date'] = $date;
    }

    public function enterprise()
    {
        return $this->belongsTo('App\Models\Empresa', 'enterprise_id');
    }

    public function operation()
    {
        return $this->belongsTo('App\Models\Faena', 'operation_id');
    }

    public function concept()
    {
        return $this->belongsTo('App\Models\Concepto', 'concept_id');
    }

    public function provider()
    {
        return $this->belongsTo('App\Models\Proveedor', 'loaded_origin_id');
    }

    public function dispenser()
    {
        return $this->belongsTo('App\Models\Dispensador', 'loaded_destination_id');
    }

    public function productiveArea()
    {
        return $this->belongsTo('App\Models\AreaProductiva', 'productive_area_id');
    }

    /*
    public function dispenserOrigen(){
        return $this->belongsTo('App\Models\Dispensador', 'loaded_origin_id');
    }*/
    /*
    public function dispenserDestino(){
        return $this->belongsTo('App\Models\Dispensador', 'loaded_destination_id');
    }*/

    public function returnloan()
    {
        return $this->belongsTo('App\Models\DevolucionPrestamo', 'returns_id');
    }

    public function work_shift()
    {
        return $this->belongsTo('App\Models\Turno', 'work_shift_id');
    }

    public function operator()
    {
        return $this->belongsTo('App\Models\Persona', 'operator_id');
    }

    public function equipment()
    {
        return $this->belongsTo('App\Models\Equipo', 'loaded_destination_id');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Tipo', 'type_id');
    }

    public function origin()
    {
        // 1 param: Nombre relacion
        // 2 param: campo tipo de relacion (loaded_origin_type)
        // 3 param: campo id de la relacion (loaded_origin_id)
        // Por convencion laravel agrega _id y _type al param 1
        return $this->morphTo('loaded_origin');

        // Algo parecido se hace para el destino

        // Luego agregas las relaciones inversas en los respectivos modelos
        // (equipo, dispensador, otros)
        // con $this->morphMany()
    }

    public function responsible()
    {
        return $this->belongsTo('App\Models\Persona', 'responsible_id');
    }

    public function destination()
    {
        return $this->morphTo('loaded_destination');
    }

    public function setNetPriceAttribute($value)
    {
        $this->setMoneyFormat($value, 'net_price');
    }

    public function getNetPriceAttribute()
    {
        return $this->getMoneyFormatted('net_price');
    }

    public function setNetValueAttribute($value)
    {
        $this->setMoneyFormat($value, 'net_value');
    }

    public function getNetValueAttribute()
    {
        return $this->getMoneyFormatted('net_value', true);
    }

    public function setFuelTaxAttribute($value)
    {
        $this->setMoneyFormat($value, 'fuel_tax');
    }

    public function getFuelTaxAttribute()
    {
        return $this->getMoneyFormatted('fuel_tax', true);
    }

    public function setTaxAttribute($value)
    {
        $this->setMoneyFormat($value, 'tax');
    }

    public function getTaxAttribute()
    {
        return $this->getMoneyFormatted('tax', true);
    }

    public function setTotalAmountAttribute($value)
    {
        $this->setMoneyFormat($value, 'total_amount');
    }

    public function getTotalAmountAttribute()
    {
        return $this->getMoneyFormatted('total_amount', true);
    }

    public function setMoneyFormat($value, $field)
    {
        $this->attributes[$field] = number_format($value, 5, '.', '');
    }

    public function setLitersAttribute($value)
    {
        $this->attributes['liters'] = preg_replace('/[\.\,]/', '', $value);
    }

    public function getLitersAttribute()
    {
        return number_format($this->attributes['liters'], 0, ',', '.');
    }

    public function setHourMeterAttribute($value)
    {
        $this->attributes['hour_meter'] = preg_replace('/[\.\,]/', '', $value);
    }

    public function getHourMeterAttribute()
    {
        return number_format($this->attributes['hour_meter'], 0, ',', '.');
    }

    public function getMoneyFormatted($field, $round = false)
    {
        $val = $this->attributes[$field];
        if ($round) {
            $val = round($val);
        }

        if (strstr($val, '.')) {
            if (strstr($val, '.00000')) {
                return number_format($val, 0, ',', '.');
            } else {
                return number_format($val, 5, ',', '.');
            }
        }

        return number_format($val, 0, ',', '.');
    }
}
