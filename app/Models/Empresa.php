<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\Empresa
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Faena[] $operations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Faena[] $operationsFilter
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Faena[] $operationsLoan
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Proveedor[] $providers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Dispensador[] $dispensers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Persona[] $operators
 * @property-read mixed $full_name
 * @property-read \App\Models\Region $region
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $region_id
 * @property string $name
 * @property string $rut
 * @property string $commune
 * @property string $address
 * @property string $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Operation[] $operationsBuy
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empresa whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empresa whereRegionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empresa whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empresa whereRut($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empresa whereCommune($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empresa whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empresa whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empresa whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empresa whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chargeable[] $chargeables
 */
class Empresa extends Model
{
    protected $table = "enterprise";

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $dates = [
        'created_at'
    ];

    protected $fillable = [
        'name',
        'rut',
        'region_id',
        'commune',
        'address',
        'created_by'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Builder
     */
    public function operations()
    {
        return $this->hasMany('App\Models\Operation', 'enterprise_id');
    }

    public function operationsFilter()
    {
        return $this->hasMany('App\Models\Faena', 'enterprise_id')->where('external', '=', 0)->where('visible', '=', 1);
    }

    public function operationsLoan()
    {
        return $this->hasMany('App\Models\Faena', 'enterprise_id')
            ->where('external', '=', 0)
            ->where('visible', '=', 1)
            ->where('to_buy', '=', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function providers()
    {
        return $this->belongsToMany('App\Models\Proveedor', 'enterprise_provider', 'enterprise_id', 'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dispensers()
    {
        //	return \App\Models\Dispensador::select('fuel_dispenser.*')->join('operation_dispenser', 'fuel_dispenser.id', '=', 'operation_dispenser.fuel_dispenser_id')
        //		->join('operation', 'operation_dispenser.operation_id', '=', 'operation.id')
        //		->where('operation.enterprise_id', '=', $this->attributes['id'])
        //		->groupBy('fuel_dispenser.id')->get();
        return $this->hasMany('App\Models\Dispensador', 'enterprise_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Builder
     */
    public function equipments()
    {
        return $this->hasMany('App\Models\Equipo', 'enterprise_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\Illuminate\Database\Eloquent\Builder
     */
    public function operators()
    {
        return $this->belongsToMany('App\Models\Persona', 'operator_operation', 'enterprise_id', 'person_id');
    }

    public function getFullNameAttribute()
    {
        return $this->get('name') . " " . $this->get('primary_last_name', '') . " " . $this->get('second_last_name', '');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    /*
    public function productiveArea(){
        
        return $this->hasManyThrough('App\Models\AreaProductiva', 'App\Models\Faena', 'enterprise_id', 'operation_id');
    }
    */

    public function afectado()
    {
        return $this->hasManyThrough('App\Models\Persona', 'App\Models\Faena', 'enterprise_id', 'operation_id')
            ->where('person.active', '=', 1)
            ->orderBy('primary_last_name', 'ASC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chargeables()
    {
        return $this->hasMany('App\Models\Chargeable', 'enterprise_id');
    }

    public function personas()
    {
        return $this->hasManyThrough('App\Models\Persona', 'App\Models\Operation', 'enterprise_id', 'operation_id');
    }
}