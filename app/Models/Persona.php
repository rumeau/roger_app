<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Persona
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $operation_id
 * @property integer $job_title_id
 * @property string $rut
 * @property string $dv
 * @property string $name
 * @property string $primary_last_name
 * @property string $second_last_name
 * @property integer $operator
 * @property integer $active
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Persona whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Persona whereOperationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Persona whereJobTitleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Persona whereRut($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Persona whereDv($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Persona whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Persona wherePrimaryLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Persona whereSecondLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Persona whereOperator($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Persona whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Persona whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Persona whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Persona whereUpdatedAt($value)
 */
class Persona extends Model
{
	protected $table='person';
	
	protected $primaryKey = 'id';
	
	// public function getfullNameAttribute(){
	// 	return $this->get('name') . " " . $this->get('primary_last_name', '') . " " . $this->get('second_last_name','');
	// }
}
