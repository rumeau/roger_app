<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrdenTrabajo
 *
 * @mixin \Eloquent
 */
class OrdenTrabajo extends Model
{
    protected $table = "work_order";
	
	protected $primaryKey = 'id';
}
