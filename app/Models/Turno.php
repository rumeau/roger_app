<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Turno
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $working_day
 * @property string $start_day
 * @property string $term_day
 * @property string $start_hour
 * @property string $term_hour
 * @property integer $lunch_time
 * @property string $start_lunch_time
 * @property string $term_lunch_time
 * @property integer $active
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereWorkingDay($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereStartDay($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereTermDay($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereStartHour($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereTermHour($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereLunchTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereStartLunchTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereTermLunchTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Turno whereUpdatedAt($value)
 */
class Turno extends Model
{
	protected $table = "work_shift";
	
	protected $primaryKey = 'id';
}
