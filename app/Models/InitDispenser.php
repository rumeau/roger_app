<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\InitDispenser
 *
 * @property-read \App\Models\Faena $operation
 * @property-read \App\Models\Dispensador $dispenser
 * @property-read \App\Models\Concepto $concept
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $operation_id
 * @property integer $fuel_dispenser_id
 * @property integer $previous_balance
 * @property integer $current_balance
 * @property integer $liters
 * @property integer $concept_id
 * @property integer $chargeable_id
 * @property string $operation_type
 * @property string $date_residue
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InitDispenser whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InitDispenser whereOperationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InitDispenser whereFuelDispenserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InitDispenser wherePreviousBalance($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InitDispenser whereCurrentBalance($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InitDispenser whereLiters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InitDispenser whereConceptId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InitDispenser whereChargeableId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InitDispenser whereOperationType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InitDispenser whereDateResidue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InitDispenser whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InitDispenser whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InitDispenser whereUpdatedAt($value)
 */
class InitDispenser extends Model
{
	protected $table = "init_dispenser";
	
	protected $primaryKey = 'id';
	
	public function operation(){
        return $this->belongsTo('App\Models\Faena', 'operation_id');
    }
    
    public function dispenser(){
    	return $this->belongsTo('App\Models\Dispensador', 'fuel_dispenser_id');
    }
	
	public function concept(){
    	return $this->belongsTo('App\Models\Concepto', 'concept_id');
    }
    
	public $timestamps = false;
	
	protected $dates = [
	    'created_at',
	    'load_date',
	];
	
	protected $fillable = [
        'operation_id',
		'fuel_dispenser_id',
		'beginning_residue',
		'capacity',
		'date_residue',
		'created_by',
		'created_at'
	];
}