<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Operador
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $enterprise_id
 * @property integer $operation_id
 * @property integer $person_id
 * @property integer $its_responsible
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Operador whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Operador whereEnterpriseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Operador whereOperationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Operador wherePersonId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Operador whereItsResponsible($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Operador whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Operador whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Operador whereUpdatedAt($value)
 */
class Operador extends Model
{
	protected $table='operator_operation';
	
	protected $primaryKey = 'id';
}





