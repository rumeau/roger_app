<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Compra
 *
 * @property-read \App\Models\Empresa $enterprise
 * @property-read \App\Models\Faena $operation
 * @property-read \App\Models\Proveedor $provider
 * @property-read \App\Models\Dispensador $dispenser
 * @property-read \App\Models\Concepto $concept
 * @property mixed $net_price
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $enterprise_id
 * @property integer $operation_id
 * @property \Carbon\Carbon $load_date
 * @property integer $order_number
 * @property integer $bill_number
 * @property integer $provider_id
 * @property integer $fuel_dispenser_id
 * @property integer $concept_id
 * @property integer $liters
 * @property integer $net_value
 * @property integer $fuel_tax
 * @property integer $tax
 * @property integer $total_amount
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereEnterpriseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereOperationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereLoadDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereOrderNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereBillNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereProviderId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereFuelDispenserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereConceptId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereLiters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereNetPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereNetValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereFuelTax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereTax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereTotalAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra whereCreatedAt($value)
 */
class Compra extends Model
{
    //
	protected $table = "fuel_purchase";
	
	protected $primaryKey = 'id';
	
	/**
	 * Metodo enterprise -> mapea id <enterprise>_id -> <nombre_metodo>_id
	 */
	public function enterprise(){
		return $this->belongsTo('App\Models\Empresa');
	}
	
	public function operation(){
		return $this->belongsTo('App\Models\Faena', 'operation_id');
	}
	
	public function provider(){
		return $this->belongsTo('App\Models\Proveedor', 'provider_id');
	}
	
	public function dispenser(){
		return $this->belongsTo('App\Models\Dispensador', 'fuel_dispenser_id');
	}
	
	public function concept(){
		return $this->belongsTo('App\Models\Concepto', 'concept_id');
	}
	
	public $timestamps = false;
	
	protected $dates = [
	    'created_at',
	    'load_date',
	];
	
	protected $fillable = [
		'enterprise_id',
		'operation_id',
		'load_date',
		'order_number',
		'bill_number',
		'provider_id',
		'fuel_dispenser_id',
		'concept_id',
		'liters',
		'net_price',
		'net_value',
		'fuel_tax',
		'tax',
		'total_amount',
		'created_by',
		'created_at'
	];
	
	/**
	 * Obtiene el numero 11111111.11 como 11.111.111,11
	 */
	public function getNetPriceAttribute()
	{
		//dd($this->attributes['net_price']);
		return number_format($this->attributes['net_price'], 5, ',', '.');
	}
	
	/**
	 * Convierte en la BD el numero 11.111.111,11 -> 11111111.11
	 */
	public function setNetPriceAttribute($number)
	{
		return $this->attributes['net_price'] = number_cl($number);
	}
}