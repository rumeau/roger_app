<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\logBalance
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $operation_id
 * @property integer $fuel_dispenser_id
 * @property integer $previous_balance
 * @property integer $current_balance
 * @property integer $liters
 * @property integer $concept_id
 * @property integer $chargeable_id
 * @property string $operation_type
 * @property string $date_residue
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\logBalance whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\logBalance whereOperationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\logBalance whereFuelDispenserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\logBalance wherePreviousBalance($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\logBalance whereCurrentBalance($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\logBalance whereLiters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\logBalance whereConceptId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\logBalance whereChargeableId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\logBalance whereOperationType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\logBalance whereDateResidue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\logBalance whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\logBalance whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\logBalance whereUpdatedAt($value)
 */
class logBalance extends Model
{
	protected $table = "init_dispenser";
	
	protected $primaryKey = 'id';

	public $timestamps = true;
	
	protected $dates = [
	    'created_at'
	];
	
	protected $fillable = [
		'operation_id',
		'fuel_dispenser_id',
        'previous_balance',
        'current_balance',
        'liters',
        'concept_id',
        'chargeable_id',
        'operation_type',
        'created_by',
		'created_at'
	];
}