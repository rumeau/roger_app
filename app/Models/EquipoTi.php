<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EquipoTi
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Componente[] $component
 * @property-read \App\Models\Familia $category
 * @property-read \App\Models\Tipo $type
 * @property-read \App\Models\Marca $brand
 * @property-read \App\Models\Modelo $model
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name
 * @property integer $brand_id
 * @property integer $model_id
 * @property string $serial_number
 * @property string $inventory_number
 * @property integer $category_id
 * @property integer $type_id
 * @property string $asignation_date
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EquipoTi whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EquipoTi whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EquipoTi whereBrandId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EquipoTi whereModelId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EquipoTi whereSerialNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EquipoTi whereInventoryNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EquipoTi whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EquipoTi whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EquipoTi whereAsignationDate($value)
 */
class EquipoTi extends Model
{
    protected $table = 'ti_equipment';
    
    protected $primaryKey = 'id';
    
    protected $with = ['brand', 'model', 'category'];
    
    public function component()
    {
        return $this->morphToMany('App\Models\Componente', 'component');
    }
    
    public function category(){
        return $this->belongsTo('App\Models\Familia', 'category_id');
    }

    public function type(){
        return $this->belongsTo('App\Models\Tipo', 'type_id');
    }
    
    public function brand(){
        return $this->belongsTo('App\Models\Marca', 'brand_id');
    }

    public function model()
    {
        return $this->belongsTo('App\Models\Modelo', 'model_id');
    }
}
