<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Equipo
 *
 * @property-read \App\Models\Empresa $enterprise
 * @property-read \App\Models\Faena $operation
 * @property-read \App\Models\Familia $category
 * @property-read \App\Models\Tipo $type
 * @property-read \App\Models\Marca $brand
 * @property-read \App\Models\Modelo $model
 * @property-read \App\Models\Status $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chargeable[] $charger
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chargeable[] $loaded
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $enterprise_id
 * @property integer $operation_id
 * @property integer $category_id
 * @property integer $type_id
 * @property integer $brand_id
 * @property integer $model_id
 * @property integer $owner_id
 * @property integer $status_id
 * @property integer $type_identification_id
 * @property integer $fuel_tank_capacity
 * @property string $cc
 * @property string $patent
 * @property string $serial_number
 * @property string $engine
 * @property integer $year
 * @property integer $lease
 * @property integer $active
 * @property integer $fuel
 * @property integer $dispenser
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereEnterpriseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereOperationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereBrandId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereModelId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereOwnerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereTypeIdentificationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereFuelTankCapacity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereCc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo wherePatent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereSerialNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereEngine($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereLease($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereFuel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereDispenser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equipo whereUpdatedAt($value)
 */
class Equipo extends Model implements CanBeAChargeInterface
{
    use CanBeAChargeTrait;

    //
    protected $table = 'equipment';

    protected $primaryKey = 'id';
    
    protected $with = ['brand', 'model', 'category'];

    public function enterprise(){
        return $this->belongsTo('App\Models\Empresa', 'enterprise_id');
    }

    public function operation(){
        return $this->belongsTo('App\Models\Faena', 'operation_id');
    }

    public function category(){
        return $this->belongsTo('App\Models\Familia', 'category_id');
    }

    public function type(){
        return $this->belongsTo('App\Models\Tipo', 'type_id');
    }

    public function brand(){
        return $this->belongsTo('App\Models\Marca', 'brand_id');
    }

    public function model()
    {
        return $this->belongsTo('App\Models\Modelo', 'model_id');
    }
    
    public function status(){
        return $this->belongsTo('App\Models\Status', 'status_id');
    }
    /*
    public function component()
    {
        return $this->morphMany('App\Models\Componente', 'equipment');
    }*/
    public function component()
    {
        return $this->morpTohMany('App\Models\Componente', 'component');
    }
    
    public $timestamps = true;
	
	protected $dates = [
	    'created_at'
	];
	
	protected $fillable = [
		'enterprise_id',
        'operation_id',
		'cc',
		'patent',
		'type_identification_id',
		'category_id',
		'type_id',
		'brand_id',
		'model_id',
		'serial_number',
		'engine',
		'year',
		'lease',
		'owner_id',
		'active',
		'status_id',
		'fuel',
		'dispenser',
		'fuel_tank_capacity'
	];
}
