<?php
/**
 * Created by PhpStorm.
 * User: Jean Rumeau
 * Date: 21/09/2015
 * Time: 17:06
 */

namespace App\Models;

/**
 * Interface CanBeAChargeInterface
 * @package App\Models
 */
interface CanBeAChargeInterface
{
    /**
     * Registros en que ha participado como cargador
     *
     * @return mixed
     */
    public function charger();

    /**
     * Registros en que ha sido cargado
     *
     * @return mixed
     */
    public function loaded();
}