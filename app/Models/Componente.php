<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Componente
 *
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $component
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $ci_type_id
 * @property integer $responsible_id
 * @property integer $component_id
 * @property string $component_type
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Componente whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Componente whereCiTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Componente whereResponsibleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Componente whereComponentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Componente whereComponentType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Componente whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Componente whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Componente whereUpdatedAt($value)
 */
class Componente extends Model
{
    //
    protected $table = "components";
	
	protected $primaryKey = 'id';
	
	protected $with = ['component'];

	public function equipmentTi()
	{
	    return $this->morphedByMany('App\Models\EquipoTi', 'component');
	}
	
	public function equipment()
	{
	    return $this->morphedByMany('App\Models\Equipo', 'component');
	}
	
	public function component()
    {
        return $this->morphTo('component');
    }
	
}
