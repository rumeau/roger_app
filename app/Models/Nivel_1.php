<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Nivel_1
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Nivel_1 whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Nivel_1 whereName($value)
 */
class Nivel_1 extends Model
{
    //
    protected $table = "type_request";
	
	protected $primaryKey = 'id';
}
