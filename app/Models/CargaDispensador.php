<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CargaDispensador
 *
 * @property-read \App\Models\Empresa $enterprise
 * @property-read \App\Models\Faena $operation
 * @property-read \App\Models\Concepto $concept
 * @property-read \App\Models\Dispensador $origin_dispenser
 * @property-read \App\Models\Dispensador $destination_dispenser
 * @property-read \App\Models\Persona $person
 * @property-read \App\Models\Turno $work_shift
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $enterprise_id
 * @property integer $operation_id
 * @property \Carbon\Carbon $load_date
 * @property integer $order_number
 * @property integer $origin_dispenser_id
 * @property integer $destination_dispenser_id
 * @property integer $concept_id
 * @property integer $operator_id
 * @property integer $work_shift_id
 * @property integer $liters
 * @property integer $counter
 * @property integer $hour_meter
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereEnterpriseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereOperationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereLoadDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereOrderNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereOriginDispenserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereDestinationDispenserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereConceptId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereOperatorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereWorkShiftId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereLiters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereCounter($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereHourMeter($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CargaDispensador whereCreatedAt($value)
 */
class CargaDispensador extends Model
{
	protected $table = "load_fuel_dispenser";
	
	protected $primaryKey = 'id';
	
	public function enterprise(){
		return $this->belongsTo('App\Models\Empresa', 'enterprise_id');
	}
	
	public function operation(){
		return $this->belongsTo('App\Models\Faena', 'operation_id');
	}
	
	public function concept(){
		return $this->belongsTo('App\Models\Concepto', 'concept_id');
	}
	
	public function origin_dispenser(){
		return $this->belongsTo('App\Models\Dispensador', 'origin_dispenser_id');
	}
	
	public function destination_dispenser(){
		return $this->belongsTo('App\Models\Dispensador', 'destination_dispenser_id');
	}
	
	public function person(){
		return $this->belongsTo('App\Models\Persona', 'operator_id');
	}
	
	public function work_shift(){
		return $this->belongsTo('App\Models\Turno', 'id');
	}
	
	public $timestamps = false;
	
	protected $dates = [
	    'created_at',
	    'load_date',
	];
	
	protected $fillable = [
		'enterprise_id',
		'operation_id',
		'load_date',
		'order_number',
		'origin_dispenser_id',
		'destination_dispenser_id',
		'concept_id',
		'operator_id',
		'work_shift_id',
		'liters',
		'counter',
		'hour_meter',
		'created_by',
		'created_at'
	];
}