<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Consumo
 *
 * @property-read \App\Models\Empresa $enterprise
 * @property-read \App\Models\Faena $operation
 * @property-read \App\Models\Concepto $concept
 * @property-read \App\Models\Equipo $equipment
 * @property-read \App\Models\Persona $operator
 * @property-read \App\Models\Turno $work_shift
 * @property-read \App\Models\Dispensador $fuel_dispenser
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $enterprise_id
 * @property integer $operation_id
 * @property integer $equipment_id
 * @property integer $operator_id
 * @property integer $work_shift_id
 * @property integer $dispenser_id
 * @property integer $ubication_id
 * @property integer $responsable_id
 * @property string $concept_id
 * @property integer $liters
 * @property integer $counter
 * @property integer $hour_meter
 * @property \Carbon\Carbon $load_date
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereEnterpriseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereOperationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereEquipmentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereOperatorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereWorkShiftId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereDispenserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereUbicationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereResponsableId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereConceptId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereLiters($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereCounter($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereHourMeter($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereLoadDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Consumo whereCreatedAt($value)
 */
class Consumo extends Model
{
    protected $table = "load_fuel_equipment";
    
    protected $primaryKey = 'id';
    
    public function enterprise(){
        return $this->belongsTo('App\Models\Empresa', 'enterprise_id');    
    }
    
    public function operation(){
		return $this->belongsTo('App\Models\Faena', 'operation_id');
	}
	
    public function concept(){
        return $this->belongsTo('App\Models\Concepto', 'concept_id');
    }
	
    public function equipment(){
        return $this->belongsTo('\App\Models\Equipo', 'equipment_id');
    }
    
    public function type(){
        $obtiene_tipos = Tipo::where ('id', '=', $this->equipment->type_id)->get();
        dd($obtiene_tipos);
        foreach ($obtiene_tipos as $obtiene_tipo){
            $type_name[] = $obtiene_tipo->name;
        }
        return $type_name[0];
    }
    
    public function brand(){
        $obtiene_marcas = Marca::where ('id', '=', $this->equipment->brand_id)->get();
        foreach ($obtiene_marcas as $obtiene_marca){
          $brand_name[] = $obtiene_marca->name;
        }
        return $brand_name[0];
    }
    
    public function model(){
        $obtiene_modelos = Modelo::where('id', '=', $this->equipment->model_id)->get();
        foreach($obtiene_modelos as $obtiene_modelo){
            $model_name[] = $obtiene_modelo->name;
        }
        return $model_name[0];
    }
    
    public function operator(){
		return $this->belongsTo('App\Models\Persona', 'operator_id');
	}
	
	public function work_shift(){
	    return $this->belongsTo('App\Models\Turno', 'work_shift_id');
	}
	
	public function fuel_dispenser(){
	    return $this->belongsTo('App\Models\Dispensador', 'dispenser_id');
	}
	
	public $timestamps = false;
	
	protected $dates = [
	    'created_at',
	    'load_date',
	];
	
	protected $fillable = [
        'enterprise_id',
		'operation_id',
		'equipment_id',
		'operator_id',
		'work_shift_id',
		'dispenser_id',
		'ubication_id',
		'responsable_id',
		'concept_id',
		'liters',
		'counter',
		'hour_meter',
		'load_date',
		'created_by',
		'created_at'
	];
}
