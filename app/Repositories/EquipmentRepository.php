<?php
/**
 * Created by PhpStorm.
 * User: jean
 * Date: 15-06-16
 * Time: 21:06
 */

namespace App\Repositories;


use App\Models\Chargeable;
use App\Models\Equipo;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class EquipmentRepository
{
    /**
     * @param Equipo $equipment
     * @param Carbon $from
     * @param Carbon $to
     * @param $total
     * @param bool $noSub
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function reportConsumption(Equipo $equipment, Carbon $from, Carbon $to, $total, $noSub = false)
    {
        $query = \DB::table('chargeables')
            ->select([
                'equipment.id',
                // ppu
                'equipment.patent',
                'type.name as type',
                'brand.name as brand',
                'model.name as model',
                'operation.name as operation_name',
                \DB::raw('COALESCE(SUM(chargeables.liters), 0) as liters'),
                \DB::raw('((COALESCE(SUM(chargeables.liters),0) * 100) / ' . $total . ') as equipment_percentage'),
            ])
            ->orderBy('equipment.id', 'ASC')
            ->leftJoin('equipment', 'chargeables.loaded_destination_id', '=', 'equipment.id')
            ->leftJoin('type', 'equipment.type_id', '=', 'type.id')
            ->leftJoin('brand', 'equipment.brand_id', '=', 'brand.id')
            ->leftJoin('model', 'equipment.model_id', '=', 'model.id')
            ->join('operation', 'equipment.operation_id', '=', 'operation.id')
            ->where('chargeables.loaded_destination_id', $equipment->id)
            ->where('chargeables.concept_id', Chargeable::CONCEPTO_CONSUMO)
            ->where('chargeables.load_date', '>=', $from->format('Y-m-d 00:00:00'))
            ->where('chargeables.load_date', '<=', $to->format('Y-m-d 23:59:59'))
            ->groupBy('chargeables.loaded_destination_id');

        if (!$noSub) {
            $query->selectSub(function($query) use ($from, $to, $equipment) {
                $query->from('chargeables as charg2')
                    ->select([
                        \DB::raw('CASE MAX(charg2.counter) WHEN MIN(charg2.counter) THEN MAX(charg2.counter) ELSE COALESCE(MAX(charg2.counter) - MIN(charg2.counter), 0) END')
                    ])
                    ->whereBetween('charg2.load_date', [$from->format('Y-m-d'), $to->format('Y-m-d')])
                    ->where('charg2.loaded_destination_id', $equipment->id)
                    ->where('charg2.concept_id', Chargeable::CONCEPTO_CONSUMO)
                    ->where('charg2.loaded_destination_id', \DB::raw('chargeables.loaded_destination_id'));
            }, 'counter')
                ->selectSub(function($query) use ($from, $to, $equipment) {
                    $query->from('chargeables as charg')
                        ->select([
                            \DB::raw('CASE MAX(charg.hour_meter) WHEN MIN(charg.hour_meter) THEN MAX(charg.hour_meter) ELSE COALESCE(MAX(charg.hour_meter) - MIN(charg.hour_meter), 0) END')
                        ])
                        ->whereBetween('charg.load_date', [$from->format('Y-m-d'), $to->format('Y-m-d')])
                        ->where('charg.loaded_destination_id', $equipment->id)
                        ->where('charg.concept_id', Chargeable::CONCEPTO_CONSUMO)
                        ->where('charg.loaded_destination_id', \DB::raw('chargeables.loaded_destination_id'));
                }, 'hour_meter');
        }

        return $query;
    }

    /**
     * @param Equipo $equipment
     * @param Carbon $from
     * @param Carbon $to
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function reportDetailConsumption(Equipo $equipment, Carbon $from, Carbon $to)
    {
        $query = \DB::table('chargeables')
            ->select([
                'chargeables.id as id',
                \DB::raw('DATE_FORMAT(chargeables.load_date, \'%d-%m-%Y\') as load_date'),
                'operation.name as operation',
                'equipment.patent as equipment',
                //ppu
                \DB::raw('CONCAT(person.primary_last_name, \' \', person.second_last_name, \', \', person.name) as operator'),
                'work_shift.code as work_shift',
                'fuel_dispenser.name as dispenser',
                'locations.name as ubication',
                'chargeables.liters',
                'chargeables.hour_meter',
            ])
            ->leftJoin('operation', 'chargeables.operation_id', '=', 'operation.id')
            ->leftJoin('equipment', 'chargeables.loaded_destination_id', '=', 'equipment.id')
            ->leftJoin('person', 'chargeables.operator_id', '=', 'person.id')
            ->leftJoin('fuel_dispenser', 'chargeables.loaded_origin_id', '=', 'fuel_dispenser.id')
            ->leftJoin('work_shift', 'chargeables.work_shift_id', '=', 'work_shift.id')
            ->leftJoin('locations', 'chargeables.ubication_id', '=', 'locations.id')
            ->where('chargeables.loaded_destination_id', $equipment->id)
            ->where('chargeables.concept_id', Chargeable::CONCEPTO_CONSUMO)
            ->whereBetween('chargeables.load_date', [$from->format('Y-m-d'), $to->format('Y-m-d')]);

        return $query;
    }
}