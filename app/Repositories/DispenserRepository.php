<?php

namespace App\Repositories;

class DispenserRepository{
    
    public function create($data)
    {
        $data['enterprise_id'] = current_enterprise()->id;
		$data['equipment_id'] = 1;
		$data['created_by'] = user()->id;
        
        // Crea el objeto dispensdor
        $newdispensador  = new \App\Models\NewDispensador($data); 

        // Asigna las relaciones
        //$chargeable->origin()->associate($origin);
        //$chargeable->destination()->associate($destination); // Se asignan las relaciones (se elimina el problema de campos null)
        // Guarda todo
        $newdispensador->save(); // se guarda todo, el objeto ya tiene todos los datos necesarios (no hay problema de campos null)
        
        //GENERA EVENTO PARA QUE SE EJECUTEN LOS LISTENERS QUE CONTIENE
        \Event::Fire(new \App\Events\DispenserCreated($newdispensador));
        
        return $newdispensador; // devuelve el objeto creado, en caso que lo necesites en el controlador
    }
    
    public function extractOrigin($concept, $origin){
        if ($concept == 1){
            return \App\Models\Proveedor::find($origin);
        }elseif($concept == 2 || $concept == 3 || $concept == 5 || $concept == 6){
            return \App\Models\Dispensador::find($origin);
        }
    }
    
    public function extractDestination($concept, $destination){
        if ($concept == 1 || $concept == 2 || $concept == 6){
            return \App\Models\Dispensador::find($destination);
        }elseif($concept == 3){
            return \App\Models\Equipo::find($destination);
        }elseif($concept == 5){
            return \App\Models\EmpresaExterna::find($destination);
        }
    }
}