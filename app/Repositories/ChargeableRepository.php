<?php

namespace App\Repositories;

use App\Models\Chargeable;
use App\Models\DevolucionPrestamo;
use App\Models\Dispensador;
use App\Models\Empresa;
use App\Models\EmpresaExterna;
use App\Models\Equipo;
use App\Models\logBalance;
use App\Models\Proveedor;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ChargeableRepository
{
    /**
     * Actualiza un Cargable
     *
     * @param Chargeable $chargeable
     * @param $data
     * @return Chargeable
     */
    public function update(Chargeable $chargeable, $data)
    {
        if(isset($data['net_price'])){
            $data['net_price']    = number_cl($data['net_price']);
            $data['net_value']    = $data['liters'] * $data['net_price'];
            $data['tax']          = round(($data['net_value']*0.19), 5, PHP_ROUND_HALF_UP);
            $data['total_amount'] = $data['net_value']+$data['fuel_tax']+$data['tax'];
        }
        
        if(isset($data['loaded_origin_id'])){
            $origin      = $this->extractOrigin($chargeable->concept_id, $data['loaded_origin_id']);
            $chargeable->origin()->associate($origin);
        }
        if(isset($data['loaded_destination_id'])){
            $destination = $this->extractDestination($chargeable->concept_id, $data['loaded_destination_id']);
            $chargeable->destination()->associate($destination);
        }
        
        $chargeable->update($data);

        return $chargeable;
    }

    /**
     * Crea un cargable
     *
     * @param $data
     * @return Chargeable
     */
    public function create($data)
    {
        if ($data['concept_id'] == Chargeable::CONCEPTO_COMPRA) {
            $data['net_price']     = number_cl($data['net_price']);
            $data['net_value']     = $data['liters'] * $data['net_price'];
            $data['tax']           = round(($data['net_value']*0.19), 5, PHP_ROUND_HALF_UP);
            $data['created_by']    = user()->id;
            $data['total_amount']  = $data['net_value'] + $data['fuel_tax'] + $data['tax'];
        }

        // Obtiene (object) origen y destino
        $origin      = $this->extractOrigin($data['concept_id'], $data['loaded_origin_id']);
        $destination = $this->extractDestination($data['concept_id'], $data['loaded_destination_id']);
        
        // Crea el objeto chargeable
        $chargeable  = new Chargeable($data); // Aqui se crea el objeto pero aun no se ha guardado (ergo no hay pate por campos null (aun))
        //$chargeable = \App\Models\Chargeable::create($data); // crea el objeto y lo guarda en BBDD (Aqui aun no se ha asignado destination ni origin, ergo pate!)
        // Este metodo es util cuando no hay relaciones polymorficas, de lo contrario mejor usar el de la linea 25
        
        // Asigna las relaciones
        $chargeable->origin()->associate($origin);
        $chargeable->destination()->associate($destination); // Se asignan las relaciones (se elimina el problema de campos null)
        // Guarda
        $chargeable->save(); // se guarda todo, el objeto ya tiene todos los datos necesarios (no hay problema de campos null)

        return $chargeable; // devuelve el objeto creado, en caso que lo necesites en el controlador
    }

    /**
     * ### COMPRA ###
     */

    /**
     * Crea una compra
     *
     * @param array $data
     * @return Chargeable
     */
    public function createPurchase($data = [])
    {
        return $this->create($data);
    }

    /**
     * Actualiza una compra
     *
     * @param Chargeable $buy
     * @param array $data
     */
    public function updatePurchase(Chargeable $buy, $data = [])
    {
        $this->update($buy, $data);
    }

    /**
     * Recepciona una compra
     *
     * - Registra la compra en  el balance, no actualiza el balance del origen por que es externo
     * - Actualiza el dispensador de origen con la cantidad resultante
     *
     * @param Chargeable $buy
     * @return Chargeable
     */
    public function confirmPurchase(Chargeable $buy)
    {
        $buy->update([
            'received' => 1,
            'received_by' => user()->id,
        ]);

        $this->logBalance($buy, $buy->destination);
        $this->updateDispenser($buy, $buy->destination);

        return $buy;
    }

    /**
     * Elimina una compra, devuelve la cantidad al origen
     *
     * - Registra en el balance
     *  - Devuelve la cantidad al origen
     *  - Quita la cantidad devuelta del destino
     * - Actualiza el origen de destino con la cantidad restaurada
     *
     * @param Chargeable $buy
     * @return bool
     */
    public function deletePurchase(Chargeable $buy)
    {
        //en $data carga todos los datos del dispensador de origen, para hacer el insert en la tabla init_dispenser
        $this->logBalance($buy, $buy->destination, 'RESTA');
        //en $data2 carga todos los datos del dispenbsador de destino, para hacer el insert en la tabla init_dispenser
        $this->logBalance($buy, $buy->origin, 'SUMA');

        $litersToReturn = raw_number($buy->liters);
        $this->updateDispenser($buy, $buy->origin, $litersToReturn);

        $this->destroy($buy);

        return true;
    }

    /**
     * ### CARGAS ###
     */

    /**
     * Crea una carga
     *
     * - Actualiza el balance
     *  - Suma la cantidad cargada al destino
     *  - Resta la cantidad cargada del origen
     * - Actualiza el dispensador de origen descontando la cantidad cargada
     *
     * @param $data
     */
    public function createCharge($data)
    {
        /**
         * @var Chargeable $charge
         */
        $charge = $this->create($data);

        //en $data carga todos los datos del dispensador de origen, para hacer el insert en la tabla init_dispenser
        $this->logBalance($charge, $charge->origin, 'RESTA');
        //en $data2 carga todos los datos del dispensador de destino, para hacer el insert en la tabla init_dispenser
        $this->logBalance($charge, $charge->destination, 'SUMA');
        // Resta
        $this->updateDispenser($charge, $charge->origin, -(raw_number($charge->liters)));
        // suma
        $this->updateDispenser($charge, $charge->destination, raw_number($charge->liters));
    }

    /**
     * Actualiza una carga
     *
     * - Actualiza balance y dispensadores solo si han sido modificados
     *
     * - Actualiza el balance solo si este se ha modificado
     *  - Si la cantidad cargada aumento:
     *   - Se obtiene la diferencia y se suma al destino
     *   - Se obtiene la diferencia y se resta del origen
     *  - Si la cantidad cargada disminuyo:
     *   - Se obtiene la diferencia y se resta del destino
     *   - Se obtiene la diferencia y se devuelve al origen
     * - Se actualiza el dispensador de origen
     *  - Si la cantidad cargada aumento:
     *   - Se resta del origen la cantidad aumentada
     *  - Si la cantidad cargada disminuyo:
     *   - Se suma al origen la cantidad devuelta
     *
     * @param Chargeable $charge
     * @param array $data
     * @return Chargeable
     */
    public function updateCharge(Chargeable $charge, $data = [])
    {
        $originalLiters   = raw_number($charge->liters);
        $chargeableLiters = raw_number($data['liters']);

        $this->update($charge, $data);

        if ($chargeableLiters == $originalLiters) {
            return $charge;
        }

        //seleccionar el dispensador de origen y de destino
        $origin         = $charge->origin;
        $destination    = $charge->destination;

        $customLiters = $chargeableLiters - $originalLiters;
        $signedCustomLiters = $customLiters;
        if ($chargeableLiters < $originalLiters) {
            $customLiters       = $originalLiters - $chargeableLiters;
            $signedCustomLiters = -($customLiters);
            $origin             = $charge->destination;
            $destination        = $charge->origin;
        }

        //en $data carga todos los datos del dispensador de origen, para hacer el insert en la tabla init_dispenser
        $this->logBalance($charge, $origin, 'RESTA', $customLiters);
        //en $data2 carga todos los datos del dispensador de destino, para hacer el insert en la tabla init_dispenser
        $this->logBalance($charge, $destination, 'SUMA', $customLiters);

        $this->updateDispenser($charge, $charge->origin, -($signedCustomLiters));
        $this->updateDispenser($charge, $charge->destination, $signedCustomLiters);

        return $charge;
    }

    /**
     * Elimina una carga
     *
     * - Actualiza el balance
     *  - Se devuelve la cantidad descontada al origen
     *  - Se resta la cantidad cargada desde el destino
     * - Se actualiza el dispensador de origen sumando la cantidad devuelta
     * - Se elimina la carga
     *
     * @param Chargeable $charge
     * @return bool
     */
    public function deleteCharge(Chargeable $charge)
    {
        //en $data carga todos los datos del dispensador de origen, para hacer el insert en la tabla init_dispenser
        $this->logBalance($charge, $charge->destination, 'RESTA');
        //en $data2 carga todos los datos del dispensador de destino, para hacer el insert en la tabla init_dispenser
        $this->logBalance($charge, $charge->origin, 'SUMA');

        $litersToReturn = raw_number($charge->liters);
        $this->updateDispenser($charge, $charge->origin, $litersToReturn);
        $this->updateDispenser($charge, $charge->destination, -($litersToReturn));

        $this->destroy($charge);

        return true;
    }

    /**
     * Crea un nuevo consumo
     *
     * - Actualiza el balance
     *  - Resta el consumo del dispensador que esta entregandolo
     *  - Suma los litros al equipo que recibe
     * - Actualiza el dispensador de origen restando el consumo
     *
     * @param $data
     * @return Chargeable
     */
    public function createConsumption($data)
    {
        $consumption    = $this->create($data);

        $origin         = $consumption->origin;
        $destination    = $consumption->destination;

        //GENERA EVENTO PARA QUE SE EJECUTEN LOS LISTENERS QUE CONTIENE
        //Las compras no crean ni actualizan datos a través de este método, el ingreso lo hace a través del ChargeableRepository
        //La actualización (en el LogBalance) la hace al momento de presionar la confirmación, eso gatilla /ajax/chargeables/ChargeablesController
        //que llama con update al ChargeUpdated del EvenServiceProvider
        $this->logBalance($consumption, $origin, 'RESTA');
        $this->logBalance($consumption, $destination, 'SUMA');

        $liters = raw_number($consumption->liters);
        $this->updateDispenser($consumption, $origin, -($liters));

        return $consumption;
    }

    /**
     * Actualiza un consumo
     *
     * - Actualiza el balance
     *  - Si se aumento la cantidad del consumo
     *   - Se resta la diferencia desde el dispensador de origen
     *   - Se suma la diferencia al equipo receptor
     * - Si se disminuye la cantidad del consumo
     *  - Se suma la diferencia devuelta al dispensador de origen
     *  - Se resta la diferencia del equipo que esta devolviendo
     * - Se actualiza el dispensador de origen con la cantidad que finalmente entrego
     *
     * @param Chargeable $consumption
     * @param array $data
     * @return Chargeable
     */
    public function updateConsumption(Chargeable $consumption, $data = [])
    {
        $originalLiters   = raw_number($consumption->liters);
        $chargeableLiters = raw_number($data['liters']);

        $this->update($consumption, $data);

        // Si no se modificaron los litros de la carga
        if ($chargeableLiters == $originalLiters) {
            return $consumption;
        }

        //seleccionar el dispensador de origen y de destino
        $origin         = $consumption->origin;
        $destination    = $consumption->destination;

        $customLiters = $chargeableLiters - $originalLiters;
        $signedCustomLiters = $customLiters;
        if ($chargeableLiters < $originalLiters) {
            $customLiters       = $originalLiters - $chargeableLiters;
            $signedCustomLiters = -($customLiters);
            $origin             = $consumption->destination;
            $destination        = $consumption->origin;
        }


        //en $data carga todos los datos del dispensador de origen, para hacer el insert en la tabla init_dispenser
        $this->logBalance($consumption, $origin, 'RESTA', $customLiters);
        //en $data2 carga todos los datos del dispensador de destino, para hacer el insert en la tabla init_dispenser
        $this->logBalance($consumption, $destination, 'SUMA', $customLiters);

        $this->updateDispenser($consumption, $consumption->origin, -($signedCustomLiters));

        return $consumption;
    }

    /**
     * Crea un prestamo
     *
     * - Se actualiza el balance
     *  - Se registra la cantidad prestada desde el dispensador de origen
     * - Se actualiza el dispensador
     *  - Se resta la cantidad prestada del dispensador de origen
     *
     * @param array $data
     * @return Chargeable
     */
    public function createLoan($data = [])
    {
        $loan           = $this->create($data);
        $origin         = $loan->origin;

        //GENERA EVENTO PARA QUE SE EJECUTEN LOS LISTENERS QUE CONTIENE
        //Las compras no crean ni actualizan datos a través de este método, el ingreso lo hace a través del ChargeableRepository
        //La actualización (en el LogBalance) la hace al momento de presionar la confirmación, eso gatilla /ajax/chargeables/ChargeablesController
        //que llama con update al ChargeUpdated del EvenServiceProvider
        $this->logBalance($loan, $origin, 'RESTA');

        $liters = raw_number($loan->liters);
        $this->updateDispenser($loan, $origin, -($liters));

        return $loan;
    }

    /**
     * Actualiza un prestamo
     *
     * @param Chargeable $loan
     * @param array $data
     * @return Chargeable
     */
    public function updateLoan(Chargeable $loan, $data = [])
    {
        $originalLiters   = raw_number($loan->liters);
        $chargeableLiters = raw_number($data['liters']);

        $this->update($loan, $data);

        // Si no se modificaron los litros de la carga
        if ($chargeableLiters == $originalLiters) {
            return $loan;
        }

        //seleccionar el dispensador de origen y de destino
        $origin         = $loan->origin;

        if ($chargeableLiters < $originalLiters) {
            $customLiters       = $originalLiters - $chargeableLiters;
            $signedCustomLiters = -($customLiters);
            //en $data2 carga todos los datos del dispensador de destino, para hacer el insert en la tabla init_dispenser
            $this->logBalance($loan, $origin, 'SUMA', $customLiters);
        } else {
            $customLiters = $chargeableLiters - $originalLiters;
            $signedCustomLiters = $customLiters;
            //en $data carga todos los datos del dispensador de origen, para hacer el insert en la tabla init_dispenser
            $this->logBalance($loan, $origin, 'RESTA', $customLiters);
        }

        $this->updateDispenser($loan, $loan->origin, -($signedCustomLiters));

        return $loan;
    }

    /**
     * @param Collection $loans
     * @param array $data
     * @return DevolucionPrestamo
     */
    public function returnLoans(Collection $loans, $data = [])
    {
        $returnType  = $data['isAmount'] == 0 ? 1 : 2;
        $amount      = $data['amount'] || 0;
        $totalLiters = 0;

        $chargeablesToReturn = [];
        /**
         * @var Chargeable $loan
         */
        foreach ($loans as $loan) {
            $loansToReturn[] = $loan->id;
            $totalLiters = $totalLiters + $loan->liters;
        }

        $data = [
            'array_chargeables_id'      => $chargeablesToReturn,
            'return_type_id'            => $returnType,
            'total_returned_liters'     => $totalLiters,
            'total_returned_money'      => 0,
            'returned_date'             => Carbon::now(),
            'created_by'                => user()->id
        ];

        /**
         * @var DevolucionPrestamo $return
         */
        if ($returnType == DevolucionPrestamo::RETURN_LITERS) { // cuando es en litros
            // Guardar en return_loan
            $return  = DevolucionPrestamo::create($data);

            foreach ($loans as $loan) {
                $this->logBalance($loan, $loan->origin, 'SUMA', false, ['concept_id' => Chargeable::CONCEPTO_DEVOLUCION]);
                $this->updateDispenser($loan, $loan->origin);

                $loan->update([
                    'returns_id' => $return->id,
                ]);
            }
        } elseif ($returnType == DevolucionPrestamo::RETURN_MONEY) { // cuando es en plata
            // Guardar en algun lugar
            $data['total_returned_liters'] = 0;
            $data['total_returned_money']  = $amount;

            $return = DevolucionPrestamo::create($data);

            foreach ($loans as $loan) {
                $loan->update([
                    'returns_id' => $return->id,
                ]);
            }
        }

        return $return;
    }

    /**
     * @param Chargeable $chargeable
     * @param Dispensador $dispenser
     * @param string $operation
     * @param bool $customLiters
     */
    public function logBalance(Chargeable $chargeable, $dispenser, $operation = 'SUMA', $customLiters = false, $extra = [])
    {
        $operation = strtoupper($operation);

        $liters    = $customLiters ? $customLiters : raw_number($chargeable->liters);

        $currentBalance = $dispenser->current_balance + $liters;
        if ($operation == 'RESTA') {
            $currentBalance = $dispenser->current_balance - $liters;
        }

        $data = [
            'operation_id'      => $chargeable->operation_id,
            'fuel_dispenser_id' => $dispenser->id,
            'previous_balance'  => $dispenser->current_balance, //SALDO DE COMBUSTIBLE INICIAL-ANTERIOR
            'current_balance'   => $currentBalance, //SALDO DE COMBUSTIBLE ACTUAL DESPUES DE HABER SUMADO O RESTADO EL CONSUMO
            'liters'            => $customLiters, //CANTIDAD DE LITROS TRANSADOS (CONSUMIDOS O AGREGADOS)
            'concept_id'        => array_get($extra, 'concept_id', $chargeable->concept_id), //CONCEPTO POR EL CUAL SE GENERÓ LA TRANSACCIÓN
            'chargeable_id'     => $chargeable->id, //ID DE LA TRANSACCIÓN QUE RESPALDA EL REGISTRO DEL LOG (tabla chargeables)
            'operation_type'    => $operation, //Suma la cantidad de combustible al dispensador de destino
            'created_by'        => user()->id,
        ];

        logBalance::create($data);
    }

    /**
     * @param Chargeable $chargeable
     * @param Dispensador $dispenser
     * @param bool $customLiters
     */
    public function updateDispenser(Chargeable $chargeable, $dispenser, $customLiters = false)
    {
        $liters    = $customLiters ? $customLiters : raw_number($chargeable->liters);
        $dispenser->current_balance = $dispenser->current_balance + $liters;
        $dispenser->save();
    }

    public function destroy(Chargeable $chargeable)
    {
        return $chargeable->delete();
    }
    
    public function extractOrigin($concept, $origin){
        if ($concept == 1){
            return Proveedor::find($origin);
        }elseif($concept == 2 || $concept == 3 || $concept == 5 || $concept == 6){
            return Dispensador::find($origin);
        }
    }
    
    public function extractDestination($concept, $destination){
        if (in_array($concept, [Chargeable::CONCEPTO_COMPRA, Chargeable::CONCEPTO_CARGA, 6])) {
            return Dispensador::find($destination);
        } elseif ($concept == Chargeable::CONCEPTO_CONSUMO) {
            return Equipo::find($destination);
        } elseif ($concept == 5) {
            return EmpresaExterna::find($destination);
        }
    }

    /**
     * @param Empresa $enterprise
     * @return Builder
     */
    public function getPurchasesByEnterprise(Empresa $enterprise)
    {
        return Chargeable::with('enterprise', 'operation', 'concept', 'provider', 'origin', 'destination', 'dispenser')
            ->where('concept_id', '=', Chargeable::CONCEPTO_COMPRA)
            ->where('enterprise_id', '=', $enterprise->id);
    }

    /**
     * @param Empresa $enterprise
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function getPurchasesByEnterpriseJoined(Empresa $enterprise)
    {
        return $enterprise->chargeables()
            ->join('operation', 'chargeables.operation_id', '=', 'operation.id')
            ->join('provider', 'chargeables.loaded_origin_id', '=', 'provider.id')
            ->join('fuel_dispenser', 'chargeables.loaded_destination_id', '=', 'fuel_dispenser.id')
            ->where('chargeables.concept_id', '=', Chargeable::CONCEPTO_COMPRA);
    }

    /**
     * @param Empresa $enterprise
     * @return Builder
     */
    public function getChargesByEnterprise(Empresa $enterprise)
    {
        return Chargeable::with('enterprise', 'operation', 'origin', 'destination', 'responsible')
            ->where('enterprise_id', '=', $enterprise->id)
            ->where('concept_id', '=', Chargeable::CONCEPTO_CARGA);
    }

    /**
     * @param Empresa $enterprise
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function getChargesByEnterpriseJoined(Empresa $enterprise)
    {
        return $enterprise->chargeables()
            ->join('operation', 'chargeables.operation_id', '=', 'operation.id')
            ->join('fuel_dispenser as origin', 'chargeables.loaded_origin_id', '=', 'origin.id')
            ->join('fuel_dispenser as destination', 'chargeables.loaded_destination_id', '=', 'destination.id')
            ->join('person', 'chargeables.responsible_id', '=', 'person.id')
            ->where('chargeables.concept_id', Chargeable::CONCEPTO_CARGA);
    }

    /**
     * @param Empresa $enterprise
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function getConsumptionsByEnterprise(Empresa $enterprise)
    {
        return $enterprise->chargeables()->with('enterprise', 'operation', 'productiveArea', 'work_shift', 'origin', 'destination', 'equipment.type', 'equipment.brand', 'equipment.model', 'operator')
            ->where('chargeables.enterprise_id', '=', $enterprise->id)
            ->where('concept_id', '=', Chargeable::CONCEPTO_CONSUMO);
    }

    /**
     * @param Empresa $enterprise
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function getConsumptionsByEnterpriseJoined(Empresa $enterprise)
    {
        return $enterprise->chargeables()
            ->join('operation', 'chargeables.operation_id', '=', 'operation.id')
            //->join('productive_area', 'chargeables.productive_area_id', '=', 'productive_area.id')
            ->join('work_shift', 'chargeables.work_shift_id', '=', 'work_shift.id')
            ->join('fuel_dispenser', 'chargeables.loaded_origin_id', '=', 'fuel_dispenser.id')
            ->join('equipment', 'chargeables.loaded_destination_id', '=', 'equipment.id')
            ->join('type', 'equipment.type_id', '=', 'type.id')
            ->join('brand', 'equipment.brand_id', '=', 'brand.id')
            ->join('model', 'equipment.model_id', '=', 'model.id')
            ->join('person', 'chargeables.operator_id', '=', 'person.id')
            ->where('chargeables.concept_id', Chargeable::CONCEPTO_CONSUMO);

            /*->with('enterprise', 'operation', 'productiveArea', 'work_shift', 'origin', 'destination', 'equipment.type', 'equipment.brand', 'equipment.model', 'operator')
            ->where('chargeables.enterprise_id', '=', $enterprise->id)
            ->where('concept_id', '=', Chargeable::CONCEPTO_CONSUMO);*/
    }

    /**
     * @param Empresa $enterprise
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function getLoansByEnterpriseJoined(Empresa $enterprise)
    {
        return $enterprise->chargeables()
            ->join('operation', 'chargeables.operation_id', '=', 'operation.id')
            ->join('fuel_dispenser', 'chargeables.loaded_origin_id', '=', 'fuel_dispenser.id')
            ->join('external_enterprise', 'chargeables.loaded_destination_id', '=', 'external_enterprise.id')
            ->join('work_shift', 'chargeables.work_shift_id', '=', 'work_shift.id')
            ->leftJoin('return_loans', 'chargeables.returns_id', '=', 'return_loans.id')
            ->leftJoin('return_type', 'return_loans.return_type_id', '=', 'return_type.id')
            ->where('concept_id', '=', Chargeable::CONCEPTO_PRESTAMO);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|Chargeable
     */
    public function findConsumptionOrFail($id)
    {
        return $this->findOrFail($id, Chargeable::CONCEPTO_CONSUMO, 'No se ha encontrado el consumo solicitado');
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|Chargeable
     */
    public function findLoanOrFail($id)
    {
        return $this->findOrFail($id, Chargeable::CONCEPTO_PRESTAMO, 'No se ha encontrado el prestamo solicitado');
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|Chargeable
     */
    public function findPurchaseOrFail($id)
    {
        return $this->findOrFail($id, Chargeable::CONCEPTO_COMPRA, 'No se ha encontrado la compra solicitada');
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|Chargeable
     */
    public function findChargeOrFail($id)
    {
        return $this->findOrFail($id, Chargeable::CONCEPTO_CARGA, 'No se ha encontrado la carga solicitada');
    }

    protected function findOrFail($id, $concept, $message)
    {
        $chargeable = Chargeable::whereId($id)->whereConceptId($concept)->first();
        if ($chargeable instanceof Chargeable) {
            return $chargeable;
        }

        throw new ModelNotFoundException($message);
    }

    public function chargeablesByDate()
    {
        $args = func_get_args();
        /**
         * @var Builder|\Illuminate\Database\Query\Builder $query
         * @var Carbon $from
         * @var Carbon $to
         */
        if (func_num_args() == 2) {
            $query  = Chargeable::query();
            $from   = $args[0];
            $to     = $args[1];
        } elseif (func_num_args() == 3) {
            $query  = $args[0];
            $from   = $args[1];
            $to     = $args[2];
        } else {
            throw new \Exception('Argumentos invalidos');
        }

        return $query->whereBetween('load_date', $from->format('Y-m-d'), $to->format('Y-m-d'));
    }

    public function totalByDate()
    {
        $args = func_get_args();
        /**
         * @var Empresa $enterprise
         * @var Builder|\Illuminate\Database\Query\Builder $query
         */
        if (func_num_args() == 1) {
            $enterprise = $args[0];
            $query = Chargeable::query();
        } elseif (func_num_args() == 2) {
            $enterprise = $args[0];
            $query = $args[1];
        }
        
        return $query->where('enterprise_id', '=', $enterprise->id)->get(\DB::raw())
    }
}