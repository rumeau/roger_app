<?php
/**
 * Created by PhpStorm.
 * User: jean
 * Date: 19-05-16
 * Time: 13:29
 */

namespace App\Repositories;


use App\Models\Empresa;
use App\Models\Persona;

class PersonRepository
{
    /**
     * @param array $data
     */
    public function create($data = [])
    {
        
    }

    /**
     * @param Persona $person
     * @param array $data
     */
    public function update(Persona $person, $data = [])
    {
        
    }

    /**
     * @param Empresa $enterprise
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function getPersonsByEnterpriseJoined(Empresa $enterprise)
    {
        $persons = $enterprise->personas()
            ->groupBy('person.id');

        return $persons->getQuery();
    }
}
