<?php
/**
 * Created by PhpStorm.
 * User: jean
 * Date: 12-05-16
 * Time: 18:47
 */

namespace App\Repositories;


use App\Models\AreaProductiva;
use App\Models\Chargeable;
use App\Models\Dispensador;
use App\Models\Empresa;
use App\Models\EmpresaExterna;
use App\Models\Equipo;
use App\Models\Operation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;

class EnterpriseRepository
{
    /**
     * @param Empresa $enterprise
     * @return $this
     */
    public static function equipments(Empresa $enterprise)
    {
        return $enterprise->equipments()
            ->join('brand', 'equipment.brand_id', '=', 'brand.id')
            ->join('model', 'equipment.model_id', '=', 'model.id')
            ->join('type', 'equipment.type_id', '=', 'type.id')
            ->orderBy('type.name', 'asc');
    }

    /**
     * @param Empresa $enterprise
     * @return mixed
     */
    public static function equipmentsList(Empresa $enterprise)
    {
        $equipments = self::equipments($enterprise);
        return $equipments->select('equipment.id', \DB::raw('CONCAT("CC: ", equipment.cc, ", ", equipment.patent, ", - ", type.name, ", ", brand.name, ", ", model.name) AS equipos'))
            ->lists('equipos', 'equipment.id');
    }

    /**
     * @param Empresa $enterprise
     * @return Builder
     */
    public static function dispensers(Empresa $enterprise)
    {
        return $enterprise->dispensers()
            ->where('active', '=', 1);
    }

    /**
     * @param Empresa $enterprise
     * @return mixed
     */
    public static function dispensersList(Empresa $enterprise)
    {
        return self::dispensers($enterprise)->lists('name', 'id');
    }

    /**
     * @param Empresa $enterprise
     * @return mixed
     */
    public static function operators(Empresa $enterprise)
    {
        return $enterprise->operators()
            ->where('its_responsible', '=', 0)
            ->orderBy('primary_last_name', 'ASC');
    }

    /**
     * @param Empresa $enterprise
     * @return mixed
     */
    public static function operatorsList(Empresa $enterprise)
    {
        $operators = self::operators($enterprise);
        return $operators->get([\DB::raw("CONCAT(`primary_last_name`,' ',`second_last_name`,', ',`name`) as fullName"), 'person.id'])
            ->lists('fullName', 'id');
    }

    /**
     * @param Empresa $enterprise
     * @return $this
     */
    public static function responsibles(Empresa $enterprise)
    {
        return $enterprise->operators()
            ->where('its_responsible', '=', 1)
            ->where('person.active', '=', 1);
    }

    /**
     * @param Empresa $enterprise
     * @return mixed
     */
    public static function responsiblesList(Empresa $enterprise)
    {
        $responsibles = self::responsibles($enterprise);
        return $responsibles->get([\DB::raw("CONCAT(`primary_last_name`,' ',`second_last_name`,', ',`name`) as fullName"), 'person.id'])
            ->lists('fullName', 'id');
    }

    /**
     * @param Empresa $enterprise
     * @return Builder
     */
    public static function productiveAreas(Empresa $enterprise)
    {
        return AreaProductiva::join('operation_productive_area', 'productive_area.id', '=', 'operation_productive_area.productive_area_id')
            ->join('operation', 'operation_productive_area.operation_id', '=', 'operation.id')
            ->where('operation.enterprise_id', '=', $enterprise->id)
            ->orderBy('productive_area.name', 'asc');
    }

    /**
     * @param Empresa $enterprise
     * @return mixed
     */
    public static function productiveAreasList(Empresa $enterprise)
    {
        $areas = self::productiveAreas($enterprise);
        return $areas->select(['productive_area.name', 'productive_area.id'])
            ->lists('productive_area.name', 'productive_area.id');
    }

    /**
     * @param Empresa $enterprise
     * @return $this
     */
    public static function externalEnterprises(Empresa $enterprise)
    {
        return EmpresaExterna::where('enterprise_id', '=', $enterprise->id)
            ->where('active', '=', 1)
            ->orderBy('name', 'ASC');
    }

    /**
     * @param Empresa $enterprise
     * @return \Illuminate\Support\Collection
     */
    public static function externalEnterprisesList(Empresa $enterprise)
    {
        $enterprises = self::externalEnterprises($enterprise);
        return $enterprises->lists('name', 'id');
    }

    public function consumptions(Empresa $enterprise)
    {
        return Chargeable::where('enterprise_id', $enterprise->id)
            ->whereConceptId(Chargeable::CONCEPTO_CONSUMO);
    }

    /**
     * @param $enterprise
     * @param Carbon|null $from
     * @param Carbon|null $to
     * @param int $total
     * @return Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function reportConsumption($enterprise, Carbon $from = null, Carbon $to = null, $total = 0)
    {
        $query = \DB::table('operation')
            ->select([
                'operation.name as operation_name',
                'operation.id as operation_id',
                \DB::raw('COALESCE(SUM(chargeables.liters), 0) as operation_liters'),
                \DB::raw('((COALESCE(SUM(chargeables.liters),0) * 100) / ' . $total . ') as operation_percentage')
            ])
            ->orderBy('chargeables.load_date', 'DESC')
            ->leftJoin('chargeables', function(JoinClause $join) use ($from, $to) {
                $join->on('operation.id', '=', 'chargeables.operation_id')
                    ->where('chargeables.concept_id', '=', Chargeable::CONCEPTO_CONSUMO);

                if (!is_null($from) && !is_null($to)) {
                    $join->where('chargeables.load_date', '>=', $from->format('Y-m-d 00:00:00'))
                        ->where('chargeables.load_date', '<=', $to->format('Y-m-d 23:59:59'));
                }
            })
            ->where('operation.enterprise_id', $enterprise->id)
            ->groupBy('operation.id');

        return $query;
    }

    /**
     * @param Empresa $enterprise
     * @param Carbon $from
     * @param Carbon $to
     * @return Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function reportDetailConsumption(Empresa $enterprise, Carbon $from, Carbon $to)
    {
        $query = \DB::table('chargeables')
            ->select([
                'chargeables.id',
                \DB::raw('DATE_FORMAT(chargeables.load_date, \'%d-%m-%Y\') as load_date'),
                'operation.name as operation_name',
                'equipment.patent as equipment_name',
                \DB::raw('CONCAT(person.primary_last_name, \' \', person.second_last_name, \', \', person.name) as operator_name'),
                'work_shift.code as work_shift',
                'fuel_dispenser.name as dispenser',
                'locations.name as ubication',
                'chargeables.liters',
                'chargeables.hour_meter',
            ])
            ->leftJoin('operation', 'chargeables.operation_id', '=', 'operation.id')
            ->leftJoin('equipment', 'chargeables.loaded_destination_id', '=', 'equipment.id')
            ->leftJoin('fuel_dispenser', 'chargeables.loaded_origin_id', '=', 'fuel_dispenser.id')
            ->leftJoin('person', 'chargeables.operator_id', '=', 'person.id')
            ->leftJoin('work_shift', 'chargeables.work_shift_id', '=', 'work_shift.id')
            ->leftJoin('locations', 'chargeables.ubication_id', '=', 'locations.id')
            ->where('chargeables.enterprise_id', $enterprise->id)
            ->where('chargeables.concept_id', Chargeable::CONCEPTO_CONSUMO);

        if (!is_null($from) && !is_null($to)) {
            $query->whereBetween('chargeables.load_date', [$from->format('Y-m-d'), $to->format('Y-m-d')]);
        }

        return $query;
    }

    /**
     * @param Empresa $enterprise
     * @param $operationId
     * @return Operation|null
     */
    public function findOperationOrFail(Empresa $enterprise, $operationId)
    {
        $operation = $enterprise->operations()->where('id', $operationId)->first();
        if ($operation instanceof Operation) {
            return $operation;
        }

        throw new ModelNotFoundException('La operación no ha sido encontrada');
    }

    /**
     * @param Empresa $enterprise
     * @param $equipmentId
     * @return Equipo|null
     */
    public function findEquipmentOrFail(Empresa $enterprise, $equipmentId)
    {
        $equipment = $enterprise->equipments()->where('id', $equipmentId)->first();
        if ($equipment instanceof Equipo) {
            return $equipment;
        }

        throw new ModelNotFoundException('El equipo no ha sido encontrado');
    }
}