<?php

namespace App\Policies;

use App\Models\Chargeable;
use App\Models\User;

class BuyPolicy
{
    /**
     * Determina si una compra puede ser confirmada por el usuario.
     *
     * @param User $user
     * @param Chargeable $chargeable
     * @return bool
     * @internal param Chargeable $buy
     */
    public function confirm(User $user, Chargeable $chargeable)
    {
        return true;
    }

    public function destroy(User $user, Chargeable $chargeable)
    {
        return true;
    }
}
