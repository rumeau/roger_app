<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ConsumptionUpdated extends Event
{
    use SerializesModels;

    public $chargeable;

    /**
     * @var array
     */
    public $original;

    /**
     * Create a new event instance.
     *
     * @param $chargeable
     * @param array $original
     */
    public function __construct($chargeable, $original = [])
    {
        $this->chargeable = $chargeable;
        $this->original   = $original;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
