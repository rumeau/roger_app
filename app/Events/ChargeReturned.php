<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChargeReturned extends Event
{
    use SerializesModels;

    public $chargeable;
    public $loan;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\App\Models\Chargeable $chargeable, \App\Models\DevolucionPrestamo $loan)
    {
        $this->chargeable = $chargeable;
        $this->loan = $loan;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
