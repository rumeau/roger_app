<?php

namespace App\Events;

use App\Events\Event;
use App\Models\Chargeable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChargeCreated extends Event
{
    use SerializesModels;

    /**
     * @var Chargeable
     */
    public $chargeable;

    public $task = 'CREATE';

    /**
     * Create a new event instance.
     *
     * @param $chargeable
     */
    public function __construct($chargeable)
    {
        $this->chargeable = $chargeable;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
