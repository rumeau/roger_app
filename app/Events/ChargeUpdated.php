<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChargeUpdated extends Event
{
    use SerializesModels;

    public $chargeable;

    /**
     * @var string
     */
    public $task;

    /**
     * @var array
     */
    public $original;

    /**
     * Create a new event instance.
     *
     * @param $chargeable
     * @param string $task
     * @param array $original
     */
    public function __construct($chargeable, $task = 'UPDATE', $original = [])
    {
        $this->chargeable = $chargeable;
        $this->task       = strtoupper($task);
        $this->original   = $original;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
