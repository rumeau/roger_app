<?php

/**
 *
 * Funciones comunes
 */

if (!function_exists('number_cl')) {
    function number_cl($number) {
        $decimal   = ',';
        $thousands = '.';
        
        $number = str_replace('.', '', $number); // saca los puntos de la separacion de miles
        $number = str_replace(',', '.', $number); // remplaza coma decimal por punto decimal
        
        // Ej: 11.111.111,11 -> 11111111.11 // numero decimal valido en programacion
        
        return $number;
    }
}

if (!function_exists('user')) {
    function user() {
        if (!\Auth::check()) {
            return null;
        }

        return \Auth::user();
    }
}

if (!function_exists('is_admin')) {
    function is_admin() {
        if (\Auth::check()) {
            return false;
        }

        return \Auth::user()->hasRole('admin');
    }
}

if (!function_exists('is_superadmin')) {
    function is_superadmin() {
        if (\Auth::check()) {
            return false;
        }

        return \Auth::user()->hasRole('super_admin');
    }
}


if (!function_exists('user_pretty_name')) {
    function user_pretty_name($user = null) {
        if ($user && property_exists('name', $user)) {
            $person = user()->person;
            $name   = $person->name;
            if (!empty($person->primary_last_name)) {
                $name .= ' ' . substr($person->primary_last_name, 0, 1) . '.';
            }
        } else {
            if (!\Auth::check()) {
                return 'Invitado';
            }
            
            $person = user()->person;
            $name   = $person->name;
            if (!empty($person->primary_last_name)) {
                $name .= ' ' . substr($person->primary_last_name, 0, 1) . '.';
            }
        }
        
        return ucwords(strtolower($name));
    }
}

if (!function_exists('current_enterprise')) {
    /**
     * @return null|\App\Models\Empresa
     */
    function current_enterprise() {
        if (!\Auth::check() || !\Session::has('user_enterprise')) {
            return null;
        }
        $enterprise = \App\Models\Empresa::find(session('user_enterprise'));
        if (!$enterprise instanceof \App\Models\Empresa) {
            return null;
        }
        
        return $enterprise;
    }
}