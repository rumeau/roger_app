<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\ChargeCreated' => [
            'App\Listeners\LogChargeCreatedListener',
            'App\Listeners\UpdateDispenserOnChargeCreatedListener'
        ],
        'App\Events\ChargeUpdated' => [
            'App\Listeners\LogChargeUpdatedListener',
            'App\Listeners\UpdateDispenserListener'
        ],
        'App\Events\ChargeReturned' => [
            'App\Listeners\ChargeReturned\LogChargeListener',
            'App\Listeners\ChargeReturned\UpdateDispenserListener',
            'App\Listeners\ChargeReturned\UpdateChargeableListener', // actualiza tabla chargeables, en columna "returns_id"
        ],
        'App\Events\ChargeReturnedMoney' => [
            'App\Listeners\ChargeReturnedMoney\UpdateChargeableListener', // actualiza tabla chargeables, en columna "returns_id"
        ],

        'App\Events\ConsumptionUpdated' => [
            'App\Listeners\LogConsumptionUpdatedListener',
            'App\LIsetners\UpdateDispenserOnConsumptionUpdatedListener',
        ],

        'App\Events\ConsumptionRemoved' => [
            'App\Lisetners\LogConsumptionRemovedListener',
            'App\Listeners\UpdateDispenserOnConsumptionRemovedListener',
        ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}